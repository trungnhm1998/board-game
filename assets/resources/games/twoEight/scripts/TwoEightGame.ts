import {TwoEightBetLayer} from './TwoEightBetLayer';
import {TwoEightPlayer} from './TwoEightPlayer';
import {TwoEightPlayerInfo} from './model/TwoEightPlayerInfo';
import {GlobalInfo, moon} from '../../../../scripts/core/GlobalInfo';
import {TwoEightRoom} from './model/TwoEightRoom';
import {GameScene} from '../../../../scripts/common/GameScene';
import {TwoEightDiceLayer} from './TwoEightDiceLayer';
import {ChessPool} from '../../../../scripts/common/ChessPool';
import {TwoEightCardLayer} from './TwoEightCardLayer';
import {COUNTDOWN_TYPE, GAME_STATE, TWO_EIGHT_AUDIO, DECK_ID, RESOURCE_BLOCK, GAME_ID} from '../../../../scripts/core/Constant';
import {TwoEightRoomService} from './service/TwoEightRoomService';
import {TwoEightHouseState} from './gamestates/TwoEightHouseState';
import {TwoEightBetState} from './gamestates/TwoEightBetState';
import {NodeUtils} from '../../../../scripts/core/NodeUtils';
import {RoomInfo} from '../../../../scripts/model/Room';
import {TwoEightLobby} from './TwoEightLobby';
import { TwoEightEndGame } from './gamestates/TwoEightEndGame';

const {ccclass, property} = cc._decorator;

const lstIndex = {
  '3': 0,
  '7': 1,
  '11': 2,
  '15': 3,
  '19': 4,
  '23': 5,
  '27': 6,
  '31': 7,
  '35': 8,
  '51': 9
};

@ccclass
export class TwoEightGame extends GameScene {

  @property(cc.Label)
  matchID: cc.Label = null;

  @property(cc.Label)
  session: cc.Label = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Node)
  dropdown: cc.Node = null;

  @property(cc.Node)
  statsPanel: cc.Node = null;

  @property(cc.Node)
  firstArrow: cc.Node = null;

  @property(TwoEightBetLayer)
  betLayer: TwoEightBetLayer = null;

  @property([TwoEightPlayer])
  players: TwoEightPlayer[] = [];

  @property([cc.Label])
  chessCounts: cc.Label[] = [];

  @property(TwoEightDiceLayer)
  diceLayer: TwoEightDiceLayer = null;

  @property(TwoEightCardLayer)
  cardLayer: TwoEightCardLayer = null;

  @property(cc.Node)
  waiting: cc.Node = null;

  @property(cc.Node)
  stateNode: cc.Node = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Label)
  betInfo: cc.Label = null;

  @property(TwoEightLobby)
  lobby: TwoEightLobby = null;

  @property(cc.Node)
  gameNode: cc.Node = null;


  isShownDropdown = false;
  isAllowQuitGameWithoutLeaveCmd = false;
  onEnter() {
    super.onEnter();
    moon.cardPool.setCardDeck(DECK_ID.TRADITIONAL);
    moon.audio.stopAllAudio();
    //moon.cardPool.setCardSize(102,131)
    moon.cardPool.setCardScale(0.8);
    moon.header.show();
    if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
      this.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
      moon.audio.playMusicInGame(TWO_EIGHT_AUDIO.LOBBY_BGM, true);
    }

    this.onResize();
  }

  onLeave() {
    super.onLeave();
    moon.audio.stopAllAudio();
    //this will get back to main menu
    //clear state
    //show game lobby, hide game node
    // ChessPool.getInstance().clear();

    if (this.state) {
      this.state.onLeave(this, {});
    }
    this.clear();

  }

  onEnterRoom(isUserAnimate = true, isReturnGame = false) {
    super.onEnterRoom();
    
    if (isReturnGame) {
      moon.gameSocket.getRoomInfoList(GlobalInfo.room.gameId);
    }
 
    this.getIntoGameRoom(isUserAnimate);


    this.hidePlayers();
    this.hideBetInfo();
    this.showWaiting();
    // ChessPool.getInstance().returnAllChesses();


  }

  onLanguageChange() {
    super.onLanguageChange();
    moon.res.preloadFolder(
      `${moon.service.getGameInfoById(GAME_ID.TWO_EIGHT).localeFolder}/${moon.locale.getCurrentLanguage()}`,
      () => {
      },
      RESOURCE_BLOCK.GAME
    ).then(() => {
          this.setLabelLanguage()
    })

   
  }

  setLabelLanguage() {
    NodeUtils.setLocaleLabels(this.ui, {
      'math_lbl': ['gameNumber', {}],
      'session_lbl': ['roundNumber', {}],
      'roomBet_lbl': ['ante', {}],
      'money_lbl': ['money', {}],
      'betInfo_lbl': ['pleaseSelectBet', {}],
      'wait_lbl': ['waitOtherPlayers', {}],
      'houseInfo_lbl': ['pleaseSelectMoneyYouCanPayAsDealer', {}],
      'quit_lbl': ['quit', {}],
      'setting_lbl': ['setting', {}],
      'record_lbl': ['record', {}],
      'playAgain_lbl': ['playAgain', {}],
      'help_lbl': ['help', {}],
    });
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
    this.lobby.onResize();
  }

  onPause() {

  }

  onResume() {

  }

  toggleDropdown() {
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.DROPDOWN, false);
    this.dropdown.active = !this.dropdown.active;
  }

  toggleStats() {
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.DROPDOWN, false);
    this.statsPanel.active = !this.statsPanel.active;
  }


  setPlayers(twoEightPlayerInfos: TwoEightPlayerInfo[]) {
    let startSeat;
    for (let playerInfo of twoEightPlayerInfos) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    for (let playerInfo of twoEightPlayerInfos) {
      let seatIndex = (playerInfo.seat + 4 - startSeat) % 4;
      let player: TwoEightPlayer = this.getPlayerByIndex(seatIndex);
      if (player) {
        player.setPlayerInfo(playerInfo);
        player.node.active = true;
      }
    }

    if (twoEightPlayerInfos.length >= 4) {
      this.hideWaiting();
    }
  }

  getPlayerByIndex(index) {
    return this.players[index];
  }

  getPlayerByUserId(userId) {
    let player = this.players.filter(player =>{
      if (player.playerInfo) {
         return player.playerInfo.userId == userId;
      }
    })[0] 
    cc.log('get player by user id',player)
    return player;
  }

  getActiveOrderedPlayers(userId) {
    let startSeat;
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == userId) {
        startSeat = playerInfo.seat;
      }
    }

    let orderedPlayers = [];
    for (let player of this.players) {
      if (player.node.active) {
        let index = (player.playerInfo.seat - startSeat + 4) % 4;
        orderedPlayers[index] = player;
      }
    }
    return orderedPlayers;
  }

  setRoom(room: TwoEightRoom) {
    this.matchID.string = room.matchId || '';
    this.matchID.node.parent.active = !!room.matchId;
    this.session.string = room.matchTotal ? `${room.matchOrder}/${room.matchTotal}` : '';
    this.session.node.parent.active = !!room.matchTotal;
    this.bet.string = moon.string.formatMoney(room.betMoney);
    for (let key in room.appearChessList) {
      let index = lstIndex[key];
      let count = room.appearChessList[key];
      this.chessCounts[index].string = count;
    }
    
  }

  playSelectDealer(userId: number) {
    return new Promise<any>(resolve => {
      let selectedPlayer: TwoEightPlayer;
      let rotateActions = [];
      let round = 5;
      let rotateDelay = 0.2;
      let numberOfSelectingPlayer = 0;
      for (let player of this.players) {
        player.showDealer(false);

        //if this player has snatched,  highlight this user
        if (player.playerInfo.snatch !== 0) {
          numberOfSelectingPlayer++;
          rotateActions.push(
            cc.callFunc(() => {
              player.showBorder(true);
              moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.DEALER_SELECTING, false);
            })
          );
          rotateActions.push(
            cc.delayTime(rotateDelay)
          );
          rotateActions.push(
            cc.callFunc(() => {
              player.showBorder(false);
            })
          );
          if (player.playerInfo.userId == userId) {
            selectedPlayer = player;
        }q
        }
      }

      let delayTime = rotateDelay * round * numberOfSelectingPlayer;
      this.node.runAction(
        cc.sequence(rotateActions).repeat(round)
      );
      this.node.runAction(
        cc.sequence(
          cc.delayTime(delayTime),
          cc.callFunc(() => {
            moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.DEALER_SELECTED, false);
          }),
          selectedPlayer.getSelectAnim(),
          cc.callFunc(() => {
            selectedPlayer.showDealer(true);
            resolve();
          })
        )
      );
    });
  }

  hideFirstArrow() {
    this.firstArrow.active = false;
  }

  setPlayerSnatchValue(userId, value) {
    let player = this.getPlayerByUserId(userId);
    player.stopThinking();
    player.setSnatchValue(value);
  }


  playFirstDeal(userId) {
    let player = this.getPlayerByUserId(userId);
    this.firstArrow.stopAllActions();
    this.firstArrow.active = true;
    this.firstArrow.opacity = 255;
    this.firstArrow.x = player.node.x;
    this.firstArrow.y = player.node.y + 110;
    let actions = [];
    let moveTime = 0.3;
    let moveY = 10;
    for (let i = 0; i < 4; i++) {
      actions.push(
        cc.moveBy(
          moveTime, cc.v2(0, -moveY)
        ),
        cc.moveBy(
          moveTime, cc.v2(0, moveY)
        ),
      );
    }
    actions.push(
      cc.fadeOut(0.1)
    );
    this.firstArrow.runAction(
      cc.sequence(actions)
    );
  }

  leaveGame() {
    cc.log('leave with allow', this.isAllowQuitGameWithoutLeaveCmd)
    if (this.isAllowQuitGameWithoutLeaveCmd){
      this.clear();
      moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
      this.returnBackToGameLobby();
    } else {
      moon.gameSocket.leaveBoard();
      this.toggleDropdown();
    }
   
  }

  showHelp() {
    moon.dialog.showHelp(GlobalInfo.room.gameId);
    this.toggleDropdown();
  }

  showSetting() {

    this.toggleDropdown();
    moon.dialog.showSettings();
  }

  showRecord() {
    this.toggleDropdown();
    moon.gameSocket.getPlayHistory(GlobalInfo.room.gameId);
  }

  hidePlayers() {
    for (let player of this.players) {
      player.node.active = false;
      player.cleanAvatar();
    }
  }

  removePlayer(userId: any) {
    let player = this.getPlayerByUserId(userId);
    if (player) {
      player.node.active = false;
    }
  }



  clear() {
    this.betLayer.clear();
    this.hideBetInfo();
    ChessPool.getInstance().returnAllChesses();
    for (let player of this.players) {
      if (player.node.active) {
        player.clear();
        player.showDealer(false);
        player.showBorder(false);
      }
    }
    let room = GlobalInfo.room as TwoEightRoom;
    if (room && room.appearChessList) {
      this.setRoom(room);
    } else {
      for (let countLabel of this.chessCounts) {
        countLabel.string = '0';
      }
    }
  }

  removeOtherPlayers() {
    for (let player of this.players) {
      if (player.playerInfo.userId != GlobalInfo.me.userId) {
        player.node.active = false;
      }
    }
  }

  addPlayer(playerInfo: TwoEightPlayerInfo) {
    let startSeat;
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    let seatIndex = (playerInfo.seat + 4 - startSeat) % 4;
    let player: TwoEightPlayer = this.getPlayerByIndex(seatIndex);
    if (player) {
      player.setPlayerInfo(playerInfo);
      player.node.active = true;
    }
  }

  updateCurrentRoom() {
    this.clear();
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    this.setRoom(room);
    this.setPlayers(room.playerInfoList);
    cc.log('return game with room', room);
    moon.cardPool.returnAllCard()
    TwoEightRoomService.getInstance().activeCountdown();
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        // this.changeState(GAME_STATE.START, TwoEightStartState);
        break;
      case COUNTDOWN_TYPE.STATE_CHOOSE_BANKER:
        this.changeState(GAME_STATE.HOUSE, TwoEightHouseState);
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        this.betLayer.setBet(room.betMoneyTotal);
        this.changeState(GAME_STATE.BET, TwoEightBetState);
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        this.betLayer.setBet(room.betMoneyTotal);

        if (this.state) {
            if (this.state instanceof TwoEightEndGame) {
                this.state.stopAllAnimation()
            }
        }
        for (let player of this.players) {
          if (player.node.active && player.playerInfo.cardList) {
            player.setChessesByIdList(player.playerInfo.cardList);
          }
        }
        if (room.matchOrder >= room.matchTotal) {
          this.allowToQuitGameWithoutLeaveCmd()
        }
        this.playEndSessionCountdown();
        break;
    }
  }

  playEndSessionCountdown() {

  }

  setBetInfo(str: string) {
    this.betInfo.string = str;
    this.betInfo.node.parent.active = true;
  }

  hideBetInfo() {
    this.betInfo.node.parent.active = false;
  }

  showWaiting() {
    this.waiting.active = true;
  }

  hideWaiting() {
    this.waiting.active = false;
  }

  hidePlayerSnatchValue() {
    for (let player of this.players) {
      if (player.node.active) {
        player.hideSnatchValue();
      }
    }
  }


  hidePlayerBetValue() {
    for (let player of this.players) {
      if (player.node.active) {
        player.hideBetValue();
      }
    }
  }

  updateRoomInfoList(roomInfoList: Array<RoomInfo>) {
    this.lobby.updateRoomInfoList(roomInfoList);
  }


  allPlayerThinking() {
    for (let player of this.players) {
      if (player.node.active) {
        player.startThinking();
      }
    }
  }


  allowToQuitGameWithoutLeaveCmd() {
    cc.log('allow to quit game')
    this.isAllowQuitGameWithoutLeaveCmd = true;
  }

  
  disallowToQuitGameWithoutLeaveCmd() {
    cc.log('allow to quit game')
    this.isAllowQuitGameWithoutLeaveCmd = false;
  }

  getIntoGameRoom(isUserAnimate = true) {
    this.isAllowQuitGameWithoutLeaveCmd = false;
    if (isUserAnimate) {
      cc.log('get into game rome 2 8');
      moon.header.hide();
      moon.dialog.hideWaiting();
      moon.audio.stopAllAudio();
      this.lobby.node.runAction(
        cc.sequence(
          cc.fadeOut(0.3),
          cc.callFunc(() => {
            this.lobby.node.active = false;
          })
        )
      );
      this.gameNode.opacity = 0;
      this.gameNode.active = true;
      this.gameNode.runAction(
        cc.sequence(
          cc.delayTime(0.1),
          cc.fadeIn(0.2),
          cc.callFunc(() => {
            moon.audio.playMusicInGame(TWO_EIGHT_AUDIO.GAME_BGM, true);
          })
        )
      );
    } else {
      moon.header.hide();
      moon.dialog.hideWaiting();
      moon.audio.stopAllAudio();
      this.lobby.node.active = false;
      this.gameNode.active = true;
      this.gameNode.opacity = 255;
      moon.audio.playMusicInGame(TWO_EIGHT_AUDIO.GAME_BGM, true);
    }
   
  }

  returnBackToGameLobby() {
    moon.header.show();
    moon.audio.stopAllAudio();
    this.gameNode.runAction(
      cc.sequence(
        cc.fadeOut(0.3),
        cc.callFunc(() => {
          this.gameNode.active = false;
        })
      )
    );
    this.lobby.node.opacity = 0;
    this.lobby.node.active = true;
    moon.dialog.hideWaiting();
    this.lobby.node.runAction(
      cc.sequence(
        cc.delayTime(0.1),
        cc.fadeIn(0.2),
        cc.callFunc(() => {
          if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
            this.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
          }
          moon.audio.playMusicInGame(TWO_EIGHT_AUDIO.LOBBY_BGM, true);
        })
      )
    );
  }
}