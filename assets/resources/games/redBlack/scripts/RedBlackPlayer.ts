import {AvatarIcon} from "../../../../scripts/common/AvatarIcon";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {moon, GlobalInfo} from "../../../../scripts/core/GlobalInfo";
import {POKER_CHESS_TYPE, POKER_CHESS_TYPE_NAME, RESOURCE_BLOCK} from "../../../../scripts/core/Constant";
import { Card } from '../../../../scripts/core/Card';
import { RedBlackPlayerInfo } from "./model/RedBlackPlayerInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackPlayer extends cc.Component {

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Sprite)
  status: cc.Sprite = null;



  @property(cc.Label)
  winAmount: cc.Label = null;

  @property(cc.Label)
  loseAmount: cc.Label = null;

  @property(cc.Sprite)
  betUnit: cc.Sprite = null;





  currentBanner: cc.Node;
  playerInfo: RedBlackPlayerInfo;


  onLanguageChange() {  
    if (!this.playerInfo) {
      return;
    }
 
  }

  onEnter() {
  
  }

  

 
  placeBet(times) {
   
  }

  setPlayerInfo(playerInfo: RedBlackPlayerInfo) {
    this.clear()
   
  }


  formatPlayerName(playerInfo: RedBlackPlayerInfo){
    return moon.string.formatPlayerName(playerInfo.displayName, GlobalInfo.me.userId === playerInfo.userId)
  }



  






  clearAnimation() {


  }



  setMoney(money: any) {
    this.money.string = moon.string.formatMoney(money);
  }

  update(dt: number) {
    
  }

  clear() {
  }
}

