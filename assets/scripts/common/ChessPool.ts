import {Chess} from "./Chess";

const {ccclass, property} = cc._decorator;

@ccclass
export class ChessPool extends cc.Component {

  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  pool: cc.NodePool;

  workingChesses = [];

  total = 40;

  constructor() {
    super();
    ChessPool.instance = this;
  }

  private static instance: ChessPool;

  static getInstance(): ChessPool {
    if (!ChessPool.instance) {
      ChessPool.instance = new ChessPool();
    }

    return ChessPool.instance;
  }

  onLoad() {
    this.pool = new cc.NodePool();
    for (let i = 0; i < this.total; i++) {
      let chess = cc.instantiate(this.chessPrefab);
      this.pool.put(chess);
    }
  }

  clear() {
    this.pool.clear();
  }

  getChess(chessId?, isOpen = false): cc.Node {
    let chessNode: cc.Node = null;
    if (this.pool.size() > 0) {
      chessNode = this.pool.get();
    } else {
      chessNode = cc.instantiate(this.chessPrefab);
    }
    chessNode.active = true;
    chessNode.removeFromParent();
    chessNode.position = cc.v2();
    chessNode.rotation = 0;
    let chess: Chess = chessNode.getComponent(Chess);
    chess.clearFront();
    if (isOpen && chessId != undefined && chessId != null) {
      chess.open(chessId);
    } else {
      chess.hide();
    }
    chess.chessId = chessId;
    this.workingChesses.push(chessNode);
    return chessNode;
  }

  returnAllChesses() {
    for (let node of this.workingChesses) {
      let chess: Chess = node.getComponent(Chess);
      chess.clearFront();
      node.active = false;
      this.pool.put(node);
    }
    this.workingChesses = [];
  }

  putChesses(chesses) {
    for (let chess of chesses) {
      chess.active = false;
      this.pool.put(chess);
    }
  }
}