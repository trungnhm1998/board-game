import {SceneComponent} from '../common/SceneComponent';
import {NodeUtils} from '../core/NodeUtils';
import * as md5 from 'md5';
import {LocalData} from '../core/LocalStorage';
import {GlobalInfo, moon} from '../core/GlobalInfo';
import {CAPTCHA_TYPE, SCENE_TYPE} from '../core/Constant';
import {Captcha} from '../common/Captcha';

const {ccclass, property} = cc._decorator;

interface LoginSNode {
  name: string;
  tabName: string;
  node: cc.Node;
}

@ccclass
export class Login extends SceneComponent {
  name = SCENE_TYPE.LOGIN;

  authCode: any;
  stacks: Array<LoginSNode> = [];
  formWidth = 630;
  slideDuration = 0.2;
  isRememberPassword = true;
  isUserEditPassword = true;

  @property(cc.Node)
  tabs: cc.Node = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.EditBox)
  lUserName: cc.EditBox = null;

  @property(cc.EditBox)
  lPassword: cc.EditBox = null;

  @property(Captcha)
  lCaptcha: Captcha = null;

  @property(cc.Label)
  lError: cc.Label = null;


  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Toggle)
  rememberPassword: cc.Toggle = null;

  currentTab: cc.Node;
  onRenameCallback;

  onEnter() {
    super.onEnter();
    moon.header.hide();
    this.lPassword.string = '';
    GlobalInfo.me.isReturn = false;
    this.showLogin();
    this.onResize();
  }

  onLeave() {
    super.onLeave();
    this.stacks = [];
    for (let tab of this.tabs.children) {
      NodeUtils.enableEditBox(tab, false);
    }
    this.currentTab = null;
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.tabs, 'le_editbox', 'enterEmail', {placeHolder: true});
  }

  onPasswordInputTextChange() {
    this.isUserEditPassword = true;
  }

  onToogleRememberPassword() {
    this.isRememberPassword = this.rememberPassword.isChecked;
    LocalData.rememberPass = this.isRememberPassword ? 1 : 0;
    moon.storage.saveLocalData();
  }

  onClickOnRememberText() {
    this.isRememberPassword = !this.rememberPassword.isChecked;
    this.rememberPassword.isChecked = this.isRememberPassword;
    // this.rememberPassword.checkMark.node.active = this.isRememberPassword;
  }

  showLogin(animate = false) {
    this.title.node.active = true;
    this.lError.string = '';
    this.showTab('login', moon.locale.get('login'), animate);

    this.lUserName.string = LocalData.last_username;
    this.isRememberPassword = LocalData.rememberPass == 1;
    this.rememberPassword.isChecked = this.isRememberPassword;
    if (this.isRememberPassword && LocalData.last_saved_password) {
      this.isUserEditPassword = false;
    }
    if (this.isRememberPassword && LocalData.last_saved_password) {
      this.lPassword.string = '34#$2@PoOO';
    }
    if (this.lUserName.string) {
      //this.lPassword.setFocus();
    } else {
      // this.lUserName.setFocus();
    }
    if (LocalData.isNextCaptcha != 1) {
      this.lCaptcha.hide();
    }
  }

  isShowTab(tabName) {
    let lastTab = this.stacks[this.stacks.length - 1];
    return lastTab && lastTab.tabName == tabName;
  }

  showTab(tabName, title, animate = false) {
    let targetTab = NodeUtils.findByName(this.tabs, tabName);
    let lastTab = this.stacks[this.stacks.length - 1];
    this.stacks.push({
      tabName: tabName,
      name: title,
      node: targetTab
    });
    if (animate) {
      if (lastTab) {
        NodeUtils.enableEditBox(lastTab.node, false);
        lastTab.node.runAction(cc.fadeOut(this.slideDuration / 3));
        lastTab.node.runAction(
          cc.moveTo(this.slideDuration, cc.v2(-this.formWidth / 2, lastTab.node.y))
        );
      }
      targetTab.active = true;
      targetTab.x = this.formWidth / 2 + this.formWidth;
      targetTab.runAction(cc.fadeIn(this.slideDuration));
      targetTab.runAction(
        cc.sequence(
          cc.moveTo(this.slideDuration, cc.v2(this.formWidth / 2, targetTab.y)),
          cc.callFunc(() => {
            if (lastTab) {
              lastTab.node.active = false;
            }
            NodeUtils.enableEditBox(targetTab);
          })
        )
      );
    } else {
      if (lastTab) {
        lastTab.node.active = false;
        NodeUtils.enableEditBox(lastTab.node, false);
        lastTab.node.x = -this.formWidth / 2;
      }
      targetTab.active = true;
      NodeUtils.enableEditBox(targetTab, true);
      targetTab.x = this.formWidth / 2;
      targetTab.opacity = 255;
    }

    this.currentTab = targetTab;
    this.title.string = title;
  }


  showLoginCaptcha(base64Image, token) {
    this.lCaptcha.show(base64Image, token, () => {
      moon.lobbySocket.getCaptcha(CAPTCHA_TYPE.LOGIN);
    });
  }

  onInputFocus() {
    if (this.currentTab) {
      NodeUtils.enableEditBox(this.currentTab, true);
    }
  }

  unFocusInput() {
    if (this.currentTab) {
      NodeUtils.enableEditBox(this.currentTab, false);
    }
  }

  onLogin() {
    moon.audio.playAudio('game_sound_btn_click');
    NodeUtils.enableEditBox(this.currentTab, false);
    let email = this.lUserName.string;
    let password = this.lPassword.string;
    let captcha = this.lCaptcha.getInput();
    let token = this.lCaptcha.getToken();

    // Temporary fix unseen issue
    this.lError.node.width = 372 + Math.random() * 3;

    if (!email) {
      this.lError.string = moon.locale.get('requireEmail');
    } else if (!moon.string.validateEmail(email)) {
      this.lError.string = moon.locale.get('invalidEmail');
    } else if (!password) {
      this.lError.string = moon.locale.get('requirePassword');
    } else if (!captcha && this.lCaptcha.node.active) {
      this.lError.string = moon.locale.get('requireCaptcha');
    } else {
      this.lError.string = '';
      if (this.isUserEditPassword) {
        password = md5(this.lPassword.string);
      } else {
        password = LocalData.last_saved_password;
      }

      LocalData.last_username = this.lUserName.string;

      if (this.isRememberPassword) {
        LocalData.last_saved_password = password;
      } else {
        LocalData.last_saved_password = '';
      }

      moon.storage.saveLocalData();
      moon.dialog.showWaiting();
      moon.lobbySocket.login(this.lUserName.string, password, captcha, token);
    }
  }

  showLoginError(msg: string) {
    this.lError.string = msg;
    let layout = this.lError.node.parent.getComponent(cc.Layout);
    if (layout) {
      layout.updateLayout();
    }
    this.lError.node.height = 100;
  }
}
