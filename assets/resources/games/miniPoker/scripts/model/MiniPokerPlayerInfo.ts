export class MiniPokerPlayerInfo {
  userId: number;
  money: number;
  betMoney: number;
  winMoney: number;
  resultCard: {
    bonusFactor: number,
    cardList: number[],
    cardType: number,
  };
  
  betList: number[];
  seat: number;
  displayName: string;
  avatar: string;
  betFactor: number;
  betDealer: number;
  formatedDisplayName: string;
}