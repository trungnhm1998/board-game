import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {GlobalInfo, moon} from "../core/GlobalInfo";
import {OTP, OTP_TYPE} from "../core/Constant";

const {ccclass, property} = cc._decorator;


@ccclass
export class OTPDialog extends SceneComponent {

  @property(cc.EditBox)
  otp: cc.EditBox = null;
  @property(cc.Node)
  otpResend: cc.Node = null;

  @property(cc.Label)
  otpCountdown: cc.Label = null;

  @property(cc.Node)
  reloadBtn: cc.Node = null;

  @property(cc.Label)
  error: cc.Label = null;

  type: string = 'email';
  onCallback: any;
  lastTask: any;

  onEnter() {
    super.onEnter();
    this.error.string = '';
    this.otp.string = '';
    this.otpCountdown.node.active = false;
    this.otpResend.active = false;
    NodeUtils.enableEditBox(this.node);
    this.otp.setFocus();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'lbl_title', 'otpSecurity', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'confirm_lbl', 'confirm');
    NodeUtils.setLocaleLabel(this.node, 'otp', 'enterOTP', {placeHolder: true});
  }

  setCallback(onCallback) {
    this.onCallback = onCallback;
  }

  onSubmit() {
    if (!this.otp.string) {
      this.error.string = moon.locale.get('requireOTP');
    } else if (this.onCallback) {
      this.onCallback(this.otp.string);
    }
  }

  onResentOTP() {
    if (this.reloadBtn) {
      this.reloadBtn.runAction(
        cc.rotateBy(0.3, 180)
      );
    }
    let email = GlobalInfo.me.email;
    moon.dialog.showCaptcha((captchaCode, token) => {
      moon.lobbySocket.getOTP(OTP.DEFAULT, captchaCode, OTP_TYPE.SECURITY, email, token)
      // .then((data) => {
      //   if (!data) return;
      //   this.setData(this.type, data[KEYS.DATA]['otpCountdown']);
      // });
    });
  }

  setData(type, countdown) {
    this.type = type;
    if (this.type == 'email') {
      NodeUtils.setLocaleLabel(this.node, 'content_lbl', 'emailOTPInfo', {params: [ moon.string.hideEmail(GlobalInfo.me.email)]});
    } else if (GlobalInfo.me.type == 'application') {
      NodeUtils.setLocaleLabel(this.node, 'content_lbl', 'gAuthOTPInfo');
    }

    if (countdown) {
      if (this.lastTask) {
        moon.timer.removeTask(this.lastTask);
        this.lastTask = null;
      }
      this.otpCountdown.node.active = true;
      this.otpResend.active = false;

      let time = +countdown;
      this.otpCountdown.string = countdown;
      this.lastTask = moon.timer.tweenNumber(time, -time,
        (val) => {
          this.otpCountdown.string = '' + Math.floor(val);
        }, (val) => {
          this.otpResend.active = true;
          this.otpCountdown.node.active = false;
        }, time);
    }
  }

  showError(err) {
    this.error.string = err;
  }
}