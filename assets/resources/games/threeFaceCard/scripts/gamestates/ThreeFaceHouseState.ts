import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {ThreeFaceGame} from "../ThreeFaceGame";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import {ThreeFaceRoom} from "../model/ThreeFaceRoom";
import {ThreeFaceSocket} from "../service/ThreeFaceSocket";
import {THREE_FACE_AUDIO} from "../ThreeFaceConstants";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceHouseState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  childView: cc.Node = null;


  countdownTask;

  onEnter(game: ThreeFaceGame, data: any) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    this.childView.active = true;
    if (room.countdownInfo.timeout > 0) {
      this.countdown.node.parent.active = true;
      this.runCountdown(room.countdownInfo.timeout, () => this.playCountDownSound(game));
    }
  }

  playCountDownSound(game: ThreeFaceGame) {
  }

  onUpdate(game: ThreeFaceGame, updateData: UpdateData) {

  }

  onLeave(game: ThreeFaceGame, data: any) {
    this.countdown.node.parent.active = false;
    game.hidePlayerSnatchValue();
  }

  runCountdown(max, playSound) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      playSound();
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num, playSound);
      } else {
        this.node.active = false;
      }
    }, 1);
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }

  onSnatch(evt) {
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    ThreeFaceSocket.getInstance().chooseBanker(true);
  }

  onNonSnatch(evt) {
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    ThreeFaceSocket.getInstance().chooseBanker(false);
  }

  onBetEnd() {
    this.childView.active = false;
  }
}