import {GameState, IGameState, UpdateData} from '../../../../../scripts/common/GameState';
import {MiniPokerRoom} from '../model/MiniPokerRoom';
import {MiniPokerGame} from '../MiniPokerGame';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {MiniPokerPlayer} from '../MiniPokerPlayer';
import {Config} from '../../../../../scripts/Config';
import {MINIPOKER_AUDIO} from '../../../../../scripts/core/Constant';

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerEndGame extends GameState implements IGameState {

  game: MiniPokerGame;

  @property(cc.Node)
  actionButtons: cc.Node = null;
  @property(dragonBones.ArmatureDisplay)
  win_en: dragonBones.ArmatureDisplay = null;
  @property(dragonBones.ArmatureDisplay)
  win_zh: dragonBones.ArmatureDisplay = null;

  onEnter(game: MiniPokerGame, data: any) {
    this.clearAnimation();
    this.game = game;
    let gameRoom: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    const roomPlayerInfoList = gameRoom.playerInfoList;
    let players: MiniPokerPlayer[] = game.getActiveOrderedPlayers();
    let dealChessses: Array<cc.FiniteTimeAction> = [];
    let showChesses: Array<cc.FiniteTimeAction> = [];
    let moveChipsToWinner: Array<cc.FiniteTimeAction> = [];
    let playWinAmountEffect: Array<cc.FiniteTimeAction> = [];
    let winner: MiniPokerPlayer = players.filter(player => player.playerInfo.winMoney > 0)[0];
    cc.log('gameRoom', gameRoom, winner, players);
    players.forEach(player => {
      let dealAction = cc.callFunc(() => {
        this.game.chessLayer.dealChess(player);
      });

      let showChessesAction = cc.callFunc(() => {
        player.showCards();
      });

      let moveChipAction = cc.callFunc(() => {
        moon.audio.playEffectInGame(MINIPOKER_AUDIO.GOLD_FLY);
        if (player.playerInfo.winMoney < 0) {
          this.game.betLayer.moveChipsToWinner(player, winner, player.playerInfo.winMoney);
        }
      });

      dealChessses.push(dealAction);
      // delay before deal chess to next player
      dealChessses.push(cc.delayTime(0));

      showChesses.push(showChessesAction);

      moveChipsToWinner.push(moveChipAction);

      playWinAmountEffect.push(cc.callFunc(() => {

        player.playAmountEffect(player.playerInfo.winMoney);
      }));
    });
    this.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          this.game.isEndGameAnimPlaying = true;
          let delay = 0.5 + (players.length - 1) * 1.1;
          cc.log('setupChess action', roomPlayerInfoList, delay);
          this.game.chessLayer.setupChesses(roomPlayerInfoList);
        }),
        cc.delayTime(0.2),
        cc.sequence(
          dealChessses
        ),
        cc.delayTime(Config.chessFlyTime + 5 * 0.2),
        cc.spawn(
          showChesses
        ),
        cc.delayTime(0.4),
        // play result anim here
        cc.callFunc(() => {
          this.game.showPlayersResultBanner();
        }),
        cc.delayTime(0.3),
        // play win effect here if winner is current user
        cc.callFunc(() => {
          if (GlobalInfo.me.userId === winner.playerInfo.userId) {
            this.playWinAnimation();
          }
        }),
        cc.spawn(moveChipsToWinner),
        cc.delayTime(0.35),
        cc.spawn(playWinAmountEffect),
        cc.delayTime(3.1),
        cc.callFunc(() => {
          this.clearAnimation();
          this.game.isEndGameAnimPlaying = false;
        })
      )
    );

  }


  playWinAnimation() {
    if (moon.locale.getCurrentLanguage() === 'en') {
      this.win_en.node.active = true;
      this.win_en.playAnimation('win_en', 1);
    }
    else {
      this.win_zh.node.active = true;
      this.win_zh.playAnimation('win_zh', 1);
    }

  }

  clearAnimation() {
    this.win_en.node.active = false;
    this.win_zh.node.active = false;
  }

  onUpdate(game: MiniPokerGame, updateData: UpdateData) {
  }

  onLeave(game: MiniPokerGame, data: any) {
  }

  onQuit() {

  }

  onPlayAgain() {

  }
}