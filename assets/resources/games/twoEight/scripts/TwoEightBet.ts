import {moon} from '../../../../scripts/core/GlobalInfo';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightBet extends cc.Component {

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  minMoney: cc.Label = null;

  gameId: any;
  betMoney: any;

  setData(bet, min, gameId) {
    this.gameId = gameId;
    let lang = moon.locale;
    this.betMoney = bet;
    this.bet.string = lang.get('bet:') + ' ' + moon.string.formatMoney(bet);
    this.minMoney.string = lang.get('minMoney:') + ' ' + moon.string.formatMoney(min);
  }

  onPlayGame() {
    moon.gameSocket.playTwoEight(this.gameId, this.betMoney);
  }
}