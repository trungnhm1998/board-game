# -*- coding: utf-8 -*-

import os
import shutil
import time
import sys
import glob
import fnmatch

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

myargs = getopts(sys.argv)

configPattern = "../assets/resources/project_*.json"
tempFile = "../assets/resources/project_temp.json"
destFile = "../assets/resources/project.json"

if '-env' in myargs:  # Example usage.
    env = myargs['-env']
    production_file = ""
    dev_file = ""
    for filename in glob.glob(configPattern):
        lst = [filename]
        filtered = fnmatch.filter(lst, '*project_*.json')
        envPattern = ("_" + env + ".")
        if envPattern in filename:
            production_file = filename

    shutil.copy2(destFile, tempFile)
    shutil.copy2(production_file, destFile)