import {IGameService} from '../../../../../scripts/services/GameService';
import {GameSocket} from '../../../../../scripts/services/SocketService';
import {COUNTDOWN_TYPE, GAME_STATE, KEYS, SERVER_EVENT} from '../../../../../scripts/core/Constant';
import {TwoEightRoomService} from './TwoEightRoomService';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {FadeOutInTransition} from '../../../../../scripts/services/SceneManager';
import {TwoEightGame} from '../TwoEightGame';
import {TwoEightRoom} from '../model/TwoEightRoom';
import {TwoEightStartState} from '../gamestates/TwoEightStartState';
import {TwoEightBetState} from '../gamestates/TwoEightBetState';
import {TwoEightEndGame} from '../gamestates/TwoEightEndGame';
import {ModelUtils} from '../../../../../scripts/core/ModelUtils';
import {TwoEightPlayerInfo} from '../model/TwoEightPlayerInfo';
import {TwoEightHouseState} from '../gamestates/TwoEightHouseState';
import {CountdownInfo} from '../../../../../scripts/model/Room';

export class TwoEightService implements IGameService {
  isActive = false;

  name = 'TwoEightService';

  game: TwoEightGame;

  waitingActions: any[] = [];

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(TwoEightGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };
    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => processAction(this.onErrorMessage, data));
  }

  updateRoomInfoList() {
    if (this.game) {
      this.game.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
    }
  }

  onJoinBoard(data) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    this.game.setPlayers(room.playerInfoList);
    this.game.setRoom(room);
    this.game.showWaiting();
  }

  onPlayerJoinBoard(data) {
    if (data.roomId !== GlobalInfo.room.roomId) {
        return;
    }

    let playerInfo: TwoEightPlayerInfo = data[KEYS.PLAYER_INFO];
    TwoEightRoomService.getInstance().addPlayerInfo(playerInfo);
    if (this.game && GlobalInfo.room.roomId == data[KEYS.ROOM_ID]) {
      this.game.addPlayer(playerInfo);
    }
  }

  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    cc.log('return 2 8 game');
    this.game.onEnterRoom(false, true);
    this.game.updateCurrentRoom();
  }

  onActionInGame(data) {
    let subData = data[KEYS.DATA];
    if (subData.roomId !== GlobalInfo.room.roomId) {
      return
    }
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.BEGIN_CHOOSE_BANKER:
          this.onChooseBanker(subData);
          break;
        case SERVER_EVENT.CHOOSE_BANKER:
          this.showUserBetForBanker(subData);
          break;
        case SERVER_EVENT.BANKER:
          this.showChoosedBanker(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.STOP_GAME:
          this.onStopGame();
          break;
        case SERVER_EVENT.RETURN_GAME:
          cc.log('return game in action in game');
          break;

      }
    };

    handleEvents();
  }

  private onErrorMessage(data) {
    moon.dialog.hideWaiting();
    let command = data[KEYS.CAUSE_COMMAND];
    let msg = data[KEYS.MESSAGE];
    // Check cause command first
    moon.dialog.showNotice(msg);
  }


  private onStopGame() {
    this.game.showWaiting();
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let roomService = TwoEightRoomService.getInstance();
    if (GlobalInfo.me.userId == userId) {
      let backToMain = () => {
        this.game.clear();
        this.clearWaitingActions();
        moon.dialog.showWaiting();
        let transition = new FadeOutInTransition(0.2);
        moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
        if (this.game.state) {
          this.game.state.onLeave(this.game,{})
        }
        this.game.returnBackToGameLobby()
      };
      backToMain();
    } else {
      this.game.removePlayer(userId);
      roomService.removePlayerInfo(userId);
    }
  }

  private onStartGame(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    TwoEightRoomService.getInstance().activeCountdown();

    this.game.hideBetInfo();
    this.game.hideWaiting();
    this.game.changeState(GAME_STATE.START, TwoEightStartState, data);
  }

  private onChooseBanker(data: any) {
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.betList = data[KEYS.BET_LIST];

    let myPlayerInfo = TwoEightRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    myPlayerInfo.betList = data[KEYS.BET_LIST];

    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_FIND_BANKER;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    TwoEightRoomService.getInstance().activeCountdown();
    this.game.changeState(GAME_STATE.HOUSE, TwoEightHouseState, data);
  }

  private showChoosedBanker(data: any) {
    cc.log('play choose banker animation');
    let userId = data[KEYS.BANKER_ID];
    // Update model
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.bankerId = userId;
    this.game.playSelectDealer(userId);
  }


  showUserBetForBanker(data: any) {
    let userId = data[KEYS.USER_ID];
    let betTimes = data[KEYS.BET_FACTOR];

    // Update model
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let playerInfo = TwoEightRoomService.getInstance().getPlayerInfo(userId);
    playerInfo.snatch = betTimes;
    // Update view
    this.game.setPlayerSnatchValue(userId, betTimes);
    cc.log('show snatch of ', userId, GlobalInfo.me.userId);
    if (userId == GlobalInfo.me.userId) {
      if (this.game.state instanceof TwoEightHouseState) {
        this.game.state.onBetEnd();
      }
    }
  }


  private onBeginBet(data: any) {
    // Update model
    let betList = data[KEYS.BET_LIST];
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;

    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];

    let myPlayerInfo = TwoEightRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    myPlayerInfo.betList = betList;
    TwoEightRoomService.getInstance().activeCountdown();
    // Update view
    this.game.changeState(GAME_STATE.BET, TwoEightBetState, room);
  }

  private onBet(data: any) {
    let userId = data[KEYS.USER_ID];
    let money = data[KEYS.MONEY];
    let betMoney = data[KEYS.BET_MONEY];
    let betMoneyTotal = data[KEYS.BET_MONEY_TOTAL];
    // Update model
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let playerInfo = TwoEightRoomService.getInstance().getPlayerInfo(userId);
    playerInfo.bet = betMoney;
    room.betMoneyTotal = betMoneyTotal;
    // Update view
    this.game.betLayer.playBet(userId, betMoney);
    if (room.bankerId == GlobalInfo.me.userId) {
      this.game.setBetInfo(`${betMoneyTotal}`);
    } else {
      this.game.setBetInfo(`${betMoney}/${betMoneyTotal}`);
    }

    let player = this.game.getPlayerByUserId(userId);
    player.placeBet(betMoney);
    player.setBetValue(betMoney);
    player.setMoney(money);
    if (userId == GlobalInfo.me.userId) {
      if (this.game.state instanceof TwoEightBetState) {
        this.game.state.onBetEnd();
      }
    }
  }

  private onEndGame(data: any) {
    // Update model
    TwoEightRoomService.getInstance().updatePlayerInfoList(data[KEYS.PLAYER_INFO_LIST]);
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    room.firstId = data[KEYS.FIRST_ID];
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;
    room.appearChessList = data[KEYS.APPEAR_CHESS_LIST];
    room.shakeDiceResult = data[KEYS.SHAKE_DICE_RESULT];
    // Update view
    this.game.changeState(GAME_STATE.END, TwoEightEndGame);
  }
}