import {TwoEightPlayerInfo} from './model/TwoEightPlayerInfo';
import {TwoEightPlayer} from './TwoEightPlayer';
import {Config} from '../../../../scripts/Config';
import {NodeUtils} from '../../../../scripts/core/NodeUtils';
import {moon} from '../../../../scripts/core/GlobalInfo';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightCardLayer extends cc.Component {

  playerCards = {};

  setupCards(playerInfoList: TwoEightPlayerInfo[], players: TwoEightPlayer[]) {
    console.trace('begin setup card')
    this.node.removeAllChildren();
    this.playerCards = {};
    let chessWidth = 60;
    playerInfoList.forEach(playerInfo => {
      cc.log('player info', playerInfo);
      playerInfo.cardList.forEach(card => {
        let cardNode = moon.cardPool.getCard(card);
        cc.log('get card with size', moon.cardPool.cardWidth, moon.cardPool.cardHeight);
        this.node.addChild(cardNode);
        cardNode.x = 0;
        cardNode.opacity = 0;
        if (this.playerCards[playerInfo.userId]) {
          this.playerCards[playerInfo.userId].push(cardNode);
        } else {
          this.playerCards[playerInfo.userId] = [];
          this.playerCards[playerInfo.userId].push(cardNode);
        }
      });
    });
  }


  dealCard(player: TwoEightPlayer, delay = 0) {
    let cards: Array<cc.Node> = this.playerCards[player.playerInfo.userId];
    let positions = player.getReceivedPositions();
    player.resetChessContent()
    let delayFactor = 0.1;

    let i = 0;
    cards.forEach(card => {
      NodeUtils.swapParent(card, card.parent, player.chessContent);
      card.runAction(
        cc.sequence(
          cc.callFunc(() => {
            card.active = true;
          }),
          cc.delayTime(delayFactor * i),
          cc.spawn(
            cc.fadeIn(0.2),
            cc.moveTo(Config.chessFlyTime, positions[i]).easing(cc.easeIn(1.0))
          )
        )
      );
      i++;
    });
    player.setCardNodes(cards);
  }
}