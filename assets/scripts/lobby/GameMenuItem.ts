import {GameItem} from '../model/GameInfo';
import {NodeUtils} from '../core/NodeUtils';
import {GlobalInfo, moon} from '../core/GlobalInfo';
import {Room} from '../model/Room';
import executionOrder = cc._decorator.executionOrder;

const {ccclass, property} = cc._decorator;

@ccclass()
@executionOrder(2)
export class GameMenuItem extends cc.Component {

  @property(cc.Sprite)
  gameName: cc.Sprite = null;

  @property(dragonBones.ArmatureDisplay)
  skeleton: dragonBones.ArmatureDisplay = null;

  @property(cc.Button)
  playnowBtn: cc.Button = null;

  @property(cc.Node)
  loading: cc.Node = null;

  gameItem: GameItem;
  onClickHandler;

  onLoad() {
  }

  onResize() {
    this.node.scale = Math.min(1, cc.winSize.height / 720);
  }

  onGameClick() {
    if (this.onClickHandler) {
      this.onClickHandler(this.gameItem.gameId);
    }
  }

  setGameItem(gameItem: GameItem, onClickHandler?) {
    this.gameItem = gameItem;
    this.loading.active = true;
    cc.loader.loadResDir(`mainHall/effect/game_${gameItem.gameId}`, (err, resources) => {
      for (let res of resources) {
        if (res instanceof dragonBones.DragonBonesAtlasAsset) {
          this.skeleton.dragonAtlasAsset = res;
        } else if (res instanceof dragonBones.DragonBonesAsset) {
          this.skeleton.dragonAsset = res;
        }
      }
      this.skeleton.armatureName = `game_${gameItem.gameId}`;
      this.skeleton.playAnimation('idle', 0);
      this.loading.active = false;
    });
    this.gameName.spriteFrame = moon.res.getSpriteFrame('gameName_' + gameItem.gameId);
  }

  hideSelectBets(animation = true) {

  }


  loadSavedConfig() {

  }

  getNearestBetInfo() {

  }

  playNow() {
    moon.audio.playAudio('game_sound_btn_click');
    NodeUtils.preventMultipleClick(this.playnowBtn);
    GlobalInfo.room = new Room();
    GlobalInfo.room.gameId = this.gameItem.gameId;
    moon.gameSocket.getRoomInfoList(this.gameItem.gameId); // for preloading
    moon.service.goToPlayScene(this.gameItem.gameId, () => {
      moon.service.getCurrentService().updateRoomInfoList();
    });
  }

  onShowHistory() {

  }
}