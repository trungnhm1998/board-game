let merchants = {
  tableId: 'merchantTable',
  addFormId: 'addForm',
  detailId: "merchantDetail",
  rows: {},
  data: {
    merchantList: [],
    machineList: [],
    insightConnectionList: [],
    currencyList: []
  },
  preprocessData: function (rawData) {
    let result = {};
    for (let i = 0; i < rawData.length; i++) {
      let rawRow = rawData[i];
      if (!result[rawRow.merchantId]) {
        result[rawRow.merchantId] = {};
      }
      let row = result[rawRow.merchantId];
      if (!row.currencies) {
        row.currencies = [];
      }
      row.currencies.push(rawRow.currency);
      row.connectionId = rawRow.connectionId;
      row.secretKey = rawRow.secretKey;
    }
    this.rows = result;
  },
  updateRowsUI: function () {
    let rowPattern = `
    <tr>
      <td><a href="merchant.html?merchantId={0}">{0}</a></td>
      <td>{1}    <a class="btn btn-outline-success" href="add-currency?merchantId={0}"><i class="fas fa-plus"></i></a></td>
      <td>{2}</td>
      <td>{3}</td>
    </tr>
    `;

    for (let rowId in this.rows) {
      let rowData = this.rows[rowId];
      let currencies = rowData.currencies.join(',');
      let rowHtml = core.string.format(rowPattern, rowId, currencies, rowData.connectionId, rowData.secretKey);
      $(`#${this.tableId}`).find(`> tbody`).append(rowHtml);
    }
  },

  updateDataTable: function () {
    $(`#${this.tableId}`).DataTable({
      searching: false,
      paging: false,
      select: true
    });
  },

  updateCurrenciesSelection: function (excludeList) {
    let pattern = `<option value="{0}">{1}</option>`;
    for (let currency of this.data.currencyList) {
      if (!excludeList || excludeList.indexOf(currency.currency) < 0) {
        let option = core.string.format(pattern, currency.currency, currency.currency);
        $(`#${this.addFormId}`).find('#currency').append(option);
      }
    }
  },

  updateConnectionSelection: function() {
    let pattern = `<option value="{0}">{1}</option>`;
    let addForm = $(`#${this.addFormId}`);

    addForm.find('#connectionId').change((evt) => {
      let connectionId = +addForm.find('#connectionId').val();
      for (let connectionInfo of this.data.insightConnectionList) {
        if (connectionId === connectionInfo.connectionId) {
          addForm.find('#secretKey').text(connectionInfo.secretKey);
        }
      }
    });
    let i = 0;
    for (let connectionInfo of this.data.insightConnectionList) {
      let option = core.string.format(pattern, connectionInfo.connectionId, connectionInfo.connectionId);
      addForm.find('#connectionId').append(option);
      if (i === 0) {
        addForm.find('#secretKey').text(connectionInfo.secretKey);
      }
      i++;
    }
  },

  onAddMerchant: function (evt) {
    let form = $(`#${this.addFormId}`);
    let data = {
      merchantId: form.find('#merchantId').val(),
      description: form.find('#description').val(),
      currency: form.find('#currency').val(),
      connectionId: form.find('#connectionId').val(),
      secretKey: form.find('#secretKey').text()
    };

    core.api.addMerchant(data, function (response) {
      if (response.code === 0) {
        // success
        let myURL = location.href;
        let myDir = myURL.substring( 0, myURL.lastIndexOf( "/" ) + 1);
        location.href = myDir;
      } else {
        alert(response.message);
      }
    })
  },

  onAddCurrency: function() {
    let form = $(`#${this.addFormId}`);
    let data = {
      merchantId: form.find('#merchantId').text(),
      description: form.find('#description').val(),
      currency: form.find('#currency').val(),
      connectionId: form.find('#connectionId').val(),
      secretKey: form.find('#secretKey').val()
    };

    core.api.addMerchant(data, function (response) {
      if (response.code === 0) {
        // success
        let myURL = location.href;
        let myDir = myURL.substring( 0, myURL.lastIndexOf( "/" ) + 1);
        location.href = myDir;
      } else {
        alert(response.message);
      }
    })
  },

  initAddCurrencyForm: function() {
    let form = $(`#${this.addFormId}`);
    let searchParams = new URLSearchParams(location.href.split("?")[1]);
    let merchantId = searchParams.get('merchantId');
    let existCurrencies = [];
    for (let merchantInfo of this.data.merchantList) {
      if (merchantInfo.merchantId === merchantId) {
        form.find('#connectionId').val(merchantInfo.connectionId);
        form.find('#secretKey').val(merchantInfo.secretKey);
        form.find('#description').val(merchantInfo.description);
        existCurrencies.push(merchantInfo.currency);
      }
    }
    form.find('#merchantId').text(merchantId);
    merchants.updateCurrenciesSelection(existCurrencies);
  },

  initMerchantDetail: function() {
    let container = $(`#${this.detailId}`);
    let searchParams = new URLSearchParams(location.href.split("?")[1]);
    let merchantId = searchParams.get('merchantId');
    container.find('#merchantId').text(merchantId);
    this.initDetailCurrencies(merchantId);
  },

  initDetailCurrencies: function(merchantId) {
    let container = $(`#${this.detailId}`);
    let pattern = `<option value="{0}">{1}</option>`;
    for (let merchantInfo of this.data.merchantList) {
      if (merchantInfo.merchantId === merchantId) {
        let option = core.string.format(pattern, merchantInfo.currency, merchantInfo.currency);
        container.find('#currency').append(option);
      }
    }
  },

  removeCurrency: function() {
    let container = $(`#${this.detailId}`);
    let merchantId = container.find('#merchantId').text();
    let currency = container.find('#currency').val();
    core.api.deleteMerchant({
      merchantId: merchantId,
      currency: currency,
      function() {
        location.reload();
      }
    });
  }
};
