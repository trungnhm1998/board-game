PROJECT_DIR=~/workspace/moon-client

cd ${PROJECT_DIR}/tools
python prebuild.py -env android.production
cd ${PROJECT_DIR}

/Applications/CocosCreator.app/Contents/MacOS/CocosCreator --path . --build "platform=android;startScene=cddde544-7cac-43fd-a22e-698bdb50f3ea;title=moon;mergeStartScene=false;debug=false;useDebugKeystore=true;appABIs=['armeabi-v7a','x86','arm64-v8a'];packageName=moon.z88.net;apiLevel=android-28;orientation={'landscapeLeft': true, 'landscapeRight': true};template=link;md5Cache=false;encryptJs=true;embedWebDebugger=false;"

cd ${PROJECT_DIR}/tools
python postbuild.py
cd ${PROJECT_DIR}

cd ${PROJECT_DIR}/build/jsb-link/frameworks/runtime-src/proj.android-studio/
./gradlew assembleRelease
cd ${PROJECT_DIR}

cd ${PROJECT_DIR}/tools
python apk_archive.py dev ~/workspace/mahjong-client-prod/apk
cd ${PROJECT_DIR}