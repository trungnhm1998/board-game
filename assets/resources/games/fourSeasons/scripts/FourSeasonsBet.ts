import { moon } from './../../../../scripts/core/GlobalInfo';
import { BetType } from './FourSeasonsConstants';
import { Random } from './../../../../scripts/core/Random';
import { Config } from './../../../../scripts/Config';
import { FourSeasonsPot } from './FourSeasonsPot';
const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsBet extends cc.Component {
  
  @property(cc.Node)
  betContentNodes: cc.Node = null;
  
  @property(cc.SpriteAtlas)
  chipAtlas: cc.SpriteAtlas = null;

  @property(cc.Node)
  sampleBetNode: cc.Node = null;

  @property(cc.Prefab)
  sampleBetChipPrefab: cc.Prefab = null;

  @property(cc.Node)
  playerNode: cc.Node = null; // for update player avatar animation

  @property(cc.Node)
  tablePlayersNode: cc.Node = null;

  @property(cc.Node)
  spadeChipsContainer: cc.Node = null; // for holding all the bet-ed chips on the table

  @property(cc.Node)
  heartChipsContainer: cc.Node = null; // for holding all the bet-ed chips on the table

  @property(cc.Node)
  clubChipsContainer: cc.Node = null; // for holding all the bet-ed chips on the table

  @property(cc.Node)
  diamondChipsContainer: cc.Node = null; // for holding all the bet-ed chips on the table

  _chipsContainerMap = {};
  chipPool: cc.NodePool = null;
  betNodeList: Array<cc.Node> = [];
  betList: number[] = [];
  currentBetMoney: number = 0;

  onLoad() {
    this.chipPool = new cc.NodePool();
    this._chipsContainerMap[BetType.SPADE] = this.spadeChipsContainer;
    this._chipsContainerMap[BetType.HEART] = this.heartChipsContainer;
    this._chipsContainerMap[BetType.CLUB] = this.clubChipsContainer;
    this._chipsContainerMap[BetType.DIAMOND] = this.diamondChipsContainer;
  }

  /**
   * Update UI footer bet based on bet list
   * @param betList bet list from joining table
   */
  updateBetList(betList: number[]) {
    this.clear();
    this.betList = betList;
    if (!this.currentBetMoney || betList.indexOf(this.currentBetMoney) < 0) {
      this.currentBetMoney = betList[0];
    }

    for (let index = 0; index < this.betList.length; index++) {
      // if betList from server lenght match the design
      let betNode = this.betNodeList[index];
      const betValue = this.betList[index];
      if (!betNode) {
        betNode = cc.instantiate(this.sampleBetNode);
        betNode.setScale(0.75);
        this.betNodeList.push(betNode);
        betNode.getComponent(cc.Button).clickEvents[0].customEventData = `${index}`;
        this.betContentNodes.addChild(betNode);
        this.betContentNodes.setPosition(0, 0); // fixed position of content got auto update to 50
      }

      betNode.active = true;

      if (this.betList[index] == this.currentBetMoney) {
        betNode.getChildByName('highlight').active = true;
      } else {
        betNode.getChildByName('highlight').active = false;
      }

      // get sprite and check if is it correct UI sprite 
      const sprite = betNode.getComponent(cc.Sprite);

      let spriteFrameToReplace = this.chipAtlas.getSpriteFrame(`b${betValue}`);

      if (!spriteFrameToReplace) {
        // custom chip value using chip with place holder
        spriteFrameToReplace = this.chipAtlas.getSpriteFrame('be');
        let betNodeCustomValueLable = betNode.getChildByName('chip_value').getComponent(cc.Label);
        betNodeCustomValueLable.node.active = true;
        betNodeCustomValueLable.string = String(betValue);
      }

      sprite.spriteFrame = spriteFrameToReplace;

      // TODO: Implement gray scale on chip which current balance less than
      if (false) {
        betNode.color = new cc.Color(120, 120, 120);
      }

    }
  }

  onSelectedBet(evt, betMoney) {
    // TODO: Implement prevent click on bet which have more than current player balance
    if (true) {
      this.currentBetMoney = this.betList[parseInt(betMoney)];
      this.updateBetList(this.betList);
    }
  }

  onBet(potToPlaceBet: FourSeasonsPot, betMoney) {
    this.playerNode.runAction(
      cc.sequence(
        cc.spawn(
          cc.moveBy(0.05, cc.v2(0, 10)),
          cc.callFunc(() => {
            // random position to place on pot first
            let betChipNode = this.chipPool.get();
            if (!betChipNode) {
              betChipNode = cc.instantiate(this.sampleBetChipPrefab);
            }
            betChipNode.opacity = 255;

            let chipSpriteFrame = this.chipAtlas.getSpriteFrame(`s${betMoney}`);

            // for custom value cannot find in chip atlas
            if (!chipSpriteFrame) {
              chipSpriteFrame = this.chipAtlas.getSpriteFrame('seb');
              const betChipLabel = betChipNode.getChildByName('custom_value_label').getComponent(cc.Label);
              betChipLabel.node.active = true;
              betChipLabel.string = String(betMoney);
            }


            betChipNode.getComponent(cc.Sprite).spriteFrame = chipSpriteFrame;
            betChipNode.active = true;
            const chipsContainer = this._chipsContainerMap[potToPlaceBet.betType];
            chipsContainer.addChild(betChipNode);
            
            
            let playerPosInWorld = this.playerNode.parent.convertToWorldSpaceAR(this.playerNode.position);
            let potPosInWorld = potToPlaceBet.node.parent.convertToWorldSpaceAR(potToPlaceBet.node.getPosition())

            let fromPos = chipsContainer.convertToNodeSpaceAR(playerPosInWorld);
            let toPos = chipsContainer.convertToNodeSpaceAR(potPosInWorld);

            // random toPos here
            const randomToPos = new Random().fromCenter(toPos.x, toPos.y, 100);

            betChipNode.setPosition(fromPos);

            betChipNode.runAction(cc.moveTo(Config.chipFlyTime, cc.v2(randomToPos.x, randomToPos.y)));

          })
        ),
        cc.moveBy(0.05, cc.v2(0, -10)),
      )
    )
  }

  /**
   * place bet from other players to pot
   * @param potToPlaceBet Pot to places chip from other players
   * @param betMoney value will be calculate to chips and place on pot ex: bet list [1,2,5] betMoney [10] -> return [5,5]
   */
  onOtherPlayersBet(potToPlaceBet: FourSeasonsPot, betMoney: number) {
    this.tablePlayersNode.runAction(
      cc.sequence(
        cc.spawn(
          cc.moveBy(0.05, cc.v2(0, 10)),
          cc.callFunc(() => {
            let chips = this.getChipsCountFromMoney(betMoney, this.betList);
            let chipNodes: cc.Node[] = [];

            for (let bet of this.betList) {
              let chipCount = chips[bet];
              let i = 0;
              while (i < chipCount) {
                // random position to place on pot first
                let betChipNode = this.chipPool.get();
                if (!betChipNode) {
                  betChipNode = cc.instantiate(this.sampleBetChipPrefab);
                }
                betChipNode.opacity = 255;
    
                let chipSpriteFrame = this.chipAtlas.getSpriteFrame(`s${bet}`);
    
                // for custom value cannot find in chip atlas
                if (!chipSpriteFrame) {
                  chipSpriteFrame = this.chipAtlas.getSpriteFrame('seb');
                  const betChipLabel = betChipNode.getChildByName('custom_value_label').getComponent(cc.Label);
                  betChipLabel.node.active = true;
                  betChipLabel.string = String(bet);
                }
    
    
                betChipNode.getComponent(cc.Sprite).spriteFrame = chipSpriteFrame;
                betChipNode.active = true;
                const chipsContainer = this._chipsContainerMap[potToPlaceBet.betType];
                chipsContainer.addChild(betChipNode);
                
                
                let playerPosInWorld = this.tablePlayersNode.parent.convertToWorldSpaceAR(this.tablePlayersNode.position);
                let potPosInWorld = potToPlaceBet.node.parent.convertToWorldSpaceAR(potToPlaceBet.node.getPosition())
    
                let fromPos = chipsContainer.convertToNodeSpaceAR(playerPosInWorld);
                let toPos = chipsContainer.convertToNodeSpaceAR(potPosInWorld);
    
                // random toPos here
                const randomToPos = new Random().fromCenter(toPos.x, toPos.y, 100);
    
                betChipNode.setPosition(fromPos);
    
                betChipNode.runAction(
                  cc.sequence(
                    cc.delayTime(Math.random()),
                    cc.moveTo(Config.chipFlyTime, cc.v2(randomToPos.x, randomToPos.y))
                  )
                );
                i++;
              }
            }



          })
        ),
        cc.moveBy(0.05, cc.v2(0, -10)),
      )
    )
  }

  getChipsCountFromMoney(money, chipList = [100, 20, 5]) {
    chipList = [].concat(chipList).sort((a, b) => b - a)
    let chipCountList = [];
    let remain = money;
    let returnChipsCount = {};
    for (let chip of chipList) {
      let chipCount = Math.floor(remain / chip);
      remain -= chipCount * chip;
      chipCountList.push(
        chipCount
      )
    }
    let i = 0;
    for (let chip of chipList) {
      returnChipsCount[chip] = chipCountList[i++];
    }
    // returnChipsCount[smallest] = Math.floor(remain / smallest);
    return returnChipsCount;
  }

  moveChipsFromHouseToPot(house: cc.Node, pot: FourSeasonsPot) {
    cc.log('FourSeasonsBet::moveChipsFromHouseToPot');
    const chipsContainer: cc.Node = this._chipsContainerMap[pot.betType];
    const cloneCount = chipsContainer.children.length;
    for (let i = 0; i < cloneCount; i++) {
      let betChipNode = this.chipPool.get();
      if (!betChipNode) {
        betChipNode = cc.instantiate(this.sampleBetChipPrefab);
      }
      
      const randomBetValue = new Random().fromList(this.betList)
      let chipSpriteFrame = this.chipAtlas.getSpriteFrame(`s${randomBetValue}`);
      
      // for custom value cannot find in chip atlas
      if (!chipSpriteFrame) {
        chipSpriteFrame = this.chipAtlas.getSpriteFrame('seb');
        const betChipLabel = betChipNode.getChildByName('custom_value_label').getComponent(cc.Label);
        betChipLabel.node.active = true;
        betChipLabel.string = String(randomBetValue);
      }
      
      betChipNode.getComponent(cc.Sprite).spriteFrame = chipSpriteFrame;
      
      betChipNode.active = true;
      chipsContainer.addChild(betChipNode);
      betChipNode.setPosition(chipsContainer.convertToNodeSpaceAR(house.parent.convertToWorldSpaceAR(house.getPosition())));
      betChipNode.opacity = 255;

      const toPos = chipsContainer.convertToNodeSpaceAR(pot.node.parent.convertToWorldSpaceAR(pot.node.getPosition()));
      const randomToPos = new Random().fromCenter(toPos.x, toPos.y, 100); 

      betChipNode.runAction(
        cc.moveTo(0.4, randomToPos.x, randomToPos.y)
      )
    }
  }

  /**
   * use to move all chips from pot to some node position then put those to pool and clean up
   * @param pot all the chips on this pot
   * @param node could be player or table players
   */
  moveChipsFromPotToNode(pot: FourSeasonsPot, node: cc.Node) {
    cc.log('FourSeasonsBet::moveChipsFromPotToNode', pot.name);

    const chipsContainer: cc.Node = this._chipsContainerMap[pot.betType];
    chipsContainer.children.forEach(chip => {
      let nodePos = chipsContainer.convertToNodeSpaceAR(node.parent.convertToWorldSpaceAR(node.getPosition()));
      chip.runAction(
        cc.sequence(
          cc.moveTo(0.4, nodePos),
          cc.callFunc(() => {
            // chip.opacity = 0;
            chip.removeFromParent();
            this.chipPool.put(chip);
          })
        )
      );
    });
  }

  clear() {

  }
}