import {TestRunner} from '../test/TestRunner';
import {SceneManager} from '../services/SceneManager';
import {ResourceManager} from '../core/ResourceManager';
import {MoonTimer, TimerComponent} from '../core/TimerComponent';
import {LocalStorage} from '../core/LocalStorage';
import {LanguageService} from '../services/LanguageService';
import {DialogManager} from '../services/DialogManager';
import {NormalShader} from '../../shader/NormalShader';
import {CMaterial} from '../shader/CMaterial';
import {CountdownShader} from '../../shader/CountdownShader';
import {GrayShader} from '../../shader/GrayShader';
import {moon} from '../core/GlobalInfo';
import {Random} from '../core/Random';
import {CString} from '../core/String';
import {GameService} from '../services/GameService';
import {MoneyService} from '../services/MoneyService';
import {NativeService} from '../services/NativeService';
import {GameSocket, LobbySocket} from '../services/SocketService';
import {CardPool} from '../core/CardPool';
import {Header} from '../lobby/Header';
import {DECK_ID, RESOURCE_BLOCK} from '../core/Constant';
import {AudioManager} from "../services/AudioManager";

const {ccclass, property} = cc._decorator;

declare var require: any;

@ccclass
export class TestScene extends cc.Component {

  test: TestRunner;

  dlgMgr: DialogManager = null;

  @property(cc.Prefab)
  dlgMgrPrefab: cc.Prefab = null;

  @property(cc.Node)
  timer: cc.Node = null;

  @property(cc.Node)
  dialogContent: cc.Node = null;

  @property(cc.Node)
  gameContent: cc.Node = null;

  @property(cc.Prefab)
  gamePrefab: cc.Prefab = null;

  gameNode: cc.Node = null;

  start() {
    this.initGlobalFacet();
    this.gameNode = cc.instantiate(this.gamePrefab);

    let timerComp = this.timer.getComponent(TimerComponent);
    moon.timer.setTimer(timerComp);

    cc.debug.setDisplayStats(false);
    // cc.view.setResizeCallback(() => this.onResize());
    // gamedev.event.register(GAME_EVENT.ON_LANGUAGE_CHANGE, () => this.onLanguageChange());

    moon.storage.loadLocalData();
    moon.res;
    moon.scene;
    let dlg = cc.instantiate(this.dlgMgrPrefab);
    this.dialogContent.addChild(dlg);
    this.dlgMgr = dlg.getComponent(DialogManager);
    moon.dialog.init(this.dlgMgr);
    CMaterial.addShader(GrayShader);
    CMaterial.addShader(CountdownShader);
    CMaterial.addShader(NormalShader);

    moon.dialog.showWaiting();
    let resourceMgr = moon.res;
    resourceMgr.preloadFolders(['card/decks'], () => {
    }, RESOURCE_BLOCK.GAME)
      .then(() => {
        resourceMgr.preloadFolders(['card/card',],
          (value) => {
          })
          .then(() => moon.cardPool.init())
          .then(() => {
            moon.locale
              .setLang('zh')
              .then(() => {
                moon.dialog.hideWaiting();
                this.gameContent.addChild(this.gameNode);
                this.startTC();
              });
          });
      });
  }


  initGlobalFacet() {
    // moon.gameSocket = new GameSocket();
    // moon.lobbySocket = new LobbySocket();
    // moon.string = new CString();
    // moon.res = ResourceManager.getInstance();
    // moon.timer = new MoonTimer();
    // moon.random = new Random();
    // moon.locale = LanguageService.getInstance();
    // moon.dialog = DialogManager.getInstance();
    // moon.native = NativeService.getInstance();
    // moon.money = MoneyService.getInstance();
    // moon.scene = SceneManager.getInstance();
    // moon.storage = new LocalStorage();
    // moon.service = GameService.getInstance();

    moon.gameSocket = new GameSocket();
    moon.lobbySocket = new LobbySocket();
    moon.storage = new LocalStorage();
    moon.storage.loadLocalData();
    let dlg = cc.instantiate(this.dlgMgrPrefab);
    this.dialogContent.addChild(dlg);
    moon.dialog = dlg.getComponent(DialogManager);
    moon.string = new CString();
    moon.res = ResourceManager.getInstance();
    moon.timer = new MoonTimer();
    moon.random = new Random();
    moon.locale = LanguageService.getInstance();
    moon.native = NativeService.getInstance();
    moon.money = MoneyService.getInstance();
    moon.scene = SceneManager.getInstance();
    moon.service = GameService.getInstance();
    moon.header = {
      hide: () => {
        cc.log('hide header');
      }
    } as Header;
    moon.cardPool = CardPool.getInstance();
    moon.cardPool.setCardDeck(DECK_ID.SIMPLIFIED_1);
    cc.log('init card pool test');
    moon.cardPool.setCardScale(0.6);
    moon.audio = AudioManager.getInstance();
  }


  startTC() {
    this.test = new TestRunner();
    this.test.setGame(this.gameNode);
    this.test.run();
  }
}