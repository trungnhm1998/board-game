import os
import shutil
import glob
import sys
import fnmatch
import datetime

def getopts(argv):
	opts = {}  # Empty dictionary to store key-value pairs.
	while argv:  # While there are arguments left to parse...
		if argv[0][0] == '-':  # Found a "-name value" pair.
			opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
		argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
	return opts

myargs = getopts(sys.argv)

def copy_and_overwrite(from_path, to_path):
    if os.path.exists(to_path):
        shutil.rmtree(to_path)
    shutil.copytree(from_path, to_path)

copy_and_overwrite("../assets/html", "../build/web-mobile/res/raw-assets/html")

if '-env' in myargs:  # Example usage.
    env = myargs['-env']
    production_file = ""
    dev_file = ""
    for filename in glob.glob("../build/web-mobile/res/raw-assets/resources/project.*.json"):
        lst = [filename]
        filtered = fnmatch.filter(lst, '*project.*.*.json')
        envPattern = ("." + env + ".")
        if envPattern in filename:
            production_file = filename
        elif len(filtered) == 0:
            dev_file = filename
        else:
            os.remove(filename)

    shutil.move(production_file, dev_file)


for filename in glob.glob("../build/web-mobile/privacy-policy.*"):
    shutil.move(filename, os.path.join("../build/web-mobile", "privacy-policy.htm"))

for filename in glob.glob("../build/web-mobile/terms-of-service.*"):
    shutil.move(filename, os.path.join("../build/web-mobile", "terms-of-service.htm"))