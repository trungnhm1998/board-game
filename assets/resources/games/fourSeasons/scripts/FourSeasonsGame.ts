import { DECK_ID } from './../../../../scripts/core/Constant';
import { moon } from './../../../../scripts/core/GlobalInfo';
import { FourSeasonsTable } from './FourSeasonsTable';
import { FourSeasonsLobby } from './FourSeasonsLobby';
import { GameScene } from './../../../../scripts/common/GameScene';
import { NodeUtils } from '../../../../scripts/core/NodeUtils';
const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsGame extends GameScene {

  @property(FourSeasonsLobby)
  lobby: FourSeasonsLobby = null;

  @property(FourSeasonsTable)
  table: FourSeasonsTable = null;

  @property(cc.Node)
  notify: cc.Node = null;
  
  onEnter() {
    super.onEnter();
    cc.log('FourSeasonsGame::onEnter');
    moon.cardPool.setCardDeck(DECK_ID.SIMPLIFIED_1);
    moon.cardPool.setCardScale(0.6);
    this.onResize();
  }

  onLeave() {

  }

  onEnterRoom() {
    cc.log('FourSeasons::onEnterRoom');
    moon.header.hide();
    this.lobby.node.runAction(
      cc.sequence(
        cc.fadeOut(0.3),
        cc.callFunc(() => {
          this.lobby.node.active = false;
        })
      )
    )

    this.table.node.opacity = 0;
    this.table.node.active = true;
    this.table.node.runAction(
      cc.sequence(
        cc.delayTime(0.1),
        cc.fadeIn(0.2),
      )
    )
  }

  onLeaveRoom() {
    cc.log('FourSeasons::onLeaveRoom');
    moon.header.show();

    this.table.node.runAction(
      cc.sequence(
        cc.fadeOut(0.3),
        cc.callFunc(() => {
          this.table.node.active = false;
        })
      )
    )

    this.lobby.node.opacity = 0;
    this.lobby.node.active = true;
    this.lobby.node.runAction(
      cc.sequence(
        cc.delayTime(0.1),
        cc.fadeIn(0.2),
      )
    )

    moon.dialog.hideWaiting();
  }

  onLanguageChange() {
    super.onLanguageChange();
    this.lobby.onLanguageChange();
    this.table.onLanguageChange();
  }

  updateRoomInfoList(roomInfoList) {
    this.lobby.updateRoomInfoList(roomInfoList);
  }

  showNotify(msg) {
    this.notify.stopAllActions();
    this.notify.active = true;
    this.notify.opacity = 0;
    NodeUtils.setLabel(this.notify, 'notify', msg);
    this.notify.runAction(
      cc.sequence(
        cc.fadeIn(0.1),
        cc.delayTime(2),
        cc.fadeOut(0.2)
      )
    );
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let ratioToScale = Math.min(designRatio / ratio, 1);
    this.lobby.onResize(ratioToScale);
    this.table.onResize(ratioToScale);
  }

}