import {RoomInfo} from "../../../../scripts/model/Room";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import { moon, GlobalInfo } from "../../../../scripts/core/GlobalInfo";
import { GAME_ID, RESOURCE_BLOCK } from "../../../../scripts/core/Constant";
import { MiniPokerGame } from "./MiniPokerGame";
import { GrayShader } from "../../../../shader/GrayShader";
import { NormalShader } from "../../../../shader/NormalShader";

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerLobby extends cc.Component {

  @property([cc.Node])
  rooms: cc.Node[] = [];

  @property(cc.Node)
  roomList: cc.Node = null ;

  @property(cc.Node)
  bgSke: cc.Node = null;


  inDelayRoomClick = false;

  updateRoomInfoList(roomInfoList: Array<RoomInfo>) {
    moon.res.preloadFolders(
      [`${moon.service.getGameInfoById(GAME_ID.POKER).hallAsset}`,
      `${moon.service.getGameInfoById(GAME_ID.POKER).hallEffect}`],
      () => {
      },
      RESOURCE_BLOCK.GAME
    ).then(() => {
                this.setRoomBtn(roomInfoList);
    })
  }

  setRoomBtn(roomInfoList: Array<RoomInfo>) {
    let i = 0;
    for (let roomInfo of roomInfoList) {

      let room = this.rooms[i];
      NodeUtils.setLocaleLabels(room, {
        'bet': ['ante:', {params: [roomInfo.bet]}],
        'minBet': ['allowed:', {params: [roomInfo.minBet]}],
      });
      let roomBtn = room.getComponent(cc.Button) as cc.Button;
      roomBtn.clickEvents[0].customEventData = roomInfo.bet;
      NodeUtils.setGameSprite(room, 'roomName', 'textRoom' + i);
  

      let bgAnim: cc.Node = NodeUtils.findByName(room, 'bgAnim');
      let bgImage: cc.Node = NodeUtils.findByName(room, 'bgImage');
      let nameNode = NodeUtils.findByName(room, 'roomName');
      let betNode = NodeUtils.findByName(room, 'minBet');
      let nameSprite: cc.Sprite = nameNode.getComponent(cc.Sprite);
      let bgSprite: cc.Sprite = bgImage.getComponent(cc.Sprite);


      if (roomInfo.minBet > GlobalInfo.me.money) {
        bgAnim.active = false;
        bgImage.active = true;

        NodeUtils.applyShader(nameSprite, GrayShader);
        NodeUtils.applyShader(bgSprite, GrayShader);
        betNode.color = cc.color().fromHEX('#ffffff');

      } else {
        bgAnim.active = true;
        bgImage.active = false;

        NodeUtils.applyShader(nameSprite, NormalShader);
        NodeUtils.applyShader(bgSprite, NormalShader);

        this.setDragonBoneAnim(room,i)
        
      }

      i++;
    }


    for (i; i < this.rooms.length; i++){
       this.rooms[i].active = false;
    }
  
  }

   
  setDragonBoneAnim(room: cc.Node, i: number) {
    let bgAnim = room.getChildByName('bgAnim')
    bgAnim.active = true;
    let dbCom = bgAnim.getComponent(dragonBones.ArmatureDisplay)
    dbCom.dragonAsset = moon.res.getSkelecton('nn_hall_icon_da_' + i + '_ske', RESOURCE_BLOCK.GAME);
    dbCom.dragonAtlasAsset = moon.res.getSpriteFrame('nn_hall_icon_da_' + i + '_tex', RESOURCE_BLOCK.GAME);
    dbCom.armatureName = 'nn_hall_icon_da_' + i ;
    dbCom.playAnimation('action', 0);
  }



  onResize() {

    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);

    this.roomList.scale = uiRatio;

    // this.bgSke.scale = uiRatio;
    // this.bgSke.y = cc.winSize.height/2;
    // this.bgSke.x = -cc.winSize.width/2;


  }
  onRoomClick(evt, value){

    if (!this.inDelayRoomClick) {    
        this.inDelayRoomClick = true
        let numValue = parseInt(value)
        cc.log('choose room',numValue)
        moon.dialog.showWaiting()
        moon.gameSocket.playMiniPoker(GAME_ID.POKER,numValue)

        moon.timer.scheduleOnce(()=>{
            this.inDelayRoomClick = false
        },2)
    }

  }

 

}