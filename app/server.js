const express = require('express');
const path = require("path");
const fs = require("fs");
const app = express();
const port = 7006;

app.set('view engine', 'pug');
app.use('/tool', express.static(path.join(__dirname, 'tool')));
app.use('/play', express.static(path.join(__dirname, 'public')));

function foundFilePath(directory, targetName) {
  let files = fs.readdirSync(directory);
  let foundFileName = '';
  for (let fileName of files) {
    if (fileName.indexOf(targetName) > -1) {
      foundFileName = fileName;
      break;
    }
  }
  return foundFileName;
}

app.get('/tool', function (req, res) {
  res.render('tool/merchant', {
    sidebar: 'merchant'
  });
});

app.get('/tool/add-merchant', function (req, res) {
  res.render('tool/add-merchant', {
    sidebar: 'merchant'
  });
});

app.get('/tool/connection', function (req, res) {
  res.render('tool/connection', {
    sidebar: 'connection'
  });
});

app.get('/tool/edit-connection', function (req, res) {
  res.render('tool/edit-connection', {
    sidebar: 'connection'
  });
});

app.get('/tool/currency', function (req, res) {
  res.render('tool/currency', {
    sidebar: 'currency'
  });
});

app.get('/tool/add-currency', function (req, res) {
  res.render('tool/add-currency', {
    sidebar: 'currency'
  });
});

app.get('/play', function (req, res) {
  let merchantId = req.query['mc'] || 1;
  let merchantPath = path.join(__dirname, 'public/mc/' + merchantId);
  let srcPath = path.join(__dirname, 'public/mc/' + merchantId + '/src');
  let publicPath = path.join(__dirname, 'public');
  let mainName = foundFilePath(merchantPath, 'main');
  let settingName = foundFilePath(srcPath, 'settings');
  let mobileCssName = foundFilePath(publicPath, 'style-mobile');
  let mainCssName = foundFilePath(publicPath, 'style-main');
  let splashName = foundFilePath(publicPath, 'splash');

  res.render('index', {
    merchantId: merchantId,
    mainName: mainName,
    settingName: settingName,
    styleMain: mainCssName,
    styleMobile: mobileCssName,
    splash: splashName
  });
});

app.get('/', function (req, res) {
  res.redirect('/play')
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));