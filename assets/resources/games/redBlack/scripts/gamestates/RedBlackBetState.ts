
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";

import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { RedBlackGame } from "../RedBlackGame";
import { RedBlackRoom } from "../model/RedBlackRoom";
import { RESOURCE_BLOCK } from "../../../../../scripts/core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackBetState extends GameState implements IGameState {

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: RedBlackGame, data: any) {
    game.setRoom(<RedBlackRoom>GlobalInfo.room);
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;

    this.playStartAnim();
    
  
    let state = this.startAnim.playAnimation(moon.locale.getCurrentLanguage(), 1);

    let i = 13;
    moon.timer.runCountdown( 13, () => {
      game.table.countDown.getComponent(cc.Label).string = i.toString() ;
      console.log(i);
      i--;
    })

  }

  playStartAnim() {
 
    this.startAnim.node.active = true;
    this.startAnim.dragonAsset = moon.res.getSkelecton('hh_startBet_ske', RESOURCE_BLOCK.GAME);
    this.startAnim.dragonAtlasAsset = moon.res.getSpriteFrame('hh_startBet_tex', RESOURCE_BLOCK.GAME);
    this.startAnim.armatureName = 'hh_startBet' ;
    this.startAnim.playAnimation('action', 1);
  }

  onUpdate(game: RedBlackGame, updateData: UpdateData) {
  }

  onLeave(game: RedBlackGame, data: any) {
  }
}