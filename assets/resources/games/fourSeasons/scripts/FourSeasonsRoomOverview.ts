import { BetType } from './FourSeasonsConstants';
import { GAME_ID } from './../../../../scripts/core/Constant';
import { FourSeasonsService } from './service/FourSeasonsService';
import { CountdownInfo } from './../../../../scripts/model/Room';
import { CountdownTask, TimerTask } from './../../../../scripts/core/TimerComponent';
import { moon } from './../../../../scripts/core/GlobalInfo';
import { FourSeasonsRoom } from './model/FourSeasonsRoom';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsRoomOverview extends cc.Component {
  _fourSeasonsRoom: FourSeasonsRoom = null;

  _roomImageId = null;

  @property(cc.Node)
  spadeHistory: cc.Node = null;

  @property(cc.Node)
  heartHistory: cc.Node = null;

  @property(cc.Node)
  clubHistory: cc.Node = null;

  @property(cc.Node)
  diamondHistory: cc.Node = null;

  @property(cc.Prefab)
  historyNodePrefab: cc.Prefab = null;

  @property(cc.SpriteFrame)
  gameRightSprite: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  gameWrongSprite: cc.SpriteFrame = null;

  _countdownTask: TimerTask = null;
  _historyContainers = {};

  // TODO: Remove roomImageId later
  init(fourSeasonsRoom: FourSeasonsRoom, roomImageId) {
    this._fourSeasonsRoom = fourSeasonsRoom;
    const { minBet, maxBet, countdownInfo } = this._fourSeasonsRoom;
    this._roomImageId = roomImageId;
    this._historyContainers[BetType.SPADE] = this.spadeHistory;
    this._historyContainers[BetType.CLUB] = this.clubHistory;
    this._historyContainers[BetType.HEART] = this.heartHistory;
    this._historyContainers[BetType.DIAMOND] = this.diamondHistory;
    this.clear();
    this.initHistory(this._fourSeasonsRoom.historyList);

    NodeUtils.setLabel(this.node, 'bet_label', `${minBet}-${maxBet}`);

    const FSService = (moon.service.getCurrentService() as FourSeasonsService);
    NodeUtils.setLabel(this.node, 'status_type', `${FSService.getStatus(countdownInfo.type)}...`);

    this.runCountdownTask(countdownInfo);
  }

  initHistory(historyList) {
    this.initPotHistory(BetType.SPADE, historyList);
    this.initPotHistory(BetType.CLUB, historyList);
    this.initPotHistory(BetType.HEART, historyList);
    this.initPotHistory(BetType.DIAMOND, historyList);
  }

  initPotHistory(potType, historyList) {
    let potHistory: number[] = historyList[potType];
    let historyContainer: cc.Node = this._historyContainers[potType];
    for (let i = 0; i < potHistory.length; i++) {
      let historyNode = cc.instantiate(this.historyNodePrefab);
      let historySpriteFrame = historyNode.getComponent(cc.Sprite);

      if (potHistory[i] == 1) {
        historySpriteFrame.spriteFrame = this.gameRightSprite;
      } else {
        historySpriteFrame.spriteFrame = this.gameWrongSprite;
      }
      historyContainer.addChild(historyNode);
    }

    historyContainer.getComponent(cc.Layout).updateLayout();
  }

  playGame() {
    moon.gameSocket.playFourSeasons(GAME_ID.FOUR_SEASONS, this._fourSeasonsRoom.roomId);
  }

  runCountdownTask(countdownInfo: CountdownInfo) {
    this._countdownTask = moon.timer.runCountdown(countdownInfo.timeout, (val) => {
      NodeUtils.setLabel(this.node, 'status_time', `${val}s`);
    })
  }

  clearCountdownTask() {
    moon.timer.removeTask(this._countdownTask);
  }

  onlanguageChange() {
    NodeUtils.setSprites(this.node, {
      "room_name_sprite": `text_roomtxt_${this._roomImageId}`
    }, false);
  }

  clear() {
    // this.roomHistory.destroyAllChildren();
    this.spadeHistory.destroyAllChildren();
    this.clubHistory.destroyAllChildren();
    this.heartHistory.destroyAllChildren();
    this.diamondHistory.destroyAllChildren();
    this.clearCountdownTask();
  }
}