PROJECT_DIR=~/workspace/moon-client
BUILD_VERSION=1.0.2
BUILD_NUMBER=43

cd ${PROJECT_DIR}/tools
python prebuild.py -env ios.production
cd ${PROJECT_DIR}

/Applications/CocosCreator.app/Contents/MacOS/CocosCreator --path . --build "platform=ios;startScene=cddde544-7cac-43fd-a22e-698bdb50f3ea;title=moon;debug=false;orientation={'landscapeLeft': true, 'landscapeRight': true};template=default;md5Cache=false;embedWebDebugger=false;encryptJs=true;"

cd ${PROJECT_DIR}/tools
python postbuild.py
cd ${PROJECT_DIR}

cd build/jsb-default/frameworks/runtime-src/proj.ios_mac/
xcrun agvtool new-version -all $BUILD_NUMBER
cd ${PROJECT_DIR}

xcodebuild build -workspace build/jsb-default/frameworks/runtime-src/proj.ios_mac/moon.xcworkspace -scheme moon-mobile -destination generic/platform=iOS build

xcodebuild -workspace build/jsb-default/frameworks/runtime-src/proj.ios_mac/moon.xcworkspace -scheme moon-mobile -sdk iphoneos -configuration AppStoreDistribution archive -archivePath $PWD/build/moon.xcarchive

now=$(date +"%Y-%m-%d")

xcodebuild -exportArchive -archivePath $PWD/build/moon.xcarchive -exportOptionsPlist ExportOptions.plist -exportPath ~/workspace/moon_build/moon-mobile_${now}/

cp -r ~/workspace/moon_build/moon-mobile_${now}/moon-mobile.ipa ~/workspace/moon-client-prod/ipa