import {SceneComponent} from "../common/SceneComponent";
import {HttpClient} from "../core/HttpClient";
import {Config} from "../Config";
import {NodeUtils} from "../core/NodeUtils";
import {moon} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class AboutDialog extends SceneComponent {

  @property(cc.Label)
  info: cc.Label = null;

  @property(cc.Label)
  version: cc.Label = null;

  onEnter() {
    super.onEnter();
  }

  onLanguageChange() {
    this.info.string = moon.locale.get('aboutInfo');
    this.version.string = `${Config.version}-${Config.env}`;

    NodeUtils.setLocaleLabel(this.node, 'term_lbl', 'term');
    NodeUtils.setLocaleLabel(this.node, 'privacy_lbl', 'privacy');
  }

  goToTerm() {
    moon.http.openURL(Config.termUrl + "?lang=" + moon.locale.getCurrentLanguage());
  }

  goToPrivacy() {
    moon.http.openURL(Config.privacyUrl + "?lang=" + moon.locale.getCurrentLanguage());
  }
}