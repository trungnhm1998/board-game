import { Card } from './../../../../scripts/core/Card';

import { TimerTask } from '../../../../scripts/core/TimerComponent';
import { moon, GlobalInfo } from '../../../../scripts/core/GlobalInfo';
import { RedBlackRoom } from './model/RedBlackRoom';
import { RedBlackPlayerInfo } from './model/RedBlackPlayerInfo';
import { RedBlackHistory } from './RedBlackHistory';
import { NodeUtils } from '../../../../scripts/core/NodeUtils';
import { COUNTDOWN_TYPE, RED_BLACK_PLACE_TYPE } from '../../../../scripts/core/Constant';
import { RedBlackBetPot } from './RedBlackBetPot';
import { RedBlackPlayer } from './RedBlackPlayer';

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackTable extends cc.Component {
 
  @property(cc.Node)
  dropdown: cc.Node = null;

  @property(cc.Label)
  matchID: cc.Label = null;

  @property(cc.Label)
  countDown: cc.Label = null;
  
  @property(cc.Label)
  playerNum: cc.Label = null;

  @property(cc.Label)
  session: cc.Label = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  betInfo: cc.Label = null;

  @property([cc.Node])
  betNodes: cc.Node[] = [];

  @property([RedBlackBetPot])
  potNodes: RedBlackBetPot[] = [];

  @property(cc.SpriteAtlas)
  chipAtlas: cc.SpriteAtlas = null;



  @property(RedBlackHistory)
  historyPanel: RedBlackHistory = null;

  @property(cc.Node)
  chipHighlight: cc.Node = null;

  @property(cc.Node)
  redCardContent: cc.Node = null;

  @property(cc.Node)
  blackCardContent: cc.Node = null;

  @property(cc.Node)
  dealerPanel: cc.Node = null;

  

  @property(RedBlackPlayer)
  myPlayer: RedBlackPlayer = null;



  localeAtlas: cc.SpriteAtlas = null;
  currentBet = 0;
  redCardNodeList: Array<cc.Node> =[]
  redCardType = 0;
  blackCardNodeList: Array<cc.Node> =[]
  blackCardType = 0;
  winPlaceBetType = 0;
  

  onEnable() {
    
  }


  clearTable() {
    this.matchID.string= '';
    this.playerNum.string= '';
    this.potNodes
    //clear Timer CountDown
    //reset currentBet
    //reset, clear betpot
    moon.cardPool.returnAllCard();
  }

  setRoom(room: RedBlackRoom) {
    this.matchID.string = room.matchId || '';
    this.playerNum.string = room.playerNum.toString() || '0';
    this.setBetList(room.betList);
    this.setPlaceBetInfoList(room.placeBetTypeList);
    this.updateCurrentTable();
  }


  setPlaceBetInfoList(placeBetInfoList: any) {
    let i = 0;
    for (let pot of this.potNodes) {
        pot.setPlaceBetType(placeBetInfoList[i].type)
        pot.setBetMoneyTotal(placeBetInfoList[i].betMoneyTotal)
        i++;
    }
  }

  getPotByPlaceBetType(placeBetType) {
    console.log(this.potNodes)
    for (let pot of this.potNodes) {
      if (pot.placeBetType == placeBetType) {
        return pot
      }
    }
  }

  setMePlayer(player: RedBlackPlayerInfo) {

  }


  updateCurrentTable() {
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
 
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_NOT_START_GAME:
        break;
      case COUNTDOWN_TYPE.STATE_READY_START_GAME:
        break;
      case COUNTDOWN_TYPE.STATE_START_GAME:
        break;
      case COUNTDOWN_TYPE.STATE_BET:
     
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CARD:
      case COUNTDOWN_TYPE.STATE_OPEN_CARD:
      case COUNTDOWN_TYPE.STATE_RESULT:
        break;
    }
  }

  setPlayer(player: RedBlackPlayerInfo) {

  }

  updateHistory() {
    this.historyPanel.renderColorHistory([])
  }


  leaveGame() {
   // moon.audio.playEffectInGame("btn_click");
    moon.gameSocket.leaveBoard();
    this.toggleDropdown();
  }

  toggleDropdown() {
    this.dropdown.active = !this.dropdown.active;
    moon.audio.playEffectInGame("btn_click");
  }

  showHelp() {
    moon.audio.playEffectInGame("btn_click");
    moon.dialog.showWaiting();
    moon.dialog.showHelp(GlobalInfo.room.gameId);
    this.toggleDropdown();
    moon.dialog.hideWaiting();
  }



  showSetting() {
    moon.audio.playEffectInGame("btn_click");
    this.toggleDropdown();
    moon.dialog.showSettings();
  }

  showRecord() {
    moon.audio.playEffectInGame("btn_click");
    this.toggleDropdown();
    moon.gameSocket.getPlayHistory(GlobalInfo.room.gameId);
  }

  clearBetList() {
    for (let betNode of this.betNodes) {
      betNode.active = false;
      NodeUtils.hideByName(betNode, 'chip_lbl');
    }
  }

  setBetList(betList) {
    this.clearBetList();
    let i = 0;
    if (!this.currentBet || betList.indexOf(this.currentBet) < 0) {
      this.currentBet = betList[0];
    }
    for (let betMoney of betList) {
      let btnNode = this.betNodes[i];
      btnNode.active = true;
      let sprite = btnNode.getComponent(cc.Sprite);
      let sp = this.chipAtlas.getSpriteFrame('b' + betMoney);
      if (sp) {
        sprite.spriteFrame = sp;
      } else {
        sprite.spriteFrame = this.chipAtlas.getSpriteFrame('be');
        NodeUtils.showByName(btnNode, 'chip_lbl');
        NodeUtils.setLabel(btnNode, 'chip_lbl', String(betMoney));
      }
      let btn: cc.Button = btnNode.getComponent(cc.Button);
      btn.clickEvents[0].customEventData = betMoney;
      this.betNodes.push(btnNode);
      if (this.currentBet == betMoney) {
        this.onBetClick({target: btnNode}, betMoney);
      }
      i++;
    }
  }

  onBetClick(evt, betMoney) {
    this.showChipHighlight(evt.target);
    this.currentBet = +betMoney;
  }

  showChipHighlight(node) {
    let worldPos = node.convertToWorldSpaceAR(cc.v2());
    this.chipHighlight.position = this.chipHighlight.parent.convertToNodeSpaceAR(worldPos);
    this.chipHighlight.active = true;
  }
  
  setCardList(placeType, cardList) {
        if (placeType == 1) {
          this.redCardNodeList = cardList;
        } else {
          this.blackCardNodeList = cardList;
        }
  }

  // storeMyPendingBet(placeBetType, money) {

  // }

  //place type to check whether get deal card pos for red or black, 1 is red, 2 is black
  getDealCardPostion(): Array<cc.Vec2> {
    let cardWidth = moon.cardPool.cardWidth * 0.8;
    let cardPositions: Array<cc.Vec2> = [];
    let width = cardWidth * 3;
    let space = 20;
    for (let i = 0; i < 3; i++) {
      cardPositions.push(cc.v2(((i * cardWidth) + cardWidth / 2 + i*space ) - width / 2, 0));
    }
    return cardPositions;
  }


  divideMoney(money, chipList = [100, 50, 20, 10, 5, 2], smallest = 2) {
    let nList = [];
    let remain = money;
    let ret = {};
    for (let chip of chipList) {
      let nChip = Math.floor(remain / chip);
      remain -= nChip * chip;
      nList.push(
        nChip
      )
    }
    let i = 0;
    for (let chip of chipList) {
      ret[chip] = nList[i++];
    }
    ret[smallest] = Math.floor(remain / smallest);
    return ret;
  }

  generateChipValueList(money) {
    let chipValues;
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
    let betList = room.betList
    if (betList) {
      let chipList = [].concat(betList);
      let smallest = chipList.shift();
      chipList = chipList.reverse();
      chipValues = this.divideMoney(money, chipList, smallest);
    } else {
      chipValues = {
        [money]: 1
      };
    }
    return chipValues;
  }



  setupChipToThrow(placeBetType, money, isMyChip = false, isUseAnimation = true) {
    cc.log(this.dealerPanel.convertToNodeSpaceAR(cc.v2()))
    let pot = this.getPotByPlaceBetType(placeBetType)
    let startPos;
    if (isMyChip) {
     startPos = pot.betArea.convertToNodeSpaceAR(
        this.myPlayer.avatar.node.convertToWorldSpaceAR(cc.v2())
      );
    } else {
      startPos = pot.betArea.convertToNodeSpaceAR(
        this.playerNum.node.convertToWorldSpaceAR(cc.v2())
      );
    }
  
    let chipValues = this.generateChipValueList(money)
    pot.initChipMaterial(this.chipAtlas)
    pot.throwChips(startPos, chipValues, isUseAnimation)

  }

}
