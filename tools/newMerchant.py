# -*- coding: utf-8 -*-

import os
import shutil
import time
import sys
import glob
import fnmatch
import re
from distutils.dir_util import copy_tree

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == "-":  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

args = getopts(sys.argv)

merchantId = args["-merchant"]

resFolder = "../assets/resources/"
merchantFolder = "../../moon-merchants/" + merchantId


# Copy Resources

def copyResources():
  print("Copying " + resFolder + " to " + merchantFolder)
  copy_tree(resFolder, merchantFolder)

# Remove unused resources
def removeUnused():
  removeDirs = ['prefabs', 'scripts']
  removeRegex = ['.*ts', 'project\_.*', '.*prefab.*']
  for root, dirs, files in os.walk(merchantFolder):
    for name in files:
      for regex in removeRegex:
        if re.search("^" + regex + "$", name):
          filePath = os.path.join(root, name)
          print("Removing " +  filePath)
          os.remove(filePath)

    for name in dirs:
      if name in removeDirs:
        folderPath = os.path.join(root, name)
        print("Removing " +  folderPath)
        shutil.rmtree(folderPath)

# Flow
copyResources()
removeUnused()
