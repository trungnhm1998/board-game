import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {PlayHistory} from "../model/HistoryInfo";
import {moon} from "../core/GlobalInfo";
import {Config} from "../Config";

const {ccclass, property} = cc._decorator;

@ccclass
export class RecordDialog extends SceneComponent {

  @property(cc.ScrollView)
  container: cc.ScrollView = null;

  @property(cc.Node)
  header: cc.Node = null;

  @property(cc.Node)
  sampleRow: cc.Node = null;

  @property(cc.Node)
  sampleText: cc.Node = null;

  @property(cc.Node)
  sampleHeaderText: cc.Node = null;

  @property(cc.Node)
  empty: cc.Node = null;

  rowWidth = 985;

  currentIndex = 0;
  isLast = false;
  fetching = false;

  historyList;

  odd = false;

  onEnter() {
    super.onEnter();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'empty_lbl', 'noGameRecord');
    NodeUtils.setSprites(this.node, {
      'title_sprite': 'game_record_title',
    });
    this.setHeader(
      [moon.locale.get('ordinalNumber'), moon.locale.get('roundId'), moon.locale.get('room'), moon.locale.get('bet'),
        moon.locale.get('profit'), moon.locale.get('endTime')],
      [0.75, 1.25, 1, 0.5, 1, 1.25],
      this.header,
      this.rowWidth
    );
  }

  onLeave() {
    super.onLeave();
  }

  closeDialog() {
    moon.audio.playAudio('game_sound_btn_click');
    this.clear();
    super.closeDialog();
  }

  setHeader(headers, flexInfo, container, rowWidth) {
    let totalFlex = 0;
    for (let flex of flexInfo) {
      totalFlex += flex;
    }

    container.removeAllChildren();
    let i = 0;
    for (let header of headers) {
      this.addHeaderText(header, flexInfo[i] / totalFlex, container, rowWidth);
      i++;
    }
  }

  addRows(rows, flexInfo, cellInfo, matchIds, colors, container, rowWidth, detailFuncName = '') {
    let totalFlex = 0;
    for (let flex of flexInfo) {
      totalFlex += flex;
    }

    this.checkLast(rows);
    for (let i = 0; i < rows.length; i++) {
      let row = rows[i];
      let matchId = matchIds[i];
      setTimeout(() => {
        this.addRow(row, flexInfo, totalFlex, cellInfo, colors, container, matchId, rowWidth, detailFuncName);
      }, 100 * i);
    }
  }

  addHeaderText(text, percent, containerNode, rowWidth) {
    let width = percent * rowWidth;
    let headerNode = cc.instantiate(this.sampleHeaderText);
    headerNode.active = true;
    headerNode.width = width;
    let headerText: cc.Label = headerNode.getComponent(cc.Label);
    headerText.string = text;
    containerNode.addChild(headerNode);
  }

  addRow(texts, flexInfo, totalFlex, cellInfo, colors, containerNode, matchId, rowWidth, detailFuncName) {
    let i = 0;
    let row = cc.instantiate(this.sampleRow);
    row.active = true;
    let rowContent: cc.Node = NodeUtils.findByName(row, 'rowContent');
    for (let text of texts) {
      let textNode = cc.instantiate(this.sampleText);
      let width = (flexInfo[i] / totalFlex) * rowWidth;
      textNode.active = true;
      textNode.width = width;
      textNode.color = cc.color().fromHEX(colors[i]);
      if (cellInfo[i] == 'text') {
        let rowText: cc.Label = textNode.getComponent(cc.Label);
        rowText.string = text;
        rowContent.addChild(textNode);
      } else if (cellInfo[i] == 'date') {
        let rowText: cc.Label = textNode.getComponent(cc.Label);
        rowText.string = text;
        rowContent.addChild(textNode);
      } else if (cellInfo[i] == 'money') {
        let rowText: cc.Label = textNode.getComponent(cc.Label);
        if (typeof text == 'string') {
          rowText.string = text;
        } else {
          rowText.string = moon.string.formatMoney(text);
        }
        rowText.overflow = cc.Label.Overflow.SHRINK;
        rowContent.addChild(textNode);
      }
      i++;
    }

    let bg = NodeUtils.findByName(row, 'bg');
    if (bg) {
      bg.opacity = this.odd ? 0 : 255;
      bg.width = containerNode.width;
    }
    this.odd = !this.odd;
    row.x = -containerNode.width / 2;
    row.opacity = 0;
    containerNode.addChild(row);
    row.runAction(
      cc.fadeIn(0.2)
    );
  }

  setHistory(historyList: PlayHistory[]) {
    this.clear();
    this.addHistory(historyList);
  }

  addHistory(historyList: PlayHistory[]) {
    let rows = [];
    let matchIds = [];
    this.historyList = this.historyList.concat(historyList);
    let i = 1;
    for (let historyItem of historyList) {
      rows.push([
        '' + i++,
        historyItem.matchId,
        this.getRoom(historyItem.roomName),
        historyItem.betMoney,
        historyItem.winMoney,
        moon.string.formatDate(historyItem.createdDate)
      ]);
      matchIds.push(historyItem.matchId);
    }

    this.addRows(
      rows,
      [0.75, 1.25, 1, 0.5, 1, 1.25],
      ['text', 'text', 'text', 'money', 'money', 'date'],
      matchIds,
      ['#785034', '#518e01', '#785034', '#518e01', '#518e01', '#785034'],
      this.container.content,
      this.rowWidth
    );

    if (this.historyList.length == 0) {
      this.showEmpty();
    } else {
      this.hideEmpty();
    }
  }

  checkLast(items) {
    this.isLast = items.length < Config.historyRowsPerFetch;
  }

  clear() {
    this.container.content.removeAllChildren();
    this.isLast = false;
    this.fetching = false;
    this.currentIndex = 0;
    this.odd = false;
    this.historyList = [];
  }

  private showEmpty() {
    this.empty.active = true;
  }

  private hideEmpty() {
    this.empty.active = false;
  }

  private getRoom(roomName) {
    return moon.locale.get('rooms')['' + roomName] || '';
  }
}

