export class ThreeFacePlayerInfo {
  userId: number;
  money: number;
  betMoney: number;
  winMoney: number;
  resultCard: {
    point: number,
    cardList: number[],
    cardType: number,
    bonusFactor: number
  };
  betList: number[];
  seat: number;
  displayName: string;
  avatar: string;
  betFactor: number;
  isChoose: boolean;
}