import {MiniPokerBetState} from './gamestates/MiniPokerBetState';
import {GameScene} from '../../../../scripts/common/GameScene';
import {MiniPokerBetLayer} from './MiniPokerBetLayer';
import {MiniPokerPlayer} from './MiniPokerPlayer';
import {MiniPokerChessLayer} from './MiniPokerChessLayer';
import {NodeUtils} from '../../../../scripts/core/NodeUtils';
import {GlobalInfo, moon} from '../../../../scripts/core/GlobalInfo';
import {MiniPokerPlayerInfo} from './model/MiniPokerPlayerInfo';
import {MiniPokerRoom} from './model/MiniPokerRoom';
import {COUNTDOWN_TYPE, DECK_ID, GAME_STATE, MINIPOKER_AUDIO, GAME_ID, RESOURCE_BLOCK, MINIPOKER_UPDATE_TYPE} from '../../../../scripts/core/Constant';
import {MiniPokerRoomService} from './service/MiniPokerRoomService';
import {RoomInfo} from '../../../../scripts/model/Room';
import {MiniPokerLobby} from './MiniPokerLobby';
import {MiniPokerReadyState} from './gamestates/MiniPokerReadyState';
import {Card} from '../../../../scripts/core/Card';
import { UpdateData } from '../../../../scripts/common/GameState';

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerGame extends GameScene {
  @property(cc.Label)
  matchID: cc.Label = null;

  @property(cc.Label)
  session: cc.Label = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Node)
  dropdown: cc.Node = null;

  @property(cc.Node)
  statsPanel: cc.Node = null;

  @property(cc.Node)
  firstArrow: cc.Node = null;

  @property(cc.Node)
  trustDialog: cc.Node = null;

  @property([MiniPokerPlayer])
  players: MiniPokerPlayer[] = [];

  @property([cc.Label])
  chessCounts: cc.Label[] = [];

  @property(MiniPokerBetLayer)
  betLayer: MiniPokerBetLayer = null;

  @property(MiniPokerChessLayer)
  chessLayer: MiniPokerChessLayer = null;

  @property(cc.Node)
  waiting: cc.Node = null;

  @property(cc.Node)
  stateNode: cc.Node = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Label)
  betInfo: cc.Label = null;

  @property(cc.Node)
  notify: cc.Node = null;

  @property(cc.Node)
  trustBtn: cc.Node = null;

  @property([cc.Label])
  trustTipsLabels: cc.Label[] = [];

  @property(cc.Node)
  gameNode: cc.Node = null;

  @property(MiniPokerLobby)
  lobby: MiniPokerLobby = null;

  @property([cc.Node])
  trustTipsBetToogle: cc.Node[] = [];

  @property(cc.Node)
  cardTypedialog: cc.Node = null;

  @property(cc.Node)
  scrollViewContent: cc.Node = null;

  @property(cc.Node)
  miniCardSample: cc.Node = null;

  @property(cc.Node)
  rowSample: cc.Node = null;

  @property(cc.Node)
  lblSample: cc.Node = null;

  isShownDropdown = false;
  isTrusting = false;
  trustingFactor = 1;
  leavedPlayersToRemove: Array<Number> = [];
  isEndGameAnimPlaying: Boolean = false;

  onEnter() {
    super.onEnter();
    this.setLabelLanguage();
    this.setLocaleSpiteFrame();
    moon.audio.stopAllAudio();
    moon.cardPool.setCardDeck(DECK_ID.SIMPLIFIED_1);
    moon.cardPool.setCardScale(0.7);
    moon.header.show();
    if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
      this.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
      moon.audio.playMusicInGame(MINIPOKER_AUDIO.LOBBY_BGM, true);
    }
    this.onResize();
  }

  onEnterRoom(isUseAnimate = true, isReturnGame = false) {
    super.onEnterRoom();
    if (isReturnGame) {
      moon.gameSocket.getRoomInfoList(GlobalInfo.room.gameId);
    }

    this.getIntoGameRoom(isUseAnimate);

    this.hideTrustDialog();

    this.hidePlayers();
    
    this.hideBetInfo();
    this.showWaiting();
    this.renderCardTypeDialog();

    // ChessPool.getInstance().returnAllChesses();
  }

  showHelp() {
    moon.dialog.showHelp(GlobalInfo.room.gameId);
    this.toggleDropdown();
  }

  showSetting() {
    this.toggleDropdown();
    moon.dialog.showSettings();
  }

  showRecord() {
    this.toggleDropdown();
    moon.gameSocket.getPlayHistory(GlobalInfo.room.gameId);
  }

  updateRoomInfoList(roomInfoList: Array<RoomInfo>) {
    this.lobby.updateRoomInfoList(roomInfoList);
  }

  setLocaleSpiteFrame() {
    this.ui.getChildByName('trustBtn').getChildByName('trust').getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('trust');
    this.ui.getChildByName('trustBtn').getChildByName('cancel').getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('cancel');
  }

  setLabelLanguage() {
    NodeUtils.setLocaleLabels(this.ui, {
      'math_lbl': ['id', {}],
      'roomBet_lbl': ['ante', {}],
      'money_lbl': ['money', {}],
      'betInfo_lbl': ['pleaseSelectBet', {}],
      'wait_lbl': ['waitOtherPlayers', {}],
      'stats_1': ['pleaseSelectMoneyYouCanPayAsDealer', {}],
      'quit_lbl': ['quit', {}],
      'setting_lbl': ['setting', {}],
      'record_lbl': ['record', {}],
      'help_lbl': ['help', {}],
      'cardType_lbl': ['cardType', {}],
      'trustDiallog_title': ['trustTips', {}],
      'ok_lbl': ['ok', {}],
      'cancel_lbl': ['cancel', {}]
    });

    for (let i = 0; i < this.trustTipsLabels.length; i++) {
      this.trustTipsLabels[i].string = moon.locale.get('trustTipsDialog')[i];
    }
    cc.log('test brach');
  }

  getIntoGameRoom(isUseAnimate = true) {
    if (isUseAnimate) {
      cc.log('get into game rome mini poker');
      moon.header.hide();
      moon.dialog.hideWaiting();
      moon.audio.stopAllAudio();
      this.lobby.node.runAction(
        cc.sequence(
          cc.fadeOut(0.3),
          cc.callFunc(() => {
            this.lobby.node.active = false;
          })
        )
      );
      this.gameNode.opacity = 0;
      this.gameNode.active = true;
      this.gameNode.runAction(
        cc.sequence(
          cc.delayTime(0.1),
          cc.fadeIn(0.2),
          cc.callFunc(() => {
            moon.audio.playMusicInGame(MINIPOKER_AUDIO.GAME_BGM, true);
          })
        )
      );
    } else {
      moon.header.hide();
      moon.dialog.hideWaiting();
      moon.audio.stopAllAudio();
      this.lobby.node.active = false;
      this.gameNode.active = true;
      this.gameNode.opacity = 255;
      moon.audio.playMusicInGame(MINIPOKER_AUDIO.GAME_BGM, true);
    }
    
  }

  onLeave() {
    super.onLeave();
    if (this.state) {
      this.state.onLeave(this, {});
    }
    // TODO: replace by card
    moon.cardPool.clear();
  }

  onLanguageChange() {
    super.onLanguageChange();
    moon.res.preloadFolder(
      `${moon.service.getGameInfoById(GAME_ID.POKER).localeFolder}/${moon.locale.getCurrentLanguage()}`,
      () => {
      },
      RESOURCE_BLOCK.GAME
    ).then(() => {
      this.renderCardTypeDialog()
      this.setLabelLanguage()    
      this.changeLabelFont()
      this.setLocaleSpiteFrame()

      if (this.state) {
        let updateData: UpdateData;
        updateData = {
          actionType: MINIPOKER_UPDATE_TYPE.LANGUAGE_CHANGE,
          data: {}
        }
        this.state.onUpdate(this, updateData)
      }
      
    })
  }


  changeLabelFont() {
    for (let player of this.players) {
      player.onLanguageChange()
    }  
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
    this.lobby.onResize();
  }

  onPause() {

  }

  onResume() {

  }

  toggleDropdown() {
    cc.log('click dropdown toogle');
    this.dropdown.active = !this.dropdown.active;
  }

  toggleStats() {
    this.statsPanel.active = !this.statsPanel.active;
  }

  setPlayers(miniPokerPlayerInfo: MiniPokerPlayerInfo[]) {
    let startSeat;
    cc.log('set players', miniPokerPlayerInfo);
    for (let playerInfo of miniPokerPlayerInfo) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    for (let playerInfo of miniPokerPlayerInfo) {
      let seatIndex = (playerInfo.seat + 6 - startSeat) % 6;
      let player: MiniPokerPlayer = this.getPlayerByIndex(seatIndex);
      if (player) {
      
        player.setPlayerInfo(playerInfo);
        player.onEnter();
        player.node.active = true;
      }
    }

    if (miniPokerPlayerInfo.length >= 6) {
      this.hideWaiting();
    }
  }

  getPlayerByIndex(index) {
    return this.players[index];
  }

  getPlayerByUserId(userId) {
    return this.players.filter(player => {
      //cc.log(player)
      if (player.playerInfo)
        return player.playerInfo.userId == userId;
    })[0];
  }

  getActiveOrderedPlayers() {
    let startSeat;
    let room: MiniPokerRoom = <MiniPokerRoom >GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }

    let orderedPlayers = [];
    for (let player of this.players) {
      if (player.node.active) {
        let index = (player.playerInfo.seat - startSeat + 6) % 6;
        orderedPlayers.push(player);
      }
    }
    return orderedPlayers;
  }

  setRoom(room: MiniPokerRoom) {
    this.matchID.string = room.matchId || '';
    this.matchID.node.parent.active = !!room.matchId;
    this.bet.string = room.betMoney.toString() || '';
    this.setBetListOfTrustTips(room.betList);
  }

  setBetListOfTrustTips(betList) {

    this.trustTipsBetToogle.forEach(btn => btn.active = false);

    for (let i = 0; i < betList.length; i++) {
      let toggle = this.trustTipsBetToogle[i];
      toggle.active = true;
      toggle.getChildByName('label').getComponent(cc.Label).string = betList[i] + moon.locale.get('times');
    }

  }

  playSelectDealer(userId: number) {

  }

  hideFirstArrow() {
    this.firstArrow.active = false;
  }

  playFirstDeal(userId) {
    let player = this.getPlayerByUserId(userId);
    this.firstArrow.stopAllActions();
    this.firstArrow.active = true;
    this.firstArrow.opacity = 255;
    this.firstArrow.x = player.node.x;
    this.firstArrow.y = player.node.y + 110;
    let actions = [];
    let moveTime = 0.3;
    let moveY = 10;
    for (let i = 0; i < 4; i++) {
      actions.push(
        cc.moveBy(
          moveTime, cc.v2(0, -moveY)
        ),
        cc.moveBy(
          moveTime, cc.v2(0, moveY)
        ),
      );
    }
    actions.push(
      cc.fadeOut(0.1)
    );
    this.firstArrow.runAction(
      cc.sequence(actions)
    );
  }

  leaveGame() {
  moon.gameSocket.leaveBoard();
    this.toggleDropdown();
    if (this.cardTypedialog.active) {
      this.cardTypedialog.active = false;
    }


  }

  returnBackToGameLobby() {
    moon.header.show();
    moon.audio.stopAllAudio();
    this.gameNode.runAction(
      cc.sequence(
        cc.fadeOut(0.3),
        cc.callFunc(() => {
          this.gameNode.active = false;
        })
      )
    );
    this.lobby.node.opacity = 0;
    this.lobby.node.active = true;
    moon.dialog.hideWaiting();
    this.lobby.node.runAction(
      cc.sequence(
        cc.delayTime(0.1),
        cc.fadeIn(0.2),
        cc.callFunc(() => {
          cc.log('back to lobby', GlobalInfo.roomInfoList);
          if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
            this.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
          }
          moon.audio.playMusicInGame(MINIPOKER_AUDIO.LOBBY_BGM, true);
        })
      )
    );
  }

  showReadyState() {
    this.stateNode.getChildByName('ready_state').active = true;
  }

  hideReadyState() {
    this.stateNode.getChildByName('ready_state').active = false;
  }


  hidePlayers() {
    cc.log('hide player');
    for (let player of this.players) {
      player.node.active = false;
    }
  }

  setBetFactorOfPlayer(playerId, betFactor) {
    let player: MiniPokerPlayer = this.getPlayerByUserId(playerId);
    player.playerInfo.betFactor = betFactor;
    player.setBet();
    if (GlobalInfo.me.userId === playerId) {
      if (this.state instanceof MiniPokerBetState) {
        this.state.onBetEnd();
      }
    }
  }

  showPlayersResultBanner() {
    for (let player of this.players) {
      if (player.node.active) {
        player.showPointBanner();
      }
    }
  }

  removePlayer(userId: any) {
    let player = this.getPlayerByUserId(userId);
    if (player) {
      player.clear()
      player.node.active = false;
    }
  }

  clear() {
    this.betLayer.clear();

    this.hideBetInfo();
    moon.cardPool.returnAllCard();
    this.clearPlayers();
    this.cancelTrusting();
    if (this.state instanceof MiniPokerReadyState)
      this.state.onLeave(this, {});
  }

  /**
   * clear all UI player
   */
  clearPlayers() {
    let roomService = MiniPokerRoomService.getInstance();
    roomService.clearPlayerInfoList();

    for (let player of this.players) {
      if (player.node.active) {
        player.clear();
        player.showBorder(false);
      }
    }
  }

  clearPlayersUI() {
    for (let player of this.players) {
      if (player.node.active) {
        cc.log('clear ui of player');
        player.clear();
      }
    }
  }

  clearPlayerBetFactor() {
    this.players.forEach(player => {
      if (player.node.active) {
        player.hideBet();
      }
    });
  }

  removeOtherPlayers() {
    for (let player of this.players) {
      if (player.playerInfo.userId != GlobalInfo.me.userId) {
        player.node.active = false;
      }
    }
  }

  addPlayer(playerInfo: MiniPokerPlayerInfo) {
    cc.log('add player', playerInfo);
    let startSeat;
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    let seatIndex = (playerInfo.seat + 6 - startSeat) % 6;
    let player: MiniPokerPlayer = this.getPlayerByIndex(seatIndex);
    if (player) {
      player.setPlayerInfo(playerInfo);
      player.onEnter();
      player.node.active = true;
    }
  }

  updateCurrentRoom(data) {
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    cc.log('return game with state', room.countdownInfo.type);
    let exceptionStateList = [COUNTDOWN_TYPE.STATE_RESULT, COUNTDOWN_TYPE.STATE_DEAL_CARD, COUNTDOWN_TYPE.STATE_OPEN_CARD]
    // if the current state in any above state
    if (exceptionStateList.indexOf(room.countdownInfo.type) >= 0) {
      this.clearPlayersUI()
    } else {
      this.hideBetInfo();
      this.setRoom(room);
      this.clearPlayersUI()
      this.setPlayers(room.playerInfoList);
      this.betLayer.clear();
    }

    MiniPokerRoomService.getInstance().activeCountdown();
    moon.cardPool.returnAllCard()
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_NOT_START_GAME:

        break;
      case COUNTDOWN_TYPE.STATE_READY_START_GAME:
        this.onReturnAtReadyStartGame(data);
        break;
      case COUNTDOWN_TYPE.STATE_START_GAME:
        // this.changeState(GAME_STATE.START, TwoEightStartState);
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        this.changeState(GAME_STATE.BET, MiniPokerBetState);
        break;
      
      case COUNTDOWN_TYPE.STATE_DEAL_CARD:
      case COUNTDOWN_TYPE.STATE_OPEN_CARD:
      case COUNTDOWN_TYPE.STATE_RESULT:
        // TODO: replace by card
        cc.log('result',moon.cardPool.workingCards.length)
          this.setPlayers(room.playerInfoList);
          this.players.forEach(player => {
            if (player.node.active && player.playerInfo.resultCard.cardList) {
              player.setCardsByIdList(player.playerInfo.resultCard.cardList);
            }
          });
        break;
    }
  }

  onReturnAtReadyStartGame(data) {
    this.changeState(GAME_STATE.READY_START, MiniPokerReadyState, data);
    this.showWaiting();
  }

  playEndSessionCountdown() {

  }

  setBetInfo(str: string) {
    this.betInfo.string = str;
    this.betInfo.node.parent.active = true;
  }

  hideBetInfo() {

  }


  showWaiting() {
    //   this.waiting.active = true;
  }

  hideWaiting() {
    this.waiting.active = false;
  }

  hideTrustDialog() {
    this.trustDialog.active = false;
  }

  showTrustDialog() {
    this.trustDialog.active = true;
  }

  /**
   * uses for stand by to remove a player when they leave the game on result state
   * @param userId user to be remove when the game start if they leave the game on result state
   */
  standByRemovePlayer(userId: number) {
    //this.leavedPlayersToRemove.push(userId);
    this.removePlayer(userId)
    MiniPokerRoomService.getInstance().removePlayerInfo(userId);
  }

  removeLeavedPlayers() {
    this.leavedPlayersToRemove.forEach(playerId => {
      this.removePlayer(playerId);
      MiniPokerRoomService.getInstance().removePlayerInfo(playerId);
    });

    this.leavedPlayersToRemove = [];
  }

  showNotify(msg) {
    this.notify.stopAllActions();
    this.notify.active = true;
    this.notify.opacity = 0;
    this.dropdown.active = false;
    NodeUtils.setLabel(this.notify, 'notify', msg);
    this.notify.runAction(
      cc.sequence(
        cc.fadeIn(0.1),
        cc.delayTime(2),
        cc.fadeOut(0.2)
      )
    );
  }

  setTrustFactor(evt, value) {
    cc.log('bet with factor', value);
    this.trustingFactor = value;
  }

  activeTrusting() {
    this.isTrusting = true;
    this.trustBtn.getChildByName('trust').active = false;
    this.trustBtn.getChildByName('cancel').active = true;
    this.hideTrustDialog();
  }

  cancelTrusting() {
    this.isTrusting = false;
    this.trustBtn.getChildByName('trust').active = true;
    this.trustBtn.getChildByName('cancel').active = false;
  }


  toogleCardTypeDialog() {
    this.cardTypedialog.active = !this.cardTypedialog.active;
    if (this.dropdown.active) {
      this.dropdown.active = false;
    }
  }

  renderCardTypeDialog() {
    let gameConfig: any = moon.service.getGameConfig(GlobalInfo.room.gameId);
    let cardTypeList = gameConfig.cardTypeGuide;
    this.scrollViewContent.removeAllChildren()
    cardTypeList.map(cardType => {
      let row = cc.instantiate(this.rowSample);
      cc.log('text ', cardType.text);
      cardType.row.map(cardId => {
        let card = cc.instantiate(this.miniCardSample);
        card.active = true;
        let cardComponent = card.getComponent(Card);
        cardComponent.setDeckId(DECK_ID.SIMPLIFIED_1);
        cardComponent.setCardSize(47, 65);
        cardComponent.setCardId(cardId);
        cardComponent.showCard();
        row.addChild(card);
      });

      let detail = cc.instantiate(this.lblSample);
      detail.active = true;
      let type = detail.getChildByName('type').getComponent(cc.Label)
      type.string = ' ' + moon.locale.get(cardType.text);
      let bonus = detail.getChildByName('bonus').getComponent(cc.Label)
      bonus.string = ' X' + cardType.bonus;

      row.addChild(detail);
      row.active = true;
      this.scrollViewContent.addChild(row);
    });
  }

}