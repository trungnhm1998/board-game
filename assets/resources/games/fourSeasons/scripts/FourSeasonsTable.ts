import { FourSeasonsService } from './service/FourSeasonsService';
import { COUNTDOWN_TYPE } from './../../../../scripts/core/Constant';
import { FourSeasonsPlayerInfo } from './model/FourSeasonsPlayerInfo';
import { FourSeasonsPlayer } from './FourSeasonsPlayer';
import { TimerTask } from './../../../../scripts/core/TimerComponent';
import { FourSeasonsEffect } from './FourSeasonsEffect';
import { BetType } from './FourSeasonsConstants';
import { FourSeasonsSocket } from './FourSeasonsSocketService';
import { FourSeasonsBet } from './FourSeasonsBet';
import { FourSeasonsRoom } from './model/FourSeasonsRoom';
import { FourSeasonsDealer } from './FourSeasonsDealer';
import { FourSeasonsPot } from './FourSeasonsPot';
import { moon, GlobalInfo } from './../../../../scripts/core/GlobalInfo';

const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsTable extends cc.Component {
  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Node)
  dropdownMenu: cc.Node = null;

  @property(FourSeasonsPot)
  housePot: FourSeasonsPot = null;

  @property(FourSeasonsPot)
  spadePot: FourSeasonsPot = null;

  @property(FourSeasonsPot)
  heartPot: FourSeasonsPot = null;

  @property(FourSeasonsPot)
  clubPot: FourSeasonsPot = null;

  @property(FourSeasonsPot)
  diamondPot: FourSeasonsPot = null;

  @property(FourSeasonsDealer)
  dealer: FourSeasonsDealer = null

  @property(FourSeasonsBet)
  betLayer: FourSeasonsBet = null;

  @property(cc.Node)
  roomHistory: cc.Node = null;

  @property(cc.Node)
  spadeHistory: cc.Node = null;

  @property(cc.Node)
  heartHistory: cc.Node = null;

  @property(cc.Node)
  clubHistory: cc.Node = null;

  @property(cc.Node)
  diamondHistory: cc.Node = null;

  @property(cc.Prefab)
  historyNodePrefab: cc.Prefab = null;

  @property(cc.SpriteFrame)
  gameRightSprite: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  gameWrongSprite: cc.SpriteFrame = null;

  @property(cc.Label)
  playerNum: cc.Label = null;

  @property(cc.Node)
  banker: cc.Node = null;

  @property(FourSeasonsEffect)
  effectLayer: FourSeasonsEffect = null;

  @property(cc.Label)
  clockTimeLabel: cc.Label = null;

  @property(dragonBones.ArmatureDisplay)
  clock: dragonBones.ArmatureDisplay = null;

  @property(cc.Label)
  matchId: cc.Label = null;

  @property(FourSeasonsPlayer)
  currentPlayer: FourSeasonsPlayer = null;

  @property(FourSeasonsPlayer)
  housePlayer: FourSeasonsPlayer = null;

  @property(cc.Label)
  tableStatus: cc.Label = null;

  _potMap = {};
  _countdownTask: TimerTask = null;
  _isPlay = false;
  _historyContainers = {};

  onResize(ratioToScale: number) {
    this.ui.scale = ratioToScale;
  }

  onLoad() {
    // window.clock = this.clock;
    this.spadePot._potClickHandler = () => {
      const { currentBetMoney } = this.betLayer;
      FourSeasonsSocket.bet(currentBetMoney, BetType.SPADE);
    }
    this.heartPot._potClickHandler = () => {
      const { currentBetMoney } = this.betLayer;
      FourSeasonsSocket.bet(currentBetMoney, BetType.HEART);
    }
    this.clubPot._potClickHandler = () => {
      const { currentBetMoney } = this.betLayer;
      FourSeasonsSocket.bet(currentBetMoney, BetType.CLUB);
    }
    this.diamondPot._potClickHandler = () => {
      const { currentBetMoney } = this.betLayer;
      FourSeasonsSocket.bet(currentBetMoney, BetType.DIAMOND);
    }

    this._potMap = {
      [BetType.SPADE]: this.spadePot,
      [BetType.HEART]: this.heartPot,
      [BetType.CLUB]: this.clubPot,
      [BetType.DIAMOND]: this.diamondPot,
      [BetType.DEALER]: this.housePot
    }

    this._historyContainers = {
      [BetType.SPADE]: this.spadeHistory,
      [BetType.HEART]: this.heartHistory,
      [BetType.CLUB]: this.clubHistory,
      [BetType.DIAMOND]: this.diamondHistory,
    }
  }

  onDropdownBtn() {
    this.dropdownMenu.active = !this.dropdownMenu.active;
  }

  onLeaveGame() {
    moon.gameSocket.leaveBoard();
    this.onDropdownBtn();
  }

  onLanguageChange() {
    this.effectLayer.onLanguageChange();
  }

  update() {
    if (this._isPlay) {
      // let clockArmature = this.clock.armature()
      // let temp = clockArmature._bones[1]
      // cc.log('slot', temp)
      // this.clockTimeLabel.node.setRotation()
    }
  }

  shuffleThenDealCards() {
    this.dealer.shuffleCards();
    const WAIT_BETWEEN_DEAL = 0.35;
    this.dealer.node.runAction(
      cc.sequence(
        cc.spawn(this.dealer._shufflingSpawnActions),
        cc.delayTime(2.0),
        cc.callFunc(() => {
          this.dealer.dealCards(this.housePot);
        }),
        cc.delayTime(WAIT_BETWEEN_DEAL),
        cc.callFunc(() => {
          this.dealer.dealCards(this.spadePot);
        }),
        cc.delayTime(WAIT_BETWEEN_DEAL * 1.3),
        cc.callFunc(() => {
          this.dealer.dealCards(this.heartPot);
        }),
        cc.delayTime(WAIT_BETWEEN_DEAL * 1.6),
        cc.callFunc(() => {
          this.dealer.dealCards(this.clubPot);
        }),
        cc.delayTime(WAIT_BETWEEN_DEAL * 1.8),
        cc.callFunc(() => {
          this.dealer.dealCards(this.diamondPot);
        })
      )
    )
  }

  updateCurrentRoom() {
    let fourSeasonsRoom = GlobalInfo.room as FourSeasonsRoom;
    const { countdownInfo } = fourSeasonsRoom;
    this.updateGameBasedOnGameState(countdownInfo.type, fourSeasonsRoom);
    this.updateTableUI(fourSeasonsRoom);
    this.updateStateStatus()

    this.runCountdown(countdownInfo.timeout, countdownInfo.type == COUNTDOWN_TYPE.STATE_BET);
  }

  updateStateStatus() {
    let room = GlobalInfo.room as FourSeasonsRoom;
    let status = (moon.service.getCurrentService() as FourSeasonsService).getStatus(room.countdownInfo.type);
    this.tableStatus.string = status;
  }

  updateGameBasedOnGameState(gameState, fourSeasonsRoom: FourSeasonsRoom) {
    const { placeBetInfoList } = fourSeasonsRoom;
    switch (gameState) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        this.clear();
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CARD:
      case COUNTDOWN_TYPE.STATE_BET:
      case COUNTDOWN_TYPE.STATE_OPEN_CARD:
      case COUNTDOWN_TYPE.STATE_RESULT:
        this.dealer.node.stopAllActions();
        this.dealer.clear();
        this.betLayer.node.stopAllActions();
        placeBetInfoList.forEach(placeBetInfo => {
          const { placeBetType, cardList, cardType } = placeBetInfo;
          let pot: FourSeasonsPot = this._potMap[placeBetType]
          pot.node.stopAllActions();

          this.dealer.setCards(pot);

          pot._cardListToOpen = cardList;
          pot._cardTypeToOpen = cardType;

          if (gameState == COUNTDOWN_TYPE.STATE_RESULT || gameState == COUNTDOWN_TYPE.STATE_OPEN_CARD) {
            pot.openCardWithoutAnim();
          }
        })
        break;
    }

  }

  updateTableUI(fourSeasonsRoom: FourSeasonsRoom) {
    const { historyList, betList, playerInfo, playerNum } = fourSeasonsRoom;
    this.initHistory(historyList);
    this.betLayer.updateBetList(betList);
    this.updatePlayerInfo(playerInfo);
    this.playerNum.string = String(playerNum);
  }

  updatePlayerInfo(fourSeasonsPlayerInfo: FourSeasonsPlayerInfo) {
    const { placeBetTypeList } = fourSeasonsPlayerInfo;
    this.currentPlayer.setPlayerInfo(fourSeasonsPlayerInfo);
    for (let placeBetType of placeBetTypeList) {
      let pot: FourSeasonsPot = this._potMap[placeBetType];
      pot.setPlayerMoney(placeBetType.betMoneyTotal);
    }
  }

  initHistory(historyList) {
    this.initPotHistory(BetType.SPADE, historyList);
    this.initPotHistory(BetType.CLUB, historyList);
    this.initPotHistory(BetType.HEART, historyList);
    this.initPotHistory(BetType.DIAMOND, historyList);
  }

  initPotHistory(potType, historyList) {
    let potHistory: number[] = historyList[potType];
    let historyContainer: cc.Node = this._historyContainers[potType];
    for (let i = 0; i < potHistory.length; i++) {
      let historyNode = cc.instantiate(this.historyNodePrefab);
      historyNode.setContentSize(cc.size(12, 12));
      let historySpriteFrame = historyNode.getComponent(cc.Sprite);

      if (potHistory[i] == 1) {
        historySpriteFrame.spriteFrame = this.gameRightSprite;
      } else {
        historySpriteFrame.spriteFrame = this.gameWrongSprite;
      }
      historyContainer.addChild(historyNode);
    }

    historyContainer.getComponent(cc.Layout).updateLayout();
  }

  /**
   * run count down and update UI for clock timer
   * @param timeout time to run count down from
   * @param onLastSecond callback on the last second
   */
  runCountdown(timeout, countdownAlarm = false, onLastSecond?) {
    this.clockTimeLabel.string = timeout;
    // clear task before run new one
    if (this._countdownTask)
      moon.timer.removeTask(this._countdownTask);

    this._countdownTask = moon.timer.runCountdown(timeout, (val) => {
      this.clockTimeLabel.string = val;
      // play audio count down on last 3 seconds
      if (val <= 3 && val > 0 && countdownAlarm) {
        this._isPlay = true;
        this.clock.playAnimation('animation', -1);
      } else {
        this.clock.playAnimation('animation2', 1);
        this._isPlay = false;
      }

      if (val == 1 && onLastSecond) {
        onLastSecond();
      }
    })
  }

  stopCountdown() {
    moon.timer.removeTask(this._countdownTask);
  }

  clear() {
    this.tableStatus.string = "";
    this.spadeHistory.destroyAllChildren();
    this.clubHistory.destroyAllChildren();
    this.heartHistory.destroyAllChildren();
    this.diamondHistory.destroyAllChildren();
    this.spadePot.clear();
    this.heartPot.clear();
    this.clubPot.clear();
    this.diamondPot.clear();
    this.housePot.clear();
    this.stopCountdown();
    this.dealer.initDeck();
  }
}
