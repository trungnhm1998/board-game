import {GameState, IGameState, UpdateData} from '../../../../../scripts/common/GameState';
import {TwoEightGame} from '../TwoEightGame';
import {NodeUtils} from '../../../../../scripts/core/NodeUtils';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {TwoEightRoom} from '../model/TwoEightRoom';
import {TWO_EIGHT_AUDIO} from '../../../../../scripts/core/Constant';
import {TwoEightRoomService} from '../service/TwoEightRoomService';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightBetState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  checkBtn: cc.Node = null;

  @property(cc.Node)
  betSample: cc.Node = null;

  @property(cc.Node)
  buttons: cc.Node = null;

  @property(cc.Node)
  childView: cc.Node = null;

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;


  countdownTask;
  autoBetValue = 1;

  onEnter(game: TwoEightGame, data: any) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
  
    let timeout = room.countdownInfo.timeout;
    game.allPlayerThinking();
    game.getPlayerByUserId(room.bankerId).stopThinking();
    game.getPlayerByUserId(room.bankerId).showDealer(true);
    if (room.bankerId == GlobalInfo.me.userId) {
      this.childView.active = false;
    } else {
      let me = game.getPlayerByUserId(GlobalInfo.me.userId);
      cc.log('BetState playerinfo', me)
      if (!me.playerInfo.bet || me.playerInfo.bet == 0) {
        this.showBetList()
      }
    }


    for (let player of game.players) {
      if (player.playerInfo.bet > 0) {
        player.setBetValue(player.playerInfo.bet)
      }
    }



    this.countdown.node.parent.active = false;
    this.startAnim.node.active = true;
    this.startAnim.timeScale = 1.5;
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.BET_START, true);

    let lang = moon.locale.getCurrentLanguage();
    this.startAnim.armatureName = lang;
    this.startAnim.playAnimation(lang, 1);

    const playCountDownSound = () => this.playCountDownSound(game);
    setTimeout(() => {
      this.startAnim.node.active = false;
      this.countdown.node.parent.active = true;
      this.runCountdown(room.countdownInfo.timeout, playCountDownSound);
    }, 750);
  }

  showBetList() {
    let betList = TwoEightRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId).betList;
    this.childView.active = true;
    this.buttons.removeAllChildren();
    for (let betValue of betList) {
      if (betValue == 0) {
        this.checkBtn.removeFromParent();
        this.buttons.addChild(this.checkBtn);
        this.checkBtn.active = true;
        this.checkBtn.position = cc.v2();
      } else {
        let betBtnNode = cc.instantiate(this.betSample);
        betBtnNode.active = true;
        let betUnit = moon.locale.get('bets');
        let valueNode = NodeUtils.findByName(betBtnNode, 'betValue');
        let valueLabel = valueNode.getComponent(cc.Label);
        valueLabel.string = '' + betValue + ' ' + betUnit;
        let betBtn: cc.Button = betBtnNode.getComponent(cc.Button);
        let clickEvent = betBtn.clickEvents[0];
        clickEvent.customEventData = betValue.toString();
        this.buttons.addChild(betBtnNode);
        betBtnNode.position = cc.v2();
      }
    }
  }

  playCountDownSound(game: TwoEightGame) {
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.COUNTDOWN, false);
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {

  }

  onLeave(game: TwoEightGame, data: any) {
    this.checkBtn.removeFromParent();
    this.node.addChild(this.checkBtn);
    this.checkBtn.active = false;
    this.countdown.node.parent.active = false;
    game.hidePlayerBetValue();
  }

  runCountdown(max, playSound) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      num--;
      playSound();
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num, playSound);
      } else {
        this.node.active = false;
      }
    }, 1);
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }

  onBet(evt, value) {
    moon.gameSocket.twoEightBet(+value);
  }

  onBetEnd() {
    this.childView.active = false;

  }
}