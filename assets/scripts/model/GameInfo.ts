export class BetInfo {
  betMoney: number;
  minBuyInMoney: number;
  minBetMoney: number;
}

/*
// move to game model sub dir
export class DragonTigerBetInfo extends BetInfo {
  groupId;
  roomIdList = [];
  betMoneyList = [];
}
*/

export class GameItem {
  gameId: number;
  gameType: string;
  gameName: string;
  url: string;
  gameInfo: Array<BetInfo> = [];
  isComingSoon: boolean;
}