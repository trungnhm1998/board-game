import {GameState, IGameState, UpdateData} from '../../../../../scripts/common/GameState';
import {TwoEightGame} from '../TwoEightGame';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {TwoEightRoom} from '../model/TwoEightRoom';
import {TWO_EIGHT_AUDIO} from '../../../../../scripts/core/Constant';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightStartState extends GameState implements IGameState {

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: TwoEightGame, data: any) {
    game.clear();
    game.setRoom(<TwoEightRoom>GlobalInfo.room);
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;

    // start at current frames
    // this.startAnim.playAnimation('gamestart_'+moon.locale.getCurrentLanguage(), 1);
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.GAMESTART, false);
    let lang = moon.locale.getCurrentLanguage();
    this.startAnim.armatureName = lang;
    this.startAnim.playAnimation(lang, 1);
    // state.currentTime = room.countdownInfo.timeout / 2 * 1.62;
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {
  }

  onLeave(game: TwoEightGame, data: any) {
  }
}