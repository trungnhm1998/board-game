import {ModelUtils} from "./ModelUtils";

export const LocalData = {
  isNextCaptcha: 0,
  clientId: '',
  lang: '',
  last_username: '',
  last_saved_password:'',
  rememberPass: 1,
  show_money: 1,
  installId: 0,
  token: '',
  sound: 0,
  music: 0,
  firstOpen: 1,
  classicBuyIn: 0,
  classicBet: 0,
  twoEightBet: 0,
  twoEightMin: 0,
  dragonTigerBet: 0,
  dragonTigerRoomId: '',
  dragonTigerRoomCategory: 1,
  autoBuyIn: 1
};

export class LocalStorage {

  loadedData = {};
  loadLocalData() {
    for (let k in LocalData) {
      let data = cc.sys.localStorage.getItem(k);
      if (data != null) {
        LocalData[k] = data;
      }
    }
    this.loadedData = {};
    ModelUtils.merge(this.loadedData, LocalData);
  }

  saveLocalData() {
    for (let k in LocalData) {
      if (LocalData[k] != this.loadedData[k]) {
        this.loadedData[k] = LocalData[k];
        cc.sys.localStorage.setItem(k, LocalData[k]);
      }
    }
  }
}
