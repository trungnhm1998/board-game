import {DragonTigerSocket} from './DragonTigerSocketService';
import {LocalData} from '../../../../../scripts/core/LocalStorage';
import {IGameService} from '../../../../../scripts/services/GameService';
import {GameSocket} from '../../../../../scripts/services/SocketService';
import {
  ERROR_TYPE,
  KEYS,
  PLACE_TYPE,
  SCENE_TYPE,
  SERVER_EVENT
} from '../../../../../scripts/core/Constant';
import DragonTigerConstants from '../DragonTigerConstants';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {DragonTigerGame} from '../DragonTigerGame';
import {DragonTigerRoom} from '../model/DragonTigerRoom';
import {DragonTigerRoomService} from './DragonTigerRoomService';
import {ModelUtils} from '../../../../../scripts/core/ModelUtils';
import {DragonTigerPot} from '../DragonTigerPot';
import {CountdownInfo} from '../../../../../scripts/model/Room';
import {TwoEightService} from '../../../twoEight/scripts/service/TwoEightService';

export class DragonTigerService implements IGameService {

  isActive = false;

  name = 'DragonTigerService';

  game: DragonTigerGame;
  waitingActions: any[] = [];

  isSwitchTable: boolean = false;
  switchTableId: string = "";
  switchTableBetMoney: number;

  // fixed multiple pet pot
  tempClientBet: {
    money: number,
    count: number
  }[] = [
    {
      money: 0,
      count: 0
    },
    {
      money: 0,
      count: 0
    },
    {
      money: 0,
      count: 0
    },
  ];

  isJoinBoard: boolean = false; // check if user just joined the board
  winPlaceType: any;

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(DragonTigerGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    // this.gameLobby = gamgeNode.getComponent(MahjongGame);
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };
    // temporary comment
    /*
    socket.on(SERVER_EVENT.GET_DRAGON_TIGER_ROOM_INFO, data => {
        if (moon.scene.isInScreen(SCENE_TYPE.PLAY))
          return processAction(this.onGetRoomInfo, data)
        return null;
      }
    );
    */
    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => processAction(this.onErrorMessage, data));
  }

  onJoinBoard(data) {
    let room = GlobalInfo.room as DragonTigerRoom;
    if (!room.countdownInfo) {
      room.countdownInfo = new CountdownInfo();
      room.countdownInfo.timeout = room.timeout;
    }
    const { gameTable } = this.game;
    gameTable.clear();
    gameTable.updateCurrentRoom();
    // gameTable.changeTableDialog.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
    // moon.gameSocket.getDragonTigerRoomInfo('');
    this.isJoinBoard = true;
  }

  onPlayerJoinBoard(data) {
    let playerNum = data[KEYS.PLAYER_NUM];
    let room = GlobalInfo.room as DragonTigerRoom;
    room.playerNum = playerNum;
    this.game.setPeopleCount(playerNum);
  }

  onErrorMessage(data) {
    if (data[KEYS.TYPE] == ERROR_TYPE.BUBBLE) {
      this.game.showNotify(data[KEYS.MESSAGE]);
    }
  }

  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    this.isJoinBoard = true;
    const { gameTable, lobby } = this.game;
    gameTable.node.active = true;
    gameTable.node.opacity = 255;
    lobby.node.active = false;
    gameTable.clear();
    gameTable.updateCurrentRoom();
    moon.dialog.hideWaiting();

    if (!GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
      moon.gameSocket.getRoomInfoList(GlobalInfo.room.gameId);
    }

  }

  onActionInGame(data) {
    let subData = data[KEYS.DATA];
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.DEAL_CARD:
          this.onDealCard(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.OPEN_CARD:
          this.onOpenCard(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        case SERVER_EVENT.UPDATE_PLAYERS_BET:
          this.onUpdatePlayersBet(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        // case SERVER_EVENT.GET_DRAGON_TIGER_ROOM_LIST:
        //   this.onGetRoomList(subData);
        //   break;
      }
    };

    handleEvents();
  }

  changeRoom(betMoney, roomId) {
    if (betMoney && roomId) {
      this.isSwitchTable = true;
      this.switchTableId = roomId;
      this.switchTableBetMoney = betMoney;
    }
  }

  private onStartGame(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    ModelUtils.merge(GlobalInfo.room, data);
    let room = GlobalInfo.room as DragonTigerRoom;
    const { SERVER_GAME_STATE } = DragonTigerConstants;
    room.countdownInfo.type = SERVER_GAME_STATE.STATE_START_GAME;

    const { gameTable } = this.game;

    this.game.updateStatus();
    gameTable.clearCards();
    gameTable.clearChips();
    gameTable.clearPots();
    DragonTigerRoomService.getInstance().clearLastMatchRoomInfo();
    gameTable.myPlayer.hideWinMoney();
    gameTable.effects.hide();
    gameTable.effects.playStartMatch(room.matchOrder);
    gameTable.runCountdown(room.timeout);
    gameTable.setRoom(room);
  }

  private onDealCard(data: any) {
    cc.log("onDealCard", data);
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as DragonTigerRoom;
    room.countdownInfo.type = DragonTigerConstants.SERVER_GAME_STATE.STATE_DEAL_CARD;
    const { gameTable } = this.game;
    this.game.updateStatus();
    this.game.runCountdown(data[KEYS.TIMEOUT], () => {

    });
    gameTable.dealer.skipCard(gameTable.clock.node, () => {
      gameTable.dealer.dealCard(gameTable.potDragon, -1, () => {
        gameTable.dealer.dealCard(gameTable.potTiger, -1, () => {
          gameTable.dealer.setCount(data[KEYS.CARD_NUM]);
        });
      });
    });
  }

  private onBeginBet(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as DragonTigerRoom;
    const { SERVER_GAME_STATE } = DragonTigerConstants;
    room.countdownInfo.type = SERVER_GAME_STATE.STATE_BET;
    const { gameTable } = this.game;
    this.game.updateStatus();
    gameTable.effects.playStartGame();
    this.game.runCountdown(data[KEYS.TIMEOUT], () => {

    });
  }

  private onOpenCard(data: any) {
    cc.log("onOpenCard", data);
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let dragonCardId = data.placeBetInfoList.filter((betInfo) => betInfo.placeBetType == PLACE_TYPE.DRAGON)[0].cardList[0];
    let tigerCardId = data.placeBetInfoList.filter((betInfo) => betInfo.placeBetType == PLACE_TYPE.TIGER)[0].cardList[0];
    this.winPlaceType = data[DragonTigerConstants.KEYS.PLACE_BET_TYPE];
    let room = GlobalInfo.room as DragonTigerRoom;
    const { SERVER_GAME_STATE } = DragonTigerConstants;
    room.countdownInfo.type = SERVER_GAME_STATE.STATE_RESULT;
    const { gameTable } = this.game;
    this.game.updateStatus();
    gameTable.effects.playStopGame();
    cc.log(`dragonTigerService:onOpenCard d: ${dragonCardId} t: ${tigerCardId}`)
    if (gameTable.potDragon.card && gameTable.potTiger.card) {

      gameTable.potDragon.card.setCardId(dragonCardId);
      gameTable.potTiger.card.setCardId(tigerCardId);
      moon.timer.scheduleOnce(() => {
        gameTable.potDragon.openCard();
      }, 1);
      moon.timer.scheduleOnce(() => {
        gameTable.potTiger.openCard();
      }, 2);

      moon.timer.scheduleOnce(() => {
        if (this.winPlaceType == PLACE_TYPE.DRAGON) {
          gameTable.effects.playDragonWin();
        } else if (this.winPlaceType == PLACE_TYPE.TIGER) {
          gameTable.effects.playTigerWin();
        } else {
          gameTable.effects.playDraw();
        }
      }, 3)
    }
    gameTable.runCountdown(data[KEYS.TIMEOUT]);
  }

  private onBet(data: any) {
    let userId = data[KEYS.USER_ID];
    let placeBetType = data[DragonTigerConstants.KEYS.PLACE_BET_TYPE];
    let myMoney = data[KEYS.MONEY];
    let potTotalBet = data[KEYS.BET_MONEY_TOTAL];
    let myPotTotalBet = data[KEYS.BET_MONEY_TOTAL_PLAYER];
    let betMoney = data[KEYS.BET_MONEY];
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    let room = GlobalInfo.room as DragonTigerRoom;
    const { SERVER_GAME_STATE } = DragonTigerConstants;
    room.countdownInfo.type = SERVER_GAME_STATE.STATE_BET;
    let playerInfo = DragonTigerRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    for (let betType of playerInfo.placeBetTypeList) {
      if (placeBetType.type == betType) {
        placeBetType.betMoneyTotal = myPotTotalBet;
      }
    }

    this.game.updateStatus();
    if (userId == GlobalInfo.me.userId) {
      let pot: DragonTigerPot;
      const { gameTable } = this.game;
      switch (placeBetType) {
        case PLACE_TYPE.DRAGON:
          pot = gameTable.potDragon;
          break;
        case PLACE_TYPE.TIGER:
          pot = gameTable.potTiger;
          break;
        case PLACE_TYPE.DRAW:
          pot = gameTable.potDraw;
          break;
      }
      pot.setTotalMoney( moon.string.round(potTotalBet, 2));
      pot.setMyMoney( moon.string.round(myPotTotalBet, 2));
      gameTable.bets.playMyBet(pot,  moon.string.round(betMoney, 2));
      gameTable.myPlayer.setMoney(myMoney);
    }
  }

  private onUpdatePlayersBet(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    const { gameTable } = this.game;
    for (let placeType of data[KEYS.PLACE_TYPE_LIST]) {
      let type = placeType[KEYS.TYPE];
      let betMoney = placeType[KEYS.BET_MONEY];
      let betMoneyTotal = placeType[KEYS.BET_MONEY_TOTAL];
      let room = GlobalInfo.room as DragonTigerRoom;
      while (this.tempClientBet[type - 1].count > 0 && betMoney - this.tempClientBet[type - 1].money >= 0) {
        betMoney =  moon.string.round(betMoney, 2) - this.tempClientBet[type - 1].money;
        this.tempClientBet[type - 1].count -= 1;
      }

      if (this.tempClientBet[type - 1].count == 0) {
        this.tempClientBet[type - 1].money = 0;
      }
      let pot: DragonTigerPot = gameTable.getPlacePots()[type];
      pot.setTotalMoney(moon.string.round(betMoneyTotal, 2));
      gameTable.bets.playPeopleBet(pot, betMoney, room.betList);
    }
  }

  private onEndGame(data: any) {
    const { gameTable } = this.game;
    let winMoney = data[KEYS.WIN_MONEY];
    let money = data[KEYS.MONEY];
    // let placeType = data[DragonTigerConstants.KEYS.PLACE_BET_TYPE];
    let timeout = data[KEYS.TIMEOUT];
    let historyList = data[KEYS.HISTORY_LIST];
    let winPot: DragonTigerPot = gameTable.getPlacePots()[this.winPlaceType];
    let pots = [gameTable.potDragon, gameTable.potTiger, gameTable.potDraw];
    let room = GlobalInfo.room as DragonTigerRoom;
    let count = 0;
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    for (let bet of this.tempClientBet) {
      bet.money = 0;
      bet.count = 0;
    }

    const { SERVER_GAME_STATE } = DragonTigerConstants;
    room.countdownInfo.type = SERVER_GAME_STATE.STATE_RESULT;

    let playerInfo = DragonTigerRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    playerInfo.money = money;
    GlobalInfo.me.money = money;

    this.game.updateStatus();
    gameTable.roomHistory.setHistory(historyList);
    this.game.runCountdown(timeout);
    for (let pot of pots) {
      if (pot != winPot) {
        moon.timer.scheduleOnce(() => {
          gameTable.bets.moveChipsToHouse(pot, () => {
            count++;
            if (count == pots.length - 1) {
              gameTable.bets.housePay(winPot, winPot ? +winPot.totalMoney.string : "", room.betList, () => {
                gameTable.bets.moveChipsToPeople(winPot, () => {
                  gameTable.myPlayer.playWinEffect( moon.string.round(winMoney, 2), GlobalInfo.me.money);
                });
              })
            }
          });
        }, 1.5);
      }
    }
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let playerNum = data[KEYS.PLAYER_NUM];
    if (roomId != GlobalInfo.room.roomId) return;

    let room = GlobalInfo.room as DragonTigerRoom;
    room.playerNum = playerNum;

    this.game.setPeopleCount(playerNum);
    if (GlobalInfo.me.userId == userId && !this.isSwitchTable) {
      let backToMain = () => {
        this.game.gameTable.clear();
        this.clearWaitingActions();
        moon.dialog.showWaiting();
        this.game.onLeaveRoom();
        // let transition = new FadeOutInTransition(0.2);
        // moon.scene.pushScene(SCENE_TYPE.MAIN_MENU, transition, true, () => {
        //   if (msg) {
        //     moon.dialog.showNotice(msg);
        //   }
        // });
        moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
      };
      backToMain();
    } else if (this.isSwitchTable) {
      this.game.gameTable.clear();
      LocalData.dragonTigerBet = this.switchTableBetMoney;
      LocalData.dragonTigerRoomId = this.switchTableId;
      moon.storage.saveLocalData();

      DragonTigerSocket.play(GlobalInfo.room.gameId, this.switchTableId);
    }
  }

  setTempBet(money, type) {
    this.tempClientBet[type - 1] = {
      money,
      count: this.tempClientBet[type - 1].count + 1
    }
  }

  updateRoomInfoList() {
    if (this.game) {
      this.game.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
    }
  }

  getStatus(type) {
    let locale = moon.locale;
    let msg = "";
    const { SERVER_GAME_STATE } = DragonTigerConstants;
    switch (type) {
      case SERVER_GAME_STATE.STATE_START_GAME:
        msg = locale.get('preparing');
        break;
      case SERVER_GAME_STATE.STATE_DEAL_CARD:
        msg = locale.get('dealing');
        break;
      case SERVER_GAME_STATE.STATE_BET:
        msg = locale.get('betting');
        break;
      case SERVER_GAME_STATE.STATE_OPEN_CARD:
        msg = locale.get('opening');
        break;
      case SERVER_GAME_STATE.STATE_RESULT:
        msg = locale.get('result');
        break;
      default:
        msg = "";
        break;
    }
    return msg;
  }
}
