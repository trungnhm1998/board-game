import { moon } from './../../../../scripts/core/GlobalInfo';
import { MockSocket } from './../../../../scripts/test/MockSocket';
import { Config } from './../../../../scripts/Config';
import { RedBlackRoom } from './model/RedBlackRoom';
import { RedBlackGame } from './RedBlackGame';
import { Card } from '../../../../scripts/core/Card';
import {TestCase} from "../../../../scripts/test/TestCase";
import {GlobalInfo} from "../../../../scripts/core/GlobalInfo";
import {UserInfo} from "../../../../scripts/model/UserInfo";
import {GAME_STATE, KEYS} from "../../../../scripts/core/Constant";

import { CountdownInfo } from "../../../../scripts/model/Room";
import { RedBlackOpenCardState } from './gamestates/RedBlackOpenCardState';
import { RedBlackStartState } from './gamestates/RedBlackStartState';
import { RedBlackDealCardState } from './gamestates/RedBlackDealCardState';
import { RedBlackEndGameState } from './gamestates/RedBlackEndGameState';
import { RedBlackBetState } from './gamestates/RedBlackBetState';
import { RedBlackEndState } from './gamestates/RedBlackEndState';

export class RedBlackTest extends TestCase {

  game: RedBlackGame;
  socket: MockSocket;

  setGame(gameNode) {
    super.setGame(gameNode);
    this.game = gameNode.getComponent(RedBlackGame);
  }

  run() {
    super.run();
    this.socket = new MockSocket();
    moon.lobbySocket.socket = (<any>this.socket);
    moon.gameSocket.socket = (<any>this.socket);
    moon.cardPool.setCardScale(0.6)

    this.setGameInfo( Config.gameInfo.filter( game => game.gameId == 4)[0])
    .then(() => {
      this.runTestCases();
    })
  }

  setup() {
    ///this.getIntoTable();
    this.setupRoom();
   this.setupCard()
   //   this.runSequence([
   //     [this.testThrowChip,3],
   //     [this.testThrowChip,3],
   //     [this.testThrowChip,3],
   //     [this.testThrowChip,3],
   //  ])
   this.runSequence([
     [this.getToStartState, 2],
     [this.getToDealCardState, 2],
     [this.getToBetState, 2],
     [this.testThrowChip, 3],
     [this.testThrowChip, 3],
     [this.testThrowChip, 3],
     [this.testThrowChip, 3],
     [this.setCardID, 1],
     [this.getToEndState, 1],
     [this.getToOpenCardState, 4],
     [this.getToEndGameState, 3]
   ]);
    //this.getToOpenCardState()
  //  this.setupPlayers();
  }
  getToStartState() {
    this.game.changeState('start_state',RedBlackStartState,{})  
  }

  getToBetState() {
   this.game.changeState('bet_state',RedBlackBetState,{})  
 }

 getToEndState() {
   this.game.changeState('end_state',RedBlackEndState,{})  
 }



  setupCard() {
    let cardList = []
    let cardList2= []
    moon.cardPool.setCardScale(0.55)
  }

  setCardID() {
      this.game.table.redCardNodeList[0].getComponent(Card).setCardId(13)
      this.game.table.redCardNodeList[1].getComponent(Card).setCardId(14)
      this.game.table.redCardNodeList[2].getComponent(Card).setCardId(15)
      this.game.table.redCardType = 1;
      this.game.table.blackCardNodeList[0].getComponent(Card).setCardId(22)
      this.game.table.blackCardNodeList[1].getComponent(Card).setCardId(26)
      this.game.table.blackCardNodeList[2].getComponent(Card).setCardId(27)
      this.game.table.blackCardType = 5;
      this.game.table.winPlaceBetType = 1;
  }

  getToDealCardState() {
    this.game.changeState('deal_state',RedBlackDealCardState,{})  
  }

  getToOpenCardState() {
      this.game.changeState('open_card_state',RedBlackOpenCardState,{})  
  }

  getToEndGameState() {
     this.game.changeState('end_game_state',RedBlackEndGameState,{})
  }


  testThrowChip() {
     this.game.table.setupChipToThrow(1,1000, false, true)
     this.game.table.setupChipToThrow(2,500, false, true)
     this.game.table.setupChipToThrow(3,1000, false, true)
  }

  getIntoTable() {
   this.game.getIntoGameRoom();
  }

  setupRoom() {
    let room = new RedBlackRoom();
    room.gameId = 1;
    room.roomId = 1;
    room.betMoney = 1;
    room.matchId = 1;
    room.timeout = 5;
    room.betList = [
      2,
      3,
      68,
      134,
      200,
      250
    ];
    room.countdownInfo = {
      type: 0,
      timeout: 10
    }
    room.redAppearCardList=[]
    room.blackAppearCardList=[]
    room.playerNum = 250;
    room.placeBetTypeList = [ {  
         type:1,
         rate:2,
         betMoneyTotal:0,
         betMoney:0,
         cardList:[  
            {  
               id:-1,
               number:0,
               type:-1
            },
            {  
               id:-1,
               number:0,
               type:-1
            },
            {  
               id:-1,
               number:0,
               type:-1
            }
         ],
         resultType:-1
      },
      {  
         type:2,
         rate:2,
         betMoneyTotal:0,
         betMoney:0,
         cardList:[  
            {  
               id:-1,
               number:0,
               type:-1
            },
            {  
               id:-1,
               number:0,
               type:-1
            },
            {  
               id:-1,
               number:0,
               type:-1
            }
         ],
         resultType:-1
      },
      {  
         type:3,
         rate:1,
         specialCardList:[  
            {  
               cardType:1,
               rate:11
            },
            {  
               cardType:2,
               rate:6
            },
            {  
              cardType:3,
               rate:4
            },
            {  
               cardType:4,
               rate:3
            },
            {  
               cardType:5,
               rate:2
            }
         ],
         betMoneyTotal:0,
         betMoney:0,
         cardList:[  

         ],
         resultType:-1
         }
      ]
    GlobalInfo.room = room;
    this.game.setRoom(room);
  }

  setupPlayers() {
 
  }

  runTestCases() {
    cc.log('begin tesst red black')
    this.setup();
   
    

    this.finalize();
  }

  runSequence(actions) {
    let ccActions = [];
    for (let action of actions) {
      let func = action[0];
      let delay = action[1];
      ccActions.push(cc.callFunc(() => {func.call(this)}));
      ccActions.push(cc.delayTime(delay));
    }

    this.game.node.runAction(cc.sequence(ccActions));
  }

  finalize() {

  }

  
}