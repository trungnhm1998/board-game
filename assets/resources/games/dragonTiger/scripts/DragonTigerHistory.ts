import DragonTigerConstants from './DragonTigerConstants';

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerHistory extends cc.Component {

  rawResults = [];
  formatedResults = [];
  containerChildNode = [];
  maxRow = 6;

  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  sampleResult: cc.Node = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  @property([cc.String])
  atlasDefines: Array<string> = [];

  @property(Number)
  historyChainsLength: number = 0;

  _historyNodePool: cc.NodePool = null;
  
  clear(cleanup = false) {
    if(cleanup)
      this.container.removeAllChildren(true);
    this.setHistory([]);
    this.refresh();
  }

  setHistory(history: Array<Array<number>>) {
    if (this._historyNodePool == null)
      this._historyNodePool = new cc.NodePool();
    this.formatedResults = history;
    this.refresh();
  }

  refresh() {
    for (let index = this.container.children.length - 1; index >= 0; index--) {
      const historyNode = this.container.children[index];
      this._historyNodePool.put(historyNode);
    }
    let layout: cc.Layout = this.container.getComponent(cc.Layout);

    let formatResults = this.formatedResults.
      slice(this.formatedResults.length - this.historyChainsLength > 0
        ? this.formatedResults.length - this.historyChainsLength
        : 0, this.formatedResults.length
      );
    let x = layout.paddingLeft + layout.cellSize.width / 2;
    let lastResultNotDraw = -1;
    const { PLACE_TYPE } = DragonTigerConstants;
    for (let chain of formatResults) {
      let y = -layout.paddingTop - layout.cellSize.height / 2;
      for (let result of chain) {
        let historyItemNode: cc.Node = null;
        if (this._historyNodePool.size() > 0) {
          historyItemNode = this._historyNodePool.get();
        } else {
          historyItemNode = cc.instantiate(this.sampleResult);
        }
        historyItemNode.cleanup();
        historyItemNode.opacity = 255;
        historyItemNode.active = true;
        historyItemNode.width = layout.cellSize.width;
        historyItemNode.height = layout.cellSize.height;
        this.container.addChild(historyItemNode);
        this.containerChildNode.push(historyItemNode);
        historyItemNode.x = x;
        historyItemNode.y = y;
        y -= layout.spacingY;
        y -= layout.cellSize.height;
        let sprite: cc.Sprite = historyItemNode.getComponent(cc.Sprite);

        if (result != PLACE_TYPE.DRAW) {
          lastResultNotDraw = result;
        }

        if (result == PLACE_TYPE.DRAGON) {
          sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[0]);
        } else if (result == PLACE_TYPE.TIGER) {
          sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[1]);
        } else if (result == PLACE_TYPE.DRAW) {
          if (lastResultNotDraw == PLACE_TYPE.TIGER) {
            sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[2]);
          } else {
            sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[3]);
          }
        }
      }
      x += layout.cellSize.width + layout.spacingX;
    }

    let lastResult: cc.Node = this.containerChildNode[this.containerChildNode.length - 1];
    if (lastResult) {
      let fadeRepeat = cc.repeatForever(
        cc.sequence(
          cc.fadeOut(1.0),
          cc.fadeIn(1.0)
        )
      );
      lastResult.runAction(fadeRepeat);
    }
  }
}