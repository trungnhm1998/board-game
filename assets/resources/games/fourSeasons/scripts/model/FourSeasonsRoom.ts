import { FourSeasonsPlayerInfo } from './FourSeasonsPlayerInfo';
import { Room, CountdownInfo } from './../../../../../scripts/model/Room';

export class FourSeasonsPlaceBetInfo {
  placeBetType: number;
  cardList: number[];
  cardType: number;
  resultType: number;
  bonusTime: number;
}

export class FourSeasonsRoom extends Room {
  roomName: string;
  countdownInfo: CountdownInfo;
  historyList: any;
  minBet: number;
  maxBet: number;
  playerInfo: FourSeasonsPlayerInfo;
  playerNum: number;
  betList: Array<number>;
  placeBetInfoList: FourSeasonsPlaceBetInfo[];
}