import { AvatarIcon } from "../../../../scripts/common/AvatarIcon";
import { FourSeasonsPlayerInfo } from "./model/FourSeasonsPlayerInfo";
import { moon } from "../../../../scripts/core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsPlayer extends cc.Component {
  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Label)
  displayName: cc.Label = null;

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  _playerInfo: FourSeasonsPlayerInfo;

  setPlayerInfo(playerInfo: FourSeasonsPlayerInfo) {
    this._playerInfo = playerInfo;

    const { money, avatar, displayName } = this._playerInfo;

    this.displayName.string = displayName;
    this.setMoney(money);
    this.avatar.setImageUrl(avatar);
  }

  setMoney(money: number) {
    this.money.string = moon.string.formatMoney(money);
  }
}