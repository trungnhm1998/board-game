const {ccclass, property} = cc._decorator;

@ccclass
export class Chess extends cc.Component {

  @property(cc.Sprite)
  backBg: cc.Sprite = null;

  @property(cc.Sprite)
  frontBg: cc.Sprite = null;

  @property(cc.Sprite)
  value: cc.Sprite = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  @property(cc.Label)
  num: cc.Label = null;

  chessId: number;

  resizeBackground(width, height) {
    this.node.width = width;
    this.node.height = height;
    this.backBg.node.width = width;
    this.backBg.node.height = height;
  }

  showUpperChess(upperChess) {

  }

  hide() {
    this.frontBg.node.active = false;
  }

  open(chessId?) {
    if (chessId) {
      this.chessId = chessId;
    }
    // TODO: temporary
    if (this.chessId >= 14) {
      this.chessId -= 13;
    }
    // END temporary

    this.frontBg.node.active = true;
    this.value.spriteFrame = this.atlas.getSpriteFrame('' + this.chessId);
    this.updateChessNum(this.chessId);
  }

  clearFront() {

  }

  updateChessNum(chessId) {
    let cases = {
      8: 'F',
      9: 'C',
      10: 'B',
      11: 'E',
      12: 'S',
      13: 'W',
      14: 'N',
    };
    let txt = chessId;
    if (chessId >= 0 && chessId <= 3) {
      txt = chessId + 1;
    } else if (chessId >= 4 && chessId <= 7) {
      txt = (chessId % 4) + 1;
    } else if (chessId >= 15 && chessId <= 23) {
      txt = ((chessId - 15) % 9) + 1;
    } else if (chessId >= 24 && chessId <= 32) {
      txt = ((chessId - 24) % 9) + 1;
    } else if (chessId >= 33 && chessId <= 41) {
      txt = ((chessId - 33) % 9) + 1;
    } else {
      txt = cases[chessId];
    }
    this.num.string = '' + txt;
  }
}