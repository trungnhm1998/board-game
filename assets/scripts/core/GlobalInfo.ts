import {UserInfo} from '../model/UserInfo';
import {ClientInfo} from '../model/ClientInfo';
import {GameItem} from "../model/GameInfo";
import {PaymentConfig} from "../model/Payment";
import {Room, RoomInfo} from "../model/Room";
import {DialogManager} from "../services/DialogManager";
import {SceneManager} from "../services/SceneManager";
import {LanguageService} from "../services/LanguageService";
import {MoneyService} from "../services/MoneyService";
import {NativeService} from "../services/NativeService";
import {GameSocket, LobbySocket} from "../services/SocketService";
import {MainMenu} from "../scenes/MainMenu";
import {LocalStorage} from "./LocalStorage";
import {GameService} from "../services/GameService";
import {Boost} from "../scenes/Boost";
import {CString} from "./String";
import {Random} from "./Random";
import {ResourceManager} from "./ResourceManager";
import {MoonTimer} from "./TimerComponent";
import {Header} from "../lobby/Header";
import { CardPool } from './CardPool';
import { AudioManager } from '../services/AudioManager';
import {HttpClient} from './HttpClient';

export interface ConfigInfo {
  transferFee: number;
  minTransferFee: number;
}

export class SceneInfo {
  sceneType: any;
  action: any;

  constructor() {

  }

  clear() {
    this.sceneType = '';
    this.action = null;
  }
}

export interface IGlobalInfo {
  gameItem?: GameItem;
  me: UserInfo;
  nextSceneInfo?: SceneInfo;
  clientInfo?: ClientInfo;
  configInfo?: ConfigInfo;
  listGame?: Array<GameItem>;
  listAvatar?: Array<string>;
  requestedCMD?: any;
  gameOnly?: boolean;
  room?: Room;
  roomInfoList: {[key:number]: Array<RoomInfo>};
  rooms?: Array<Room>;
  paymentConfig?: PaymentConfig;
}

export const GlobalInfo: IGlobalInfo = {
  me: new UserInfo(),
  nextSceneInfo: new SceneInfo(),
  room: new Room(),
  gameOnly: false,
  requestedCMD: {},
  listGame: [],
  listAvatar: [],
  rooms: [],
  roomInfoList: []
};

export interface IME {
  dialog?: DialogManager,
  scene?: SceneManager,
  locale?: LanguageService,
  money?: MoneyService,
  native?: NativeService,
  http?: HttpClient,
  gameSocket?: GameSocket,
  lobbySocket?: LobbySocket,
  lobby?: MainMenu;
  storage?: LocalStorage;
  service?: GameService;
  root?: Boost;
  string?: CString;
  header?: Header;
  random?: Random;
  res?: ResourceManager;
  timer?: MoonTimer;
  cardPool?:CardPool;
  audio?: AudioManager;
}

export const moon: IME = {};