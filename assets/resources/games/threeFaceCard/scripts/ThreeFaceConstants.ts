export const THREE_CARD_CARD_TYPE = {
  BUST_NINE: 1,
  BOMB: 2,
  THREE_FACE_CARD: 3,
  SEVEN_TO_NINE: 4,
  ZERO_TO_SIX: 5,
};

export const THREE_FACE_AUDIO = {
  LOBBY_BGM: 'sg_room_bg',
  GAME_BGM: 'sg_game_bg',
  DEALER_SELECTING: 'sg_selecting',
  DEALER_SELECTED: 'sg_banker_selected',
  GAMESTART: 'sg_kaishi',
  CLICK: 'sg_click',
  GOLD_FLY: 'sg_send_chip',
  CARD_FLY: 'sg_send_card',
  WIN: 'sg_win'
};