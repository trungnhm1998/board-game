import {MiniPokerPlayerInfo} from './model/MiniPokerPlayerInfo';
import {AvatarIcon} from "../../../../scripts/common/AvatarIcon";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {moon, GlobalInfo} from "../../../../scripts/core/GlobalInfo";
import {POKER_CHESS_TYPE, POKER_CHESS_TYPE_NAME, RESOURCE_BLOCK} from "../../../../scripts/core/Constant";
import { Card } from '../../../../scripts/core/Card';

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerPlayer extends cc.Component {

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Sprite)
  status: cc.Sprite = null;

  @property(cc.Node)
  betRow: cc.Node = null;

  @property(cc.Node)
  highlight: cc.Node = null;

  @property(cc.Node)
  chessContent: cc.Node = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  winAmount: cc.Label = null;

  @property(cc.Label)
  loseAmount: cc.Label = null;

  @property(cc.Sprite)
  betUnit: cc.Sprite = null;

  @property(dragonBones.ArmatureDisplay)
  playerWin: dragonBones.ArmatureDisplay = null;

  @property(cc.Node)
  pointBanner: cc.Node = null



  playWinAmountEffectCalled: boolean = false;
  currentWinAmountValue: number = 0; // uses for inc/dec effect until this var = userInfo.winAmount
  winAmountEffectInterval: number = 0;
  currentBanner: cc.Node;
  playerInfo: MiniPokerPlayerInfo;
  bannerAction: cc.Action;
  pointLabelAction: cc.Action;
  private cardNodes: any;

  onLanguageChange() {  
    if (!this.playerInfo) {
      return;
    }
    
    this.bet.font =  moon.res.getFont('bettimes_font', RESOURCE_BLOCK.GAME)
    if (this.betRow.active) {
            this.setBet()
    }
    let pointLabel = this.pointBanner.getChildByName('label');
    if (pointLabel.active) {
      NodeUtils.setLocaleLabel(pointLabel, 'betFactor', "betFactor", {params: [this.playerInfo.resultCard.bonusFactor.toString()]})
      let resultTypeText = pointLabel.getChildByName('point').getComponent(cc.Sprite)
      resultTypeText.spriteFrame = moon.res.getGameSpriteFrame(POKER_CHESS_TYPE_NAME[this.playerInfo.resultCard.cardType])
    }
  }

  onEnter() {
    // this.hideBet()
    // this.resetPointBanner()
  }

  hideBet() {
    //this.bet.node.active = false;
    //this.betUnit.node.active = false;
    if (this.playerInfo)
      this.playerInfo.betFactor = 0;
    cc.log('hide bet row')
    this.betRow.active = false;
    // this.status.node.active = false;
  }

  setBet() {
    this.betRow.active = true;
    let font = moon.res.getFont("bettimes_font")
    //this.betRow.getChildByName('betValue').getComponent(cc.Label)=
    NodeUtils.setLocaleLabel(this.betRow, 'betValue', 'betTimes', {
      font: font,
      params: [this.playerInfo.betFactor.toString()]
    });
  }

  placeBet(times) {
    this.status.node.active = false;
    this.betRow.active = false;
    this.bet.node.active = true;
    this.betUnit.node.active = true;
    this.bet.string = times;
    let lang = moon.locale.getCurrentLanguage();
    if (times > 1) {
      this.betRow.active = true;
      this.betUnit.spriteFrame = moon.res.getSpriteFrame('bets_' + lang, RESOURCE_BLOCK.GAME);
    } else if (times == 1) {
      this.betRow.active = true;
      this.betUnit.spriteFrame = moon.res.getSpriteFrame('bet_' + lang, RESOURCE_BLOCK.GAME);
    } else if (times == 0) {
      this.status.node.active = true;
      this.status.spriteFrame = moon.res.getSpriteFrame('check_' + lang, RESOURCE_BLOCK.GAME);
    } else {
      this.status.node.active = true;
      this.status.spriteFrame = moon.res.getSpriteFrame('thinking_' + lang, RESOURCE_BLOCK.GAME);
    }
  }

  setPlayerInfo(playerInfo: MiniPokerPlayerInfo) {
    this.clear()
    this.playerInfo = playerInfo;
    if (!playerInfo.resultCard) {
      this.playerInfo.resultCard = {
        bonusFactor: 1,
        cardList: [],
        cardType: 0,
      };
    } else {
      this.playerInfo.resultCard = playerInfo.resultCard;
    }
    playerInfo.formatedDisplayName = this.formatPlayerName(playerInfo)
    this.playerName.string = playerInfo.formatedDisplayName;
    this.money.string = moon.string.formatMoney(playerInfo.money);
    this.avatar.setImageUrl(playerInfo.avatar);
    this.resetPointBanner();
    if (this.playerInfo.resultCard.cardType > 0)
      this.showPointBanner(false);
    this.betRow.active = false;
    if (this.playerInfo.betFactor > 0)
      this.setBet();

    if (this.playerInfo.winMoney > 0 || this.playerInfo.winMoney < 0) {
      let setWinMoney = (target: cc.Label) => {
        target.node.active = true;
        target.string = this.playerInfo.winMoney > 0 ? '+' + `${this.playerInfo.winMoney}` 
                                                      : `${this.playerInfo.winMoney}` ;
        
      }

      setWinMoney(this.playerInfo.winMoney > 0 ? this.winAmount : this.loseAmount);
    }
  }


  formatPlayerName(playerInfo: MiniPokerPlayerInfo){
    return moon.string.formatPlayerName(playerInfo.displayName, GlobalInfo.me.userId === playerInfo.userId)
  }

  getSelectAnim() {
    let delayTime = 0.4;
    let round = 5;
    return cc.sequence(
      cc.callFunc(() => {
        this.highlight.opacity = 180;
      }),
      cc.delayTime(delayTime),
      cc.callFunc(() => {
        this.highlight.opacity = 0;
      }),
      cc.delayTime(delayTime),
    ).repeat(round);
  }

  showBorder(isShown = true) {
    // this.highlight.runAction(
    //   cc.fadeTo(0.05, isShown ? 180 : 0)
    // )
  }

  /**
   * return the array of positions for placing chess on "hand"
   */
  getReceivedPositions(): Array<cc.Vec2> {
    let chessWidth = 60 * 0.8;
    let chessPositions: Array<cc.Vec2> = [];
    let width = chessWidth * 5;
    for (let i = 0; i < 5; i++) {
      chessPositions.push(cc.v2(((i * chessWidth) + chessWidth / 2) - width / 2, 0));
    }
    return chessPositions;
  }

  setCardNodes(cards: any) {
    this.cardNodes = cards;
  }

  getResultType() {

  }

  getTextResultFromChessList() {


  }

  resetPointBanner() {
    let banner = this.pointBanner.getChildByName('red_pointBanner');
    banner.active = false;
    banner = this.pointBanner.getChildByName('yellow_pointBanner');
    banner.active = false;
    banner = this.pointBanner.getChildByName('blue_pointBanner');
    banner.active = false;
    banner = this.pointBanner.getChildByName('purple_pointBanner');
    banner.active = false;
    this.pointBanner.getChildByName('label').active = false

  }

  clearAnimation() {

    let pointLabel = this.pointBanner.getChildByName('label');
    pointLabel.stopAllActions()
    if (this.currentBanner) {
      this.currentBanner.stopAllActions()
    }
  }

  showPointBanner(isUseAnimation = true) {
    cc.log('show point banner')
    if (this.pointBanner) {
      this.pointBanner.active = true;
      this.resetPointBanner();
      let banner: cc.Node;
      if (!this.playerInfo.resultCard) return;
      switch (this.playerInfo.resultCard.cardType) {
        case POKER_CHESS_TYPE.STRAIGHT_FLUSH:
          banner = this.pointBanner.getChildByName('blue_pointBanner');
          break;
        case POKER_CHESS_TYPE.FOUR_OF_A_KIND:
          banner = this.pointBanner.getChildByName('blue_pointBanner');
          break;
        case POKER_CHESS_TYPE.FULL_HOUSE:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          break;
        case POKER_CHESS_TYPE.STRAIGHT:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          break;
        case POKER_CHESS_TYPE.FLUSH:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          break;
        case POKER_CHESS_TYPE.THREE_OF_A_KIND:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          break;
        case POKER_CHESS_TYPE.TWO_PAIRS:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          break;
        case POKER_CHESS_TYPE.PAIR:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          break;
        case POKER_CHESS_TYPE.HIGH_CARD:
          banner = this.pointBanner.getChildByName('purple_pointBanner');
          break;
      }

      this.currentBanner = banner;

      banner.active = true;

      if (isUseAnimation) {
        banner.scaleX = 0;
        banner.scaleY = 0;
        this.bannerAction = cc.sequence(
          cc.scaleTo(0.4, 1),
          cc.delayTime(0.1),
        )
        banner.runAction(
          this.bannerAction
        );
      } else {
        banner.scaleX = 1;
        banner.scaleY = 1;
      }
    

      let pointLabel = this.pointBanner.getChildByName('label');
      pointLabel.active = true;

      NodeUtils.setLocaleLabel(pointLabel, 'betFactor', "betFactor", {params: [this.playerInfo.resultCard.bonusFactor.toString()]})

      let resultTypeText = pointLabel.getChildByName('point').getComponent(cc.Sprite)
      resultTypeText.spriteFrame = moon.res.getGameSpriteFrame(POKER_CHESS_TYPE_NAME[this.playerInfo.resultCard.cardType])

      if (isUseAnimation) {
        pointLabel.scaleX = 0;
        pointLabel.scaleY = 0;
        this.pointLabelAction = cc.sequence(
          cc.scaleTo(0.15, 1.5),
          cc.delayTime(0.1),
          cc.scaleTo(0.15, 1)
        )
        pointLabel.runAction(
          this.pointLabelAction
        )
      } else {
        pointLabel.scaleX = 1;
        pointLabel.scaleY = 1;
      }
  
    }
  }

  showCards() {
    // TODO: replace by card
    let actions = [];
    for (let cardNode of this.cardNodes) {
      let card: Card = cardNode.getComponent(Card);
      actions.push(cc.callFunc(() => {
        card.showCard();
      }));
      actions.push(cc.delayTime(0.02));
    }
    this.node.runAction(
      cc.sequence(actions)
    );
    //
    //
  }


  setCardsByIdList(cardList) {
    cc.log('set card by ids list')
    let cardPosition = this.getReceivedPositions();
    let cardCount = 0;
    let cardNodes = [];
    cardList.forEach(cardId => {
      let cardNode =moon.cardPool.getCard(cardId);
      let cardComponent: Card = cardNode.getComponent(Card);
      this.chessContent.addChild(cardNode);
      cardNodes.push(cardNode);
      cardComponent.showCard()
      cardNode.position = cardPosition[cardCount];
      cardCount++;
    });
    this.setCardNodes(cardNodes);
    // TODO: replace by card
    // let chessPositions = this.getReceivedPositions();
    // let chessCount = 0;
    // let chessNodes = [];
    // chessList.forEach(chess => {
    //   let chessNode = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chess);
    //   chessNode.setScale(0.8, 0.8);
    //   let chessComponent: Chess = chessNode.getComponent(Chess);
    //   this.chessContent.addChild(chessNode);
    //   chessNodes.push(chessNode);
    //   chessComponent.chessId = chess;
    //   chessComponent.pushDownBig();
    //
    //   chessNode.position = chessPositions[chessCount];
    //   chessCount++;
    // });
    // this.setChessNodes(chessNodes);
  }


  setMoney(money: any) {
    this.money.string = moon.string.formatMoney(money);
  }

  playWinEffect() {
    this.playerWin.node.active = true;
    this.playerWin.playAnimation('playerWin', 1);
  }

  playAmountEffect(amount) {
    this.playWinAmountEffectCalled = true;
    this.currentWinAmountValue = 0;
    let playMoneyAnim = (target) => {
      target.stopAllActions();
      target.position = cc.v2(target.position.x, 0);
      target.opacity = 255;
      target.runAction(
        cc.moveBy(0.5, cc.v2(0, 90)).easing(cc.easeBackOut()),
      )
    };
    if (amount > 0) {
      this.winAmount.node.active = true;
      playMoneyAnim(this.winAmount.node);
    } else {
      this.loseAmount.node.active = true;
      playMoneyAnim(this.loseAmount.node);
    }
  }

  update(dt: number) {
    if (this.playWinAmountEffectCalled) {
      const animSpeed = 1.0;
      const positiveWinMoneyValue = Math.abs(this.playerInfo.winMoney);
      if (this.currentWinAmountValue < positiveWinMoneyValue && this.winAmountEffectInterval <= animSpeed) {
        // the whole inc/ dec anim will only play in animSpeed
        // based on animSpeed faster/slower will have bigger/smaller 
        let valueToUpdate = positiveWinMoneyValue >= 40 ? Math.round((positiveWinMoneyValue * (dt * animSpeed))) : 1;
        this.currentWinAmountValue = this.currentWinAmountValue + valueToUpdate
        if (this.playerInfo.winMoney > 0) {
          this.winAmount.string = "+" + this.currentWinAmountValue;
        } else {
          this.loseAmount.string = "-" + this.currentWinAmountValue;
        }
        this.winAmountEffectInterval += dt;
      } else {
        this.winAmountEffectInterval = 0;
        this.currentWinAmountValue = positiveWinMoneyValue;
        this.playWinAmountEffectCalled = false;
        if (this.playerInfo.winMoney > 0) {
          this.winAmount.string = "+" + this.currentWinAmountValue;
        } else {
          this.loseAmount.string = "-" + this.currentWinAmountValue;
        }
        this.playerInfo.money += this.playerInfo.winMoney;
        this.money.string = moon.string.formatMoney(this.playerInfo.money);
      }
    }

  }

  clear() {
    // this.status.node.active = false;
    // this.betRow.active = false;
    this.winAmount.node.active = false;
    this.loseAmount.node.active = false;
    //this.highlight.opacity = 0;
    this.clearAnimation()
    this.resetPointBanner();
    this.hideBet()
    this.pointBanner.active = false
    cc.log('lear point banner')
    this.chessContent.children.forEach(chessNode => {
      chessNode.cleanup();
    });
    this.chessContent.cleanup();
  }
}

