import {NodeUtils} from "../core/NodeUtils";
import {CountdownShader} from "../../shader/CountdownShader";
import {CMaterial} from "../shader/CMaterial";
import {moon} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class Countdown extends cc.Component {

  @property(cc.Node)
  countdown: cc.Node = null;

  @property(cc.Label)
  cdTime: cc.Label = null;

  task;

  runCountdown(timeout) {
    this.stopCountdown();
    this.node.opacity = 255;
    this.countdown.opacity = 255;
    let material: CMaterial = NodeUtils.applyShader(this.countdown.getComponent(cc.Sprite), CountdownShader);
    material.setParamValue('percent', 0);
    this.cdTime.string = timeout;
    this.task = moon.timer.tweenNumber(0, 1,
      (percent) => {
        let time = Math.ceil(timeout * (1 - percent));
        if (time < 3) {
          this.cdTime.node.color = cc.color().fromHEX('#ff6a5d')
        } else {
          this.cdTime.node.color = cc.color().fromHEX('#FFFFFF')
        }
        this.cdTime.string = '' + Math.ceil(timeout * (1 - percent));
        material.setParamValue('percent', 1 - percent);
      }, () => {
        this.node.opacity = 0;
      }, timeout);
  }

  stopCountdown() {
    if (this.task) {
      moon.timer.removeTask(this.task);
      this.task = null;
    }
    this.countdown.opacity = 0;
    this.node.opacity = 0;
  }
}