import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {GlobalInfo, moon} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;


@ccclass
export class TransferDialog extends SceneComponent {

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Node)
  transfer: cc.Node = null;

  @property(cc.Node)
  transferConfirm: cc.Node = null;

  @property(cc.Node)
  transferSuccess: cc.Node = null;

  @property(cc.Node)
  transferFailed: cc.Node = null;


  @property(cc.EditBox)
  transferAmount: cc.EditBox = null;

  @property(cc.EditBox)
  receivePlayer: cc.EditBox = null;

  @property(cc.Node)
  invalidPlayer: cc.Node = null;

  @property(cc.Node)
  invalidAmount: cc.Node = null;

  @property(cc.Label)
  invalidPlayerLabel: cc.Label = null;

  @property(cc.Label)
  invalidAmountLabel: cc.Label = null;

  @property(cc.Label)
  fee: cc.Label = null;

  @property(cc.Label)
  email: cc.Label = null;

  @property(cc.Label)
  accountId: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Label)
  feeConfirm: cc.Label = null;

  receiverEmail = '';
  receiverId = '';

  waitingForReceiver = false;

  task: any;

  feeAmount = 0;

  onEnter() {
    super.onEnter();
    this.hideAll();
    this.waitingForReceiver = false;
    this.transferAmount.string = '0';
    this.receivePlayer.string = '';
    this.onTransferAmountChange();
  }

  onLanguageChange() {
    let lang = moon.locale;
    this.title.string = lang.get('transfer').toUpperCase();
  }

  hideAll() {
    this.transfer.active = false;
    this.transferConfirm.active = false;
    this.transferSuccess.active = false;
    this.transferFailed.active = false;
  }

  showTransfer() {
    this.hideAll();
    this.transfer.active = true;
    this.title.string = moon.locale.get('transfer').toUpperCase();
    NodeUtils.setLocaleLabel(this.transfer, 'transferAmount_lbl', 'transferAmount');
    NodeUtils.setLocaleLabel(this.transfer, 't_editbox', 'inputTransferAmount', {placeHolder: true});
    NodeUtils.setLocaleLabel(this.transfer, 'receive_lbl', 'toPlayer');
    NodeUtils.setLocaleLabel(this.transfer, 'ie_editbox', 'inputIdOrEmail', {placeHolder: true});
    NodeUtils.setLocaleLabel(this.transferConfirm, 'confirm_lbl', 'confirm');
  }

  showTransferConfirm() {
    if (this.receiverEmail == GlobalInfo.me.email) {
      this.showInvalidPlayer(
        moon.locale.get('transferToYourSelf')
      );
      return;
    }
    this.hideAll();
    this.title.string = moon.locale.get('transferConfirm').toUpperCase();
    this.transferConfirm.active = true;
    NodeUtils.setLocaleLabel(this.transferConfirm, 'transferTo_lbl', 'transferTo');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'email_lbl', 'email');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'id_lbl', 'id');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'money_lbl', 'money');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'fee_lbl', 'fee');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'confirm_lbl', 'confirm');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'cancel_lbl', 'cancel');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'transferNotice_lbl', 'transferNotice');
    NodeUtils.setLocaleLabel(this.transferConfirm, 'transferWarn', 'transferWarn');
    this.email.string = this.receiverEmail;
    this.accountId.string = this.receiverId;
    this.money.string = this.transferAmount.string + ' Zc';
    this.feeConfirm.string = this.feeAmount + ' Zc';
  }

  showTransferSuccess() {
    this.hideAll();
    this.title.string = moon.locale.get('transferSuccess').toUpperCase();
    this.transferSuccess.active = true;
    NodeUtils.setLocaleLabel(this.transferSuccess, 'transferSuccess_lbl', 'transferSuccessInfo');
    NodeUtils.setLocaleLabel(this.transferSuccess, 'continue_lbl', 'continue');
  }

  showTransferFailed() {
    this.hideAll();
    this.title.string = moon.locale.get('transferFailed').toUpperCase();
    this.transferFailed.active = true;
    NodeUtils.setLocaleLabel(this.transferFailed, 'transferFailed_lbl', 'transferFailedInfo');
    NodeUtils.setLocaleLabel(this.transferFailed, 'retry_lbl', 'retry');
  }

  onTransferAmountChange() {
    this.hideInvalidAmount();
    let feeRate = (GlobalInfo.configInfo.transferFee || 1) / 100;
    let decimalSeperator = '.';
    let hasDotLast = this.transferAmount.string[this.transferAmount.string.length - 1] == decimalSeperator;
    let decimal = this.transferAmount.string.split(decimalSeperator)[1];
    let amount =  moon.string.parseMoney(this.transferAmount.string);
    if (isNaN(amount)) {
      this.transferAmount.string = this.transferAmount.string.slice(0, this.transferAmount.string.length - 1);
      hasDotLast = false;
    }
    amount =  moon.string.parseMoney(this.transferAmount.string) || 0;
    amount = Math.min(amount, GlobalInfo.me.money);
    this.feeAmount = Math.max(GlobalInfo.configInfo.minTransferFee || 0, amount * feeRate);
    if (decimal && decimal[decimal.length - 1] == '0') {
      this.transferAmount.string = '' +  moon.string.formatMoney(Math.floor(amount)) + decimalSeperator + decimal;
    } else {
      this.transferAmount.string = '' +  moon.string.formatMoney(amount) + (hasDotLast ? decimalSeperator : '');
    }
    this.fee.string =  moon.string.format(
      moon.locale.get('transferFee'),
       moon.string.formatMoney(this.feeAmount),
      feeRate * 100
    );
  }

  onReceivePlayerChange() {
    this.receiverEmail = '';
    this.receiverId = '';
    this.invalidPlayer.active = false;
    // if (this.task) {
    //   moon.timer.removeTask(this.task);
    // }
    // this.task = moon.timer.scheduleOnce(() => {
    //   moon.lobbySocket.checkAccountValid(
    //     this.receivePlayer.string
    //   );
    // }, 1);
  }

  hideInvalidPlayer() {
    this.invalidPlayer.active = false;
    this.invalidPlayerLabel.string = "";
  }

  showInvalidPlayer(msg) {
    this.invalidPlayer.active = true;
    this.invalidPlayerLabel.string = msg;
  }

  showInvalidAmount(msg) {
    this.invalidAmount.active = true;
    this.invalidAmountLabel.string = msg;
  }

  hideInvalidAmount() {
    this.invalidAmount.active = false;
    this.invalidAmountLabel.string = "";
  }

  onTransfer() {
    if (this.invalidPlayer.active) {
      moon.lobbySocket.checkAccountValid(
        this.receivePlayer.string,
         moon.string.parseMoney(this.transferAmount.string)
      );
      return;
    }

    let money =  moon.string.parseMoney(this.transferAmount.string);
    if (money) {
      if (this.receiverEmail && this.receiverId) {
        this.showTransferConfirm();
      } else {
        this.waitingForReceiver = true;
        moon.lobbySocket.checkAccountValid(
          this.receivePlayer.string,
           moon.string.parseMoney(this.transferAmount.string)
        );
      }
    } else {
      this.showInvalidAmount(
        moon.locale.get('requireTransferAmount')
      );
    }
  }

  onConfirmTransfer() {
    let money =  moon.string.parseMoney(this.transferAmount.string);
    moon.lobbySocket.transfer(
      this.receivePlayer.string,
      money
    );
  }

  setReceiverInfo(email, id) {
    this.receiverEmail = email;
    this.receiverId = id;
    if (this.waitingForReceiver) {
      this.waitingForReceiver = false;
      this.showTransferConfirm();
    }
  }
}
