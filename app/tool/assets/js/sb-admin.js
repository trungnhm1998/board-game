function showAlert() {
  /*var x = document.getElementById("snackbar");
  x.className = "show";  
  setTimeout(function(){ 
    $('#snackbar').html('');
    x.className = x.className.replace("show", ""); 
  }, 3000);*/
}

let core = {
  string: {
    format: function (str, ...args) {
      let content = str;
      for (let i = 0; i < args.length; i++) {
        let replacement = new RegExp(`\{${i}+\}`, 'g');
        content = content.replace(replacement, args[i]);
      }
      return content;
    }
  },
  api: {
    token: '9HnCAePXkLMYGRDSfZA2vYCD',
    baseUrl: 'https://backend.devslo.club/',
    get: function (url, callback) {
      $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        headers: {
          'Token': this.token
        },
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
          callback(result);
        },
        error: function (error) {
          callback(error);
        }
      });
    },
    post: function (url, data, callback) {
      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(data),
        headers: {
          'Token': this.token
        },
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
          callback(result);
        },
        error: function (error) {
          callback(error);
        }
      });
    },
    getMerchants: function (callback) {
      this.get(this.baseUrl + 'z88-backend/tool/get-data-list', function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    addMerchant: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/add-merchant', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    deleteMerchant: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/delete-merchant', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    addCurrency: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/add-currency-type', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    deleteCurrency: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/delete-currency-type', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    addConnection: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/add-connection', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    deleteConnection: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/delete-connection', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
    updateConnection: function (data, callback) {
      this.post(this.baseUrl + 'z88-backend/tool/update-connection', data, function (response) {
        if (callback) {
          callback(response);
        }
      });
    },
  }
};

(function ($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle").on('click', function (e) {
    e.preventDefault();
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function (event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });

})(jQuery); // End of use strict
