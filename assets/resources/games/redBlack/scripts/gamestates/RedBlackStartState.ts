
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";

import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { RedBlackGame } from "../RedBlackGame";
import { RedBlackRoom } from "../model/RedBlackRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackStartState extends GameState implements IGameState {

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  redAnim: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  blackAnim: dragonBones.ArmatureDisplay = null;



  onEnter(game: RedBlackGame, data: any) {
    game.setRoom(<RedBlackRoom>GlobalInfo.room);
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;

    this.startAnim.playAnimation('action', 1);
    this.redAnim.playAnimation('action0',0);
    this.blackAnim.playAnimation('action0',0);
    let i = 3;
    moon.timer.runCountdown( 3, () => {
      game.table.countDown.getComponent(cc.Label).string = i.toString() ;
      console.log(i);
      i--;
    })


  }

  onUpdate(game: RedBlackGame, updateData: UpdateData) {
  }

  onLeave(game: RedBlackGame, data: any) {
  }
}