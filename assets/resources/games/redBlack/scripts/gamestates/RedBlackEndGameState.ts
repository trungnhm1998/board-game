import { Card } from './../../../../../scripts/core/Card';
import { RED_BLACK_TYPE_NAME } from '../../../../../scripts/core/Constant';
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { RedBlackGame } from "../RedBlackGame";
import { RedBlackRoom } from "../model/RedBlackRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackEndGameState extends GameState implements IGameState {
  game: RedBlackGame;

  @property(dragonBones.ArmatureDisplay)
  canonAnim: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  winAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: RedBlackGame, data: any) {
    this.game = game;
    let highlight1 = this.game.table.getPotByPlaceBetType(1);
    let highlight2 = this.game.table.getPotByPlaceBetType(2);
    let highlight3 = this.game.table.getPotByPlaceBetType(3);

    if(this.checkPairOf7(game.table.redCardNodeList, game.table.blackCardNodeList)) {
      this.canonAnim.playAnimation('newAnimation',1)
      this.canonAnim.removeEventListener('complete', this.onEndAnim, this);
      this.canonAnim.addEventListener('complete', this.onEndAnim, this);
      highlight2.playHighlightAnim();
    }

    // highlight1.playHighlightAnim();
    
    // highlight3.playHighlightAnim();
  }

  checkPairOf7(redCardNodeList, blackCardNodeList) {
    let numbOf7InRed = 0;
    redCardNodeList.map(card => {
      if (Math.floor(card.getComponent(Card).cardId / 4) === 6) {
        numbOf7InRed++;
      }
    });
    let numbOf7InBlack = 0;
    blackCardNodeList.map(card => {
      if (Math.floor(card.getComponent(Card).cardId / 4) === 6) {
        numbOf7InBlack++;
      }
    });
    if ( numbOf7InRed === 2 ||  numbOf7InBlack === 2) {
      return true;
    }
    return false;
  }

  onEndAnim() {
    this.canonAnim.node.active = false;
  }

  onUpdate(game: RedBlackGame, updateData: UpdateData) {}

  onLeave(game: RedBlackGame, data: any) {}
}