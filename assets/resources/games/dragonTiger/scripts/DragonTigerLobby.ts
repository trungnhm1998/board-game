import { DragonTigerGame } from './DragonTigerGame';
import { GAME_ID } from './../../../../scripts/core/Constant';
import { CountdownInfo } from './../../../../scripts/model/Room';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
import { DragonTigerRoom } from './model/DragonTigerRoom';
import { LocalData } from './../../../../scripts/core/LocalStorage';
import { DragonTigerRoomOverview } from './DragonTigerRoomOverview';
import { moon } from './../../../../scripts/core/GlobalInfo';

const {ccclass, property} = cc._decorator;

const MAX_ROOM_PER_CATEGORY = 5;

const ROOM_CATEGORIES = {
  FRESH: 1,
  BEGINNER: 2,
  INTERMEDIATE: 3,
  ADVANCED: 4
}

const DEFAULT_CATEGORY = ROOM_CATEGORIES.FRESH;

@ccclass
export class DragonTigerLobby extends cc.Component {
  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Prefab)
  roomOverviewPrefab: cc.Prefab = null;

  @property(cc.Node)
  roomListNode: cc.Node = null;

  @property([cc.Toggle])
  roomListCategoryToggleList: cc.Toggle[] = [];

  _roomOverviewPool: cc.NodePool = null;
  _roomInfoList: Array<DragonTigerRoom> = [];
  _justChangedCategory: boolean = true;

  onLanguageChange() {
    let backgroundSprites = {};
    for (let index = 0; index < 4; index++) {
      const key = `Background_${index}`;
      backgroundSprites[key] = `lh_game_room_title_${index}`;
    }

    NodeUtils.setSprites(this.node, {
      'fresh': 'lh_game_room_title_0_selected',
      'beginner': 'lh_game_room_title_1_selected',
      'intermediate': 'lh_game_room_title_2_selected',
      'advanced': 'lh_game_room_title_3_selected',
      ...backgroundSprites
    }, false)

    this.roomListNode.children.forEach(roomNode => {
      let dragonTigerRoomComponent: DragonTigerRoomOverview = roomNode.getComponent(DragonTigerRoomOverview);
      dragonTigerRoomComponent.onLanguageChange();
    });
  }

  onLoad() {
    if (this._roomOverviewPool == null)
      this._roomOverviewPool = new cc.NodePool();
    let { dragonTigerRoomCategory } = LocalData;
    dragonTigerRoomCategory = parseInt(String(dragonTigerRoomCategory));
    if (dragonTigerRoomCategory > 0 && dragonTigerRoomCategory <= ROOM_CATEGORIES.ADVANCED) {
      this.roomListCategoryToggleList[dragonTigerRoomCategory - 1].check();
    } else {
      LocalData.dragonTigerRoomCategory = 1;
      moon.storage.saveLocalData();
    }

    // init prefab for room list
    for (let index = 0; index < MAX_ROOM_PER_CATEGORY; index++) {
      let roomOverviewNode = cc.instantiate(this.roomOverviewPrefab);
      this._roomOverviewPool.put(roomOverviewNode);
    }

    this._justChangedCategory = true;
  }

  update() {
    this.updateRoomOverviewNodes();
  }

  updateRoomInfoList(roomInfoList: Array<DragonTigerRoom>) {
    // GlobalInfo.roomInfoList got updated
    // start to setup UI based on data from server
    if (this._justChangedCategory)
      this.clearRoomOverview();
    if (roomInfoList == null)
      return;
    this._roomInfoList = roomInfoList;
    let { dragonTigerRoomCategory } = LocalData;
    dragonTigerRoomCategory = parseInt(String(dragonTigerRoomCategory));
    const roomCategory = this._roomInfoList.filter((roomInfo) => roomInfo.roomName == dragonTigerRoomCategory);
    // cc.log('globalInfo', GlobalInfo, this._roomInfoList, roomCategory, dragonTigerRoomCategory);
    if (roomCategory) {
      for (let index = 0; index < roomCategory.length; index++) {
        // not needed this the line below
        // const { maxBet, minBet, betList, roomId, playerNum, countdownInfo } = roomCategory[index];
        let roomOverviewNode: cc.Node = null;

        // reuse room node
        roomOverviewNode = this.roomListNode.children[index];

        // if room node out of index, haven't instantitate before
        if (!roomOverviewNode) {
          // try to get from pool
          if (this._roomOverviewPool.size() > 0) {
            roomOverviewNode = this._roomOverviewPool.get();
          } else {
            // create new when pool null
            roomOverviewNode = cc.instantiate(this.roomOverviewPrefab);
          }
        }

        this.roomListNode.addChild(roomOverviewNode);

        const roomOverviewComponent = roomOverviewNode.getComponent(DragonTigerRoomOverview);

        roomOverviewComponent.updateUI(roomCategory[index]);
        roomOverviewComponent.onLanguageChange();

        // TODO: better way to call setHistory?
        roomOverviewComponent.roomHistory.setHistory(roomCategory[index].historyList);

      }
    }
  }

  // for updating node value based on countdown task
  updateRoomOverviewNodes() {
    this.roomListNode.children.forEach(roomOverviewNode => {
      let roomOverviewComponent = roomOverviewNode.getComponent(DragonTigerRoomOverview);

      const DTGame = (moon.service.getCurrentGameNode(GAME_ID.DRAGON_TIGER) as cc.Node).getComponent(DragonTigerGame);
      // get countdown task based on room id
      const countdownTaskWithRoomId = DTGame._lobbyCountdownTask[roomOverviewComponent._roomId];

      // check null first
      if (countdownTaskWithRoomId) {
        // not null
        const { countdownInfo, val, ended } = countdownTaskWithRoomId;
        // if countdown still running
        if (!ended) {
          roomOverviewComponent.updateRoomStatus(countdownInfo, val);
        }
      }
      
    })
  }

  onResize(uiRatio: number) {
    this.ui.scale = uiRatio;
  }

  onChangeRoomCategory(evt: cc.EventTarget, toggleValue) {
    // make sure dragonTigerRoomCategory > 0
    this._justChangedCategory = true;
    LocalData.dragonTigerRoomCategory = ROOM_CATEGORIES.FRESH + parseInt(toggleValue);
    moon.storage.saveLocalData();
    this.clearRoomOverview();

    this.updateRoomInfoList(this._roomInfoList);
    this._justChangedCategory = false;
  }

  clearRoomOverview() {
    let roomList = this.roomListNode.children;
    const roomListLength = roomList.length
    for (let index = roomListLength - 1; index >= 0; index--) {
      const room = roomList[index];
      this._roomOverviewPool.put(room);
      // room.removeFromParent();
      // room.active = false;
    }
  }

  clear() {
    this.clearRoomOverview();
    this._justChangedCategory = true;
  }
}