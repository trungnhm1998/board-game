import {RedBlackRoom} from "../model/RedBlackRoom";
import {RedBlackPlayerInfo} from "../model/RedBlackPlayerInfo";
import {KEYS} from "../../../../../scripts/core/Constant";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";

export class RedBlackRoomService {
  private countdownTasks = {};

  private static instance: RedBlackRoomService;

  static getInstance(): RedBlackRoomService {
    if (!RedBlackRoomService.instance) {
        RedBlackRoomService.instance = new RedBlackRoomService();
    }

    return RedBlackRoomService.instance;
  }

  
  activeCountdown() {
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
    if (room && room.countdownInfo && room.countdownInfo.timeout > 0) {
      this.stopCountdown(room.roomId);
      this.countdownTasks[room.roomId] = moon.timer.tweenNumber(room.countdownInfo.timeout, -room.countdownInfo.timeout, (val) => {
        room.countdownInfo.timeout = val;
      }, () => {
      }, room.countdownInfo.timeout);
    }
  }

  stopCountdown(roomId) {
    if (this.countdownTasks[roomId]) {
      moon.timer.removeTask(this.countdownTasks[roomId]);
      this.countdownTasks[roomId] = null;
    }
  }
}