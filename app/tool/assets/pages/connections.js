let connections = {
  tableId: 'connectionTable',
  addFormId: 'addForm',
  data: {
    merchantList: [],
    machineList: [],
    insightConnectionList: [],
    currencyList: []
  },
  updateDataTable: function () {
    $(`#${this.tableId}`).DataTable({
      searching: false,
      paging: false
    });
  },

  init: function () {
    let rowPattern = `
    <tr>
      <td>{0}</td>
      <td>{1}</td>
      <td>
      <a class="btn btn-outline-primary" href="edit-connection?connectionId={0}&secretKey={1}"><i class="fas fa-edit"></i></a>
      <div class="btn btn-outline-danger" onclick="connections.onDeleteConnection({0}, '{1}')"><i class="fas fa-minus"></i></div>
      </td>
    </tr>
    `;
    for (let insightInfo of this.data.insightConnectionList) {
      let rowHtml = core.string.format(rowPattern, insightInfo.connectionId, insightInfo.secretKey);
      $(`#${this.tableId}`).find(`> tbody`).append(rowHtml);
    }

    this.updateDataTable();
  },

  initEditPage: function () {
    let form = $(`#${this.addFormId}`);
    let searchParams = new URLSearchParams(location.href.split("?")[1]);
    let connectionId = searchParams.get('connectionId');
    let secretKey = searchParams.get('secretKey');
    form.find('#oldConnectionId').text(connectionId);
    form.find('#oldSecretKey').text(secretKey);
  },

  onAddConnection: function () {
    let form = $(`#${this.addFormId}`);
    let connectionId = +form.find('#connectionId').val();
    let secretKey = '' + form.find('#secretKey').val();
    core.api.addConnection({
      connectionId: connectionId,
      secretKey: secretKey,
    }, function () {
      location.reload();
    });
  },

  onDeleteConnection: function (connectionId, secretKey) {
    core.api.deleteConnection({
      connectionId: connectionId,
      secretKey: secretKey,
    }, function () {
      location.reload();
    });
  },

  onUpdateConnection: function () {
    let form = $(`#${this.addFormId}`);
    let oldConnectionId = +form.find('#oldConnectionId').text();
    let oldSecretKey = '' + form.find('#oldSecretKey').text();
    let connectionId = +form.find('#connectionId').val();
    let secretKey = '' + form.find('#secretKey').val();
    core.api.updateConnection({
      connectionIdOld: oldConnectionId,
      secretKeyOld: oldSecretKey,
      connectionIdNew: connectionId,
      secretKeyNew: secretKey,
    }, function () {
      let myURL = location.href;
      let myDir = myURL.substring( 0, myURL.lastIndexOf( "/" ) + 1);
      location.href = myDir + "connection.html";
    });
  },
};