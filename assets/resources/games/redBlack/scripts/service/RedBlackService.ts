import { RedBlackGame } from './../RedBlackGame';
import {IGameService} from '../../../../../scripts/services/GameService';
import {GameSocket} from '../../../../../scripts/services/SocketService';
import {COUNTDOWN_TYPE, GAME_STATE, KEYS, SERVER_EVENT, RED_BLACK_PLACE_TYPE, RED_BLACK_KEY} from '../../../../../scripts/core/Constant';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {FadeOutInTransition} from '../../../../../scripts/services/SceneManager';
import { RedBlackRoom } from '../model/RedBlackRoom';
import {ModelUtils} from '../../../../../scripts/core/ModelUtils';
import { RedBlackRoomService } from './RedBlackRoomService';
import {CountdownInfo} from '../../../../../scripts/model/Room';
import {RedBlackStartState} from '../gamestates/RedBlackStartState'
import {RedBlackDealCardState} from '../gamestates/RedBlackDealCardState'
import {RedBlackBetState} from '../gamestates/RedBlackBetState'





export class RedBlackService implements IGameService {

  isActive = false;

  name = 'RedBlackService';

  game: RedBlackGame;

  waitingActions: any[] = [];

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(RedBlackGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };
    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => processAction(this.onErrorMessage, data));
  }

  updateRoomInfoList() {
 
  }

  onJoinBoard(data) {
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
    cc.log('set join board info',data, room);
    this.game.clearTable()
    this.game.setMyPlayer(data.playerInfo);
    this.game.setRoom(room);
    this.processWaitingActions();
  }

  onPlayerJoinBoard(data) {
   console.log(data)
  }

  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    cc.log('return game and set room data', GlobalInfo.room);
    this.game.onEnterRoom(false, true);
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
    this.game.clearTable()
    this.game.setRoom(room);
    //add Function to make setupRoom
  }

  private onErrorMessage(data) {
   
  }

  private onActionInGame(data) {
    let subData = data[KEYS.DATA];
    if (subData.roomId !== GlobalInfo.room.roomId) {
      return
    }
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.DEAL_CARD:
           this.onDealCard(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
           this.onBeginBet(subData);
          break;
        case SERVER_EVENT.BET:
          // this.onStartGame(subData);
          break;
        case SERVER_EVENT.UPDATE_PLAYERS_BET:
            this.updatePlayersBet(subData);
          break;
        case SERVER_EVENT.OPEN_CARD:
          // this.onStartGame(subData);
          break;
        case SERVER_EVENT.END_GAME:
          // this.onStartGame(subData);
          break;
      }
    };
    handleEvents();
  }

  private onStartGame(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: RedBlackRoom = <RedBlackRoom> GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    room.betMoney = data.betMoney;
    room.matchId = data.matchId;
    RedBlackRoomService.getInstance().activeCountdown();

    this.game.changeState(GAME_STATE.START, RedBlackStartState, data);
  }

  private onDealCard(data: any) {
    // cc.log("onDealCard", data);
    // let roomId = data[KEYS.ROOM_ID];
    // if (roomId != GlobalInfo.room.roomId) return;
    // let room = GlobalInfo.room as DragonTigerRoom;
    // room.countdownInfo.type = DragonTigerConstants.SERVER_GAME_STATE.STATE_DEAL_CARD;
    // const { gameTable } = this.game;
    // this.game.updateStatus();
    // this.game.runCountdown(data[KEYS.TIMEOUT], () => {

    // });
    // gameTable.dealer.skipCard(gameTable.clock.node, () => {
    //   gameTable.dealer.dealCard(gameTable.potDragon, -1, () => {
    //     gameTable.dealer.dealCard(gameTable.potTiger, -1, () => {
    //       gameTable.dealer.setCount(data[KEYS.CARD_NUM]);
    //     });
    //   });
    // });
      this.game.changeState('deal_state',RedBlackDealCardState,{})  

  }

  private onBeginBet(data: any) {
    this.game.changeState("bet_state", RedBlackBetState, {});
  }

  private updatePlayersBet( data: any) {
    cc.log(data)
  }


  private onStopGame() {
  
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let roomService = RedBlackRoomService.getInstance();
    let backToMain = () => {
        roomService.stopCountdown(roomId);
        moon.dialog.showWaiting();
        let transition = new FadeOutInTransition(0.2);
        moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
        this.game.returnBackToGameLobby()
    };
    backToMain();
  } 
  
  bet(placeBetType) {
    //add sound when bet here
    let bet = this.game.table.currentBet;
    cc.log('pot click', placeBetType, bet)
    let data = {
        [KEYS.SUB_COMMAND]: SERVER_EVENT.BET,
        [KEYS.DATA]: {
          [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
          [RED_BLACK_KEY.PLACE_BET_TYPE]: placeBetType,
          [KEYS.BET_MONEY]: bet
        }
    };
    moon.gameSocket.emit(SERVER_EVENT.BET, data)
  }

}