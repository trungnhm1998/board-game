import {KEYS, SERVER_EVENT} from "../../../../../scripts/core/Constant";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";

export class ThreeFaceSocket {

  private static instance: ThreeFaceSocket;

  static getInstance(): ThreeFaceSocket {
    if (!ThreeFaceSocket.instance) {
      ThreeFaceSocket.instance = new ThreeFaceSocket();
    }

    return ThreeFaceSocket.instance;
  }

  bet(betStr) {
    let betValue = parseInt(betStr);
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BET,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.BET_FACTOR]: betValue
      }
    };

    moon.gameSocket.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  chooseBanker(isChoose = false) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.CHOOSE_BANKER,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.IS_CHOOSE]: isChoose
      }
    };

    moon.gameSocket.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }
}