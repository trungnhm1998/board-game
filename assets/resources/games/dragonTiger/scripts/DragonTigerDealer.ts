import { DragonTigerBetInfo } from './model/DragonTigerRoom';
import { CardPool } from './../../../../scripts/core/CardPool';
import { Card } from './../../../../scripts/core/Card';
import { moon } from './../../../../scripts/core/GlobalInfo';
import {DragonTigerPot} from "./DragonTigerPot";
import {ChessPool} from "../../../../scripts/common/ChessPool";
import {Config} from "../../../../scripts/Config";
import {Chess} from "../../../../scripts/common/Chess";
import DragonTigerConstants from './DragonTigerConstants';

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerDealer extends cc.Component {

  @property(cc.Label)
  count: cc.Label = null;

  @property(cc.Node)
  centerNode: cc.Node = null;

  setCount(count) {
    this.count.string = count;
  }

  dealCard(pot: DragonTigerPot, cardId, callback?) {
    let dealPos = pot.getDealPos();
    let cardNode = moon.cardPool.getCard(cardId)
    this.node.addChild(cardNode);

    // setup properties
    cardNode.rotation = 90;
    cardNode.position = cc.v2();
    cardNode.scale = 0.5;

    // play anim
    cardNode.runAction(
      cc.spawn(
        cc.callFunc(() => {
          moon.audio.playEffectInGame('cards_dealing', false);
        }),
        cc.moveTo(Config.chessFlyTime, this.node.convertToNodeSpaceAR(dealPos)).easing(cc.easeCubicActionIn()),
        cc.rotateTo(Config.chessFlyTime, 0),
        cc.scaleTo(Config.chessFlyTime, 1),
        cc.sequence(
          cc.delayTime(Config.chessFlyTime),
          cc.callFunc(() => {
            pot.setcard(cardNode.getComponent(Card));
            if (callback) {
              callback();
            }
          })
        )
      )
    );
    // pot.setChess(chessNode.getComponent(Chess));
    this.decrease();
  }

  skipCard(houseNode: cc.Node, callback?) {
    let cardNode: cc.Node = moon.cardPool.getCard(0);
    this.node.addChild(cardNode);
    
    // setup properties this 3 lines will be use alot
    cardNode.rotation = -90;
    cardNode.position = cc.v2();
    cardNode.scale = 0.4;

    // play UI
    cardNode.runAction(
      cc.sequence(
        cc.spawn(
          cc.callFunc(() => {
            moon.audio.playEffectInGame('cards_dealing', false);
          }),
          cc.moveTo(Config.chessFlyTime, this.node.convertToNodeSpaceAR(this.centerNode.convertToWorldSpaceAR(cc.v2()))).easing(cc.easeCubicActionIn()),
          cc.rotateTo(Config.chessFlyTime, 0),
          cc.scaleTo(Config.chessFlyTime, 1, 1),
        ),
        cc.delayTime(Config.chessFlyTime),
        cc.spawn(
          cc.moveTo(Config.chessFlyTime, this.node.convertToNodeSpaceAR(houseNode.convertToWorldSpaceAR(cc.v2()))).easing(cc.easeCubicActionIn()),
          cc.rotateTo(Config.chessFlyTime, -90),
          cc.scaleTo(Config.chessFlyTime, 0.4, 0.4),
        ),
        cc.callFunc(() => {
          cardNode.removeFromParent();
          moon.cardPool.putCards(cardNode);
          if (callback)
            callback();
        })
      )
    )

    this.decrease();
  }

  decrease() {
    let count = +this.count.string;
    count--;
    count = Math.max(0, count);
    this.count.string = '' + count;
  }

  clear() {
    this.count.string = '0';
  }

  setCards(placeBetInfoList: DragonTigerBetInfo[], potDragon: DragonTigerPot, potTiger: DragonTigerPot) {
    let setPotCard = (pot: DragonTigerPot, cardId) => {
      let dealPosition: cc.Vec2 = pot.getDealPos();
      let cardNode = CardPool.getInstance().getCard(cardId);
      let card: Card = cardNode.getComponent(Card);
      if (cardId == -1) {
        card.hide();
      } else {
        card.showCard();
      }
      this.node.addChild(cardNode);
      cardNode.rotation = 0;
      cardNode.position = this.node.convertToNodeSpaceAR(dealPosition);
      pot.setcard(card);
    }

    placeBetInfoList.forEach(placeBetInfo => {
      let pot: DragonTigerPot = null;
      switch (placeBetInfo.type) {
        case DragonTigerConstants.PLACE_TYPE.DRAGON:
          pot = potDragon;
          break;
        case DragonTigerConstants.PLACE_TYPE.TIGER:
          pot = potTiger;
          break;
      }
      if (pot)
        setPotCard(pot, placeBetInfo.cardList[0]);
    });
  }

/*
  setChesses(chessList: number[], potDragon: DragonTigerPot, potTiger: DragonTigerPot) {
    let setPotChess = (pot: DragonTigerPot, chessId) => {
      let dealPos = pot.getDealPos();
      let chessNode = ChessPool.getInstance().getChess(chessId);
      let chess: Chess = chessNode.getComponent(Chess);
      if (chessId == -1) {
        chess.hide();
      } else {
        chess.open(chessId);
      }
      this.node.addChild(chessNode);
      chessNode.rotation = 0;
      chessNode.position = this.node.convertToNodeSpaceAR(dealPos);
      pot.setChess(chess);
    };

    setPotChess(potDragon, chessList[0]);
    setPotChess(potTiger, chessList[1]);
  }
*/
}