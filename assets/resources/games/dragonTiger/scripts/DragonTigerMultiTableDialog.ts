import { GlobalInfo } from './../../../../scripts/core/GlobalInfo';
import { GAME_ID } from './../../../../scripts/core/Constant';
import { DragonTigerGame } from './DragonTigerGame';
import { GameService } from './../../../../scripts/services/GameService';
import { DragonTigerHistory } from './DragonTigerHistory';
import { DragonTigerRoomOverview } from './DragonTigerRoomOverview';
import { DragonTigerRoom } from './model/DragonTigerRoom';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
const { ccclass, property } = cc._decorator;

@ccclass
export class DragonTigerMultiTableDialog extends cc.Component {

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Node)
  tableOverviewSample: cc.Node = null;

  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  formScrollView: cc.Node = null;

  @property(cc.Node)
  sampleResult: cc.Node = null;

  @property(cc.Prefab)
  roomOverviewPrefab: cc.Prefab = null;

  @property(cc.Node)
  roomListNode: cc.Node = null;
  /**
   * overviewTables have 16 childs
   */
  _roomInfoList: Array<DragonTigerRoom> = [];
  _tableIds: Array<String> = [];
  _currentActiveTable: string = '';
  _hasUpdatedScrollList = false;

  onLoad() {
    // this.setupOverviewTables();
    // this.updateTableLocale();
  }

  onEnable() {
    // check if all the roomInfoList is null or not
    if (this._roomInfoList.length == 0) {
      // haven't init any
      // init node
    }
    
  }

  update() {
    this.updateRoomOverview();
  }

  updateRoomOverview() {
    for (let i = 0; i < this.roomListNode.children.length; i++) {
      const roomOverview = this.roomListNode.children[i];
      if (roomOverview) {
  
        let roomOverviewComponent = roomOverview.getComponent(DragonTigerRoomOverview);
    
        const DTGame = (GameService.getInstance().getCurrentGameNode(GAME_ID.DRAGON_TIGER) as cc.Node).getComponent(DragonTigerGame);
        if (DTGame._lobbyCountdownTask && DTGame._lobbyCountdownTask[roomOverviewComponent._roomId]) {
          const { countdownInfo, val } = DTGame._lobbyCountdownTask[roomOverviewComponent._roomId]; 

          // update UI
          roomOverviewComponent.updateRoomStatus(countdownInfo, val) ;

          let maskNode = roomOverviewComponent.node.getChildByName("currentTableMask");
          if (roomOverviewComponent._roomId != GlobalInfo.room.roomId) {
            maskNode.active = false;
          } else {
            maskNode.active = true;
            this._currentActiveTable = roomOverviewComponent._roomId;
            if (!this._hasUpdatedScrollList) {
              this._hasUpdatedScrollList = true;
              let scrollView = this.formScrollView.getComponent(cc.ScrollView);
              let containerLayout = this.container.getComponent(cc.Layout);
              if (this._currentActiveTable != '') {
                let tableNode = this.roomListNode.children[this._tableIds.indexOf(this._currentActiveTable)];
                let offset = (tableNode.height / 2) + containerLayout.paddingTop;
                let tableOffsetY = tableNode.y * -1;
                scrollView.scrollToOffset(cc.v2(0, tableOffsetY - offset));
              } else {
                scrollView.scrollToPercentVertical(1);
              }
            }
          }
        }
      }
    }
  }

  onDisable() {
    // clear table history/ countdown info for every table when dialog disable/inactive
    // Sockets.game.getDragonTigerRoomList(false);
    this._hasUpdatedScrollList = false;
  }

  onLanguageChange() {
  }

  // for init all the room node
  updateRoomInfoList(roomInfoList: any) {
    this._roomInfoList = roomInfoList as Array<DragonTigerRoom>;
    // GlobalInfo.roomInfoList got updated
    // start to setup UI based on data from server
    if (roomInfoList == null || this.roomListNode.children.length > 0)
      return;
    // cc.log('globalInfo', GlobalInfo, this._roomInfoList, roomCategory, dragonTigerRoomCategory);
    for (let index = 0; index < this._roomInfoList.length; index++) {
      // not needed this the line below
      // const { maxBet, minBet, betList, roomId, playerNum, countdownInfo } = roomCategory[index];
      let roomOverviewNode: cc.Node = null;

      // reuse room node
      roomOverviewNode = this.roomListNode.children[index];

      // if room node out of index, haven't instantitate before
      if (!roomOverviewNode) {
        // instantiate a new node
        roomOverviewNode = cc.instantiate(this.roomOverviewPrefab);
      }

      this.roomListNode.addChild(roomOverviewNode);
      
      const roomOverviewComponent = roomOverviewNode.getComponent(DragonTigerRoomOverview);
      
      roomOverviewComponent.updateUI(this._roomInfoList[index]);
      roomOverviewComponent.onLanguageChange();
      
      this._tableIds.push(roomOverviewComponent._roomId);
      if (GlobalInfo.room.roomId == roomOverviewComponent._roomId)
        this._currentActiveTable = roomOverviewComponent._roomId;
      // TODO: better way to call setHistory?
      roomOverviewComponent.roomHistory.setHistory(this._roomInfoList[index].historyList);
    }
  }

  clear() {
    for (let roomNode of this.roomListNode.children) {
      let roomHistory = roomNode.getChildByName("history");
      let roomHistoryComponent = roomHistory.getComponent(DragonTigerHistory);

      // clear history
      roomHistoryComponent.clear();
    }

    this.roomListNode.removeAllChildren();
  }
}
