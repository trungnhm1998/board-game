#ifndef __SimpleNativeClassJS_h__
#define __SimpleNativeClassJS_h__

#include "jsapi.h"
#include "jsfriendapi.h"
#include "cocos2d.h"
#include "ScriptingCore.h"

USING_NS_CC;

// ***********Inheritence testing*********

class SimpleNativeClass {
    public:
        SimpleNativeClass();
        ~SimpleNativeClass();

        void addTexture2DFromBase64(std::string &base64string, std::string &key);
        char* getBase64FromTexture2D(std::string &key);
};

void register_all_autogentestbindings(JSContext *cx, JS::HandleObject global);


#endif