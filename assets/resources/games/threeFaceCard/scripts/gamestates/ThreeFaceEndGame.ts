import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {ThreeFaceRoom} from "../model/ThreeFaceRoom";
import {ThreeFaceGame} from "../ThreeFaceGame";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import {ThreeFacePlayer} from "../ThreeFacePlayer";
import {RESOURCE_BLOCK} from "../../../../../scripts/core/Constant";
import {THREE_FACE_AUDIO} from "../ThreeFaceConstants";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceEndGame extends GameState implements IGameState {

  game: ThreeFaceGame;

  @property(dragonBones.ArmatureDisplay)
  winSke: dragonBones.ArmatureDisplay = null;

  onEnter(game: ThreeFaceGame, data: any) {
    this.clearAnimation();
    this.game = game;
    let gameRoom: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    let players: ThreeFacePlayer[] = game.getActiveOrderedPlayers();
    let showChesses: Array<cc.FiniteTimeAction> = [];
    let moveChipsToWinner: cc.FiniteTimeAction;
    let playWinAmountEffect: Array<cc.FiniteTimeAction> = [];
    let winners: ThreeFacePlayer[] = [];
    let losers: ThreeFacePlayer[] = [];
    let banker: ThreeFacePlayer;
    let isMeWin = false;
    let chipTwoSteps = false;
    for (let player of players) {
      if (player.playerInfo.winMoney > 0) {
        winners.push(player);
        if (player.playerInfo.userId == GlobalInfo.me.userId) {
          isMeWin = true;
        }
      } else if (player.playerInfo.winMoney < 0) {
        if (player.playerInfo.userId != gameRoom.bankerId) {
          losers.push(player);
        }
      }
      if (player.playerInfo.userId == gameRoom.bankerId) {
        banker = player;
      } else {
        if (player.playerInfo.winMoney > 0) {
          chipTwoSteps = true;
        }
      }
      showChesses.push(cc.callFunc(() => {
        player.showCards();
      }));
      playWinAmountEffect.push(cc.callFunc(() => {
        player.playAmountEffect(player.playerInfo.winMoney);
      }));
    }
    moveChipsToWinner = cc.callFunc(() => {
      moon.audio.playEffectInGame(THREE_FACE_AUDIO.GOLD_FLY);
      game.betLayer.moveChipsToHouse(banker, losers, () => {
        let i = 0;
        let isOnlyHouseWin = winners.length == 1 && winners[0].playerInfo.userId == banker.playerInfo.userId;
        if (!isOnlyHouseWin) {
          moon.audio.playEffectInGame(THREE_FACE_AUDIO.GOLD_FLY);
        }
        for (let player of players) {
          if (winners.indexOf(player) >= 0 && player.playerInfo.userId != banker.playerInfo.userId) {
            this.game.betLayer.moveChipsToWinner(banker, player, player.playerInfo.winMoney, () => {
              i++;
              if (i == winners.length) {

              }
            });

          }
        }
      });
    });

    this.node.runAction(
      cc.sequence(
        cc.spawn(
          showChesses
        ),
        cc.delayTime(0.4),
        // play result anim here
        cc.callFunc(() => {
          this.game.showPlayersResultBanner();
        }),
        cc.delayTime(0.3),
        // play win effect here if winner is current user
        cc.callFunc(() => {
          if (isMeWin) {
            moon.audio.playEffectInGame(THREE_FACE_AUDIO.WIN);
            this.playWinAnimation();
          }
        }),
        moveChipsToWinner,
        cc.delayTime(chipTwoSteps ? 2 : 1),
        cc.spawn(playWinAmountEffect),
        cc.delayTime(2),
        cc.callFunc(() => {
          this.clearAnimation();
        })
      )
    );
  }


  playWinAnimation() {
    this.winSke.node.active = true;
    this.winSke.dragonAsset = moon.res.getSkelecton('win_ske', RESOURCE_BLOCK.GAME);
    this.winSke.dragonAtlasAsset = moon.res.getSpriteFrame('win_tex', RESOURCE_BLOCK.GAME);
    this.winSke.armatureName = 'win';
    this.winSke.removeEventListener('complete', this.clearAnimation, this);
    this.winSke.addEventListener('complete', this.clearAnimation, this);
    this.winSke.playAnimation('win', 1);
  }

  clearAnimation() {
    this.winSke.node.active = false;
  }

  onUpdate(game: ThreeFaceGame, updateData: UpdateData) {
  }

  onLeave(game: ThreeFaceGame, data: any) {
  }
}