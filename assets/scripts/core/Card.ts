import {moon} from "./GlobalInfo";


const {ccclass, property} = cc._decorator;

@ccclass
export class Card extends cc.Component {

  @property(cc.Sprite)
  backBg: cc.Sprite = null;

  @property(cc.Sprite)
  frontBg: cc.Sprite = null;

  cardId: number;
  deckId: number;
  cardWidth: number;
  cardHeight: number;

  onLoad() {
  }

  setCardSize(width, height) {
    this.cardHeight = height;
    this.cardWidth = width;
  }

  resizeCard(width, height) {
    this.node.width = width;
    this.node.height = height;
    this.backBg.node.width = width;
    this.backBg.node.height = height;
    this.frontBg.node.width = width;
    this.frontBg.node.height = height;
  }

  setDeckId(deckId) {
    this.deckId = deckId;
  }

  showCard() {
    let cardAtlas = moon.cardPool.getCurrentAtlas();
    if (this.cardId != null && this.cardId >= 0 && cardAtlas) {
      this.frontBg.node.active = true;
      this.frontBg.node.opacity = 255;
      let cardIndex = this.cardId.toString();
      if (this.cardId < 10) {
        cardIndex = '0' + cardIndex;
      }
      this.frontBg.spriteFrame = cardAtlas.getSpriteFrame('atlas_card_' + this.deckId + '_' + cardIndex);
      this.resizeCard(this.cardWidth, this.cardHeight);
    }
  }

  faceDown() {
    let cardAtlas = moon.cardPool.getCurrentAtlas();
    this.frontBg.node.active = false;
    this.backBg.node.active = true;
    this.backBg.spriteFrame = cardAtlas.getSpriteFrame('atlas_card_' + this.deckId + '_back');
    this.resizeCard(this.cardWidth, this.cardHeight);
  }

  setCardId(cardId) {
    this.cardId = cardId;
    this.getCardPoint();
  }

  clearFront() {
    this.cardId = null;
    this.frontBg.node.active = false;
    this.resizeCard(this.cardWidth, this.cardHeight);
  }

  getCardPoint() {
    if (this.cardId) {
      let res = this.cardId / 4 + 1;
      return parseInt(String(res), 10);
    }
  }

  hide() {
    this.frontBg.node.active = false;
  }
}