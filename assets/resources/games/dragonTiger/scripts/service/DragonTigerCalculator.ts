export class DragonTigerCalculator {
  static divideMoney(money, chipList = [100, 20, 5], smallest = 1) {
    let nList = [];
    let remain = money;
    let ret = {};
    for (let chip of chipList) {
      let nChip = Math.floor(remain / chip);
      remain -= nChip * chip;
      nList.push(
        nChip
      )
    }
    let i = 0;
    for (let chip of chipList) {
      ret[chip] = nList[i++];
    }
    ret[smallest] = Math.floor(remain / smallest);
    return ret;
  }
}