import {MiniPokerRoom} from '../model/MiniPokerRoom';
import {GameState, IGameState, UpdateData} from '../../../../../scripts/common/GameState';
import {MiniPokerGame} from '../MiniPokerGame';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {NodeUtils} from '../../../../../scripts/core/NodeUtils';
import { MINIPOKER_UPDATE_TYPE } from '../../../../../scripts/core/Constant';

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerBetState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  checkBtn: cc.Node = null;

  @property(cc.Node)
  betSample: cc.Node = null;

  @property(cc.Node)
  buttons: cc.Node = null;

  @property(cc.Node)
  childView: cc.Node = null;

  countdownTask;
  autoBetValue = 1;

  onEnter(game: MiniPokerGame, data: any) {
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    cc.log('bet state', room);
    let betList = room.betList;
    this.autoBetValue = betList[0] || 1;
    let timeout = room.countdownInfo.timeout;

    let me = game.getPlayerByUserId(GlobalInfo.me.userId)
    //if this user has not bet, then show the bet list
    if (me.playerInfo.betFactor == 0) {
      this.childView.active = true;
    }
    if (game.isTrusting) {
      cc.log('auto bet with bet factor', game.trustingFactor);
      moon.gameSocket.miniPokerBet(parseInt(game.trustingFactor.toString()));
    }
    else {
      this.showBetList()
    }

    this.showCoundownt();
    this.runCountdown(timeout);
  }

  showBetList() {
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    cc.log('show bet list', room);
    let betList = room.betList;
    this.buttons.removeAllChildren()
    for (let betValue of betList) {
      let betBtnNode = cc.instantiate(this.betSample);
      betBtnNode.active = true;
      let betUnit = moon.locale.get('bets');
      let valueNode = NodeUtils.findByName(betBtnNode, 'betValue');
      let valueLabel = valueNode.getComponent(cc.Label);
      valueLabel.string = '' + betValue + ' ' + betUnit;
      let betBtn: cc.Button = betBtnNode.getComponent(cc.Button);
      let clickEvent = betBtn.clickEvents[0];
      clickEvent.customEventData = betValue;
      this.buttons.addChild(betBtnNode);
      betBtnNode.position = cc.v2();
    }
  }

  onUpdate(game: MiniPokerGame, updateData: UpdateData) {
      cc.log('betState on Update')
      switch (updateData.actionType) {
          case MINIPOKER_UPDATE_TYPE.LANGUAGE_CHANGE: 
              cc.log('language change update')
              this.showBetList();
              break;
      }
  }


  showCoundownt() {
    this.countdown.node.parent.active = true;
    this.countdown.node.active = true;
  }

  hideCoundownt() {
    this.countdown.node.parent.active = false;
    this.countdown.node.active = false;
  }

  onLeave(game: MiniPokerGame, data: any) {
    //   this.checkBtn.removeFromParent();
    // this.node.addChild(this.checkBtn);
    //  this.checkBtn.active = false;
  }

  runCountdown(max) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num);
      } else {
        this.hideCoundownt();
        this.hideBetFactor();
      }
    }, 1);
  }

  hideBetFactor() {
    this.childView.active = false;
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }

  onBet(evt, value) {
    cc.log('bet with factor', value);

    moon.gameSocket.miniPokerBet(+value);
  }

  onBetEnd() {
    this.childView.active = false;
  }
}