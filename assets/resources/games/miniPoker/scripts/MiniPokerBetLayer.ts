import { MahjongPokerPlayer } from './MahjongPokerPlayer';
const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerBetLayer extends cc.Component {

	@property(cc.Node)
	chipSample: cc.Node = null;

	@property(cc.SpriteAtlas)
	atlas: cc.SpriteAtlas = null;

	@property(cc.Node)
	betArea: cc.Node = null;

	chipPool: cc.NodePool;

	onLoad() {
		this.chipPool = new cc.NodePool();
	}

	moveChipsToWinner(loser: MahjongPokerPlayer, winner: MahjongPokerPlayer, winMoney: number) {
		let loserWorldPos = this.betArea.parent.convertToWorldSpaceAR(loser.node.position);
		let loserNodePos = this.betArea.convertToNodeSpaceAR(loserWorldPos);
		let winnerWorldPos = this.betArea.parent.convertToWorldSpaceAR(winner.node.position);
		let winnerNodePos = this.betArea.convertToNodeSpaceAR(winnerWorldPos);
		let actions = [];
		const chipsThrows = (Math.abs(winMoney) > 10 ? 10 : Math.abs(winMoney));
		for (let i = 0; i < chipsThrows; i++) {
			let chipNode = this.getChip(1);
			this.betArea.addChild(chipNode);
			chipNode.position = loserNodePos;
			chipNode.active = true;
			chipNode.opacity = 0;
			actions.push(
				cc.callFunc(() => {
					chipNode.runAction(
						cc.sequence(
							cc.spawn(
								cc.moveTo(0.5, cc.v2(winnerNodePos.x, winnerNodePos.y + 70)),
								cc.fadeIn(0.07),
							),
							cc.callFunc(() => {
								chipNode.active = false;
								chipNode.removeFromParent();
								this.chipPool.put(chipNode);
							}),
						)
					)
				})
			);
			actions.push(cc.delayTime(0.05));
		}
		this.node.runAction(cc.sequence(actions));
	}

	getChip(chip: number) {
		let chipNode = this.chipPool.get();
    if (!chipNode) {
      chipNode = cc.instantiate(this.chipSample);
    }
    chipNode.active = true;
    // let chipSprite: cc.Sprite = chipNode.getComponent(cc.Sprite);
    // chipSprite.spriteFrame = this.atlas.getSpriteFrame('chip' + chip);
    // (<any>chipNode).value = chip;
    return chipNode;
	}

	returnChips(nodes?) {
		let chipsNodes = [].concat(this.betArea.children);
		if (nodes) {
			chipsNodes = chipsNodes.concat(nodes);
		}

		chipsNodes.forEach(chipNode => {
			chipNode.removeFromParent();
			chipNode.active = false;
			this.chipPool.put(chipNode);
		});
	}

	clear() {
		this.returnChips();
	}
}