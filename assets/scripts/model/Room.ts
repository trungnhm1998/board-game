export class Room {
  roomId;
  gameId: number;
  betMoney: number;
}

export class CountdownInfo {
  type: number;
  timeout: number;
}

export class RoomInfo {
  roomName;
  maxBet;
  minBet;
  bet;
  betList;
  roomIdList;
}