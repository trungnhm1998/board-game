import { GlobalInfo, moon } from './../../../../../scripts/core/GlobalInfo';
import { KEYS, SERVER_EVENT } from './../../../../../scripts/core/Constant';
import DragonTigerConstants from '../DragonTigerConstants';

export class DragonTigerSocket {
  static bet(bet, placeBetType) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BET,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [DragonTigerConstants.KEYS.PLACE_BET_TYPE]: placeBetType,
        [KEYS.BET_MONEY]: bet
      }
    };

    moon.gameSocket.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  static play(gameId, roomId) {
    let data = {
      [KEYS.GAME_ID]: gameId,
      [KEYS.ROOM_ID]: roomId,
    };

    moon.gameSocket.emit(SERVER_EVENT.PLAY_GAME, data);
  }

}
