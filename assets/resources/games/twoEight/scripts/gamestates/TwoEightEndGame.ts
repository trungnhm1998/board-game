import {GameState, IGameState, UpdateData} from '../../../../../scripts/common/GameState';
import {TwoEightGame} from '../TwoEightGame';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {TwoEightRoom} from '../model/TwoEightRoom';
import {TWO_EIGHT_AUDIO} from '../../../../../scripts/core/Constant';
import {Config} from '../../../../../scripts/Config';
import {LanguageService} from '../../../../../scripts/services/LanguageService';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightEndGame extends GameState implements IGameState {

  game: TwoEightGame;

  @property(cc.Node)
  actionButtons: cc.Node = null;


  @property(dragonBones.ArmatureDisplay)
  takeAllAnim: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  loseAllAnim: dragonBones.ArmatureDisplay = null;


  onEnter(game: TwoEightGame, data: any) {
    this.game = game;
    this.game.diceLayer.node.active = true;
    this.node.active = true;

    console.trace('Go to state EndGame')
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
    this.actionButtons.active = false;
    this.playAllAnimation_2()
  }


  playAllAnimation() {
    let game = this.game
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
    game.diceLayer.playDiceAnim(room.shakeDiceResult[0], room.shakeDiceResult[1], () => {
      this.node.runAction(
        cc.sequence(
          cc.callFunc(() => {
            game.playFirstDeal(room.firstId);
          }),
          cc.delayTime(1.5),
          cc.callFunc(() => {
            game.cardLayer.setupCards(room.playerInfoList, players);
          }),
          cc.delayTime(0.2),
          cc.callFunc(() => {
            for (let i = 0; i < players.length; i++) {
              game.cardLayer.dealCard(players[i], i * 0.5);
            }
          }),
          cc.delayTime(Config.chessFlyTime + (players.length - 1) * 0.3),
          cc.callFunc(() => {
            for (let i = 0; i < players.length; i++) {
              players[i].showChesses(i * 1.4);
            }
          }),
          cc.delayTime(0.7 + (players.length - 1) * 1.6),
          cc.callFunc(() => {
            let room = <TwoEightRoom>GlobalInfo.room;
            game.setRoom(<TwoEightRoom>GlobalInfo.room);
            game.betLayer.moveChipsToHouse(room.bankerId, () => {
              moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.WIN_GAME);
              for (let playerInfo of room.playerInfoList) {
                let player = game.getPlayerByUserId(playerInfo.userId);
                if (playerInfo.winMoney > 0 && playerInfo.userId != room.bankerId) {
                  game.betLayer.moveChipsToWinner(room.bankerId, playerInfo.userId, playerInfo.bet);
                  player.playWinEffect();
                }
                player.setMoney(playerInfo.money);
                player.playAmountEffect(playerInfo.winMoney);
              }
            });

            if (this.bankerTakeAll()) {
              moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.TAKE_ALL);
              this.playTakeAllAnim();
            }
            if (this.bankerLoseAll()) {
              this.playLoseAllAnim();
            }

            if (room.matchOrder >= room.matchTotal) {
              moon.timer.scheduleOnce(() => {
                this.game.clear();
                this.game.allowToQuitGameWithoutLeaveCmd()
                this.game.removeOtherPlayers();
                this.takeAllAnim.node.active = false;
                this.loseAllAnim.node.active = false;
                this.actionButtons.active = true;
              }, 3);
            }
          })
        ),
      );
    });
  }

  playAllAnimation_2() {
    let game = this.game
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
    game.diceLayer.playDiceAnim(room.shakeDiceResult[0], room.shakeDiceResult[1], () => {
      this.runSequence([
            [this.playCardAnimation, 0.7 + (players.length - 1) * 1.6],
            [this.playResultAnimation]
      ])
    })
  }

  
  playCardAnimation() {
    let game = this.game
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
    this.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          game.playFirstDeal(room.firstId);
        }),
        cc.delayTime(1.5),
        cc.callFunc(() => {
          game.cardLayer.setupCards(room.playerInfoList, players);
        }),
        cc.delayTime(0.2),
        cc.callFunc(() => {
          for (let i = 0; i < players.length; i++) {
            game.cardLayer.dealCard(players[i], i * 0.5);
          }
        }),
        cc.delayTime(Config.chessFlyTime + (players.length - 1) * 0.3),
        cc.callFunc(() => {
          for (let i = 0; i < players.length; i++) {
            players[i].showChesses(i * 1.4);
          }
        })
      )
    )
  }

  playResultAnimation() {
    let game = this.game
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
    this.node.runAction(
      cc.sequence(
        cc.delayTime(0.7 + (players.length - 1) * 1.6),
        cc.callFunc(() => {
          let room = <TwoEightRoom>GlobalInfo.room;
          game.setRoom(<TwoEightRoom>GlobalInfo.room);
          game.betLayer.moveChipsToHouse(room.bankerId, () => {
            moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.WIN_GAME);
            for (let playerInfo of room.playerInfoList) {
              let player = game.getPlayerByUserId(playerInfo.userId);
              if (playerInfo.winMoney > 0 && playerInfo.userId != room.bankerId) {
                game.betLayer.moveChipsToWinner(room.bankerId, playerInfo.userId, playerInfo.bet);
                player.playWinEffect();
              }
              player.setMoney(playerInfo.money);
              player.playAmountEffect(playerInfo.winMoney);
            }
          });

          if (this.bankerTakeAll()) {
            moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.TAKE_ALL);
            this.playTakeAllAnim();
          }
          if (this.bankerLoseAll()) {
            this.playLoseAllAnim();
          }

          if (room.matchOrder >= room.matchTotal) {
            moon.timer.scheduleOnce(() => {
              this.game.clear();
              this.game.removeOtherPlayers();
              this.game.allowToQuitGameWithoutLeaveCmd()
              this.takeAllAnim.node.active = false;
              this.loseAllAnim.node.active = false;
              this.actionButtons.active = true;
            }, 3);
          }
        })
      ),
    );
  }


  //this will choose which animation will be played according to remaining time
  playAnimationByRemainTime(remainTime) {
    let game = this.game
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
  
    if (remainTime >= 15) {
        this.playAllAnimation_2()
    }

    if (remainTime < 15 && remainTime >= 10) {
      this.runSequence([
        [this.playCardAnimation, 0.7 + (players.length - 1) * 1.6],
        [this.playResultAnimation]
      ])
    }

  }

  runSequence(actions) {
    let ccActions = [];
    for (let action of actions) {
      let func = action[0];
      let delay = action[1];
      ccActions.push(cc.callFunc(() => {func.call(this)}));
      ccActions.push(cc.delayTime(delay));
    }

    this.game.node.runAction(cc.sequence(ccActions));
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {
  }

  onLeave(game: TwoEightGame, data: any) {
    game.clear();

    this.takeAllAnim.node.active = false;
    this.loseAllAnim.node.active = false;
  }

  onQuit() {
    this.takeAllAnim.node.active = false;
    this.loseAllAnim.node.active = false;

    this.game.clear();
    this.actionButtons.active = false;
    // let transition = new FadeOutInTransition(0.2);
    // moon.scene.pushScene(SCENE_TYPE.MAIN_MENU, transition, true, () => {
    // });
    moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
    // moon.gameSocket.leaveBoard()
    this.game.returnBackToGameLobby();
  }

  onPlayAgain() {
    this.game.clear();
    this.actionButtons.active = false;
    this.game.disallowToQuitGameWithoutLeaveCmd()
    moon.gameSocket.playTwoEight(GlobalInfo.room.gameId, GlobalInfo.room.betMoney);
  }

  //banker win, all other user lose
  bankerTakeAll() {
    let room = <TwoEightRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId === room.bankerId) {
        if (playerInfo.winMoney < 0) {
          return false;
        }
      } else {
        if (playerInfo.winMoney > 0) {
          return false;
        }
      }
    }
    return true;
  }

  playTakeAllAnim() {
    this.takeAllAnim.node.active = true;
    let lang = LanguageService.getInstance().getCurrentLanguage();
    this.takeAllAnim.armatureName = lang;
    this.takeAllAnim.playAnimation(lang, 1);
  }

  bankerLoseAll() {
    let room = <TwoEightRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId === room.bankerId) {
        if (playerInfo.winMoney > 0) {
          return false;
        }
      } else {
        if (playerInfo.winMoney < 0) {
          return false;
        }
      }
    }
    return true;
  }

  playLoseAllAnim() {
    this.loseAllAnim.node.active = true;
    let lang = LanguageService.getInstance().getCurrentLanguage();
    this.loseAllAnim.armatureName = lang;
    this.loseAllAnim.playAnimation(lang, 1);
  }

  stopAllAnimation() {
    this.game.diceLayer.node.stopAllActions()
    this.game.diceLayer.node.active = false;
    this.node.stopAllActions()
    this.node.active = false;
  }
}