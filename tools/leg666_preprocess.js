var fs = require('fs');
var path = require('path');
var webp = require('webp-converter');
var sizeOf = require('image-size');


var walk = function (dir, done) {
  var results = [];
  fs.readdir(dir, function (err, list) {
    if (err) return done(err);
    var pending = list.length;
    if (!pending) return done(null, results);
    list.forEach(function (file) {
      file = path.resolve(dir, file);
      fs.stat(file, function (err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function (err, res) {
            results = results.concat(res);
            if (!--pending) done(null, results);
          });
        } else {
          results.push(file);
          if (!--pending) done(null, results);
        }
      });
    });
  });
};

function format(str) {
  var content = str;
  for (var i = 0; i < arguments.length; i++) {
    var replacement = '{' + i + '}';
    content = content.replace(replacement, arguments[i + 1]);
  }
  return content;
}


function removeMD5(Path) {
  let fileName = path.basename(filePath);
  let dirName = path.dirname(filePath);
  let ext = path.extname(filePath);
  let s1 = fileName.lastIndexOf("_");
  let newFileName = fileName.slice(0, s1) + ext;
  let newFilePath = path.format({
    dir: dirName,
    base: newFileName
  });
  if (filePath != newFilePath) {
    fs.rename(filePath, newFilePath, (err) => {
      if (err) throw err;
      console.log('complete:' + newFilePath);
    });
  }
}

function convetFntJson(filePath) {
  let ext = path.extname(filePath);
  if (ext.indexOf(".fnt") >= 0) {
    let data = fs.readFileSync(filePath);
    try {
      let jsonData = JSON.parse(data.toString());
      let fileName = jsonData.file.slice(0, jsonData.file.lastIndexOf("."));
      let lineHeight = 32;
      let count = 0;
      let chars = [];
      for (let char in jsonData.frames) {
        let frame = jsonData.frames[char];
        let charStr = format(
          `char id={0} x={1} y={2} width={3} height={4} xoffset={5} yoffset={6} xadvance={7} yadvance={8} page=0 chnl=0 letter="{9}"`,
          char.charCodeAt(0), frame.x, frame.y, frame.w, frame.h, frame.offX, frame.offY, frame.sourceW, frame.sourceH, char
        );
        chars.push(charStr);
        count++;
      }
      let fntData = format(`info face="{0}" size=32 bold=0 italic=0 charset="" unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=5,5
        common lineHeight={1} base=26 scaleW=128 scaleH=128 pages=1 packed=0 alphaChnl=1 redChnl=0 greenChnl=0 blueChnl=0
        page id=0 file="{2}.png"
        chars count={3}
        {4}
        
        kernings count=0
        `, fileName, lineHeight, fileName, count, chars.join('\n'));
      fs.writeFileSync(filePath, fntData);
    } catch (e) {

    }
  }
}

function convertWebp(filePath) {
  let ext = path.extname(filePath);
  if (ext.indexOf(".webp") >= 0) {
    let fileName = path.basename(filePath, '.webp');
    let dirName = path.dirname(filePath);
    webp.dwebp(filePath, [dirName, fileName + '.png'].join("/"), "-o", function (status, error) {
      //if conversion successful status will be '100'
      //if conversion fails status will be '101'
      if (status == 100) {
        fs.unlink(filePath, (err) => {
          if (err) throw err;
          console.log(filePath + ' was deleted');
        });
      }
    });
  }
}

function convertAtlasJsonToPlist(filePath) {
  if (filePath.indexOf('atlas') >= 0) {
    let ext = path.extname(filePath);
    if (ext.indexOf('.json') >= 0) {
      let dirName = path.dirname(filePath);
      let fileName = path.basename(filePath, '.json');
      let newFilePath = [dirName, fileName + '.plist'];
      try {
        let data = fs.readFileSync(filePath);
        let jsonData = JSON.parse(data.toString());
        let imageFileName = jsonData.file;
        let frameList = [];

        for (let key in jsonData.frames) {
          let frame = jsonData.frames[key];
          let frameStr = format(`
            <key>{0}</key>
            <dict>
            <key>frame</key>
            <string>{{{1},{2}},{{3},{4}}}</string>
            <key>offset</key>
            <string>{{5},{6}}</string>
            <key>rotated</key>
            <false/>
            <key>sourceColorRect</key>
            <string>{{0,0},{{7},{8}}}</string>
            <key>sourceSize</key>
            <string>{{9},{10}}</string>
            </dict>
            `,
            key.replace("_png", ".png"), frame.x, frame.y, frame.w, frame.h, frame.offX, frame.offY, frame.w, frame.h, frame.w, frame.h
          );

          frameList.push(frameStr);
        }

        let imagePath = dirName + "/" + imageFileName;
        var dimensions = sizeOf(imagePath);
        let plistData = format(`
          <?xml version="1.0" encoding="UTF-8"?>
          <!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
          <plist version="1.0">
          <dict>
          <key>frames</key>
          <dict>
          {0}
          </dict>
          <key>metadata</key>
          <dict>
          <key>format</key>
          <integer>2</integer>
          <key>size</key>
          <string>{{1},{2}}</string>
          <key>textureFileName</key>
          <string>{3}</string>
          </dict>
          </dict>
          </plist>
          `, frameList.join(""), dimensions.width, dimensions.height, imageFileName);
        console.log(newFilePath.join("/"));
        fs.writeFile(newFilePath.join("/"), plistData, (err) => {
          if (!err) {
            fs.unlink(filePath, (err) => {

            });
          }
        });
      } catch (e) {

      }
    }
  }
}

walk('./', (err, results) => {
  for (let filePath of results) {
    // removeMD5(filePath);
    convetFntJson(filePath);
    // convertWebp(filePath);
    convertAtlasJsonToPlist(filePath);
  }
});