import { MiniPokerPlayerInfo } from './model/MiniPokerPlayerInfo';
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {MiniPokerPlayer} from "./MiniPokerPlayer";
import {Config} from "../../../../scripts/Config";
import { moon } from '../../../../scripts/core/GlobalInfo';
import { Card } from '../../../../scripts/core/Card';


const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerChessLayer extends cc.Component {

  playerCards = {};

  setupChesses(playerInfoList: Array<MiniPokerPlayerInfo>) {
    this.node.removeAllChildren();
    this.playerCards = {};
    let chessWidth = 60;
    playerInfoList.forEach(playerInfo => {
      cc.log('player info', playerInfo)
      playerInfo.resultCard.cardList.forEach(chess => {
        let cardNode = moon.cardPool.getCard(chess)
        let cardHeigh = moon.cardPool.cardHeight;
        let cardWidth = moon.cardPool.cardWidth;
        cardNode.getComponent(Card).setCardSize(cardWidth, cardHeigh)
        cc.log('get card with size',cardWidth, cardHeigh)
        this.node.addChild(cardNode)
        cardNode.x = 0;
        cardNode.opacity = 0;
         if (this.playerCards[playerInfo.userId]) {
          this.playerCards[playerInfo.userId].push(cardNode);
        } else {
          this.playerCards[playerInfo.userId] = []
          this.playerCards[playerInfo.userId].push(cardNode);
        }
        // TODO: replace by cards
        // let chessNode = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chess);
        // chessNode.setScale(0.8, 0.8);
        // let chessComponent: Chess = chessNode.getComponent(Chess);
        // this.node.addChild(chessNode);
        // chessComponent.chessId = chess;
        // chessComponent.pushUpBig();
        // chessNode.x = 0;
        // chessNode.opacity = 0;
        // if (this.playerChesses[playerInfo.userId]) {
        //   this.playerChesses[playerInfo.userId].push(chessNode);
        // } else {
        //   this.playerChesses[playerInfo.userId] = []
        //   this.playerChesses[playerInfo.userId].push(chessNode);
        // }
      });
    });
  }

  dealChess(player: MiniPokerPlayer, delay = 0) {
    let cards: Array<cc.Node> = this.playerCards[player.playerInfo.userId];
    let positions = player.getReceivedPositions();
    let delayFactor = 0.1;

    let i = 0;
    cards.forEach(card => {
      NodeUtils.swapParent(card, card.parent, player.chessContent);
      card.runAction(
        cc.sequence(
          cc.callFunc(() => {
            card.active = true;
          }),
          cc.delayTime(delayFactor * i),
          cc.spawn(
            cc.fadeIn(0.2),
            cc.moveTo(Config.chessFlyTime, positions[i]).easing(cc.easeIn(1.0))
          )
        )
      );
      i++;
    });
    player.setCardNodes(cards);
  }
}