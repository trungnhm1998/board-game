import {CountdownInfo, Room} from "../../../../../scripts/model/Room";
import {RedBlackPlayerInfo} from "./RedBlackPlayerInfo";

export class RedBlackRoom extends Room {
  matchId;
  redAppearCardList;
  blackAppearCardList;
  betList;
  timeout;
  playerInfoList: RedBlackPlayerInfo[];
  firstId;
  countdownInfo: CountdownInfo;
  betMoneyTotal;
  playerNum: number;
  totalRedBetMoney: number;
  totalBlackBetMoney: number;
  totalSpecialBetMoney: number;
  placeBetTypeList: any[]
}