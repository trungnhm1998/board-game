import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {moon} from "../core/GlobalInfo";
import {LocalData, LocalStorage} from "../core/LocalStorage";

const {ccclass, property} = cc._decorator;

@ccclass
export class SettingDialog extends SceneComponent {

  @property(cc.Slider)
  music: cc.Slider = null;

  @property(cc.Slider)
  sound: cc.Slider = null;

  @property(cc.Node)
  musicBg: cc.Node = null;

  @property(cc.Node)
  soundBg: cc.Node = null;

  lastSlideTime = 0;

  onEnter() {
    super.onEnter();
    this.music.progress = LocalData.music;
    this.sound.progress = LocalData.sound;
    this.onMusicSlide();
    this.onSoundSlide();
  }

  onLeave() {
    super.onLeave();
    moon.storage.saveLocalData();
  }

  onMusicSlide() {
    this.musicBg.width = this.music.progress * this.music.node.width;
    let curTime = (new Date()).getTime();
    if (curTime - this.lastSlideTime > 50) {
      this.lastSlideTime = curTime;
      moon.audio.setMusicVolume(this.music.progress);
    }
  }

  onSoundSlide() {
    this.soundBg.width = this.sound.progress * this.sound.node.width;
    let curTime = (new Date()).getTime();
    if (curTime - this.lastSlideTime > 50) {
      this.lastSlideTime = curTime;
      moon.audio.setSoundVolume(this.sound.progress);
    }
  }

  onLanguageChange() {
    NodeUtils.setSprites(this.node,
      {
        'title_sprite': 'game_setting_title',
        'music_sprite': 'game_setting_txt_1',
        'sound_sprite': 'game_setting_txt_2',
        'language_sprite': 'game_setting_txt_3',
        'screen_sprite': 'game_setting_txt_4',
        'locale_sprite': 'game_setting_language',
        'fullscreen_sprite': !moon.header.isFull ? 'game_setting_fullScreen' : 'game_setting_restore',
      }
    );
    moon.dialog.hideWaiting();
  }

  onToggleFullScreen() {
    moon.audio.playAudio('game_sound_btn_click');
    moon.header.toggleFullscreen();
    NodeUtils.setSprites(this.node,
      {
        'fullscreen_sprite': moon.header.isFull ? 'game_setting_fullScreen' : 'game_setting_restore',
      }
    )
  }

  onToggleLanguage() {
    moon.audio.playAudio('game_sound_btn_click');
    let lang = moon.locale.getCurrentLanguage();
    if (lang == 'en') {
      lang = 'zh';
    } else {
      lang = 'en';
    }
    moon.dialog.showWaiting();
    moon.locale.setLang(lang);
  }

  closeDialog() {
    moon.audio.playAudio('game_sound_btn_click');
    super.closeDialog();
  }
}