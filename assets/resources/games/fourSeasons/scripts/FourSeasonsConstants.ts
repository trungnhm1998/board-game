export enum BetType {
  DEALER = 0,
  SPADE = 1,
  HEART,
  CLUB,
  DIAMOND,
}

export const BetTypeStr = [
  'spade',
  'club',
  'heart',
  'diamond'
]

export const KEYS = {
  PLACE_BET_TYPE: "placeBetType",
}

export const CardType = {
  1: {
    name: 'straight_flush',
    banner: 'red_banner'
  },
  2: {
    name: 'four_of_a_kind',
    banner: 'red_banner'
  },
  3: {
    name: 'full_house',
    banner: 'red_banner'
  },
  4: {
    name: 'flush',
    banner: 'orange_banner'
  },
  5: {
    name: 'straight',
    banner: 'orange_banner'
  },
  6: {
    name: 'three_of_a_kind',
    banner: 'orange_banner'
  },
  7: {
    name: 'two_pairs',
    banner: 'blue_banner'
  },
  8: {
    name: 'one_pair',
    banner: 'blue_banner'
  },
  9: {
    name: 'high_card',
    banner: 'green_banner'
  }
}