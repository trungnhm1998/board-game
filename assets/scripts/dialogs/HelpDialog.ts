import {SceneComponent} from "../common/SceneComponent";
import {moon} from "../core/GlobalInfo";
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

let colors = {
  gameSelected: '#fff2cd',
  gameNormal: '#81601f',
  tabNormal: '#5b6d3c',
  tabSelected: '#b6760c'
};

export interface IGameHelp {
  [key: string]: ITabHelp;
}

export interface ITabHelp {
  name: string;
  tabSize: number[];
  tabs: [{
    name: string,
    type: string,
    content: string,
  }];
}

@ccclass
export class HelpDialog extends SceneComponent {

  @property(cc.Node)
  gameNode: cc.Node = null;

  @property(cc.Node)
  tabNode: cc.Node = null;

  @property(cc.ScrollView)
  gamesView: cc.ScrollView = null;

  @property(cc.ScrollView)
  content: cc.ScrollView = null;

  @property(cc.Node)
  tabs: cc.Node = null;

  @property(cc.RichText)
  textContent: cc.RichText = null;

  @property(cc.Sprite)
  imageContent: cc.Sprite = null;

  gameHelp: IGameHelp;

  lastGameNode;
  lastGameId;
  gameNodes = {};

  lastTabNode;
  tabNodes = {};
  currentLang = '';

  onEnter() {
    super.onEnter();
  }

  onLanguageChange() {
    NodeUtils.setSprites(this.node,
      {
        'title_sprite': 'game_help_title',
      }
    );
    if (this.currentLang != moon.locale.getCurrentLanguage()) {
      this.gamesView.content.removeAllChildren();
      this.initGames();
      this.currentLang = moon.locale.getCurrentLanguage();
    }
  }

  initGames() {
    this.gameHelp = moon.locale.get("helpRules");
    let keys = Object.keys(this.gameHelp);
    for (let gameId of keys) {
      this.addGame(gameId);

    }
    this.onChangeGame(null, keys[0]);
  }

  onChangeGameClick(evt, gameId) {
    moon.audio.playAudio('game_sound_btn_click');
    this.onChangeGame(evt, gameId);
  }

  onChangeGame(evt, gameId) {
    if (this.lastGameNode == this.gameNodes[gameId]) {
      return;
    }

    if (this.lastGameNode) {
      NodeUtils.hideByName(this.lastGameNode, 'selected');
      let gameNameNode = NodeUtils.findByName(this.lastGameNode, 'gameName');
      gameNameNode.color = cc.color().fromHEX(colors.gameNormal);
    }
    this.lastGameNode = this.gameNodes[gameId];
    this.lastGameId = gameId;
    NodeUtils.showByName(this.lastGameNode, 'selected');
    let gameNameNode = NodeUtils.findByName(this.lastGameNode, 'gameName');
    gameNameNode.color = cc.color().fromHEX(colors.gameSelected);
    this.setTabs(this.gameHelp[gameId]);
  }

  onChangeTabClick(evt, tabName) {
    moon.audio.playAudio('game_sound_btn_click');
    this.onChangeTab(evt, tabName);
  }

  onChangeTab(evt, tabName) {
    if (this.lastTabNode) {
      NodeUtils.hideByName(this.lastTabNode, 'selected');
      let gameNameNode = NodeUtils.findByName(this.lastTabNode, 'tabName');
      gameNameNode.color = cc.color().fromHEX(colors.tabNormal);
    }
    this.lastTabNode = this.tabNodes[tabName];
    NodeUtils.showByName(this.lastTabNode, 'selected');
    let gameNameNode = NodeUtils.findByName(this.lastTabNode, 'tabName');
    gameNameNode.color = cc.color().fromHEX(colors.tabSelected);
    this.showContent(this.gameHelp[this.lastGameId], tabName);
  }

  showContent(tabHelp: ITabHelp, tabName) {
    let tabInfo = tabHelp.tabs.filter(tab => tab.name == tabName)[0];
    this.textContent.node.active = false;
    this.imageContent.node.active = false;
    switch (tabInfo.type) {
      case 'text':
        this.textContent.node.active = true;
        this.textContent.string = tabInfo.content;
        break;
      case 'image':
        this.imageContent.node.active = true;
        this.imageContent.spriteFrame = moon.res.getSpriteFrame(tabInfo.content);
        break;
    }
  }

  private addGame(gameId: string) {
    let gameNode = cc.instantiate(this.gameNode);
    gameNode.active = true;
    NodeUtils.setLabel(gameNode, 'gameName', this.gameHelp[gameId].name);
    this.gamesView.content.addChild(gameNode);
    this.gameNodes[gameId] = gameNode;
    let btn = gameNode.getComponent(cc.Button);
    btn.clickEvents[0].customEventData = gameId;
  }

  private setTabs(tabHelp: ITabHelp) {
    this.tabs.removeAllChildren();
    this.tabNodes = [];
    let totalSize = 0;
    for (let size of tabHelp.tabSize) {
      totalSize += size;
    }

    for (let i = 0; i < tabHelp.tabs.length; i++) {
      let tab = tabHelp.tabs[i];
      let tabNode = cc.instantiate(this.tabNode);
      tabNode.width = this.tabs.width * tabHelp.tabSize[i] / totalSize;
      tabNode.active = true;
      this.tabs.addChild(tabNode);
      tabNode.position = cc.v2();
      this.tabNodes[tab.name] = tabNode;
      NodeUtils.setLabel(tabNode, 'tabName', tab.name);
      let btn = tabNode.getComponent(cc.Button);
      btn.clickEvents[0].customEventData = tab.name;
    }
    this.onChangeTab(null, tabHelp.tabs[0].name);
  }

  showByGameId(gameId) {
    this.onChangeGame(null, gameId);
  }

  closeDialog() {
    moon.audio.playAudio('game_sound_btn_click');
    super.closeDialog();
  }
}