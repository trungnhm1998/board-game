import {MiniPokerStartState} from '../gamestates/MiniPokerStartState';
import {MiniPokerEndGame} from '../gamestates/MiniPokerEndGame';
import {MiniPokerPlayerInfo} from '../model/MiniPokerPlayerInfo';
import {MiniPokerRoom} from '../model/MiniPokerRoom';
import {MiniPokerBetState} from '../gamestates/MiniPokerBetState';
import {MiniPokerReadyState} from '../gamestates/MiniPokerReadyState';
import {IGameService} from '../../../../../scripts/services/GameService';
import {MiniPokerGame} from '../MiniPokerGame';
import {COUNTDOWN_TYPE, GAME_STATE, KEYS, SERVER_EVENT} from '../../../../../scripts/core/Constant';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {CountdownInfo} from '../../../../../scripts/model/Room';
import {ModelUtils} from '../../../../../scripts/core/ModelUtils';
import {FadeOutInTransition} from '../../../../../scripts/services/SceneManager';
import {GameSocket} from '../../../../../scripts/services/SocketService';
import {MiniPokerRoomService} from './MiniPokerRoomService';


export class MiniPokerService implements IGameService {
  isActive = false;

  name = 'MiniPokerService';

  game: MiniPokerGame;

  waitingActions: any[] = [];

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(MiniPokerGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };

    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => processAction(this.onErrorMessage, data));
  }

  onJoinBoard(data) {
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    cc.log('set join board info', room);
    this.game.setPlayers(room.playerInfoList);
    this.game.setRoom(room);
    this.game.showWaiting();
    this.processWaitingActions();
  }

  onPlayerJoinBoard(data) {
    if (data.roomId !== GlobalInfo.room.roomId) {
      return;
    }
    
    let playerInfo: MiniPokerPlayerInfo = data[KEYS.PLAYER_INFO];
    MiniPokerRoomService.getInstance().addPlayerInfo(playerInfo);
    if (this.game && GlobalInfo.room.roomId == data[KEYS.ROOM_ID]) {
      this.game.addPlayer(data.playerInfo);
    }
    let room: MiniPokerRoom = <MiniPokerRoom> GlobalInfo.room;
    cc.log('player join board', room);
  }

  //add players to this game and show on UI
  // addPlayersToGame(playerInfoList){
  //   let roomService= MiniPokerRoomService.getInstance();
  //   playerInfoList.map(player=>{
  //     cc.log('add player',player)
  //     roomService.addPlayerInfo(player)
  //   })
  //   let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
  //   this.game.setPlayers(room.playerInfoList);
  // }


  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    cc.log('return game and set room data', GlobalInfo.room);
    this.game.onEnterRoom(false, true);
    this.game.updateCurrentRoom(data);
  }

  onReadyStartGame(data) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    this.game.changeState(GAME_STATE.READY_START, MiniPokerReadyState, data);
    this.game.showWaiting();
  }

  onErrorMessage(data: any) {
    moon.dialog.hideWaiting();
    let command = data[KEYS.CAUSE_COMMAND];
    let msg = data[KEYS.MESSAGE];
    // Check cause command first
    moon.dialog.showNotice(msg);
  }

  onActionInGame(data) {

    let subData = data[KEYS.DATA];
    if (subData.roomId !== GlobalInfo.room.roomId) {
      return
    }
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.READY_START_GAME:
          this.onReadyStartGame(subData);
          break;
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        case SERVER_EVENT.ERROR_MESSAGE:
          this.onErrorMessage(subData);
          break;
      }
    };
    handleEvents();
  }


  private onStopGame() {
    this.game.showWaiting();
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let roomService = MiniPokerRoomService.getInstance();
    if (GlobalInfo.me.userId == userId) {
      let backToMain = () => {
        this.game.clear();

        roomService.stopCountdown(roomId);
        this.clearWaitingActions();
        moon.dialog.showWaiting();
        let transition = new FadeOutInTransition(0.2);
        moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
        if (this.game.state) {
          this.game.state.onLeave(this.game,{})
        }
        this.game.returnBackToGameLobby()
      };
      backToMain();
    } else {
      this.game.standByRemovePlayer(userId);
    }
  }

  onStartGame(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: MiniPokerRoom = <MiniPokerRoom> GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    room.betMoney = data.betMoney;
    room.matchId = data.matchId;
    MiniPokerRoomService.getInstance().activeCountdown();

    this.game.hideWaiting();
    this.game.changeState(GAME_STATE.START, MiniPokerStartState, data);
  }

  private onBeginBetDoDealer(data: any) {

  }


  private onBeginBet(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    this.game.changeState(GAME_STATE.BET, MiniPokerBetState, data);
  }

  private onBet(data: any) {
    this.game.setBetFactorOfPlayer(data.userId, data.betFactor);
  }

  private onEndGame(data: any) {
    // Update Data/Model for client
    MiniPokerRoomService.getInstance().updatePlayerInfoList(data[KEYS.PLAYER_INFO_LIST]);
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;

    // update animation and UI
    this.game.changeState(GAME_STATE.END, MiniPokerEndGame);
  }

  updateRoomInfoList() {

  }
}