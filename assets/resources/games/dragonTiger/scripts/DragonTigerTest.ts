import {TestCase} from "../../../../scripts/test/TestCase";
import {DragonTigerPlayerInfo} from "./model/DragonTigerPlayerInfo";
import {GlobalInfo} from "../../../../scripts/core/GlobalInfo";
import {UserInfo} from "../../../../scripts/model/UserInfo";
import {DragonTigerBetInfo, DragonTigerRoom} from './model/DragonTigerRoom';
import {DragonTigerGame} from "./DragonTigerGame";
import {GameItem} from "../../../../scripts/model/GameInfo";

export class DragonTigerTest extends TestCase {

  game: DragonTigerGame;

  setGame(gameNode) {
    super.setGame(gameNode);
    this.game = gameNode.getComponent(DragonTigerGame);
  }

  run() {
    super.run();
    this.setGameInfo({
      gameId: 2,
      packageName: "dragonTiger",
      prefabPath: "games/dragonTiger/dragonTigerGame",
      localeFolder: "games/dragonTiger/locale",
      service: "DragonTigerService",
      deckPath: 'card/decks/1'
    }).then(() => {
      this.runTestCases();
    })
  }

  runTestCases() {
    this.setup();
    this.runSequence(
      [
        [this.testInitBets, 0],
        [this.testSetPlayer, 0],
        [this.testSetRoom, 0],
        [this.testPeopleCount, 0],
        [this.testClock, 0],
        [this.testRemainChess, 1],
        [this.testStatus, 0],
        [this.testPots, 0.5],
        [this.testHistory, 0],
        [this.testSkipCard, 1],
        [this.testDealDragonChess, 1],
        [this.testDealTigerChess, 1],
        [this.testShowCard, 0],
        [this.testPlayBets, 1],
        [this.testHousePay, 1],
        [this.testEndMoneyFly, 1],
        [this.testStartGameEffect, 1.5],
        [this.testStopGameEffect, 1.5],
        [this.testDragonWinEffect, 1.5],
        [this.testTigerWinEffect, 1.5],
        [this.testDrawEffect, 1.5],
        [this.testPlayStartMatch, 1.5],
        [this.testDragonWinEffect, 1.5],
        [this.testClearGame, 0]
      ]
    );
    this.finalize();
  }

  setup() {
    this.setupRoom();
    this.setupPlayers();
    this.game.onLanguageChange();
  }

  setupRoom() {
    let room = new DragonTigerRoom();
    room.gameId = 1;
    room.roomId = 1;
    room.betMoney = 1;
    room.matchId = 1;
    room.matchOrder = 1;
    room.timeout = 5;
    room.roomId = "T3001";
    GlobalInfo.room = room;

    let gameItem: any = new GameItem();
    gameItem.gameInfo = [
      {
        "groupId": "Bronze",
        "roomIdList": [
          "T3001",
          "T3002",
          "T3003",
          "T3004"
        ],
        "betMoneyList": [
          0.1,
          0.5,
          1,
          2,
          5,
          10
        ]
      }
    ];

    GlobalInfo.gameItem = gameItem;
  }

  setupPlayers() {
    GlobalInfo.me = new UserInfo();
    GlobalInfo.me.userId = 1;

    let playerInfo1 = new DragonTigerPlayerInfo();
    playerInfo1.userId = 1;
    playerInfo1.displayName = "player 1";
    playerInfo1.money = 2000;
    playerInfo1.avatar = 'https://i.imgur.com/yqC45Nj.png';

    let room = GlobalInfo.room as DragonTigerRoom;
    room.playerInfo = playerInfo1;
  }

  runSequence(actions) {
    let ccActions = [];
    for (let action of actions) {
      let func = action[0];
      let delay = action[1];
      let returnCallback;
      ccActions.push(cc.callFunc(() => {
        try {
          returnCallback = func.call(this);
          if (returnCallback && typeof returnCallback == 'function') {

          } else {
            console.log(`Done`);
          }
        } catch (e) {
          console.error(`Failed:`, e)
        }

      }));
      ccActions.push(cc.delayTime(delay));
      ccActions.push(cc.callFunc(() => {
        if (returnCallback && typeof returnCallback == 'function') {
          returnCallback();
          console.log(`Done`);
        }
      }))
    }

    this.game.node.runAction(cc.sequence(ccActions));
  }

  finalize() {

  }


  private testInitBets() {
    let betList = [0.1, 0.5, 1, 2, 5, 10];
    // this.game.bets.setBetList(betList);
    // this.assertEqual(6, this.game.bets.node.childrenCount);
  }

  private testSetPlayer() {
    let room = GlobalInfo.room as DragonTigerRoom;
    // this.game.setPlayer(room.playerInfo);
    // this.assertEqual('player 1', this.game.myPlayer.playerName.string)
    // this.assertEqual('2,000', this.game.myPlayer.money.string)
  }

  private testSetRoom() {
    let room = GlobalInfo.room as DragonTigerRoom;
    this.game.setRoom(room);
    // this.assertEqual('T3001', this.game.roomId.string);
    // this.assertEqual('1', this.game.matchId.string);
    this.assertEqual('0.1-10', this.game.limit.string);
  }

  private testPeopleCount() {
    this.game.setPeopleCount(0);
    // this.assertEqual('0', this.game.peopleCount.string);

    this.game.setPeopleCount(100);
    // this.assertEqual('100', this.game.peopleCount.string);
  }

  private testClock() {
    this.game.runCountdown(10);
    this.assertNotEqual('0', this.game.clock.string);
  }

  private testRemainChess() {
    this.game.setRemain(10);
    this.assertEqual('10', this.game.gameTable.chessRemain.string);
    this.game.decreaseRemain();
    this.assertEqual('9', this.game.gameTable.chessRemain.string);
  }

  private testStatus() {
    this.game.setStatus('BETTING');
    // this.assertEqual('BETTING', this.game.gameStatus.string);
    this.game.setStatus('RESULT');
    // this.assertEqual('RESULT', this.game.gameStatus.string);
  }

  private testPots() {
    // this.game.potDragon.setTotalMoney(1000.1);
    // this.game.potDragon.setMyMoney(100);
    //
    // this.game.potTiger.setTotalMoney(500);
    // this.game.potTiger.setMyMoney(50);
    //
    // this.game.potDraw.setTotalMoney(300);
    // this.game.potDraw.setMyMoney(0.1);
    //
    // this.assertEqual('1,000.1', this.game.potDragon.totalMoney.string);
    // this.assertEqual('100', this.game.potDragon.myMoney.string);
    // this.assertEqual('500', this.game.potTiger.totalMoney.string);
    // this.assertEqual('50', this.game.potTiger.myMoney.string);
    // this.assertEqual('300', this.game.potDraw.totalMoney.string);
    // this.assertEqual('0.1', this.game.potDraw.myMoney.string);
  }

  private testHistory() {
    this.game.gameTable.roomHistory.setHistory([
      [1],[1,2,3],[1,2],[2],[3,1,3,1,1,1],[1]
    ]);
    // this.assertEqual(11, this.game.history.formatedResults.length);
    // this.game.history.addResult(1);
    // this.assertEqual(12, this.game.history.formatedResults.length);
    // this.game.history.addResult(1);
    // this.assertEqual(12, this.game.history.formatedResults.length);
    // this.game.history.addResult(2);
    // this.assertEqual(13, this.game.history.formatedResults.length);
    // this.game.history.addResult(3);
    // this.assertEqual(13, this.game.history.formatedResults.length);
    // this.game.history.addResult(3);
    // this.assertEqual(14, this.game.history.formatedResults.length);
  }

  private testPlayBets() {
    const { gameTable } = this.game;
    gameTable.bets.playMyBet(gameTable.potDragon, 50);
    // gameTable.bets.playPeopleBet(gameTable.potDragon, (10 + 5 + 2 + 1 + 0.5 + 0.1), (<DragonTigerBetInfo[]>GlobalInfo.gameItem.gameInfo)[0].betMoneyList);

    gameTable.bets.playMyBet(gameTable.potTiger, 50);
    // gameTable.bets.playPeopleBet(gameTable.potTiger, (10 + 5 + 2 + 1 + 0.5 + 0.1), (<DragonTigerBetInfo[]>GlobalInfo.gameItem.gameInfo)[0].betMoneyList);

    gameTable.bets.playMyBet(gameTable.potDraw, 50);
    // gameTable.bets.playPeopleBet(gameTable.potDraw, (10 + 5 + 2 + 1 + 0.5 + 0.1), (<DragonTigerBetInfo[]>GlobalInfo.gameItem.gameInfo)[0].betMoneyList);

    return (() => {
      this.assertEqual(7, gameTable.potDragon.betArea.childrenCount);
      this.assertEqual(7, gameTable.potTiger.betArea.childrenCount);
      this.assertEqual(7, gameTable.potDraw.betArea.childrenCount);
    });
  }

  private testHousePay() {
    const { gameTable } = this.game;
    // gameTable.bets.housePay(gameTable.potDragon, (10 + 5 + 2 + 1 + 0.5 + 0.1), (<DragonTigerBetInfo[]>GlobalInfo.gameItem.gameInfo)[0].betMoneyList);
    // gameTable.bets.housePay(gameTable.potTiger, (10 + 5 + 2 + 1 + 0.5 + 0.1), (<DragonTigerBetInfo[]>GlobalInfo.gameItem.gameInfo)[0].betMoneyList);
    // gameTable.bets.housePay(gameTable.potDraw, (10 + 5 + 2 + 1 + 0.5 + 0.1), (<DragonTigerBetInfo[]>GlobalInfo.gameItem.gameInfo)[0].betMoneyList);

    return (() => {
      this.assertEqual(13, gameTable.potDragon.betArea.childrenCount);
      this.assertEqual(13, gameTable.potTiger.betArea.childrenCount);
      this.assertEqual(13, gameTable.potDraw.betArea.childrenCount);
    });
  }

  private testEndMoneyFly() {
    const { gameTable } = this.game;
    gameTable.bets.moveChipsToHouse(gameTable.potDraw);
    gameTable.bets.moveChipsToHouse(gameTable.potTiger);
    gameTable.bets.moveChipsToPeople(gameTable.potDragon);

    return (() => {
      this.assertEqual(0, gameTable.potDragon.betArea.childrenCount);
      this.assertEqual(0, gameTable.potTiger.betArea.childrenCount);
      this.assertEqual(0, gameTable.potDraw.betArea.childrenCount);
    });
  }

  private testStartGameEffect() {
    const { gameTable } = this.game;
    gameTable.effects.playStartGame();
    return (() => {
      gameTable.effects.hide();
    });
  }


  private testStopGameEffect() {
    const { gameTable } = this.game;
    gameTable.effects.playStopGame();
    return (() => {
      gameTable.effects.hide();
    });
  }

  private testDragonWinEffect() {
    const { gameTable } = this.game;
    gameTable.effects.playDragonWin();
    return (() => {
      gameTable.effects.hide();
    });
  }

  private testTigerWinEffect() {
    const { gameTable } = this.game;
    gameTable.effects.playTigerWin();
    return (() => {
      gameTable.effects.hide();
    });
  }

  private testDrawEffect() {
    const { gameTable } = this.game;
    gameTable.effects.playDraw();
    return (() => {
      gameTable.effects.hide();
    });
  }

  private testPlayStartMatch() {
    const { gameTable } = this.game;
    gameTable.effects.playStartMatch(0);
    return (() => {
      gameTable.effects.hide();
    });
  }

  private testDealDragonChess() {
    this.game.gameTable.dealer.dealCard(this.game.gameTable.potDragon, 12);
    return (() => {

    });
  }

  private testDealTigerChess() {
    this.game.gameTable.dealer.dealCard(this.game.gameTable.potTiger, 13);
    return (() => {

    });
  }

  private testSkipCard() {
    this.game.gameTable.dealer.skipCard(this.game.gameTable.clock.node);
    return (() => {
    });
  }

  private testShowCard() {
    // this.game.potDragon.openCard();
    // this.game.potTiger.openCard();
  }

  private testClearGame() {
    this.game.gameTable.clear();
  }
}