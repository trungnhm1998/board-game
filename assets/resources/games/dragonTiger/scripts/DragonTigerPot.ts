import { DragonTigerSocket } from './service/DragonTigerSocketService';
import { Card } from './../../../../scripts/core/Card';
import {DragonTigerService} from './service/DragonTigerService';
import {moon} from "../../../../scripts/core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerPot extends cc.Component {
  @property(cc.Label)
  totalMoney: cc.Label = null;

  @property(cc.Label)
  myMoney: cc.Label = null;

  @property(cc.Node)
  betArea: cc.Node = null;

  @property(cc.Node)
  dealNode: cc.Node = null;

  private bet: any;
  private placeBetType: any;
  card: Card;

  setcard(card) {
    this.clearCard();
    this.card = card;
  }

  openCard() {
    if (this.card) {
      this.card.showCard();
      this.node.runAction(cc.sequence(
        cc.callFunc(() => {
          moon.audio.playEffectInGame('cards_open', false);
        }),
        cc.callFunc(() => {
          const cardPoints = this.card.getCardPoint();
          moon.audio.playEffectInGame(`longhu_n0${cardPoints}`, true);
        })
      ))
    }
  }

  clear() {
    this.myMoney.string = '0';
    this.totalMoney.string = '0';
    this.clearCard();
  }

  clearCard() {
    if (this.card) {
      this.card.node.active = false;
      this.card.node.removeFromParent();
      moon.cardPool.putCards(this.card);
      this.card = null;
    }
  }

  setBetMoney(bet) {
    this.bet = bet;
  }

  setPlaceBetType(placeType) {
    this.placeBetType = placeType;
  }

  setMyMoney(money) {
    this.myMoney.string =  moon.string.formatMoney(moon.string.round(money, 2));
  }

  setTotalMoney(money) {
    this.totalMoney.string =  moon.string.formatMoney(moon.string.round(money, 2));
  }

  onBetClick() {
    if (this.bet) {
      let dragonTigerService: DragonTigerService = moon.service.getCurrentService() as DragonTigerService;
      dragonTigerService.setTempBet(this.bet, this.placeBetType);
      DragonTigerSocket.bet(this.bet, this.placeBetType);
    }
  }

  getDealPos() {
    return this.dealNode.convertToWorldSpaceAR(cc.v2());
  }
}