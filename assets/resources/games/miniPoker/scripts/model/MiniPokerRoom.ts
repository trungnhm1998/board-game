import {MiniPokerPlayerInfo} from "./MiniPokerPlayerInfo";
import {CountdownInfo, Room} from "../../../../../scripts/model/Room";

export class MiniPokerRoom extends Room {
  matchId;
  matchOrder;
  matchTotal;
  appearChessList;
  betList;
  timeout;
  playerInfoList: MiniPokerPlayerInfo[];
  firstId;
  countdownInfo: CountdownInfo;
  dealerId;
  betMoneyTotal;
  shakeDiceResult;
}