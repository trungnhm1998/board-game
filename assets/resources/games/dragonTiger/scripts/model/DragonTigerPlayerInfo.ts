import {DragonTigerBetInfo} from "./DragonTigerRoom";

export class DragonTigerPlayerInfo {
  userId: any;
  displayName: string;
  money: number;
  avatar: string;
  placeBetTypeList: DragonTigerBetInfo[];
}