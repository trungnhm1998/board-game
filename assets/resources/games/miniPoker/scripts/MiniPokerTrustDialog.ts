const {ccclass, property} = cc._decorator;

@ccclass
export default class MiniPokerTrustDialog extends cc.Component {

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Node)
  tableOverviewSample: cc.Node = null;

  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  formScrollView: cc.Node = null;

  @property(cc.Node)
  sampleResult: cc.Node = null;

  @property([cc.Toggle])
  betToogle: cc.Toggle[] = [];


  onLoad() {


    this.updateTableLocale();
  }

  onEnable() {

  }

  onDisable() {

  }

  onLanguageChange() {
    this.updateTableLocale();
  }

  setupOverviewTables() {

  }


  updateTableLocale() {

  }


}