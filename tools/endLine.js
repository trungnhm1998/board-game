const fs = require("fs");
const path = require("path");

const walkSync = function (dir, filelist) {
  const files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function (file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = walkSync(dir + '/' + file, filelist);
    }
    else {
      let ext = path.extname(file);
      if (ext == '.meta') {
        filelist.push(dir + '/' + file);
      }
    }
  });
  return filelist;
};

let files = walkSync('./assets');
for (let filePath of files) {
  let ext = path.extname(filePath);
  let str = fs.readFileSync(filePath);
  let content = str.toString();
  content = content.replace(/\\r\\n/g, '\n');
  fs.writeFileSync(filePath, content);
}