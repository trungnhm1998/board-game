import {RecordDialog} from "../dialogs/RecordDialog";

const {ccclass, property} = cc._decorator;

@ccclass
export default class TestComponent extends cc.Component {

  @property(RecordDialog)
  hisDialog: RecordDialog = null;

  // LIFE-CYCLE CALLBACKS:

  // onLoad () {}

  start() {
    this.hisDialog.onEnter();
  }


  // update (dt) {}
}
