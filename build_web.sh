PROJECT_DIR=~/workspace/moon-client

cd ${PROJECT_DIR}/tools
python prebuild.py -env web.production
cd ${PROJECT_DIR}

/Applications/CocosCreator.app/Contents/MacOS/CocosCreator --path . --build "platform=web-mobile;md5Cache=true;startScene=cddde544-7cac-43fd-a22e-698bdb50f3ea;title=moon;inlineSpriteFrames=true;mergeStartScene=true;debug=false;webOrientation=landscape;"

cd ${PROJECT_DIR}/tools
python postbuild.py
cd ${PROJECT_DIR}

cp -r build/web-mobile/* ../moon-client-prod/play/test/
cd ../moon-client-prod/play
python deploy.py
cd ${PROJECT_DIR}





