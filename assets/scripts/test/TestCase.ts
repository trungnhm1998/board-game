import {IGameService} from "../services/GameService";
import {RESOURCE_BLOCK} from "../core/Constant";
import {moon} from "../core/GlobalInfo";

export class TestCase {
  gameNode;

  run() {
  }

  setGame(gameNode) {
    this.gameNode = gameNode;
  }

  setGameInfo(gameInfo) {
    return new Promise<any>((resolve) => {
      moon.service.preloadGameService(gameInfo.service)
        .then(() => {
          moon.service.currentGameInfo = gameInfo;
          let service: IGameService = moon.service.getCurrentService();
          service.setGameNode(this.gameNode);

          let resourceMgr = moon.res;
          resourceMgr.preloadFolder(
            `${moon.service.currentGameInfo.localeFolder}/${moon.locale.getCurrentLanguage()}`,
            (value) => {
            }, RESOURCE_BLOCK.GAME).then(() => {
            resolve();
          });
        })
    });
  }

  assertEqual(expect, actual, msg?) {
    if (expect == actual) {
    } else {
      if (!msg) {
        msg = `${actual} is not equal to expected: ${expect}`;
      }
      throw new Error(msg);
    }
  }

  assertNotEqual(expect, actual, msg?) {
    if (expect != actual) {
    } else {
      if (!msg) {
        msg = `${actual} is equal to expected: ${expect}`;
      }
      throw new Error(msg);
    }
  }
}