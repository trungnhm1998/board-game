import {IShader} from '../scripts/shader/CMaterial';

const renderEngine = (<any>cc).renderer.renderEngine;
const renderer = renderEngine.renderer;

export const CountdownShader: IShader = {
  name: 'Countdown',
  params: [
    {name: 'percent', type: renderer.PARAM_FLOAT, defaultValue: 0},
  ],

  update(material, percent) {
    material.setParamValue('percent', percent);
  },

  defines: [],

  vert: `
        uniform mat4 viewProj;
        attribute vec3 a_position;
        attribute vec2 a_uv0;
        varying vec2 uv0;
        
        void main () {
            vec4 pos = viewProj * vec4(a_position, 1);
            gl_Position = pos;
            uv0 = a_uv0;
        }`,

  frag:
    `
        #define PI 3.14159
        
        uniform sampler2D texture;
        #define PI 3.14159

        varying vec2 uv0;
        uniform float percent;
        
        
        void main()
        {
            vec4 rColor = texture2D(texture, uv0);
            vec2 v = vec2(uv0.x-0.5, uv0.y-0.5);
            float angleRad =  atan(v.y, v.x);
        
            if (v.x < 0.0 && v.y < 0.0) {
                angleRad += 2.0 * PI;
            }
        
            angleRad += PI / 2.0;
        
            float startAngle = percent * PI * 2.0;
            if ((angleRad < 2.0 * PI  && angleRad > startAngle ) ) {
                rColor = rColor;
            } else {
                rColor = vec4(0.0);
            }
        
            gl_FragColor = rColor;
        }
    `
};