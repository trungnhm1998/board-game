import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {LocalData} from "../core/LocalStorage";
import {moon} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;


@ccclass
export class BuyinDialog extends SceneComponent {

  @property(cc.Slider)
  moneySlider: cc.Slider = null;

  @property(cc.Node)
  moneySliderFg: cc.Node = null;

  @property(cc.Toggle)
  autoBuyIn: cc.Toggle = null;

  @property(cc.Label)
  currentMoney: cc.Label = null;

  @property(cc.Label)
  min: cc.Label = null;

  @property(cc.Label)
  max: cc.Label = null;

  minValue = 0;
  maxValue = 0;
  money = 0;
  step = 1;
  percentStep = 1;
  totalStep = 1;
  percentList = [];

  onEnter() {
    super.onEnter();
    this.autoBuyIn.isChecked = (<any>LocalData).autoBuyIn == '1';
  }

  onLeave() {
    super.onLeave();
  }

  onLanguageChange() {
    super.onLanguageChange();
    NodeUtils.setLocaleLabels(this.node, {
      'title': ['buyIn', {upper: true}],
      'auto_lbl': ['autoBuyIn', {}],
      'ok_lbl': ['buy', {upper: true}],
    });
  }

  onMoneySlide() {
    let progress = this.moneySlider.progress;
    let percentIndex = 0;
    for (let i = 0; i < this.totalStep - 1; i++) {
      let floor = this.percentList[i];
      let ceil = this.percentList[i + 1];
      if (progress >= floor && progress <= ceil) {
        let center = (floor + ceil) / 2;
        if (progress < center) {
          progress = floor;
          percentIndex = i;
        } else {
          progress = ceil;
          percentIndex = i + 1;
        }
        break;
      }
    }
    this.moneySlider.progress = progress;
    this.moneySliderFg.width = progress * this.moneySlider.node.width;
    if (percentIndex == this.totalStep - 1) {
      this.money = this.maxValue;
    } else {
      this.money = Math.floor(this.minValue + percentIndex * this.step);
    }
    this.currentMoney.string = moon.string.formatMoney(this.money);
  }

  onToogleAuto() {
    LocalData.autoBuyIn = this.autoBuyIn.isChecked ? 1 : 0;
    moon.storage.saveLocalData();
  }

  setMinMax(min, max, bet = 1) {
    let times = Math.floor(max / min);
    let tempMax = min * times;
    this.step = this.calculateStep(bet, min, tempMax);
    this.percentStep = this.step / (tempMax - min);
    this.totalStep = Math.floor((tempMax - min) / this.step);

    this.percentList = [];
    for (let i = 0; i < this.totalStep; i++) {
      this.percentList.push(this.percentStep * (i + 1));
    }
    this.percentList[0] = 0;
    this.percentList[this.totalStep - 1] = 1;
    this.minValue = min;
    this.maxValue = max;
    this.refreshMinMax();
    this.onMoneySlide();
  }

  refreshMinMax() {
    this.min.string = moon.string.formatMoney(this.minValue);
    this.max.string = moon.string.formatMoney(this.maxValue);
  }

  calculateStep(bet, min, max) {
    let tempStep = Math.floor(max - min) / 16;
    if (bet > tempStep) {
      return bet;
    } else {
      return Math.floor(tempStep / bet) * bet;
    }
  }

  onBuyIn() {

  }
}