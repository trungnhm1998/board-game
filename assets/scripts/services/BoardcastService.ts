export class BoardcastService {
  private static _instance: BoardcastService;

  static getInstance(): BoardcastService {
    if (!this._instance) {
      this._instance = new BoardcastService;
    }

    return this._instance;
  }
}
