const renderEngine = (<any>cc).renderer.renderEngine;
const renderer = renderEngine.renderer;
const gfx = renderEngine.gfx;
const Material: IMaterial = renderEngine.Material;

export interface IMaterial {
  new (param: boolean);
}

export interface IShader {
  name;
  params;

  update(...args: any[]);

  defines;
  vert;
  frag;

  [key: string]: any;
}

export const shaders = {};

export class CMaterial extends Material {

  name = '';
  _effect;
  _texture;
  _color;
  _texIds;

  static addShader(shader) {
    if (shaders[shader.name]) {
      console.log("addShader - shader already exist: ", shader.name);
      return;
    }

    if ((<any>cc).renderer._forward) {
      (<any>cc).renderer._forward._programLib.define(shader.name, shader.vert, shader.frag, shader.defines || []);
      shaders[shader.name] = shader;
    } else {
      //在微信上初始时cc.renderer._forward不存在，需要等引擎初始化完毕才能使用
      cc.game.once((<any>cc).game.EVENT_ENGINE_INITED, function () {
        (<any>cc).renderer._forward._programLib.define(shader.name, shader.vert, shader.frag, shader.defines || []);
        shaders[shader.name] = shader;
      });
    }
  }

  static getShader(name) {
    return shaders[name];
  }

  get effect() {
    return this._effect;
  }

  get texture() {
    return this._texture;
  }

  set texture(val) {
    if (this._texture !== val) {
      this._texture = val;
      this._effect.setProperty('texture', val.getImpl());
      this._texIds['texture'] = val.getId();
    }
  }

  get color() {
    return this._color;
  }

  set color(val) {
    let color = this._color;
    color.r = val.r / 255;
    color.g = val.g / 255;
    color.b = val.b / 255;
    color.a = val.a / 255;
    this._effect.setProperty('color', color);
  }

  constructor(shaderName?, params?, defines?) {
    super(false);

    let pass = new renderer.Pass(shaderName);
    pass.setDepth(false, false);
    pass.setCullMode(gfx.CULL_NONE);
    pass.setBlend(
      gfx.BLEND_FUNC_ADD,
      gfx.BLEND_SRC_ALPHA, gfx.BLEND_ONE_MINUS_SRC_ALPHA,
      gfx.BLEND_FUNC_ADD,
      gfx.BLEND_SRC_ALPHA, gfx.BLEND_ONE_MINUS_SRC_ALPHA
    );

    let techParams = [
      {name: 'texture', type: renderer.PARAM_TEXTURE_2D},
      {name: 'color', type: renderer.PARAM_COLOR4}
    ];
    if (params) {
      techParams = techParams.concat(params);
    }
    let mainTech = new renderer.Technique(
      ['transparent'],
      techParams,
      [pass]
    );

    this.name = shaderName;

    this._effect = new renderer.Effect(
      [mainTech],
      {},
      defines,
    );
    this._texture = null;
    this._color = {r: 1, g: 1, b: 1, a: 1};

    this._mainTech = mainTech;
  }

  clone() {
    let copy = new CMaterial();
    copy.texture = this.texture;
    copy.color = this.color;
    copy.updateHash();
    return copy;
  }

  // 设置自定义参数的值
  setParamValue(name, value) {
    this._effect.setProperty(name, value);
  }

  // 获取自定义参数的值
  getParamValue(name) {
    return this._effect.getProperty(name);
  }

  // 设置定义值
  setDefine(name, value) {
    this._effect.define(name, value);
  }
}