import {SceneComponent} from '../common/SceneComponent';
import {GameItem} from '../model/GameInfo';
import {GameMenuItem} from '../lobby/GameMenuItem';
import {GlobalInfo, moon} from '../core/GlobalInfo';
import {SCENE_TYPE, SERVER_EVENT} from '../core/Constant';
import {NodeUtils} from '../core/NodeUtils';

const {ccclass, property} = cc._decorator;

@ccclass
export class MainMenu extends SceneComponent {
  name = SCENE_TYPE.MAIN_MENU;

  @property(cc.ScrollView)
  games: cc.ScrollView = null;

  @property(cc.Node)
  backConfirm: cc.Node = null;

  @property(cc.Prefab)
  menuItemPrefab: cc.Prefab = null;

  @property(cc.Node)
  bgSke: cc.Node = null;

  @property(cc.Node)
  birdSke: cc.Node = null;

  currentGameItem: GameMenuItem;

  gameItems = [];

  onLoad() {
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.games.node.scale = uiRatio;
    this.bgSke.scaleX = cc.winSize.width / 1280;
    this.bgSke.scaleY = cc.winSize.height / 720;
    this.bgSke.y = cc.winSize.height / 2;
    this.bgSke.x = -cc.winSize.width / 2;

    this.birdSke.scaleX = cc.winSize.width / 1280;
    this.birdSke.scaleY = cc.winSize.height / 720;
    this.birdSke.y = cc.winSize.height / 2;
    this.birdSke.x = -cc.winSize.width / 2;
  }

  onEnter() {
    super.onEnter();
    moon.header.setMainLobby();
    moon.header.show();
    moon.audio.playAudio('mainhall_sound_bg', true, true);
    this.setGameItems(GlobalInfo.listGame);
    this.onResize();
  }

  onLanguageChange() {
    moon.header.onLanguageChange();
    NodeUtils.setLocaleLabels(this.node, {
      // 'autoBuyIn_lbl': ['autoBuyIn', {}],
    });
    this.setGameItems(GlobalInfo.listGame);
  }

  onMoneyChange(value?, amount?) {
    moon.header.setMoney(value);
  }

  setGameItems(gameItems: Array<GameItem>) {
    let activeGameItems = [].concat(this.gameItems);
    this.gameItems = [];
    for (let gameItem of gameItems) {
      let menuItemNode = activeGameItems.shift();
      if (!menuItemNode) {
        menuItemNode = cc.instantiate(this.menuItemPrefab);
      }
      let menuItem: GameMenuItem = menuItemNode.getComponent(GameMenuItem);
      menuItem.setGameItem(gameItem);
      if (!menuItemNode.parent) {
        this.games.content.addChild(menuItemNode);
      }
      this.gameItems.push(menuItemNode);
    }

    for (let removeGameItem of activeGameItems) {
      removeGameItem.removeFromParent();
    }
  }

  showProfile() {
    if (GlobalInfo.requestedCMD[SERVER_EVENT.GET_PROFILE]) {
      moon.dialog.showProfile();
    } else {
      moon.dialog.showWaiting();
    }
    GlobalInfo.requestedCMD[SERVER_EVENT.GET_PROFILE] = true;
    moon.lobbySocket.getProfile();
  }

  showSettings() {
    moon.dialog.showSettings();
  }

  showTutorial() {
    moon.dialog.showHelp();
  }

  showBackConfirm() {
    if (this.backConfirm.active) {
      cc.game.end();
      return;
    }
    this.backConfirm.active = true;
    this.backConfirm.opacity = 255;
    this.backConfirm.stopAllActions();
    this.backConfirm.runAction(
      cc.sequence(
        cc.delayTime(2),
        cc.fadeOut(1),
        cc.callFunc(() => {
          this.backConfirm.active = false;
        })
      )
    );
  }
}
