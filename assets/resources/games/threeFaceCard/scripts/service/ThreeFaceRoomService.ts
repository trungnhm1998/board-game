import {ThreeFaceRoom} from "../model/ThreeFaceRoom";
import {ThreeFacePlayerInfo} from "../model/ThreeFacePlayerInfo";
import {KEYS} from "../../../../../scripts/core/Constant";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";

export class ThreeFaceRoomService {
  private countdownTasks = {};

  private static instance: ThreeFaceRoomService;

  static getInstance(): ThreeFaceRoomService {
    if (!ThreeFaceRoomService.instance) {
      ThreeFaceRoomService.instance = new ThreeFaceRoomService();
    }

    return ThreeFaceRoomService.instance;
  }

  updatePlayerInfoList(playerInfoList: ThreeFacePlayerInfo[]) {
    for (let i = 0; i < playerInfoList.length; i++) {
      const playerInfo = playerInfoList[i];
      let currentRoomPlayerInfo = this.getPlayerInfo(playerInfo.userId);
      let keysToUpdate = Object.keys(playerInfo);
      if (currentRoomPlayerInfo[KEYS.RESULT_CARD] == undefined) {
        currentRoomPlayerInfo[KEYS.RESULT_CARD] = {
          [KEYS.BONUS_FACTOR]: 0,
          [KEYS.CARD_LIST]: [],
          [KEYS.CARD_TYPE]: 0,
          [KEYS.POINT]: 0,
        }
      }
      keysToUpdate.forEach(key => {
        if (key == KEYS.RESULT_CARD) {
          let subKeysToUpdate = Object.keys(playerInfo[KEYS.RESULT_CARD]);
          subKeysToUpdate.forEach(subKey => {
            currentRoomPlayerInfo[key][subKey] = playerInfo[key][subKey]
          });
        }
        currentRoomPlayerInfo[key] = playerInfo[key];
      });
    }
  }

  getPlayerInfo(userId) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    for (let i = 0; i < room.playerInfoList.length; i++) {
      const playerInfo = room.playerInfoList[i];
      if (playerInfo.userId == userId) {
        return playerInfo;
      }
    }
  }

  removePlayerInfo(userId: any) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    let newPlayerInfoList = room.playerInfoList.filter(player => player.userId !== userId)
    room.playerInfoList = newPlayerInfoList
  }

  clearPlayerInfoList() {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.playerInfoList = []
  }

  addPlayerInfo(playerInfo) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.playerInfoList.push(playerInfo);
  }

  activeCountdown() {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    if (room && room.countdownInfo && room.countdownInfo.timeout > 0) {
      this.stopCountdown(room.roomId);
      this.countdownTasks[room.roomId] = moon.timer.tweenNumber(room.countdownInfo.timeout, -room.countdownInfo.timeout, (val) => {
        room.countdownInfo.timeout = val;
      }, () => {
      }, room.countdownInfo.timeout);
    }
  }

  stopCountdown(roomId) {
    if (this.countdownTasks[roomId]) {
      moon.timer.removeTask(this.countdownTasks[roomId]);
      this.countdownTasks[roomId] = null;
    }
  }

  clearCardList() {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      playerInfo.resultCard = {
        point: 1,
        cardList: [-1, -1, -1],
        cardType: 1,
        bonusFactor: 1
      }
    }
  }
}