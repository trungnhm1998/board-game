import {TwoEightPlayerInfo} from './model/TwoEightPlayerInfo';
import {RESOURCE_BLOCK, TWO_EIGHT_AUDIO, TWO_EIGHT_RESULT_TYPE, COUNTDOWN_TYPE} from '../../../../scripts/core/Constant';
import {AvatarIcon} from '../../../../scripts/common/AvatarIcon';
import {GlobalInfo, moon} from '../../../../scripts/core/GlobalInfo';
import {Card} from '../../../../scripts/core/Card';
import {LanguageService} from '../../../../scripts/services/LanguageService';
import { CString } from '../../../../scripts/core/String';
import { TwoEightRoom } from './model/TwoEightRoom';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightPlayer extends cc.Component {

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Sprite)
  status: cc.Sprite = null;

  @property(cc.Node)
  betRow: cc.Node = null;

  @property(cc.Node)
  snatchRow: cc.Node = null;

  @property(cc.Label)
  snatch: cc.Label = null;

  @property(cc.Node)
  dealer: cc.Node = null;

  @property(cc.Node)
  highlight: cc.Node = null;

  @property(cc.Node)
  chessContent: cc.Node = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  winAmount: cc.Label = null;

  @property(cc.Label)
  loseAmount: cc.Label = null;

  @property(cc.Sprite)
  betUnit: cc.Sprite = null;

  @property(dragonBones.ArmatureDisplay)
  playerWin: dragonBones.ArmatureDisplay = null;

  @property(cc.Node)
  pointBanner: cc.Node = null;

  playerInfo: TwoEightPlayerInfo;
  private chessNodes: any;

  onLanguageChange() {

  }

  placeBet(times) {
    this.status.node.active = false;
    this.betRow.active = false;
    this.bet.node.active = true;
    this.betUnit.node.active = true;
    this.bet.string = times;
    let lang = moon.locale.getCurrentLanguage();
    if (times > 1) {
      // this.betRow.active = true;
      // this.betUnit.spriteFrame = moon.res.getSpriteFrame('bets', RESOURCE_BLOCK.GAME);
    } else if (times == 1) {
      // this.betRow.active = true;
      // this.betUnit.spriteFrame = moon.res.getSpriteFrame('bet', RESOURCE_BLOCK.GAME);
    } else if (times == 0) {
      this.status.node.active = true;
      this.status.spriteFrame = moon.res.getSpriteFrame('check', RESOURCE_BLOCK.GAME);
    } else {
      this.status.node.active = true;
      this.status.spriteFrame = moon.res.getSpriteFrame('thinking', RESOURCE_BLOCK.GAME);
    }
  }

  setPlayerInfo(playerInfo: TwoEightPlayerInfo) {
    cc.log('set playerinfo',playerInfo)
    this.playerInfo = playerInfo;
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    if (playerInfo.betFactor) {
      switch (room.countdownInfo.type) {
        case COUNTDOWN_TYPE.STATE_CHOOSE_BANKER:
          this.playerInfo.snatch = playerInfo.betFactor
          break;
        case COUNTDOWN_TYPE.STATE_BET:
          this.playerInfo.bet = playerInfo.betFactor
          break;
      }
    }
    
    playerInfo.formatedDisplayName = this.formatPlayerName(playerInfo);
    this.playerName.string = playerInfo.formatedDisplayName;
    this.money.string = moon.string.formatMoney(playerInfo.money);
    this.avatar.setImageUrl(playerInfo.avatar);
  }

  formatPlayerName(playerInfo: TwoEightPlayerInfo) {
    return moon.string.formatPlayerName(playerInfo.displayName, GlobalInfo.me.userId === playerInfo.userId)
  }

  getSelectAnim() {
    let delayTime = 0.1;
    let round = 3;
    return cc.sequence(
      cc.callFunc(() => {
        this.highlight.opacity = 180;
      }),
      cc.delayTime(delayTime),
      cc.callFunc(() => {
        this.highlight.opacity = 0;
      }),
      cc.delayTime(delayTime),
    ).repeat(round);
  }

  showBorder(isShown = true) {
    this.highlight.runAction(
      cc.fadeTo(0.05, isShown ? 180 : 0)
    );
  }

  showDealer(isShown = true) {
    this.dealer.active = isShown;
    this.dealer.getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('dealer');
  }

  getReceivedPositions() {
    let fittedChessWidth = moon.cardPool.cardWidth * 0.6;
    let pos1 = cc.v2();
    let pos2 = cc.v2(fittedChessWidth, 0);
    return [pos1, pos2];
  }

  setCardNodes(chesses: any) {
    this.chessNodes = chesses;
    this.chessContent.active = true;
  }

  getCardPoint(cardId: number) {
    let resF = cardId / 4 + 1;
    let resInt = parseInt(String(resF), 10);
    //if this is K, then count it as 10 by the rule of this game
    if (resInt === 13) resInt = 10;
    return resInt;

  }

  getResultType() {
    const card = this.playerInfo.cardList;
    cc.log('get result type of',card)
    if (!card) return '';
    let cardPoint_1 = this.getCardPoint(card[0]);
    let cardPoint_2 = this.getCardPoint(card[1]);
    //check if this is a pair
    if (cardPoint_1 === cardPoint_2) {
      return TWO_EIGHT_RESULT_TYPE.PAIR;
    }
    //check if it a 2-8
    if ((cardPoint_1 == 2 && cardPoint_2 == 8) || (cardPoint_1 == 8 && cardPoint_2 == 2)) {
      return TWO_EIGHT_RESULT_TYPE.TWO_EIGHT;
    }

    let point = (cardPoint_1 + cardPoint_2) % 10;
    if (point == 0) {
      return TWO_EIGHT_RESULT_TYPE.ZERO;
    }
    else {
      return TWO_EIGHT_RESULT_TYPE.OTHER;
    }
  }

  setSnatchValue(value) {
    this.stopThinking();
    this.playerInfo.snatch = value;
    this.snatchRow.active = true;
    if (value > 0) {

      this.snatchRow.getChildByName('house_ico').active = true;
      this.snatchRow.getChildByName('value').active = true;
      this.snatchRow.getChildByName('nonSnatch').active = false;

      this.snatchRow.getChildByName('house_ico').getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('snatchFactor_ico');
      this.snatch.string = value.toString() + ' ' + LanguageService.getInstance().get('times');

    } else {
      this.snatchRow.getChildByName('house_ico').active = false;
      this.snatchRow.getChildByName('value').active = false;

      let snatchNode = this.snatchRow.getChildByName('nonSnatch');
      snatchNode.active = true;
      snatchNode.getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('nonSnatch');
    }


    this.snatchRow.runAction(
      cc.sequence(
        cc.scaleTo(0.15, 1.5),
        cc.delayTime(0.1),
        cc.scaleTo(0.15, 1)
      )
    );
  }

  hideSnatchValue() {
    this.snatchRow.active = false;
  }

  setBetValue(value) {
    this.stopThinking();
    this.betRow.active = true;
    this.bet.string = value.toString() + ' ' + LanguageService.getInstance().get('times');
    this.betRow.getChildByName('bet_ico').getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('betFactor_ico');
    this.betRow.runAction(
      cc.sequence(
        cc.scaleTo(0.15, 1.5),
        cc.delayTime(0.1),
        cc.scaleTo(0.15, 1)
      )
    );
  }

  hideBetValue() {
    this.betRow.active = false;
  }


  //get player point, just use in case player get pair or normal point
  getPlayerPoints() {
    const chessIds = this.playerInfo.cardList;
    let cardPoint_1 = this.getCardPoint(chessIds[0]);
    let cardPoint_2 = this.getCardPoint(chessIds[1]);
    if (cardPoint_1 !== cardPoint_2) {
      return (cardPoint_1 + cardPoint_2) % 10;
    } else {
      return cardPoint_1;
    }
  }

  getTextResultFromChessList() {
    let locale = moon.locale;
    const chessIds = this.playerInfo.cardList;
    if (!chessIds) return '';
    //check if this is a pair

    let resType = this.getResultType();
    if (resType == TWO_EIGHT_RESULT_TYPE.PAIR) {
      let kind;
      switch (chessIds[0]) {
        case 10:
          kind = locale.get('white');
          break;
        default:

      }
      let point = this.getCardPoint(chessIds[0]);
      if (point !== 10) {
        kind = point.toString() + ' ' + locale.get('dots');
      } else {
        return moon.string.format(locale.get('pairOf'), 'K');
      }

      return moon.string.format(locale.get('pairOf'), kind);
    }
    //check if it a 2-8
    if (resType == TWO_EIGHT_RESULT_TYPE.TWO_EIGHT) {
      return locale.get('twoEight');
    }

    //else calculate it point
    let cardPoint_1 = this.getCardPoint(chessIds[0]);
    let cardPoint_2 = this.getCardPoint(chessIds[1]);
    let total = (cardPoint_1 + cardPoint_2) % 10;
    if (total < 2) {
      return total + ' ' + locale.get('point');
    }
    else {
      return total + ' ' + locale.get('points');
    }
  }


  resetPointBanner() {
    let banner = this.pointBanner.getChildByName('red_pointBanner');
    banner.active = false;
    banner = this.pointBanner.getChildByName('yellow_pointBanner');
    banner.active = false;
    banner = this.pointBanner.getChildByName('grey_pointBanner');
    banner.active = false;
    banner = this.pointBanner.getChildByName('pink_pointBanner');
    banner.active = false;
  }

  resetChessContent() {
    this.chessContent.removeAllChildren()
  }

  showPointBanner() {
    if (this.pointBanner) {
      this.pointBanner.active = true;
      this.resetPointBanner();
      let banner: cc.Node;
      let soundFileName;
      switch (this.getResultType()) {
        case TWO_EIGHT_RESULT_TYPE.PAIR:
          banner = this.pointBanner.getChildByName('pink_pointBanner');
          soundFileName = 'pair_' + this.getPlayerPoints();
          break;
        case TWO_EIGHT_RESULT_TYPE.TWO_EIGHT:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          soundFileName = '28';
          break;
        case TWO_EIGHT_RESULT_TYPE.OTHER:
          banner = this.pointBanner.getChildByName('red_pointBanner');
          soundFileName = 'point_' + this.getPlayerPoints();
          break;
        case TWO_EIGHT_RESULT_TYPE.ZERO:
          banner = this.pointBanner.getChildByName('grey_pointBanner');
          soundFileName = 'point_0';
          break;
      }

      banner.active = true;
      banner.x = 0;
      banner.y = 0;
      banner.scaleX = 0;
      banner.scaleY = 0;
      banner.runAction(
        cc.sequence(
          cc.scaleTo(0.2, 0.95),
          cc.delayTime(0.1),
        )
      );

      let point = this.pointBanner.getChildByName('point');
      point.active = true;

      let label = point.getComponent(cc.Label);
      let locale = moon.locale;

      label.string = this.getTextResultFromChessList();
      if (locale.getCurrentLanguage() === 'en') {
        label.fontSize = 20;
      }
      else {
        label.fontSize = 25;
      }

      point.scaleX = 0;
      point.scaleY = 0;
      moon.audio.playVoiceInGame(soundFileName);
      point.runAction(
        cc.sequence(
          cc.scaleTo(0.15, 1.5),
          cc.delayTime(0.1),
          cc.scaleTo(0.15, 1)
        )
      );
    }
  }

  showChesses(delay = 0) {
    let actions = [];
    actions.push(cc.delayTime(delay));
    for (let chessNode of this.chessNodes) {
      let chess: Card = chessNode.getComponent(Card);
      actions.push(cc.callFunc(() => {
        //chess.hide();
        moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.CARD);
        chess.showCard();
      }));
      actions.push(cc.delayTime(0.5));

    }
    actions.push(cc.callFunc(this.showPointBanner, this));
    this.node.runAction(
      cc.sequence(actions)
    );
  }

  setChessesByIdList(chessList) {
    this.chessContent.removeAllChildren()
    let chessNode1 = moon.cardPool.getCard(chessList[0]);
    let chessNode2 = moon.cardPool.getCard(chessList[1]);
    let chess1: Card = chessNode1.getComponent(Card);
    let chess2: Card = chessNode2.getComponent(Card);
    this.chessContent.addChild(chessNode1);
    this.chessContent.addChild(chessNode2);
    chess1.showCard();
    chess2.showCard();
    let positions = this.getReceivedPositions();
    chessNode1.position = positions[0];
    chessNode2.position = positions[1];
    this.setCardNodes([chessNode1, chessNode2]);
  }

  setMoney(money: any) {
    this.money.string = moon.string.formatMoney(money);
  }

  playWinEffect() {
    this.playerWin.node.active = true;
    this.playerWin.playAnimation('playerWin', 1);
  }

  playAmountEffect(amount) {
    let playMoneyAnim = (target) => {
      let startPos = this.playerName.node.position;
      target.stopAllActions();
      target.position = startPos;
      target.opacity = 255;
      target.runAction(
        cc.sequence(
          cc.moveBy(0.5, cc.v2(0, 50)).easing(cc.easeBackOut()),
          cc.delayTime(2),
          cc.fadeOut(0.1)
        )
      );
    };
    if (amount > 0) {
      this.winAmount.node.active = true;
      this.winAmount.string = '+' + amount;
      playMoneyAnim(this.winAmount.node);
    } else {
      this.loseAmount.node.active = true;
      this.loseAmount.string = '' + amount;
      playMoneyAnim(this.loseAmount.node);
    }
  }


  resetPlayerGameStatus() {
    
    this.playerInfo.bet = 0;
    this.playerInfo.snatch = 0;
    this.playerInfo.cardList = []
  }

  clear() {
    
    if (this.playerInfo) {
      this.resetPlayerGameStatus()
    }
    this.status.node.active = false;
    this.betRow.active = false;
    this.winAmount.node.active = false;
    this.loseAmount.node.active = false;
    this.dealer.active = false;
    this.highlight.opacity = 0;
    this.pointBanner.active = false;
    this.chessContent.active = false;
    this.snatchRow.active = false;
  }

  startThinking() {
    this.status.node.active = true;
    this.status.spriteFrame = moon.res.getGameSpriteFrame('in_thinking');
  }

  stopThinking() {
    this.status.node.active = false;
  }

  cleanAvatar() {
    this.avatar.clear();
  }

}