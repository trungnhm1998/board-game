
import {BaseSocket} from "../services/SocketService";
import {gamedev} from "gamedevjs";

export class MockSocket extends BaseSocket {
  connect(url) {
    return new Promise<any>((resolve) => {
      resolve();
    });
  }

  on(serverEvent, callback, replace = false) {
    gamedev.event.register(serverEvent, callback, replace);
  }

  off(serverEvent, callback?) {
    gamedev.event.unregister(serverEvent);
  }

  offAll() {
    gamedev.event.unregisterAll();
  }

  emit(serverEvent, data = {}) {
    gamedev.event.emit(serverEvent, data);
  }

  close() {
  }
}