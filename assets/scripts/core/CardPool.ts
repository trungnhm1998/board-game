import {Card} from "./Card";
import {moon} from "./GlobalInfo";
import {RESOURCE_BLOCK} from "./Constant";

export class CardPool {
  cardPrefab = null;
  pool: cc.NodePool;
  workingCards: cc.Node[] = [];
  total = 52;
  deckId;
  defaultCardWidth: number = 125;
  defaultCardHeight: number = 173;
  cardWidth: number = 125;
  cardHeight: number = 173;
  atlas: cc.SpriteAtlas;

  private static instance: CardPool;

  static getInstance(): CardPool {
    if (!CardPool.instance) {
      CardPool.instance = new CardPool();
    }

    return CardPool.instance;
  }

  init() {
    cc.log('on load card pool');

    return new Promise((resolve) => {
      cc.loader.loadRes('card/card/card', (err, res) => {
        if (!err) {
          this.cardPrefab = res;
          this.pool = new cc.NodePool();
          for (let i = 0; i < this.total; i++) {
            let card = cc.instantiate(this.cardPrefab);
            this.pool.put(card);
          }
        }
        resolve();
      });
    });
  }

  clear() {
    this.pool.clear();
  }

  setCardDeck(deckId) {
    this.deckId = deckId;
  }

  setCardSize(width, height) {
    this.cardWidth = width;
    this.cardHeight = height;
  }

  setCardScale(scale) {
    cc.log('set card scale', scale);
    this.cardWidth = scale * this.defaultCardWidth;
    this.cardHeight = scale * this.defaultCardHeight;
  }

  getCard(cardId = -1): cc.Node {
    let cardNode = null;
    if (this.pool.size() > 0) {
      cardNode = this.pool.get();
    } else {
      cardNode = cc.instantiate(this.cardPrefab);
    }
    cardNode.active = true;
    cardNode.removeFromParent();
    cardNode.position = cc.v2();
    cardNode.rotation = 0;
    let card: Card = cardNode.getComponent(Card);
    card.setCardSize(this.cardWidth, this.cardHeight);
    card.clearFront();
    card.setDeckId(this.deckId);
    card.setCardId(cardId);
    card.faceDown();

    this.workingCards.push(cardNode);
    return cardNode;
  }

  returnAllCard() {
    cc.log('return all card');
    for (let node of this.workingCards) {
      node.stopAllActions();
      node.opacity = 255;
      let card: Card = node.getComponent(Card);
      card.clearFront();
      node.active = false;
      this.pool.put(node);
    }
    this.workingCards = [];
  }

  putCards(cards) {
    for (let card of cards) {
      card.stopAllActions();
      card.opacity = 255;
      card.active = false;
      this.pool.put(card);
    }
  }

  getCurrentAtlas() {
    if (!this.atlas) {
      this.updateAtlas();
    }
    return this.atlas;
  }

  updateAtlas() {
    this.atlas = moon.res.getAtlas('atlas_card_' + this.deckId, RESOURCE_BLOCK.GAME);
  }
}