const PLACE_TYPE = {
  DRAGON: 1,
  TIGER: 2,
  DRAW: 3,
  DRAW_DRAGON: -1,
  DRAW_TIGER: -2
};

const KEYS = {
  PLACE_BET_TYPE: "placeBetType",
}

const SERVER_GAME_STATE = {
  STATE_NOT_START_GAME: 0,
  STATE_READY_START_GAME: 1,
  STATE_START_GAME: 2,
  STATE_RESTART_GAME: 3,
  STATE_CHOOSE_BANKER: 4,
  STATE_FIND_BANKER: 5,
  STATE_BET: 6,
  STATE_DEAL_CARD: 7,
  STATE_OPEN_CARD: 8,
  STATE_RESULT: 9,
};

const DragonTigerConstants = {
  PLACE_TYPE,
  KEYS,
  SERVER_GAME_STATE
}

export default DragonTigerConstants;