import {NodeUtils} from "../core/NodeUtils";
import {KEYS} from "../core/Constant";
import {SceneComponent} from "../common/SceneComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class AppDialog extends SceneComponent {
  callback: any;

  @property(cc.EditBox)
  otp: cc.EditBox = null;

  @property(cc.Label)
  secretKey: cc.Label = null;

  @property(cc.Sprite)
  qrCode: cc.Sprite = null;

  onEnter() {
    super.onEnter();
    this.otp.string = '';
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'lbl_title', 'activeGAuth', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'confirm_lbl', 'confirm');
    NodeUtils.setLocaleLabel(this.node, 'cancel_lbl', 'cancel');
    NodeUtils.setLocaleLabel(this.node, 'content_lbl', 'authInfo');
    NodeUtils.setLocaleLabel(this.node, 'otp', 'enterOTP', {placeHolder: true});
  }

  setData(secretKey, urlQR) {
    this.secretKey.string = secretKey;
    cc.log('qr code url',urlQR)
    moon.res.setRemoteImage(this.qrCode, urlQR);
  }

  setCallback(callback) {
    this.callback = callback;
  }

  onSubmit() {
    moon.lobbySocket.selectAuthorizeType(KEYS.APPLICATION, this.otp.string, KEYS.ON);
      // .then((data) => {
      //   if (!data) return;
      //
      //   if (data[KEYS.DATA][KEYS.MESSAGE]) {
      //     moon.dialog.showNotice(data[KEYS.DATA][KEYS.MESSAGE]);
      //   }
      //
      //   if (this.callback) {
      //     this.callback();
      //   }
      //   this.closeDialog();
      // });
  }

  onCopy() {
     moon.string.copyText(this.secretKey.string);
  }
}
