import {MiniPokerRoom} from "../model/MiniPokerRoom";
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import {MiniPokerGame} from "../MiniPokerGame";

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerReadyState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  countdownTask;

  onEnter(game: MiniPokerGame, data: any) {

    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;
    let timeout = room.countdownInfo.timeout;

    this.showCoundownt();
    this.runCountdown(timeout);
  }

  onUpdate(game: MiniPokerGame, updateData: UpdateData) {

  }


  showCoundownt() {
    this.countdown.node.parent.active = true;
    this.countdown.node.active = true;
  }

  hideCoundownt() {
    this.countdown.node.parent.active = false;
    this.countdown.node.active = false;
  }

  onLeave(game: MiniPokerGame, data: any) {
    //   this.checkBtn.removeFromParent();
    // this.node.addChild(this.checkBtn);
    //  this.checkBtn.active = false;
        
    game.clearPlayersUI();
    game.betLayer.clear();
    game.hideBetInfo();
    game.clearPlayerBetFactor();
    // TODO: replace by card
    moon.cardPool.returnAllCard();
    this.hideCoundownt();
    this.stopCountdown()
  }

  runCountdown(max) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num);
      } else {
        this.hideCoundownt()
      }
    }, 1);
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }


}