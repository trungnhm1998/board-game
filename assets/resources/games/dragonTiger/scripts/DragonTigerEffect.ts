import { RESOURCE_BLOCK, GAME_ID } from './../../../../scripts/core/Constant';
import {moon} from "../../../../scripts/core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerEffect extends cc.Component {

  @property(dragonBones.ArmatureDisplay)
  startGame: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  stopGame: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  dragonWin: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  tigerWin: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  draw: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  startMatch: dragonBones.ArmatureDisplay = null;

  @property(cc.Label)
  startMatchNum: cc.Label = null;

  @property(cc.Node)
  mask: cc.Node = null;

  onLoad() {
    this.hide();
  }

  onLanguageChange() {
    this.hide();
    moon.res.preloadFolder(
      `${moon.service.getGameInfoById(GAME_ID.DRAGON_TIGER).localeFolder}/${moon.locale.getCurrentLanguage()}`,
      () => {
      },
      RESOURCE_BLOCK.GAME
    ).then(() => {

      this.startGame.dragonAsset = moon.res.getSkelecton('startGame_ske', RESOURCE_BLOCK.GAME);
      this.startGame.dragonAtlasAsset = moon.res.getGameSpriteFrame('startGame_tex');
  
      this.dragonWin.dragonAsset = moon.res.getSkelecton('dragonWin_ske', RESOURCE_BLOCK.GAME);
      this.dragonWin.dragonAtlasAsset = moon.res.getGameSpriteFrame('dragonWin_tex');
  
      this.tigerWin.dragonAsset = moon.res.getSkelecton('tigerWin_ske', RESOURCE_BLOCK.GAME);
      this.tigerWin.dragonAtlasAsset = moon.res.getGameSpriteFrame('tigerWin_tex');
  
      this.draw.dragonAsset = moon.res.getSkelecton('draw_ske', RESOURCE_BLOCK.GAME);
      this.draw.dragonAtlasAsset = moon.res.getGameSpriteFrame('draw_tex');
  
      this.stopGame.dragonAsset = moon.res.getSkelecton('stopGame_ske', RESOURCE_BLOCK.GAME);
      this.stopGame.dragonAtlasAsset = moon.res.getGameSpriteFrame('stopGame_tex');

    })  
  }

  playStartGame() {
    this.startGame.node.active = true;
    moon.audio.playEffectInGame('longhu_start', true);
    this.startGame.playAnimation('action', 1);
  }

  playStopGame() {
    this.stopGame.node.active = true;
    moon.audio.playEffectInGame("longhu_stop", true);
    this.stopGame.playAnimation('action', 1);
  }

  playDragonWin() {
    this.dragonWin.node.active = true;
    moon.audio.playEffectInGame("longhu_win_long", true);
    this.dragonWin.playAnimation('action', 1);
  }

  playTigerWin() {
    this.tigerWin.node.active = true;
    moon.audio.playEffectInGame("longhu_win_hu", true);
    this.tigerWin.playAnimation('action', 1);
  }

  playDraw() {
    this.draw.node.active = true;
    moon.audio.playEffectInGame("longhu_win_he", true);
    this.draw.playAnimation('action', 1);
  }

  playStartMatch(matchOrder) {
    if (matchOrder > 0) {
      this.startMatch.node.active = true;
      this.startMatchNum.node.active = true;
      this.startMatchNum.string = matchOrder;
    }
    this.mask.active = true;
    this.startMatch.playAnimation('startMatch', 1);
    moon.timer.scheduleOnce(() => {
      this.startMatch.node.active = false;
      this.mask.active = false;
      this.startMatchNum.node.active = false;
    }, 1.5);
  }

  hide() {
    this.startGame.node.active = false;
    this.stopGame.node.active = false;
    this.tigerWin.node.active = false;
    this.dragonWin.node.active = false;
    this.draw.node.active = false;
    this.startMatch.node.active = false;
    this.mask.active = false;
    this.startMatchNum.node.active = false;

    this.startGame.node.cleanup();
    this.stopGame.node.cleanup();
    this.tigerWin.node.cleanup();
    this.dragonWin.node.cleanup();
    this.draw.node.cleanup();
    this.startMatch.node.cleanup();
    this.startMatchNum.node.cleanup();
  }
}