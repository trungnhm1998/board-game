import {ThreeFaceRoom} from "../model/ThreeFaceRoom";
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {ThreeFaceGame} from "../ThreeFaceGame";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import {NodeUtils} from "../../../../../scripts/core/NodeUtils";
import {ThreeFaceSocket} from "../service/ThreeFaceSocket";
import {ThreeFaceRoomService} from "../service/ThreeFaceRoomService";
import {THREE_FACE_AUDIO} from "../ThreeFaceConstants";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceBetState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  betSample: cc.Node = null;

  @property(cc.Node)
  buttons: cc.Node = null;

  @property(cc.Node)
  childView: cc.Node = null;

  @property(cc.Node)
  bankerInfo: cc.Node = null;

  countdownTask;
  autoBetValue = 1;

  onEnter(game: ThreeFaceGame, data: any) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    cc.log('bet state', room);
    let betList = ThreeFaceRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId).betList;
    this.autoBetValue = betList[0] || 1;
    let timeout = room.countdownInfo.timeout;

    this.childView.active = true;
    this.buttons.removeAllChildren();
    this.bankerInfo.active = room.bankerId == GlobalInfo.me.userId;
    this.buttons.active = room.bankerId != GlobalInfo.me.userId;
    if (this.buttons.active) {
      for (let betValue of betList) {
        let betBtnNode = cc.instantiate(this.betSample);
        betBtnNode.active = true;
        let betUnit = moon.locale.get('bets');
        let valueNode = NodeUtils.findByName(betBtnNode, 'betValue');
        let valueLabel = valueNode.getComponent(cc.Label);
        valueLabel.string = '' + betValue + ' ' + betUnit;
        let betBtn: cc.Button = betBtnNode.getComponent(cc.Button);
        let clickEvent = betBtn.clickEvents[0];
        clickEvent.customEventData = '' + betValue;
        this.buttons.addChild(betBtnNode);
        betBtnNode.position = cc.v2();
      }
    }

    if (timeout > 0) {
      this.showCoundownt();
      this.runCountdown(timeout);
    }
  }

  onUpdate(game: ThreeFaceGame, updateData: UpdateData) {

  }


  showCoundownt() {
    this.countdown.node.parent.active = true;
    this.countdown.node.active = true;
  }

  hideCoundownt() {
    this.countdown.node.parent.active = false;
    this.countdown.node.active = false;
  }

  onLeave(game: ThreeFaceGame, data: any) {
    this.stopCountdown();
    this.hideBetFactor();
    this.hideCoundownt();
  }

  runCountdown(max) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num);
      } else {
        this.hideCoundownt();
        this.hideBetFactor();
      }
    }, 1);
  }

  hideBetFactor() {
    this.childView.active = false;
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }

  onBet(evt, value) {
    cc.log('bet with factor', value);
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    ThreeFaceSocket.getInstance().bet(+value);
  }

  onBetEnd() {
    this.childView.active = false;
  }
}