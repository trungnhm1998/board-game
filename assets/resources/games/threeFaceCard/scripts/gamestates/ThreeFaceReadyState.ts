import {ThreeFaceRoom} from "../model/ThreeFaceRoom";
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import {ThreeFaceGame} from "../ThreeFaceGame";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceReadyState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  countdownTask;

  onEnter(game: ThreeFaceGame, data: any) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    let timeout = room.countdownInfo.timeout;

    for (let player of game.players) {
      if (player.playerInfo) {
        player.setMoney(player.playerInfo.money);
      }
    }

    if (timeout > 0) {
      this.showCoundownt();
      this.runCountdown(timeout);
    }
  }

  onUpdate(game: ThreeFaceGame, updateData: UpdateData) {

  }


  showCoundownt() {
    this.countdown.node.parent.active = true;
    this.countdown.node.active = true;
  }

  hideCoundownt() {
    this.countdown.node.parent.active = false;
    this.countdown.node.active = false;
  }

  onLeave(game: ThreeFaceGame, data: any) {
    this.hideCoundownt();
    this.stopCountdown()
  }

  runCountdown(max) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num);
      } else {
        this.hideCoundownt()
      }
    }, 1);
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }


}