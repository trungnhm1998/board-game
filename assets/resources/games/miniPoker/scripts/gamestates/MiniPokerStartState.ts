import {MiniPokerRoom} from "../model/MiniPokerRoom";
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {MiniPokerGame} from "../MiniPokerGame";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { MINIPOKER_AUDIO } from "../../../../../scripts/core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerStartState extends GameState implements IGameState {

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: MiniPokerGame, data: any) {



    game.setRoom(<MiniPokerRoom>GlobalInfo.room);
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;

    // start at current frames
    moon.audio.playEffectInGame(MINIPOKER_AUDIO.GAMESTART)
    let state = this.startAnim.playAnimation(moon.locale.getCurrentLanguage(), 1);
  }

  onUpdate(game: MiniPokerGame, updateData: UpdateData) {
  }

  onLeave(game: MiniPokerGame, data: any) {
  }
}