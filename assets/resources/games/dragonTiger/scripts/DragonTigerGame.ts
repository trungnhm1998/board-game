import { DECK_ID } from './../../../../scripts/core/Constant';
import { CardPool } from './../../../../scripts/core/CardPool';
import { DragonTigerTable } from './DragonTigerTable';
import {GameScene} from "../../../../scripts/common/GameScene";
import {DragonTigerRoom} from "./model/DragonTigerRoom";
import {DragonTigerPlayerInfo} from "./model/DragonTigerPlayerInfo";
import {TimerTask} from "../../../../scripts/core/TimerComponent";
import {DragonTigerPlayer} from "./DragonTigerPlayer";
import {DragonTigerHistory} from "./DragonTigerHistory";
import {DragonTigerBets} from "./DragonTigerBets";
import {DragonTigerPot} from "./DragonTigerPot";
import {GlobalInfo, moon} from "../../../../scripts/core/GlobalInfo";
import {DragonTigerEffect} from "./DragonTigerEffect";
import {DragonTigerDealer} from "./DragonTigerDealer";
import {COUNTDOWN_TYPE, PLACE_TYPE} from "../../../../scripts/core/Constant";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import MultiTableSwitchDialog from './DragonTigerChangeTableDialog'
import {DragonTigerService} from './service/DragonTigerService'
import {RoomInfo} from "../../../../scripts/model/Room";
import {DragonTigerLobby} from "./DragonTigerLobby";

const {ccclass, property} = cc._decorator;


@ccclass
export class DragonTigerGame extends GameScene {

  @property(cc.Label)
  clock: cc.Label = null;

  @property(cc.Label)
  limit: cc.Label = null;

  @property(cc.Node)
  notify: cc.Node = null;

  @property(DragonTigerLobby)
  lobby: DragonTigerLobby = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(DragonTigerTable)
  gameTable: DragonTigerTable = null;

  // for every table this will be a hashmap/dictionary
  /**
   * {
   *  "roomId": {
   *    countdownTask: {},
   *    val: number,
   *  }
   * }
   */
  _lobbyCountdownTask = null

  showSwitchDialog = false;

  onEnter() {
    super.onEnter();
    cc.log('onEnter');
    // // deactive dialog on first run
    // this.switchTableDialog.node.opacity = 0;
    // this.switchTableDialog.node.active = false;

    // if (this.switchTableDialog.overviewTables.length > 0) {
    //   this.switchTableDialog.activeCurrentTableMask(GlobalInfo.room.roomId);
    // }

    this.gameTable.node.active = false;
    CardPool.getInstance().setCardDeck(DECK_ID.TRADITIONAL);
    CardPool.getInstance().setCardScale(1.0);
    this.onResize();
  }

  onEnable() {
  }

  onEnterRoom() {
    super.onEnterRoom();
    cc.log('DragonTigerGame::onEnterRoom()')

    const DTService = moon.service.getCurrentService() as DragonTigerService
    if (DTService.isSwitchTable) {
      DTService.isSwitchTable = false;
    } else {
      moon.header.hide();
      this.lobby.node.runAction(
        cc.sequence(
          cc.fadeOut(0.3),
          cc.callFunc(() => {
            this.lobby.node.active = false;
          })
        )
      )

      this.gameTable.node.opacity = 0;
      this.gameTable.node.active = true;
      this.gameTable.node.runAction(
        cc.sequence(
          cc.delayTime(0.1),
          cc.fadeIn(0.2),
        )
      )
    }
  }

  onLeaveRoom() {
    super.onLeaveRoom();

    moon.header.show();
    this.gameTable.node.runAction(
      cc.sequence(
        cc.fadeOut(0.3),
        cc.callFunc(() => {
          this.gameTable.node.active = false;
        })
      )
    )

    this.lobby.node.opacity = 0;
    this.lobby.node.active = true;
    this.lobby.node.runAction(
      cc.sequence(
        cc.delayTime(0.1),
        cc.fadeIn(0.2),
      )
    )

    moon.dialog.hideWaiting();
  }

  onLeave() {
    cc.log('dragonTigerGame::onLeave');
    this.lobby.clear();
    this.gameTable.clear();
    this.stopCountdown();
  }

  onLanguageChange() {
    super.onLanguageChange();
    NodeUtils.setLocaleLabels(this.ui, {
      "match_lbl": ["matchID", {}],
      "room_lbl": ["room", {}],
      "limit_lbl": ["limit", {}],
      "switch_lbl": ["switch", {}],
      "bet_lbl": ["bet", {}],
      "quit_lbl": ["quit", {}],
      "help_lbl": ["help", {}],
      "setting_lbl": ["setting", {}],
      "record_lbl": ["record", {}],
      "playing_lbl": ["playing", {}]
    });
    // this.switchTableDialog.onLanguageChange();
    this.lobby.onLanguageChange();
    this.gameTable.onLanguageChange();
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
    this.lobby.onResize(uiRatio);
  }

  onPause() {

  }

  onResume() {

  }

  // clear() {
  //   moon.timer.stopAllTasks();
  //   this.myPlayer.clear();
  //   for (let player of this.topPlayers) {
  //     player.clear();
  //   }
  //   this.potTiger.clear();
  //   this.potDragon.clear();
  //   this.potDraw.clear();
  //   this.bets.clear();
  //   this.history.clear();
  //   this.effects.hide();
  //   this.dealer.clear();
  //   this.limit.string = '';
  //   this.roomId.string = '';
  //   this.matchId.string = '';
  //   this.peopleCount.string = '0';
  //   this.gameStatus.string = '';

  //   this.switchTableDialog.node.active = false;
  // }

  clear () {
    this.lobby.clear();
    this.gameTable.clear();
  }

  setRoom(room: DragonTigerRoom) {
    this.gameTable.setupTable(room);
  }

  setPeopleCount(count) {
    this.gameTable.setPlayerNum(count);
  }

  setTopPlayers(playerInfo: DragonTigerPlayerInfo) {

  }

  updateCurrentRoom() {
    /*
    let room = GlobalInfo.room as DragonTigerRoom;
    this.setRoom(room);
    this.setPlayer(room.playerInfo);
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        this.clearChesses();
        this.effects.playStartMatch(room.matchOrder);
        this.runCountdown(room.timeout);
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CHESS:
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        for (let placeType of room.placeTypeList) {
          let pot: DragonTigerPot = this.placePots[placeType.type];
          this.bets.playPeopleBet(pot, placeType.betMoneyTotal, room.betMoneyList, 0);
        }
        break;
      case COUNTDOWN_TYPE.STATE_OPEN_CHESS:
        for (let placeType of room.placeTypeList) {
          let pot: DragonTigerPot = this.placePots[placeType.type];
          this.bets.playPeopleBet(pot, placeType.betMoneyTotal, room.betMoneyList, 0);
        }
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        for (let placeType of room.placeTypeList) {
          let pot: DragonTigerPot = this.placePots[placeType.type];
          this.bets.playPeopleBet(pot, placeType.betMoneyTotal, room.betMoneyList, 0);
        }
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        break;
    }
    this.runCountdown(room.countdownInfo.timeout);

    // update dialog Overview table mask
    if (this.switchTableDialog.overviewTables.length > 0) {
      this.switchTableDialog.activeCurrentTableMask(GlobalInfo.room.roomId);
    }
    */
  }

  removePlayer(userId: any) {

  }

  // run count down to update table timer/ status
  runCountdown(timeout, onLastSecond?) {
    this.gameTable.runCountdown(timeout, onLastSecond);
    // this.clock.string = timeout;
    // this.countdownTask = moon.timer.runCountdown(timeout, (val) => {
    //   this.clock.string = val;
    //   if (val == 1 && onLastSecond) {
    //     onLastSecond();
    //   }
    // })
  }

  stopCountdown() {
    for (let lobbyCountdownTask in this._lobbyCountdownTask) {
      if (lobbyCountdownTask)
        moon.timer.removeTask(this._lobbyCountdownTask[lobbyCountdownTask].countdownTask);
    }

    this._lobbyCountdownTask = null;

    moon.timer.stopAllTasks();
  }

  decreaseRemain() {
    this.gameTable.decreaseRemain();
  }

  setRemain(num) {
    this.gameTable.setRemain(num);
  }

  setStatus(text) {
    this.gameTable.setStatus(text);
  }

  showNotify(msg) {
    this.notify.stopAllActions();
    this.notify.active = true;
    this.notify.opacity = 0;
    NodeUtils.setLabel(this.notify, 'notify', msg);
    this.notify.runAction(
      cc.sequence(
        cc.fadeIn(0.1),
        cc.delayTime(2),
        cc.fadeOut(0.2)
      )
    );
  }

  updateStatus() {
    let room = GlobalInfo.room as DragonTigerRoom;
    let status = (moon.service.getCurrentService() as DragonTigerService).getStatus(room.countdownInfo.type);
    this.setStatus(status);
  }

  onEnterRoomPress(evt, data) {
    cc.log('onEnterRoomPress', data);
    this.clear();
    // let [betMoney, roomId] = data.split(",");
    // LocalData.dragonTigerBet = betMoney;
    // LocalData.dragonTigerRoomId = roomId;
    // moon.storage.saveLocalData();
    // let DTService = moon.service.getCurrentService() as DragonTigerService;
    // DTService.switchTable(betMoney, roomId);
    // moon.gameSocket.leaveBoard();
  }

  updateRoomInfoList(roomInfoList) {
    this.lobby.updateRoomInfoList(roomInfoList as Array<DragonTigerRoom>);
    this.gameTable.changeTableDialog.updateRoomInfoList(roomInfoList as Array<DragonTigerRoom>);

    // start countdownTask for all room
    if (this._lobbyCountdownTask == null) {
      this._lobbyCountdownTask = {};
      roomInfoList.forEach(roomInfo => {
        const { roomId, countdownInfo } = <DragonTigerRoom>roomInfo;
        
        // init placeholder data
        this._lobbyCountdownTask[roomId] = {
          countdownTask: null,
          ended: false,
          val: -1,
          countdownInfo,
        }
        
        // start countdown and call update room UI
        this._lobbyCountdownTask[roomId].countdownTask = moon.timer.runCountdown(countdownInfo.timeout, (val) => {
          this._lobbyCountdownTask[roomId].ended = false;
          this._lobbyCountdownTask[roomId].val = val;
        }, () => {
          // stop countdown flag when done
          this._lobbyCountdownTask[roomId].ended = true;
        })
  
      });
    }
  }
}