const fs = require('fs');

// -- CONFIG --


// -- DIALOG TITLE --
// let targetFNT = 'assets/resources/fonts/' + 'title.fnt';
// let updateData = {
//   'VA': -4,
//   'AV': -4,
//   'AT': -4,
//   'TA': -4,
//   'CT': -4,
//   'ƯỞ': -4,
//   "ỬA": -6,
//   'ẤT': -4,
//   'ƯỢ': -4,
//   'ỢT': -8,
//   'LƯ': -4
// };
// -- END DIALOG TITLE --

// -- SPIN --
// let targetFNT = 'assets/resources/fonts/' + 'spin.fnt';
// let updateData = {
//   'UA': 2,
//   'AY': -6,
//   'QU': 2,
// };
// -- END SPIN --

// -- INGAME --
let targetFNT = 'assets/resources/fonts/' + 'game.fnt';
let updateData = {
  'qu': 0,
  'ua': 0,
  'ay': 0,
  'tự': 0,
  'iữ': 0,
  'BE': 0,
  '00': 1,
  'QU': 0,
  'UA': 0,
  'IN': 1,
  'MI': 1,
  'To': 0,
  'Tr': 0,
  'wa': 0,
  'ar': 1,
  'rd': 0,
  'st': 0,
  'to': 0,
  'Re': 0,
  'un': 0,
  'Hi': 1,
  'or': 0,
  'ƯỞ': -1,
  'HƯ': 1,
  'Mi': 1,
  'in': 1,
  'ni': 1,
  'ig': 0,
  'ga': -2,
  're': 0,
  'JA': 0,
  'AC': 0,
  'CK': 0,
  'WA': -4,
  'ew': 0,
  'NG': 1,
  'LƯ': 0,
  'ƯỢ': 0,
  'ỢT': 0,
  'ng': 0,
  'hu': 0,
  'et': 0,
  'pi': 1,
  'ag': 0,
  'ai': 1,
  'fo': 0,
  'ol': 0,
  'ld': 0,
  'Au': 0,
  'ut': 0,
  'Sp': 0,
  'ta': 0,
  'no': 0,
  'he': 0,
  'fi': 0,
  'it': 0,
  'In': 1,
  'No': 0,
  'Da': 0,
  'at': 0,
  'Tw': -1,
  'Pa': -1,
  'wo': 0,
  'ir': 1,
  'ne': 1,
  'On': 1,
  'Ro': 0,
  'oy': 0,
  'ya': -1,
  'lu': 1,
  'us': 1,
  'sh': 1,
  'al': 0,
  'Fl': 1,
  'Fo': 0,
  'Fu': 0,
  'tr': 1,
  'ul': 1,
  'll': 1,
  'ra': -1,
  'gh': 0,
  'ht': 1,
  'hr': 1,
  'nd': 1,
  'JQ': 1,
  'QK': 1,
  'KA': 1,
  'Ho': 1,
  'ou': 1,
  'Ti': 1,
  'Cư': 1,
  'Wi': 1,
  'LI': 1,
  'NE': 2,
  'OD': 1,
  'EN': 1,
  'DD': 1,
  'HO': 1,
  'OO': 1,
  'IN': 2,
  'LI': 2
};
// -- END INGAME --

let space = 10;
let tab = 30;

// -- END CONFIG --

let fileData = fs.readFileSync(targetFNT, 'utf8');
let lines = fileData.split('\n');


let results = [];

function buildKerning(obj) {
  return `kerning first=${obj.first}  second=${obj.second}  amount=${obj.amount}`
}

for (let line of lines) {
  if (line.indexOf('kerning ') >= 0) {
    let lineData = line.trim().replace('kerning', '').trim();
    let pairs = lineData.split('  ');
    let obj = {};
    for (let pair of pairs) {
      let pairArr = pair.split('=');
      obj[pairArr[0]] = pairArr[1];
    }

    let pattern = String.fromCharCode(obj.first) + String.fromCharCode(obj.second);
    if (updateData[pattern] != undefined) {
      obj.amount = updateData[pattern];
      console.log(obj);
      results.push(buildKerning(obj));
    } else {
      results.push(line);
    }
  } else if (line.indexOf('char id=32 ') > -1) {
    results.push(`char id=32 x=0 y=0 width=0 height=0 xoffset=0 yoffset=0 xadvance=${space} page=0 chnl=0 letter=" "`)
  } else if (line.indexOf('char id=9 ') > -1) {
    results.push(`char id=9 x=0 y=0 width=0 height=0 xoffset=0 yoffset=0 xadvance=${tab} page=0 chnl=0 letter="   "`)
  } else {
    results.push(line);
  }
}

let writeData = results.join('\n');
fs.writeFileSync(targetFNT, writeData, 'utf8');


//  -- PAIRS --
// VA A' AC AG AO AQ AT AU AV AW AY BA BE BL BP BR BU BV BW BY CA CO CR DA DD DE DI DL DM DN DO DP DR DU DV DW DY EC EO FA FC FG FO F. F, GE GO GR GU HO IC IG IO JA JO KO L' LC LT LV LW LY LG LO LU M MG MO NC NG NO OA OB OD OE OF OH OI OK OL OM ON OP OR OT OU OV OW OX OY PA PE PL PO PP PU PY P. P, P; P: QU RC RG RY RT RU RV RW RY SI SM ST SU TA TC TO UA UC UG UO US VA VC VG VO VS WA WC WG WO YA YC YO YS ZO Ac Ad Ae Ag Ao Ap Aq At Au Av Aw Ay Bb Bi Bk Bl Br Bu By B. B, Ca Cr C. C, Da D. D, Eu Ev Fa Fe Fi Fo Fr Ft Fu Fy F. F, F; F: Gu He Ho Hu Hy Ic Id Iq Io It Ja Je Jo Ju J. J, Ke Ko Ku Lu Ly Ma Mc Md Me Mo Nu Na Ne Ni No Nu N. N, Oa Ob Oh Ok Ol O. O, Pa Pe Po Rd Re Ro Rt Ru Si Sp Su S. S, Ta Tc Te Ti To Tr Ts Tu Tw Ty T. T, T; T: Ua Ug Um Un Up Us U. U, Va Ve Vi Vo Vr Vu V. V, V; V: Wd Wi Wm Wr Wt Wu Wy W. W, W; W: Xa Xe Xo Xu Xy Yd Ye Yi Yp Yu Yv Y. Y, Y; Y: ac ad ae ag ap af at au av aw ay ap bl br bu by b. b, ca ch ck da dc de dg do dt du dv dw dy d. d, ea ei el em en ep er et eu ev ew ey e. e, fa fe ff fi fl fo f. f, ga ge gh gl go gg g. g, hc hd he hg ho hp ht hu hv hw hy ic id ie ig io ip it iu iv ja je jo ju j. j, ka kc kd ke kg ko la lc ld le lf lg lo lp lq lu lv lw ly ma mc md me mg mn mo mp mt mu mv my nc nd ne ng no np nt nu nv nw ny ob of oh oj ok ol om on op or ou ov ow ox oy o. o, pa ph pi pl pp pu p. p, qu t. ra rd re rg rk rl rm rn ro rq rr rt rv ry r. r, sh st su s. s, td ta te to t. t, ua uc ud ue ug uo up uq ut uv uw uy va vb vc vd ve vg vo vv vy v. v, wa wx wd we wg wh wo w. w, xa xe xo y. y, ya yc yd ye yo Se in Sk ki CT ƯỞ ỬA IN ẤT ƯỢ ỢT LƯ Mi in ni CK PO ar AR MI ai AI Ai aI In ir IR Ir On us Fl FL ul Ul UL ll tr TR hr HR Hr JQ QK KA Cư LI NE OD EN DD OO

// -- END PAIRS --
