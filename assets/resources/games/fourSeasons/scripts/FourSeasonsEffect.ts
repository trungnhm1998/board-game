import { GAME_ID, RESOURCE_BLOCK } from './../../../../scripts/core/Constant';
import { moon } from './../../../../scripts/core/GlobalInfo';
const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsEffect extends cc.Component {
  @property(dragonBones.ArmatureDisplay)
  startGame: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  stopGame: dragonBones.ArmatureDisplay = null;

  @property(cc.Node)
  mask: cc.Node = null;

  onLoad() {
    this.hide();
  }

  onLanguageChange() {
    this.hide();
    moon.res.preloadFolder(
      `${moon.service.getGameInfoById(GAME_ID.FOUR_SEASONS).localeFolder}/${moon.locale.getCurrentLanguage()}`,
      () => {
      },
      RESOURCE_BLOCK.GAME
    ).then(() => {

      this.startGame.dragonAsset = moon.res.getSkelecton('startGame_ske', RESOURCE_BLOCK.GAME);
      this.startGame.dragonAtlasAsset = moon.res.getGameSpriteFrame('startGame_tex');
  
      this.stopGame.dragonAsset = moon.res.getSkelecton('stopGame_ske', RESOURCE_BLOCK.GAME);
      this.stopGame.dragonAtlasAsset = moon.res.getGameSpriteFrame('stopGame_tex');

    })  
  }

  playStartGame() {
    this.startGame.node.active = true;
    this.startGame.playAnimation('action', 1);
  }

  playStopGame() {
    this.stopGame.node.active = true;
    this.stopGame.playAnimation('action', 1);
  }

  hide() {
    this.startGame.node.active = false;
    this.stopGame.node.active = false;
    this.mask.active = false;

    this.startGame.node.cleanup();
    this.stopGame.node.cleanup();
  }
}