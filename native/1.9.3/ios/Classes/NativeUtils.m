//
//  NativeUtils.m
//  Slot-mobile
//
//  Created by Thinh Tran on 11/14/17.
//

#import <Foundation/Foundation.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <UIKit/UIKit.h>
#import <sys/utsname.h>
//#import <GoogleSignIn/GoogleSignIn.h>
#import "NativeUtils.h"
#include <sys/sysctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <dlfcn.h>
#define PRIVATE_PATH "/System/Library/PrivateFrameworks/CoreTelephony.framework/CoreTelephony"

@implementation NativeUtils

static NSInteger _pauseCount = 0;

+(NativeUtils *)sharedInstance {
    static NativeUtils *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

+(NSString *)getDeviceId {
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [[currentDevice identifierForVendor] UUIDString];
}

+(NSString *)getDeviceName {
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [currentDevice model];
}

+(NSString *)getDeviceModel {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

+(NSString *)getCarrierName {
    NSString* carrier_name = @"";
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    if(carrier != nil)
    {
        carrier_name = [carrier carrierName];
//        NSString * mobileCountryCode = [carrier mobileCountryCode];
//        if([mobileCountryCode isEqualToString:(@"452")])
//        {
//            NSString* networkCode = [carrier mobileNetworkCode];
//            if([networkCode isEqualToString:(@"01")])
//            {
//                carrier_name = @"VN MOBIFONE";
//            }
//            else if([networkCode isEqualToString:(@"02")])
//            {
//                carrier_name = @"VINAPHONE";
//            }
//            else if([networkCode isEqualToString:(@"04")])
//            {
//                carrier_name = @"VIETTEL";
//            }
//            else if([networkCode isEqualToString:(@"05")])
//            {
//                carrier_name = @"Vietnammobile";
//            }
//        }
    }
    
    return carrier_name;
}

+(NSString *)getNetworkType {
    NSArray *subviews = [[[[UIApplication sharedApplication] valueForKey:@"statusBar"] valueForKey:@"foregroundView"]subviews];
    NSNumber *dataNetworkItemView = nil;
    
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }
    
    NSString *networkType = @"Unknown";
    switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"]integerValue]) {
        case 0:
            networkType = @"Unknown";
            break;
            
        case 1:
            networkType = @"2G";
            break;
            
        case 2:
            networkType = @"3G";
            break;
            
        case 3:
            networkType = @"4G";
            break;
            
        case 4:
            networkType = @"4G";
            break;
            
        case 5:
            networkType = @"Wifi";
            break;
            
        default:
            break;
    }
    
    return networkType;
}

+(NSString *)getImsi {
    return @"";
//    void *kit = dlopen(PRIVATE_PATH,RTLD_LAZY);
//    NSString *imsi = nil;
//    int (*CTSIMSuppo em(kit, "CTSIMSupportCo pyMobileSubscriberIdentity");
//    imsi = (NSString*)CTSIMSupportCopyMobileSubscriberIdentity(nil);
//    dlclose(kit);
//    if (imsi == nil) {
//        return @"";
//    }
//    return imsi;
}

+(NSString *)getLocalIP {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Get NSString from C String
                address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

+(NSString *)getAppVersion {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return version;
}

+(NSString *)getOsVersion {
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [currentDevice systemVersion];
}

+(void)signInGoogle {
//    [[GIDSignIn sharedInstance] signIn];
}

+(void)openURL:(NSString *)url {
    NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *inputUrl = [NSURL URLWithString:webStringURL];
    if([[UIApplication sharedApplication] canOpenURL:inputUrl]) {
        NSLog(@"OK");
        [[UIApplication sharedApplication] openURL:inputUrl];
    } else {
        NSLog(@"Failed to open url");
    }
}

+(void)copyText:(NSString *)text {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = text;
}

+(NSString *)pasteText {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *text = pasteboard.string;
    return text;
}

+(void)onGoogleLogin:(NSString *)token {
//    NSString* pattern = @"var MainMenu = require('MainMenu').MainMenu; MainMenu.onLoginGoogleComplete(\"%@\");";
//    NSString* evalString = [NSString stringWithFormat:pattern, token];
//    [[AppController sharedInstance] invokeJSMethod:evalString];
}

+(void)onFacebookLogin:(NSString *)token {
//    NSString* pattern = @"var MainMenu = require('MainMenu').MainMenu; MainMenu.onLoginFacebookComplete(\"%@\")";
//    NSString* evalString = [NSString stringWithFormat:pattern, token];
//
//    [[AppController sharedInstance] invokeJSMethod:evalString];
}

+(void)onGameResume {
    @try {
        if (_pauseCount == 0) {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            [UIApplication sharedApplication].idleTimerDisabled = YES;
            return;
        }
//        NSString* evalString = @"var MainMenu = require('MainMenu').MainMenu; MainMenu.onGameResume()";
//        [[AppController sharedInstance] invokeJSMethod:evalString];
    }
    @catch (NSException * e) {
        
    }
}

+(void)onGamePause {
    
}

+(void)showRewardVideo {
//    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
//        
//        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
//        
//    }
}

@end
