import {SceneComponent} from "../common/SceneComponent";
import {AvatarIcon} from "../common/AvatarIcon";
import {NodeUtils} from "../core/NodeUtils";
import {GlobalInfo} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;


@ccclass
export class AvatarDialog extends SceneComponent {

  @property(cc.Node)
  avatarSample: cc.Node = null;

  @property(AvatarIcon)
  myAvatar: AvatarIcon = null;

  @property(cc.ScrollView)
  avatars: cc.ScrollView = null;

  @property(cc.Node)
  saveBtn: cc.Node = null;

  @property(cc.Node)
  maleSelect: cc.Node = null;

  @property(cc.Node)
  femaleSelect: cc.Node = null;

  avatarCount = 8;

  avatarPool: cc.NodePool;

  isMan = true;

  selectedAvatar: cc.Node;
  selectedAvatarName = '';

  onLoad() {
    this.avatarPool = new cc.NodePool('avatarPool');
  }

  onEnter() {
    super.onEnter();
    if (this.isMan) {
      this.showManAvatar();
    } else {
      this.showWomenAvatar();
    }
  }

  onLanguageChange() {
    NodeUtils.setSprites(this.node,
      {
        'title_sprite': 'game_head_title',
        'male_sprite': this.isMan ? 'game_head_title_man_1' : 'game_head_title_man_0',
        'female_sprite': this.isMan ? 'game_head_title_women_0' : 'game_head_title_women_1',
        'chosen_sprite': 'game_head_txt1'
      }
    );

    NodeUtils.setLocaleLabel(this.saveBtn, 'save_lbl', 'save');
  }

  showByCurrentAvatar() {
    let avatarLink = GlobalInfo.me.avatar || 'man';
    if (avatarLink.indexOf('women') >= 0) {
      let startIndex = avatarLink.indexOf('_women_') + 7;
      let avatarName = avatarLink.slice(startIndex, avatarLink.length);
      let avatarIndex = avatarName.replace('_women_', '');
      this.showWomenAvatar();
    } else {
      let startIndex = avatarLink.indexOf('_man_') + 5;
      let avatarName = avatarLink.slice(startIndex, avatarLink.length);
      let avatarIndex = avatarName.replace('_man_', '');
      this.showManAvatar();
    }
  }

  initAvatars() {
    this.returnToPool();
    for (let i = 0; i < this.avatarCount; i++) {
      let avatarNode = this.getAvatarNode(i);
      this.avatars.content.addChild(avatarNode);
    }
  }

  getAvatarNode(index) {
    let avatarNode = this.avatarPool.get();
    if (!avatarNode) {
      avatarNode = cc.instantiate(this.avatarSample);
    }
    let btn: cc.Button = avatarNode.getComponent(cc.Button);
    let clickEvent = btn.clickEvents[0];
    clickEvent.customEventData = index;
    avatarNode.active = true;
    let avatarIcon: AvatarIcon = avatarNode.getComponent(AvatarIcon);
    let gender = this.isMan ? 'man' : 'women';
    avatarIcon.node.stopAllActions();
    avatarIcon.node.opacity = 0;
    cc.loader.loadRes(`images/head/head_c_${gender}${index}`, (err, texture) => {
      avatarIcon.setSpriteFrame(new cc.SpriteFrame(texture));
      avatarIcon.node.runAction(cc.fadeIn(0.2))
    });
    return avatarNode;
  }

  returnToPool() {
    let nodes = [].concat(this.avatars.content.children);
    for (let avatarNode of nodes) {
      this.avatarPool.put(avatarNode);
    }
  }

  updateAvatar() {
    this.closeDialog();
  }

  onSelectAvatar(event, index) {
    if (this.selectedAvatar) {
      NodeUtils.hideByName(this.selectedAvatar, 'selected');
    }
    this.selectedAvatar = this.avatars.content.children[index];
    NodeUtils.showByName(this.selectedAvatar, 'selected');
    let gender = this.isMan ? 'man' : 'women';
    this.selectedAvatarName = `${gender}${index}`;
    let selectedAvatarIcon = this.selectedAvatar.getComponent(AvatarIcon);
    this.myAvatar.setSpriteFrame(selectedAvatarIcon.getSpriteFrame());
  }

  showManAvatar() {
    this.isMan = true;
    this.maleSelect.active = this.isMan;
    this.femaleSelect.active = !this.isMan;
    this.initAvatars();
    this.updateTabUI();
  }

  showWomenAvatar() {
    this.isMan = false;
    this.maleSelect.active = this.isMan;
    this.femaleSelect.active = !this.isMan;
    this.initAvatars();
    this.updateTabUI();
  }

  updateTabUI() {
    NodeUtils.setSprites(this.node,
      {
        'male_sprite': this.isMan ? 'game_head_title_man_1' : 'game_head_title_man_0',
        'female_sprite': this.isMan ? 'game_head_title_women_0' : 'game_head_title_women_1',
      }
    );
  }
}