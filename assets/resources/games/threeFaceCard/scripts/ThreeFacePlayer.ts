import {ThreeFacePlayerInfo} from './model/ThreeFacePlayerInfo';
import {AvatarIcon} from "../../../../scripts/common/AvatarIcon";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {GlobalInfo, moon} from "../../../../scripts/core/GlobalInfo";
import {Card} from '../../../../scripts/core/Card';
import {THREE_CARD_CARD_TYPE, THREE_FACE_AUDIO} from "./ThreeFaceConstants";
import {ThreeFaceRoom} from "./model/ThreeFaceRoom";
import {COUNTDOWN_TYPE} from "../../../../scripts/core/Constant";
import {TimerTask} from "../../../../scripts/core/TimerComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFacePlayer extends cc.Component {

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Node)
  highlight: cc.Node = null;

  @property(cc.Node)
  chessContent: cc.Node = null;

  @property(cc.Node)
  snatchRow: cc.Node = null;

  @property(cc.Node)
  dealer: cc.Node = null;

  @property(cc.Node)
  resultSpecial: cc.Node = null;

  @property(cc.Node)
  resultLabel: cc.Node = null;


  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  winAmount: cc.Label = null;

  @property(cc.Label)
  loseAmount: cc.Label = null;

  @property(dragonBones.ArmatureDisplay)
  playerWin: dragonBones.ArmatureDisplay = null;

  @property(cc.Sprite)
  bannerBg: cc.Sprite = null;

  @property(cc.SpriteAtlas)
  spriteAtlas: cc.SpriteAtlas = null;

  winTask: TimerTask;
  playerInfo: ThreeFacePlayerInfo;
  bannerAction: cc.Action;
  private cardNodes: any;

  onLanguageChange() {

  }

  onEnter() {
    this.clear();
  }

  hideBet() {
    this.bet.node.active = false;
    cc.log('hide bet row');
  }

  setBet() {
    this.bet.node.active = true;
    let font = moon.res.getFont("betFactor");
    NodeUtils.setLocaleLabel(this.node, 'betValue', 'nTimes', {
      font: font,
      params: [this.playerInfo.betFactor.toString()]
    });
  }

  placeBet(times) {
    this.bet.node.active = true;
    this.bet.string = times;
  }

  getAvatarPosition() {
    return this.avatar.node.convertToWorldSpaceAR(cc.v2());
  }

  setPlayerInfo(playerInfo: ThreeFacePlayerInfo) {
    this.playerInfo = playerInfo;
    if (!playerInfo.resultCard) {
      this.playerInfo.resultCard = {
        point: 1,
        cardList: [-1, -1, -1],
        cardType: 1,
        bonusFactor: 1
      };
    } else {
      this.playerInfo.resultCard = playerInfo.resultCard;
    }
    this.playerName.string = moon.string.formatPlayerName(playerInfo.displayName);
    this.money.string = moon.string.formatMoney(playerInfo.money);
    this.avatar.setImageUrl(playerInfo.avatar);
    this.resetPointBanner();
    this.bet.node.active = false;
    this.hideSnatchValue();
    this.dealer.active = false;
    if (this.playerInfo.betFactor > 0) {
      this.setBet();
    }

    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    if (room.countdownInfo) {
      switch (room.countdownInfo.type) {
        case COUNTDOWN_TYPE.STATE_FIND_BANKER:
          this.setCardsByIdList(this.playerInfo.resultCard.cardList);
          if (this.playerInfo.isChoose) {
            this.playerSnatch();
          } else {
            this.playerNonSnatch();
          }
          break;
        case COUNTDOWN_TYPE.STATE_NOT_START_GAME:
        case COUNTDOWN_TYPE.STATE_READY_START_GAME:
          break;
        case COUNTDOWN_TYPE.STATE_RESULT:
          this.setCardsByIdList(this.playerInfo.resultCard.cardList);
          if (this.playerInfo.resultCard.cardType > 0) {
            this.showPointBanner();
          }
          this.playAmountEffect(playerInfo.winMoney, false);
          break;
        default:
          this.setCardsByIdList(this.playerInfo.resultCard.cardList);
      }
    }

    if (this.playerInfo.winMoney > 0 || this.playerInfo.winMoney < 0) {
      let setWinMoney = (target: cc.Label) => {
        target.node.active = true;
        target.string = `${this.playerInfo.winMoney}`;
      };

      setWinMoney(this.playerInfo.winMoney > 0 ? this.winAmount : this.loseAmount);
    }
  }


  formatPlayerName(playerInfo: ThreeFacePlayerInfo) {
    if (GlobalInfo.me.userId === playerInfo.userId) {
      if (playerInfo.displayName.length > 7) {
        return playerInfo.displayName.slice(0, 6) + '...';
      } else {
        return playerInfo.displayName.slice();
      }
    } else {
      return '****' + playerInfo.displayName.slice(-3);
    }
  }

  getSelectAnim() {
    let delayTime = 0.2;
    let round = 1;
    let actions = [];
    for (let i = 0; i < round; i++) {
      actions = actions.concat([
        cc.callFunc(() => {
          this.highlight.opacity = 255;
          moon.audio.playEffectInGame(THREE_FACE_AUDIO.DEALER_SELECTING, false);
        }),
        cc.delayTime(delayTime),
        cc.callFunc(() => {
          this.highlight.opacity = 0;
        }),
        cc.delayTime(delayTime)
      ]);
    }
    return cc.sequence(actions).repeat(round);
  }

  showBorder(isShown = true) {
    this.highlight.runAction(
      cc.fadeTo(0.05, isShown ? 255 : 0)
    );
  }

  showDealer(isShown) {
    this.dealer.active = isShown;
    this.dealer.getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('dealer');
  }

  /**
   * return the array of positions for placing chess on "hand"
   */
  getReceivedPositions(): Array<cc.Vec2> {
    let chessWidth = moon.cardPool.cardWidth;
    let chessPositions: Array<cc.Vec2> = [];
    let width = chessWidth * 3;
    let cardDistance = chessWidth;
    let startPos = -width / 2 + chessWidth / 2;
    if (this.playerInfo.userId !== GlobalInfo.me.userId) {
      startPos = -chessWidth / 2;
      cardDistance = chessWidth / 2;
    }


    for (let i = 0; i < 3; i++) {
      chessPositions.push(cc.v2((i * cardDistance) + startPos, 0));
    }
    return chessPositions;
  }

  setCardNodes(cards: any) {
    this.cardNodes = cards;
  }

  getResultType() {

  }

  getTextResultFromChessList() {


  }

  resetPointBanner() {
    this.bannerBg.node.active = false;
    this.resultLabel.active = false;
    this.resultSpecial.active = false;
  }

  clearAnimation() {
    this.bannerBg.node.stopAllActions();
    this.node.stopAllActions();
    this.winAmount.node.stopAllActions();
    this.loseAmount.node.stopAllActions();
  }

  showPointBanner() {
    if (this.bannerBg) {
      this.bannerBg.node.active = true;
      this.resetPointBanner();
      if (!this.playerInfo.resultCard) return;

      this.resultLabel.active = false;
      this.resultSpecial.active = false;
      switch (this.playerInfo.resultCard.cardType) {
        case THREE_CARD_CARD_TYPE.ZERO_TO_SIX:
          this.resultLabel.active = true;
          if (this.playerInfo.resultCard.point == 0) {
            this.bannerBg.spriteFrame = this.spriteAtlas.getSpriteFrame('black_banner');
          } else {
            this.bannerBg.spriteFrame = this.spriteAtlas.getSpriteFrame('yellow_banner');
          }
          break;
        case THREE_CARD_CARD_TYPE.SEVEN_TO_NINE:
          this.resultLabel.active = true;
          this.bannerBg.spriteFrame = this.spriteAtlas.getSpriteFrame('blue_banner');
          break;
        case THREE_CARD_CARD_TYPE.THREE_FACE_CARD:
          this.resultSpecial.active = true;
          this.bannerBg.spriteFrame = this.spriteAtlas.getSpriteFrame('red_banner');
          break;
        case THREE_CARD_CARD_TYPE.BOMB:
          this.resultSpecial.active = true;
          this.bannerBg.spriteFrame = this.spriteAtlas.getSpriteFrame('purple_banner');
          break;
        case THREE_CARD_CARD_TYPE.BUST_NINE:
          this.resultSpecial.active = true;
          this.bannerBg.spriteFrame = this.spriteAtlas.getSpriteFrame('purple_banner');
          break;
      }

      this.bannerBg.node.active = true;
      this.bannerBg.node.scaleX = 0;
      this.bannerBg.node.scaleY = 0;
      this.bannerAction = cc.sequence(
        cc.scaleTo(0.4, 1),
        cc.delayTime(0.1),
      ).easing(cc.easeBackOut());
      this.bannerBg.node.runAction(
        this.bannerAction
      );
      if (this.resultLabel.active) {
        this.resultLabel.active = true;
        let pointSprite = this.resultLabel.getChildByName('point').getComponent(cc.Sprite);
        let nTimeSprite = this.resultLabel.getChildByName('betFactor').getComponent(cc.Sprite);
        let postfix = this.playerInfo.resultCard.point > 0 ? '' : '_black';
        pointSprite.spriteFrame = moon.res.getGameSpriteFrame(`${this.playerInfo.resultCard.point}_point`);
        nTimeSprite.spriteFrame = moon.res.getGameSpriteFrame(`${this.playerInfo.resultCard.bonusFactor}_time${postfix}`);
      } else {
        let specialSprite = this.resultSpecial.getComponent(cc.Sprite);
        specialSprite.spriteFrame = moon.res.getGameSpriteFrame(`${this.playerInfo.resultCard.cardType}_special`);
      }
    }
  }

  showCards() {
    let actions = [];
    let i = 0;
    for (let cardNode of this.cardNodes) {
      let card: Card = cardNode.getComponent(Card);
      actions.push(cc.callFunc(() => {
        card.setCardId(this.playerInfo.resultCard.cardList[i++]);
        card.showCard();
      }));
      actions.push(cc.delayTime(0.02));
    }
    this.node.runAction(
      cc.sequence(actions)
    );
  }


  setCardsByIdList(cardList) {
    let cardPosition = this.getReceivedPositions();
    let cardCount = 0;
    let cardNodes = [];
    cardList.forEach(cardId => {
      let cardNode = moon.cardPool.getCard(cardId);
      let cardComponent: Card = cardNode.getComponent(Card);
      this.chessContent.addChild(cardNode);
      cardNodes.push(cardNode);
      cardComponent.showCard();
      cardNode.position = cardPosition[cardCount];
      cardCount++;
    });
    this.setCardNodes(cardNodes);

  }

  setMoney(money: any) {
    this.money.string = moon.string.formatMoney(money);
  }

  playWinEffect() {
    this.playerWin.node.active = true;
    this.playerWin.playAnimation('playerWin', 1);
  }

  playAmountEffect(amount, isAnim = true) {
    amount = amount || 0;
    if (this.winTask) {
      moon.timer.removeTask(this.winTask);
    }
    let label = amount > 0 ? this.winAmount : this.loseAmount;
    let yDest = 105;
    let prefix = '';
    let setResultMoney = (label, val) => {
      if (val > 0) {
        prefix = '+';
      } else {
        prefix = '';
      }
      label.string = prefix + moon.string.formatMoney(val);
    };
    let playMoneyAnim = (target) => {
      target.stopAllActions();
      target.opacity = 255;
      if (isAnim) {
        this.winTask = moon.timer.tweenNumber(0, amount, (val) => {
          setResultMoney(label, val);
        }, () => {
          setResultMoney(label, amount);
          this.setMoney(this.playerInfo.money);
        }, 0.5);
        target.position = cc.v2(0, 0);
        target.runAction(
          cc.moveBy(0.5, cc.v2(0, yDest)).easing(cc.easeBackOut()),
        );
      } else {
        target.position = cc.v2(0, yDest);
        setResultMoney(label, amount);
        this.setMoney(this.playerInfo.money);
      }
    };
    if (amount > 0) {
      this.winAmount.node.active = true;
      playMoneyAnim(this.winAmount.node);
    } else {
      this.loseAmount.node.active = true;
      playMoneyAnim(this.loseAmount.node);
    }
  }

  hideSnatchValue() {
    this.snatchRow.active = false;
  }

  playerSnatch() {
    this.snatchRow.getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('snatch');
    this.snatchRow.active = true;
  }

  playerNonSnatch() {
    this.snatchRow.getComponent(cc.Sprite).spriteFrame = moon.res.getGameSpriteFrame('nonSnatch');
    this.snatchRow.active = true;
  }


  clear() {
    this.winAmount.node.active = false;
    this.loseAmount.node.active = false;
    this.highlight.opacity = 0;
    this.showDealer(false);
    this.clearAnimation();
    this.resetPointBanner();
    this.hideBet();
    this.bannerBg.node.active = false;
    this.resultLabel.active = false;
    this.resultSpecial.active = false;
    this.hideSnatchValue();
    this.chessContent.children.forEach(chessNode => {
      chessNode.removeFromParent();
      moon.cardPool.putCards([chessNode]);
    });
    if (this.winTask) {
      moon.timer.removeTask(this.winTask);
    }
  }
}

