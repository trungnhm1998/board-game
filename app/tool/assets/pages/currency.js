let currency = {
  tableId: 'connectionTable',
  addFormId: 'addForm',
  data: {
    merchantList: [],
    machineList: [],
    insightConnectionList: [],
    currencyList: []
  },
  updateDataTable: function () {
    $(`#${this.tableId}`).DataTable({
      searching: false,
      paging: false
    });
  },

  init: function() {
    let rowPattern = `
    <tr>
      <td>{0}</td>
      <td>{1}</td>
      <td>
      <div class="btn btn-outline-danger" onclick="currency.onDeleteCurrency('{0}')"><i class="fas fa-minus"></i></div>
      </td>
    </tr>
    `;
    for (let currencyInfo of this.data.currencyList) {
      let rowHtml = core.string.format(rowPattern, currencyInfo.currency, currencyInfo.description);
      $(`#${this.tableId}`).find(`> tbody`).append(rowHtml);
    }

    this.updateDataTable();
  },

  onAddCurrency: function() {
    let form = $(`#${this.addFormId}`)
    let currencyId = form.find('#currencyId').val();
    let description = form.find('#description').val();
    core.api.addCurrency({
      currency: currencyId,
      description: description,
    }, function() {
      location.reload();
    });
  },

  onDeleteCurrency: function(currency) {
    core.api.deleteCurrency({
      currency: currency,
    }, function() {
      location.reload();
    });
  }
};