import {RoomInfo} from "../../../../scripts/model/Room";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import { moon } from "../../../../scripts/core/GlobalInfo";
import { GAME_ID, RESOURCE_BLOCK } from "../../../../scripts/core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackLobby extends cc.Component {

  @property([cc.Node])
  rooms: cc.Node[] = [];

  @property(cc.Node)
  roomList: cc.Node = null ;

  @property(cc.Node)
  lobbyBG: cc.Node = null ;


  inDelayRoomClick = false;

  updateRoomInfoList(roomInfoList: Array<RoomInfo>) {
    let i = 0;
    cc.log('update room info', roomInfoList)
    this.roomList.removeAllChildren()
    for (let roomInfo of roomInfoList) {
      let pos = i % 4;
      let room = cc.instantiate(this.rooms[pos]);
      i = i + 1;
      room.active = true;
      NodeUtils.setLocaleLabels(room, {
        'minBet': ['allowed:', {params: [roomInfo.minBet]}],
      });
      let roomBtn = room.getComponent(cc.Button) as cc.Button;

      roomBtn.clickEvents[0].customEventData = roomInfo.roomId;

      let bgAnim = room.getChildByName('background')
      let dbCom = bgAnim.getComponent(dragonBones.ArmatureDisplay)
      dbCom.dragonAsset = moon.res.getSkelecton('hh_hall_icon_' + (pos+1) + '_ske', RESOURCE_BLOCK.GAME);
      dbCom.dragonAtlasAsset = moon.res.getSpriteFrame('hh_hall_icon_' + (pos+1) + '_tex', RESOURCE_BLOCK.GAME);
      dbCom.armatureName = 'hh_hall_icon_' + (pos+1) ;
      dbCom.playAnimation('action', 0);

      this.roomList.addChild(room)
    }

    for (i; i <= this.rooms.length - 1; i++) {
      this.rooms[i].active = false;
    }
  }

  onResize() {

    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);

    this.roomList.scale = uiRatio;

    this.lobbyBG.scale = uiRatio;
    this.lobbyBG.y = cc.winSize.height/2;
    this.lobbyBG.x = -cc.winSize.width/2;


  }
  onRoomClick(evt, value){

    if (!this.inDelayRoomClick) {    
        this.inDelayRoomClick = true
        let numValue = parseInt(value)
        cc.log('choose room',numValue)
        moon.dialog.showWaiting()
        moon.gameSocket.playRedBlack(GAME_ID.RED_BLACK,numValue)

        moon.timer.scheduleOnce(()=>{
            this.inDelayRoomClick = false
        },2)
    }
  }
}