import {TestCase} from "./TestCase";

declare var require: any;

export class TestRunner {
  gameNode;
  testSuite: Array<TestCase> = [];

  constructor() {
    let testClass = this.getTestClass('RedBlackTest');
    if (testClass) {
      this.testSuite.push(new testClass());
    }
  }

  getTestClass(className) {
    return require(className)[className];
  }

  run() {
    this.testSuite.map(tc =>
      tc.run()
    );
  }

  setGame(gameNode) {
    this.gameNode = gameNode;

    for (let tc of this.testSuite) {
      tc.setGame(this.gameNode);
    }
  }
}