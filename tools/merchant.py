# -*- coding: utf-8 -*-

import os
import shutil
import time
import sys
import glob
import fnmatch
import re

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == "-":  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

args = getopts(sys.argv)

merchantId = args["-merchant"]

buildFolder = "../build/web-mobile/"

merchantFolder = "../app/public/mc/" + merchantId + "/"
merchantText = "mc/" + merchantId

# Create merchant folder
if os.path.exists(merchantFolder):
    shutil.rmtree(merchantFolder)

print("Creating folder " + merchantFolder)
os.mkdir(merchantFolder)

# Copy resources
def copyResources():
  def copyResource(folderName):
    if os.path.exists(merchantFolder + folderName):
      print("Removing " + merchantFolder + folderName)
      shutil.rmtree(merchantFolder + folderName)

    print("Copying " + buildFolder + folderName + " to " + merchantFolder + folderName)
    shutil.copytree(buildFolder + folderName, merchantFolder + folderName)

  copyResource("res")
  copyResource("src")
  copyResource("subpackages")

# Copy main
def copyMain():
  for name in os.listdir(buildFolder):
    if re.search("^main.*js$", name):
      shutil.copy2(buildFolder + name, merchantFolder + name)
      openFile = open(merchantFolder + name, 'r')
      mainData = openFile.read().replace("'src/", "'" + merchantText + "/src/")
      mainData = mainData.replace("'jsb-adapter/", "'" + merchantText + "/jsb-adapter/")
      mainData = mainData.replace("'res/", "'" + merchantText + "/res/")
      openFile.close()
      writeFile = open(merchantFolder + name, 'w')
      writeFile.write(mainData)


# Flow
copyResources()
copyMain()
