import {PROVIDER_TYPE} from "../core/Constant";

export class ClientInfo {
  userAgent = '';
  platform = '';
  deviceId = '';
  lang = '';
  version = '';
  channel = '';
  clientId = '';
  idFa = '';
  gaId = '';
  lite = false;
  providerId = PROVIDER_TYPE.Z88;
  isLite = false;
}