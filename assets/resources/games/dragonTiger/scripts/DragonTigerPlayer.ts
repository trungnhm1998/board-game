import {DragonTigerPlayerInfo} from "./model/DragonTigerPlayerInfo";
import {AvatarIcon} from "../../../../scripts/common/AvatarIcon";
import {moon} from "../../../../scripts/core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerPlayer extends cc.Component {

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Label)
  winMoney: cc.Label = null;

  @property(AvatarIcon)
  avatarIcon: AvatarIcon = null;
  playerInfo: DragonTigerPlayerInfo;

  clear() {

  }

  cleanAvatar() {
    this.avatarIcon.clear();
  }

  setPlayerInfo(myPlayerInfo: DragonTigerPlayerInfo) {
    this.playerInfo = myPlayerInfo;
    this.playerName.string = myPlayerInfo.displayName;
    this.setMoney(myPlayerInfo.money);
    this.avatarIcon.setImageUrl(myPlayerInfo.avatar);
  }

  playBetAnim() {
    this.avatarIcon.node.stopAllActions();
    this.avatarIcon.node.runAction(
      cc.sequence(
        cc.moveTo(0.15, cc.v2(this.avatarIcon.node.x, 15)),
        cc.moveTo(0.15, cc.v2(this.avatarIcon.node.x, 0)),
      )
    )
  }

  setMoney(myMoney: number) {
    this.money.string =  moon.string.formatMoney(myMoney);
  }

  playWinEffect(winMoney, endMoney) {
    if (winMoney > 0) {
      this.winMoney.node.active = true;
      this.winMoney.string = '+' + winMoney;
    }
    moon.audio.playEffectInGame("end_bet");
    let currentMoney = this.money.string ?  moon.string.parseMoney(this.money.string) : (endMoney - winMoney);
    moon.timer.tweenNumber(currentMoney, endMoney - currentMoney, (val) => {
      this.money.string =  moon.string.formatMoney(val);
    }, (val) => {
      this.money.string =  moon.string.formatMoney(endMoney);
    });
  }

  hideWinMoney() {
    this.winMoney.node.active = false;
  }
}