export class FourSeasonsPlayerInfo {
  userId: any;
  displayName: string;
  money: number;
  avatar: string;
  placeBetTypeList: any = [];
}