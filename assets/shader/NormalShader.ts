import {IShader} from '../scripts/shader/CMaterial';

const renderEngine = (<any>cc).renderer.renderEngine;
const renderer = renderEngine.renderer;

export const NormalShader: IShader = {
  name: 'Normal',
  params: [
  ],

  update(material, percent) {
  },

  defines: [],

  vert: `
      uniform mat4 viewProj;
      attribute vec3 a_position;
      attribute vec2 a_uv0;
      attribute vec4 a_color;
      varying vec2 uv0;
      varying vec4 color;
      
      void main () {
          vec4 pos = viewProj * vec4(a_position, 1);
          gl_Position = pos;
          uv0 = a_uv0;
          color = a_color;
      }`,

  frag:
    `
      uniform sampler2D texture;
      varying vec2 uv0;
      varying vec4 color;
      
      void main()
      {
        vec4 fragColor = texture2D(texture, uv0);
        fragColor *= color;
        gl_FragColor = fragColor;
      }
    `
};