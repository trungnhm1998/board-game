import {CountdownInfo, Room} from "../../../../../scripts/model/Room";
import {TwoEightPlayerInfo} from "./TwoEightPlayerInfo";

export class TwoEightRoom extends Room {
  matchId;
  matchOrder;
  matchTotal;
  appearChessList;
  betList;
  timeout;
  playerInfoList: TwoEightPlayerInfo[];
  firstId;
  countdownInfo: CountdownInfo;
  bankerId;
  betMoneyTotal;
  shakeDiceResult;
}