import {Config} from './../Config';
import {gamedev} from 'gamedevjs';
import {GAME_EVENT} from '../core/Constant';
import {LocalData} from '../core/LocalStorage';
import {moon} from "../core/GlobalInfo";


export class LanguageService {

  dictionary = {};

  lang = '';

  constructor() {
    let savedLanguage = cc.sys.localStorage.getItem('lang');
    if (savedLanguage) {
      let languages = Config.languageOptions.map(lang => lang.name);
      if (languages.indexOf(savedLanguage) < 0) {
        savedLanguage = Config.defaultLanguage
      }
    }
    this.setLang(savedLanguage || LocalData.lang);
  }

  supportUnicodeCharacter() {
    if ((<any>cc).TextUtils) {
      (<any>cc).TextUtils.label_wordRex = /([a-zA-Z0-9\.\,@Ä-ÖÜäöüßéèçàùêâîôûа-яА-ЯЁёÀÁÂẤÃÈÉÊÌÍÎÐÒÓÔÕ×ÙÚÛÝàáâấãèéêìíîòóôùúûảẢậẬịỊựỰẹẸệỆọỌểỂẩẨộỘưƯđĐổỔặẶăĂửỬạẠắẮằẰẳẲẵẴầẦẫẪẻẺẽẼếẾềỀễỄủỦũŨụỤứỨừỪữỮỏỎơƠớỚờỜởỞỡỠợỢỉỈĩĨồỒỗỖốỐộỘ]+|\S)/;
      (<any>cc).TextUtils.label_firsrEnglish = /^[a-zA-Z0-9\.\,@Ä-ÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁёÀÁÂẤÃÈÉÊÌÍÎÐÒÓÔÕ×ÙÚÛÝàáâấãèéêìíîòóôùúûảẢậẬịỊựỰẹẸệỆọỌểỂẩẨộỘưƯđĐổỔặẶăĂửỬạẠắẮằẰẳẲẵẴầẦẫẪẻẺẽẼếẾềỀễỄủỦũŨụỤứỨừỪữỮỏỎơƠớỚờỜởỞỡỠợỢỉỈĩĨồỒỗỖốỐộỘ]/;
      (<any>cc).TextUtils.label_lastEnglish = /[a-zA-Z0-9\.\,@Ä-ÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁёÀÁÂẤÃÈÉÊÌÍÎÐÒÓÔÕ×ÙÚÛÝàáâấãèéêìíîòóôùúûảẢậẬịỊựỰẹẸệỆọỌểỂẩẨộỘưƯđĐổỔặẶăĂửỬạẠắẮằẰẳẲẵẴầẦẫẪẻẺẽẼếẾềỀễỄủỦũŨụỤứỨừỪữỮỏỎơƠớỚờỜởỞỡỠợỢỉỈĩĨồỒỗỖốỐộỘ]+$/;
    }
  }

  refreshDictionary() {
    let resMgr = moon.res;
    return resMgr.readJsonFile(`languages/${this.getCurrentLanguage()}/${this.getCurrentLanguage()}`)
      .then((val) => {
        resMgr.preloadFolders(
          [`languages/${this.getCurrentLanguage()}`],
          () => {
          }).then(() => {
          this.dictionary = val;
          gamedev.event.emit(GAME_EVENT.ON_LANGUAGE_CHANGE);
        });
      });
  }

  getCurrentLanguage() {
    let savedLanguage = cc.sys.localStorage.getItem('lang');
    return this.lang || savedLanguage ||  Config.defaultLanguage;
  }

  setLang(langCode) {
    this.lang = langCode;
    LocalData.lang = langCode;
    moon.storage.saveLocalData();
    return this.refreshDictionary();
  }

  getLanguageOptions() {
    return Config.languageOptions;
  }

  get(key: string) {
    return this.dictionary[key];
  }

  private static instance: LanguageService;

  static getInstance(): LanguageService {
    if (!LanguageService.instance) {
      LanguageService.instance = new LanguageService();
      LanguageService.instance.supportUnicodeCharacter();
    }

    return LanguageService.instance;
  }
}