import {LanguageService} from '../services/LanguageService';
import {DialogManager} from '../services/DialogManager';
import {LocalStorage} from '../core/LocalStorage';
import {GAME_EVENT, SCENE_TYPE} from '../core/Constant';
import {SceneManager} from '../services/SceneManager';
import {ResourceManager} from '../core/ResourceManager';
import {Config, loadProjectConfig} from '../Config';
import {MoonTimer, TimerComponent, TimerTask} from '../core/TimerComponent';
import {NativeService} from '../services/NativeService';
import {GameService} from '../services/GameService';
import {GameSocket, LobbySocket} from '../services/SocketService';
import {SceneComponent} from '../common/SceneComponent';
import {NodeUtils} from '../core/NodeUtils';
import {gamedev} from 'gamedevjs';
import {CMaterial} from '../shader/CMaterial';
import {GrayShader} from '../../shader/GrayShader';
import {CountdownShader} from '../../shader/CountdownShader';
import {NormalShader} from '../../shader/NormalShader';
import {GlobalInfo, moon} from '../core/GlobalInfo';
import {MoneyService} from '../services/MoneyService';
import {CString} from '../core/String';
import {Random} from '../core/Random';
import {Header} from '../lobby/Header';
import {CardPool} from '../core/CardPool';
import {AudioManager} from '../services/AudioManager';
import {HttpClient} from '../core/HttpClient';
import {Room} from '../model/Room';

const {ccclass, property} = cc._decorator;

@ccclass
export class Boost extends cc.Component {
  reconnectTask: TimerTask;

  @property(cc.Prefab)
  dlgMgrPrefab: cc.Prefab = null;

  @property(cc.Node)
  timer: cc.Node = null;

  @property(cc.Node)
  bg: cc.Node = null;

  @property(cc.Node)
  scenes: cc.Node = null;

  @property(cc.Node)
  headerContent: cc.Node = null;

  @property(cc.Node)
  reconnectNode: cc.Node = null;

  @property(cc.Node)
  header: cc.Node = null;

  @property(cc.Node)
  dialogContent: cc.Node = null;

  @property(cc.Node)
  play: cc.Node = null;

  login: cc.Node;
  mainMenu: cc.Node;

  onLoad() {
    this.initGlobalFacet();
    moon.header.hide();
  }

  start() {
    loadProjectConfig()
      .then(() => {
        this.boostrapGame();
      });
  }

  initGlobalFacet() {
    moon.http = new HttpClient();
    moon.gameSocket = new GameSocket();
    moon.lobbySocket = new LobbySocket();
    moon.storage = new LocalStorage();
    moon.storage.loadLocalData();
    let dlg = cc.instantiate(this.dlgMgrPrefab);
    this.dialogContent.addChild(dlg);
    moon.dialog = dlg.getComponent(DialogManager);
    moon.string = new CString();
    moon.res = ResourceManager.getInstance();
    moon.timer = new MoonTimer();
    moon.random = new Random();
    // moon.lobby = this.mainmenu.getComponent(MainMenu);
    moon.locale = LanguageService.getInstance();
    moon.native = NativeService.getInstance();
    moon.money = MoneyService.getInstance();
    moon.scene = SceneManager.getInstance();
    moon.service = GameService.getInstance();
    moon.root = this;
    moon.header = this.header.getComponent(Header);
    moon.cardPool = CardPool.getInstance();
    moon.audio = AudioManager.getInstance();
  }

  boostrapGame() {
    let timerComp = this.timer.getComponent(TimerComponent);
    moon.timer.setTimer(timerComp);

    cc.debug.setDisplayStats(false);
    this.startPauseResumeHandler();
    cc.view.setResizeCallback(() => this.onResize());
    gamedev.event.register(GAME_EVENT.ON_LANGUAGE_CHANGE, () => this.onLanguageChange());

    CMaterial.addShader(GrayShader);
    CMaterial.addShader(CountdownShader);
    CMaterial.addShader(NormalShader);

    this.onResize();
    let loadingDlg = moon.dialog.showWaiting();

    let gameId: any = moon.http.getUrlParameter('gameId');
    if (!cc.sys.isNative && cc.sys.isBrowser) {
      if (gameId) {
        gameId = +gameId;
        GlobalInfo.gameOnly = true;
      } else {
        GlobalInfo.gameOnly = false;
      }
    }

    if (GlobalInfo.gameOnly) {
      let folderList = [
        'card/card'
      ];
      moon.res.preloadFolders(folderList,
        (value) => {
        }
      ).then(() => {
        GlobalInfo.room = new Room();
        GlobalInfo.room.gameId = gameId;
        moon.gameSocket.connect(Config.gameSocket)
          .then(() => {
            moon.scene.addScene(SCENE_TYPE.PLAY, this.play);
            moon.gameSocket.getRoomInfoList(gameId); // for preloading
            moon.service.goToPlayScene(gameId, () => {
              moon.service.getCurrentService().updateRoomInfoList();
            });
          })
      })
    } else {
      let folderList = [
        'scenes',
        `languages/${moon.locale.getCurrentLanguage()}/common`,
        'card/card'
      ];
      moon.res.preloadFolders(folderList,
        (value) => {
          loadingDlg.setProgress(Math.floor(value * 100));
        }
      )
        .then(() => moon.cardPool.init())
        .then(() => {
          if (GlobalInfo.gameOnly) {

          } else {
            this.login = cc.instantiate(cc.loader.getRes('scenes/login'));
            this.mainMenu = cc.instantiate(cc.loader.getRes('scenes/main_menu'));
            this.scenes.addChild(this.login, 0);
            this.scenes.addChild(this.mainMenu, 1);
            this.play.zIndex = 2;
            this.login.active = false;
            this.mainMenu.active = false;
            this.play.active = false;
            moon.scene.addScene(SCENE_TYPE.LOGIN, this.login);
            moon.scene.addScene(SCENE_TYPE.MAIN_MENU, this.mainMenu);
            moon.scene.addScene(SCENE_TYPE.PLAY, this.play);
            moon.lobbySocket.connect(Config.curServer)
              .then(() => {
                moon.scene.pushScene(SCENE_TYPE.LOGIN, null, () => {
                  this.bg.active = false;
                });
              });
          }
        });
    }


    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
  }

  onKeyDown(event) {
    switch (event.keyCode) {
      case cc.macro.KEY.back:
        if (cc.sys.isNative) {
          moon.service.onBackPressed();
        }
        break;
    }
  }

  startPauseResumeHandler() {
    cc.game.on(cc.game.EVENT_HIDE, this.onPause.bind(this));
    cc.game.on(cc.game.EVENT_SHOW, this.onResume.bind(this));
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.reconnectNode, 'reconnect_lbl', 'reconnect');
    NodeUtils.setLocaleLabel(this.reconnectNode, 'backConfirm_lbl', 'backConfirm');
  }

  onPause() {
    moon.service.pause();
  }

  onResume() {
    moon.service.resume();
  }

  onResize() {
    moon.dialog.node.width = this.scenes.width = cc.winSize.width;
    moon.dialog.node.height = this.scenes.height = cc.winSize.height;
    this.node.rotation = 0;
    let sceneNode = moon.scene.curScene;
    if (sceneNode) {
      let sceneComp: SceneComponent = sceneNode.getComponent(SceneComponent);
      if (sceneComp) {
        sceneComp.onResize();
      }
    }
    NodeUtils.updateWidgetAlignment(this.node);
    moon.dialog.onResize();
  }

  reconnecting = false;

  showReconnect(isShown = true) {
    if (this.reconnectNode.active == isShown || this.reconnecting) return;
    let lblNode = NodeUtils.findByName(this.reconnectNode, 'reconnect_lbl');
    lblNode.stopAllActions();

    this.reconnectNode.active = isShown;
    if (this.reconnectTask) {
      moon.timer.removeTask(this.reconnectTask);
    }
    if (isShown) {
      lblNode.runAction(
        cc.sequence(
          cc.fadeTo(0.5, 255),
          cc.delayTime(0.5),
          cc.fadeTo(0.5, 110),
          cc.delayTime(0.3),
        ).repeatForever()
      )
    }
    if (moon.scene.isInScreen(SCENE_TYPE.LOGIN)) {
      NodeUtils.enableEditBox(this.login, false);
    }
    this.reconnectTask = moon.timer.scheduleOnce(() => {
      if (!moon.lobbySocket.connected) {
        this.reconnectNode.active = false;
        this.reconnecting = true;
        moon.dialog.showNotice(
          moon.locale.get('unableReconnect'), {
            callback: () => {
              this.reconnecting = false;
              moon.service.logout();
            },
            okText: moon.locale.get('reLogin')
          }
        )
      }
    }, 30);
  }

  hideReconnect() {
    this.showReconnect(false);
  }

}
