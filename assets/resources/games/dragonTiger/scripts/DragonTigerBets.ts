import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {DragonTigerPot} from "./DragonTigerPot";
import {DragonTigerCalculator} from "./service/DragonTigerCalculator";
import {DragonTigerPlayer} from "./DragonTigerPlayer";
import {Config} from "../../../../scripts/Config";
import {moon} from "../../../../scripts/core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerBets extends cc.Component {

  @property(cc.Node)
  betBtnSample: cc.Node = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  @property([cc.Node])
  betNodes: cc.Node[] = [];

  @property(cc.Node)
  selected: cc.Node = null;

  @property(cc.Node)
  chipSample: cc.Node = null;

  @property(cc.Node)
  people: cc.Node = null;

  @property(DragonTigerPlayer)
  myPlayer: DragonTigerPlayer = null;

  @property(cc.Node)
  clock: cc.Node = null;

  chipPool: cc.NodePool;
  currentBet = 0;
  private betCallback: any;

  onLoad() {
    this.chipPool = new cc.NodePool();
  }

  clear() {
    for (let betNode of this.betNodes) {
      betNode.active = false;
      NodeUtils.hideByName(betNode, 'chip_lbl');
    }
    this.hideSelected();
  }

  setBetCallback(callback) {
    this.betCallback = callback;
  }

  setBetList(betList) {
    this.clear();
    let i = 0;
    if (!this.currentBet || betList.indexOf(this.currentBet) < 0) {
      this.currentBet = betList[0];
    }
    for (let betMoney of betList) {
      let btnNode = this.betNodes[i];
      btnNode.active = true;
      let sprite = btnNode.getComponent(cc.Sprite);
      let sp = this.atlas.getSpriteFrame('b' + betMoney);
      if (sp) {
        sprite.spriteFrame = sp;
      } else {
        sprite.spriteFrame = this.atlas.getSpriteFrame('be');
        NodeUtils.showByName(btnNode, 'chip_lbl');
        NodeUtils.setLabel(btnNode, 'chip_lbl', String(betMoney));
      }
      let btn: cc.Button = btnNode.getComponent(cc.Button);
      btn.clickEvents[0].customEventData = betMoney;
      this.betNodes.push(btnNode);
      if (this.currentBet == betMoney) {
        this.onBetClick({target: btnNode}, betMoney);
      }
      i++;
    }
  }

  onBetClick(evt, betMoney) {
    this.showSelected(evt.target);
    moon.audio.playEffectInGame("btn_click");
    this.currentBet = +betMoney;
    if (this.betCallback) {
      this.betCallback(this.currentBet);
    }
  }

  showSelected(node) {
    let worldPos = node.convertToWorldSpaceAR(cc.v2());
    this.selected.position = this.selected.parent.convertToNodeSpaceAR(worldPos);
    this.selected.active = true;
  }

  hideSelected() {
    this.selected.active = false;
  }

  returnChips(pot) {
    let chips = [].concat(pot.betArea.children);
    for (let chip of chips) {
      chip.removeFromParent();
      chip.active = false;
      this.chipPool.put(chip);
    }
  }

  getChips(chip: number, amount: number) {
    let nodes = [];
    for (let i = 0; i < amount; i++) {
      nodes.push(this.getChip(chip));
    }

    return nodes;
  }

  getChip(chip: number) {
    let chipNode = this.chipPool.get();
    if (chipNode) {

    } else {
      chipNode = cc.instantiate(this.chipSample);
    }
    chipNode.active = true;
    let chipValue =  moon.string.toRoundString(chip,2);
    let chipSprite: cc.Sprite = chipNode.getComponent(cc.Sprite);
    chipSprite.spriteFrame = this.atlas.getSpriteFrame('s' + chip);
    if (chipSprite.spriteFrame) {
      NodeUtils.hideByName(chipNode, 'chip_lbl');
    } else {
      chipSprite.spriteFrame = this.atlas.getSpriteFrame('seb');
      NodeUtils.showByName(chipNode, 'chip_lbl');
      NodeUtils.setLabel(chipNode, 'chip_lbl', chipValue);
    }
    (<any>chipNode).value = chipValue;
    return chipNode;
  }

  throwChips(betArea, nodes, startPos, destPos, animTime = Config.chipFlyTime) {
    let radius = 90;
    for (let node of nodes) {
      betArea.addChild(node);
      let pos = moon.random.fromCenter(destPos.x, destPos.y, radius);
      node.position = cc.v2(startPos.x, startPos.y);
      node.runAction(
        cc.moveTo(animTime, cc.v2(pos.x, pos.y)).easing(cc.easeCubicActionOut())
      )
    }
  }

  playMyBet(pot: DragonTigerPot, money) {
    let startPos = pot.betArea.convertToNodeSpaceAR(
      this.myPlayer.avatarIcon.node.convertToWorldSpaceAR(cc.v2())
    );
    this.myPlayer.playBetAnim();
    moon.audio.playEffectInGame("lh_bet_my");
    this.playBet(pot, money, startPos);
  }

  playPeopleBet(pot: DragonTigerPot, money, betList, animTime = Config.chipFlyTime) {
    let startPos = pot.betArea.convertToNodeSpaceAR(
      this.people.convertToWorldSpaceAR(cc.v2())
    );
    // fixed duplicated chips/sound
    if (money > 0)
      moon.audio.playEffectInGame("lh_bet_other");
    this.playBet(pot, money, startPos, betList, animTime);
  }

  moveChipsToHouse(pot: DragonTigerPot, callback?) {
    this.moveChipsFromPot(pot, this.clock, callback);
  }

  moveChipsToPeople(pot: DragonTigerPot, callback?) {
    this.moveChipsFromPot(pot, this.people, callback);
  }

  housePay(pot: DragonTigerPot, money, betList, callback?) {
    let startPos = pot.betArea.convertToNodeSpaceAR(
      this.clock.convertToWorldSpaceAR(cc.v2())
    );
    this.playBet(pot, money, startPos, betList, Config.chipFlyTime, callback);
  }

  private playBet(pot: DragonTigerPot, money, startPos, betList?, animTime = Config.chipFlyTime, callback?) {
    let chipValues;
    if (betList) {
      let chipList = [].concat(betList);
      let smallest = chipList.shift();
      chipList = chipList.reverse();
      chipValues = DragonTigerCalculator.divideMoney(money, chipList, smallest);
    } else {
      chipValues = {
        [money]: 1
      };
    }

    let desPositions = moon.random.nPointsFromRect(pot.betArea.x, pot.betArea.y, pot.betArea.width, pot.betArea.height,
      Object.keys(chipValues).length,
      50);
    let i = 0;
    for (let chip in chipValues) {
      let amount = chipValues[chip];
      if (amount > 0) {
        let chipNodes = this.getChips(+chip, amount);
        this.throwChips(pot.betArea, chipNodes, startPos, desPositions[i++], animTime);
      }
    }
    if (callback) {
      moon.timer.scheduleOnce(() => {
        callback();
      }, animTime + 0.1);
    }
  }

  private moveChipsFromPot(pot: DragonTigerPot, targetNode, callback?) {
    let clockPos = pot.betArea.convertToNodeSpaceAR(targetNode.convertToWorldSpaceAR(cc.v2()));
    let chips = [].concat(pot.betArea.children);
    for (let chip of chips) {
      chip.runAction(
        cc.sequence(
          cc.moveTo(
            Config.chipFlyTime, clockPos
          ).easing(cc.easeCubicActionOut()),
          cc.callFunc(() => {
            chip.removeFromParent();
            chip.active = false;
            this.chipPool.put(chip);
            if (callback) {
              callback();
            }
          })
        )
      )
    }
  }

}