import {ThreeFaceBetState} from './gamestates/ThreeFaceBetState';
import {GameScene} from '../../../../scripts/common/GameScene';
import {ThreeFaceBetLayer} from './ThreeFaceBetLayer';
import {ThreeFacePlayer} from './ThreeFacePlayer';
import {ThreeFaceCardLayer} from './ThreeFaceCardLayer';
import {NodeUtils} from '../../../../scripts/core/NodeUtils';
import {GlobalInfo, moon} from '../../../../scripts/core/GlobalInfo';
import {ThreeFacePlayerInfo} from './model/ThreeFacePlayerInfo';
import {ThreeFaceRoom} from './model/ThreeFaceRoom';
import {COUNTDOWN_TYPE, DECK_ID, GAME_STATE} from '../../../../scripts/core/Constant';
import {ThreeFaceRoomService} from './service/ThreeFaceRoomService';
import {RoomInfo} from '../../../../scripts/model/Room';
import {ThreeFaceLobby} from './ThreeFaceLobby';
import {ThreeFaceReadyState} from './gamestates/ThreeFaceReadyState';
import {ThreeFaceHouseState} from "./gamestates/ThreeFaceHouseState";
import {THREE_FACE_AUDIO} from "./ThreeFaceConstants";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceGame extends GameScene {
  @property(cc.Label)
  matchID: cc.Label = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Node)
  dropdown: cc.Node = null;

  @property([ThreeFacePlayer])
  players: ThreeFacePlayer[] = [];

  @property(ThreeFaceBetLayer)
  betLayer: ThreeFaceBetLayer = null;

  @property(ThreeFaceCardLayer)
  cardLayer: ThreeFaceCardLayer = null;

  @property(cc.Node)
  waiting: cc.Node = null;

  @property(cc.Node)
  stateNode: cc.Node = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Node)
  gameNode: cc.Node = null;

  @property(ThreeFaceLobby)
  lobby: ThreeFaceLobby = null;

  onEnter() {
    super.onEnter();
    this.gameNode.active = false;
    this.hideBetInfo();
    this.setLocaleSpiteFrame();
    moon.cardPool.setCardDeck(DECK_ID.SIMPLIFIED_1);
    moon.cardPool.setCardScale(0.7);
    if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
      this.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
      moon.audio.playMusicInGame(THREE_FACE_AUDIO.LOBBY_BGM);
    }
    this.onResize();
  }


  onEnterRoom(isUseAnimate = true, requestRoomInfo = true) {
    super.onEnterRoom();
    if (requestRoomInfo) {
      moon.gameSocket.getRoomInfoList(GlobalInfo.room.gameId);
    }
    this.getIntoGameRoom(isUseAnimate);
    this.hidePlayers();
    this.hideBetInfo();
    this.showWaiting();
  }

  onLeaveRoom() {
    super.onLeaveRoom();
  }

  showHelp() {
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    moon.dialog.showHelp(GlobalInfo.room.gameId);
    this.toggleDropdown();
  }

  showSetting() {
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    this.toggleDropdown();
    moon.dialog.showSettings();
  }

  showRecord() {
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    this.toggleDropdown();
    moon.gameSocket.getPlayHistory(GlobalInfo.room.gameId);
  }


  updateRoomInfoList(roomInfoList: Array<RoomInfo>) {
    this.lobby.updateRoomInfoList(roomInfoList);
  }

  setLocaleSpiteFrame() {
  }

  setLabelLanguage() {
    NodeUtils.setLocaleLabels(this.ui, {
      'math_lbl': ['matchID', {}],
      'roomBet_lbl': ['ante', {}],
      'money_lbl': ['money', {}],
      'bankInfo_lbl': ['waitPlaceBet', {}],
      'wait_lbl': ['waitOtherPlayers', {}],
      'quit_lbl': ['quit', {}],
      'ok_lbl': ['ok', {}],
      'setting_lbl': ['setting', {}],
      'record_lbl': ['record', {}],
      'help_lbl': ['help', {}],
      'cancel_lbl': ['cancel', {}]
    });
  }


  getIntoGameRoom(isUseAnimate = true) {
    if (isUseAnimate) {
      cc.log('get into game rome three face');
      moon.header.hide();
      moon.dialog.hideWaiting();
      moon.audio.stopAllAudio();
      this.lobby.node.runAction(
        cc.sequence(
          cc.fadeOut(0.3),
          cc.callFunc(() => {
            this.lobby.node.active = false;
          })
        )
      );
      this.gameNode.opacity = 0;
      this.gameNode.active = true;
      this.gameNode.runAction(
        cc.sequence(
          cc.delayTime(0.1),
          cc.fadeIn(0.2),
          cc.callFunc(() => {
            moon.audio.playMusicInGame(THREE_FACE_AUDIO.GAME_BGM, true);
          })
        )
      );
    } else {
      moon.header.hide();
      moon.dialog.hideWaiting();
      moon.audio.stopAllAudio();
      this.lobby.node.active = false;
      this.gameNode.active = true;
      this.gameNode.opacity = 255;
      moon.audio.playMusicInGame(THREE_FACE_AUDIO.GAME_BGM, true);
    }

  }

  onLeave() {
    super.onLeave();
    if (this.state) {
      this.state.onLeave(this, {});
    }
    // TODO: replace by card
    moon.cardPool.clear();
  }

  onLanguageChange() {
    super.onLanguageChange();
    this.setLabelLanguage();
    this.setLocaleSpiteFrame();
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
    this.lobby.onResize();
  }

  onPause() {

  }

  onResume() {

  }

  toggleDropdown() {
    cc.log('click dropdown toogle 1');
    this.dropdown.active = !this.dropdown.active;
    if (this.dropdown.active) {
      moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    }
  }

  setPlayers(playerInfos: ThreeFacePlayerInfo[], bankerId = -1) {
    let startSeat;
    cc.log('set players', playerInfos);
    for (let playerInfo of playerInfos) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    for (let playerInfo of playerInfos) {
      let seatIndex = (playerInfo.seat + 5 - startSeat) % 5;
      let player: ThreeFacePlayer = this.getPlayerByIndex(seatIndex);
      if (player) {
        player.node.active = true;
        player.onEnter();
        player.setPlayerInfo(playerInfo);
      }
      player.showDealer(playerInfo.userId == bankerId);
    }

    if (playerInfos.length >= 5) {
      this.hideWaiting();
    }
  }

  getPlayerByIndex(index) {
    return this.players[index];
  }

  getPlayerByUserId(userId): ThreeFacePlayer {
    return this.players.filter(player => {
      //cc.log(player)
      if (player.playerInfo)
        return player.playerInfo.userId == userId;
    })[0];
  }

  getActiveOrderedPlayers() {
    let startSeat;
    let room: ThreeFaceRoom = <ThreeFaceRoom >GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }

    let orderedPlayers = [];
    for (let player of this.players) {
      if (player.node.active) {
        let index = (player.playerInfo.seat - startSeat + 6) % 6;
        orderedPlayers.push(player);
      }
    }
    return orderedPlayers;
  }

  setRoom(room: ThreeFaceRoom) {
    this.matchID.string = room.matchId || '';
    this.matchID.node.parent.active = !!room.matchId;
    this.bet.string = room.betMoney.toString() || '';
  }

  playSelectDealer(userId: number) {
    return new Promise<any>(resolve => {
      let selectedPlayer: ThreeFacePlayer;
      let rotateActions = [];
      let rotateDelay = 0.2;
      let players = this.getActiveOrderedPlayers();
      let numberOfSelectingPlayer = players.filter(player => player.playerInfo.isChoose).length || players.length;
      let totalRotateTime = Math.max(0, numberOfSelectingPlayer - 1);
      let round = Math.floor(totalRotateTime / (rotateDelay * numberOfSelectingPlayer));

      for (let player of players) {
        player.showDealer(false);
        //if this player has snatched,  highlight this user
        if (player.playerInfo.isChoose || numberOfSelectingPlayer == players.length) {
          rotateActions.push(
            cc.callFunc(() => {
              player.showBorder(true);
              moon.audio.playEffectInGame(THREE_FACE_AUDIO.DEALER_SELECTING, false);
            })
          );
          rotateActions.push(
            cc.delayTime(rotateDelay)
          );
          rotateActions.push(
            cc.callFunc(() => {
              player.showBorder(false);
            })
          );
          if (player.playerInfo.userId == userId) {
            selectedPlayer = player;
          }
        }
      }

      if (numberOfSelectingPlayer > 1) {
        this.node.runAction(
          cc.sequence(rotateActions).repeat(round)
        );
      }
      this.node.runAction(
        cc.sequence(
          cc.delayTime(totalRotateTime),
          selectedPlayer.getSelectAnim(),
          cc.callFunc(() => {
            moon.audio.playEffectInGame(THREE_FACE_AUDIO.DEALER_SELECTED, false);
          }),
          cc.callFunc(() => {
            selectedPlayer.showDealer(true);
            resolve();
          })
        )
      );
    });
  }

  leaveGame() {
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.CLICK);
    moon.gameSocket.leaveBoard();
    this.toggleDropdown();

  }

  returnBackToGameLobby() {
    moon.header.show();
    moon.audio.stopAllAudio();
    this.gameNode.runAction(
      cc.sequence(
        cc.fadeOut(0.3),
        cc.callFunc(() => {
          this.gameNode.active = false;
        })
      )
    );
    this.lobby.node.opacity = 0;
    this.lobby.node.active = true;
    moon.dialog.hideWaiting();
    this.lobby.node.runAction(
      cc.sequence(
        cc.delayTime(0.1),
        cc.fadeIn(0.2),
        cc.callFunc(() => {
          if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]) {
            this.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
          }
          moon.audio.playMusicInGame(THREE_FACE_AUDIO.LOBBY_BGM, true);
        })
      )
    );
  }

  showReadyState() {
    this.stateNode.getChildByName('ready_state').active = true;
  }

  hideReadyState() {
    this.stateNode.getChildByName('ready_state').active = false;
  }


  hidePlayers() {
    cc.log('hide player');
    for (let player of this.players) {
      player.node.active = false;
    }
  }

  setBetFactorOfPlayer(playerId, betFactor) {
    let player: ThreeFacePlayer = this.getPlayerByUserId(playerId);
    player.playerInfo.betFactor = betFactor;
    player.setBet();
    if (GlobalInfo.me.userId === playerId) {
      if (this.state instanceof ThreeFaceBetState) {
        this.state.onBetEnd();
      }
    }
  }

  showPlayersResultBanner() {
    for (let player of this.players) {
      if (player.node.active) {
        player.showPointBanner();
      }
    }
  }

  removePlayer(userId: any) {
    let player = this.getPlayerByUserId(userId);
    if (player) {
      player.node.active = false;
    }
  }

  clear() {
    this.betLayer.clear();

    this.hideBetInfo();
    moon.cardPool.returnAllCard();
    this.clearPlayers();

    if (this.state instanceof ThreeFaceReadyState)
      this.state.onLeave(this, {});
  }

  /**
   * clear all UI player
   */
  clearPlayers() {
    let roomService = ThreeFaceRoomService.getInstance();
    roomService.clearPlayerInfoList();

    for (let player of this.players) {
      if (player.node.active) {
        player.clear();
        player.showBorder(false);
      }
    }
  }

  clearPlayersUI() {
    for (let player of this.players) {
      if (player.node.active) {
        cc.log('clear ui of player');
        player.clear();
      }
    }
  }

  clearPlayerBetFactor() {
    this.players.forEach(player => {
      if (player.node.active) {
        player.hideBet();
      }
    });
  }

  removeOtherPlayers() {
    for (let player of this.players) {
      if (player.playerInfo.userId != GlobalInfo.me.userId) {
        player.node.active = false;
      }
    }
  }

  addPlayer(playerInfo: ThreeFacePlayerInfo) {
    cc.log('add player', playerInfo);
    let startSeat;
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    let seatIndex = (playerInfo.seat + 6 - startSeat) % 6;
    let player: ThreeFacePlayer = this.getPlayerByIndex(seatIndex);
    if (player) {
      player.node.active = true;
      player.onEnter();
      player.setPlayerInfo(playerInfo);
    }
  }

  updateCurrentRoom(data) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    cc.log('return game with state', room.countdownInfo.type);
    this.node.stopAllActions();
    moon.cardPool.returnAllCard();
    this.clearPlayersUI();
    this.hideBetInfo();
    this.setRoom(room);
    this.setPlayers(room.playerInfoList, room.bankerId);
    this.betLayer.clear();
    this.hideWaiting();
    ThreeFaceRoomService.getInstance().activeCountdown();
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_NOT_START_GAME:
        this.showWaiting();
        break;
      case COUNTDOWN_TYPE.STATE_READY_START_GAME:
        this.changeState(GAME_STATE.READY_START, ThreeFaceReadyState, data);
        break;
      case COUNTDOWN_TYPE.STATE_START_GAME:
        // this.changeState(GAME_STATE.START, TwoEightStartState);
        break;
      case COUNTDOWN_TYPE.STATE_CHOOSE_BANKER:
      case COUNTDOWN_TYPE.STATE_FIND_BANKER:
        this.changeState(GAME_STATE.HOUSE, ThreeFaceHouseState);
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        this.changeState(GAME_STATE.BET, ThreeFaceBetState);
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        break;
    }
  }

  showWaiting() {
    this.waiting.active = true;
  }

  hideBetInfo() {
    for (let player of this.players) {
      player.hideBet();
    }
  }

  hideWaiting() {
    this.waiting.active = false;
  }

  /**
   * uses for stand by to remove a player when they leave the game on result state
   * @param userId user to be remove when the game start if they leave the game on result state
   */

  hidePlayerSnatchValue() {
    for (let player of this.players) {
      if (player.node.active) {
        player.hideSnatchValue();
      }
    }
  }

  setPlayerSnatch(playerId, isPlayerSnatch) {
    cc.log('set player snatch');
    let player = this.getPlayerByUserId(playerId);
    player.playerInfo.isChoose = isPlayerSnatch;
    if (isPlayerSnatch) {
      player.playerSnatch();
    } else {
      player.playerNonSnatch();
    }
  }
}