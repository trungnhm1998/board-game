import {ThreeFaceStartState} from '../gamestates/ThreeFaceStartState';
import {ThreeFacePlayerInfo} from '../model/ThreeFacePlayerInfo';
import {ThreeFaceRoom} from '../model/ThreeFaceRoom';
import {ThreeFaceBetState} from '../gamestates/ThreeFaceBetState';
import {ThreeFaceReadyState} from '../gamestates/ThreeFaceReadyState';
import {IGameService} from '../../../../../scripts/services/GameService';
import {ThreeFaceGame} from '../ThreeFaceGame';
import {COUNTDOWN_TYPE, GAME_STATE, KEYS, SERVER_EVENT} from '../../../../../scripts/core/Constant';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {CountdownInfo} from '../../../../../scripts/model/Room';
import {ModelUtils} from '../../../../../scripts/core/ModelUtils';
import {GameSocket} from '../../../../../scripts/services/SocketService';
import {ThreeFaceRoomService} from './ThreeFaceRoomService';
import {ThreeFaceHouseState} from "../gamestates/ThreeFaceHouseState";
import {ThreeFaceEndGame} from "../gamestates/ThreeFaceEndGame";


export class ThreeFaceService implements IGameService {
  isActive = false;

  name = 'ThreeFaceService';

  game: ThreeFaceGame;

  waitingActions: any[] = [];

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(ThreeFaceGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };

    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => processAction(this.onErrorMessage, data));
  }

  onJoinBoard(data) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    cc.log('set join board info', room);
    this.game.setPlayers(room.playerInfoList);
    this.game.setRoom(room);
    this.game.showWaiting();
    this.processWaitingActions();
  }

  onPlayerJoinBoard(data) {
    if (GlobalInfo.room.roomId != data[KEYS.ROOM_ID]) {
      return
    }
    let playerInfo: ThreeFacePlayerInfo = data[KEYS.PLAYER_INFO];
    ThreeFaceRoomService.getInstance().addPlayerInfo(playerInfo);
    if (this.game && GlobalInfo.room.roomId == data[KEYS.ROOM_ID]) {
      this.game.addPlayer(data.playerInfo);
    }
    let room: ThreeFaceRoom = <ThreeFaceRoom> GlobalInfo.room;
    cc.log('player join board', room);
  }

  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    cc.log('return game and set room data', GlobalInfo.room);
    this.game.onEnterRoom(false, true);
    this.game.updateCurrentRoom(data);
  }

  onReadyStartGame(data) {
    if (GlobalInfo.room.roomId != data[KEYS.ROOM_ID]) {
      return
    }
    ModelUtils.merge(GlobalInfo.room, data);
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_READY_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    this.game.hideWaiting();
    this.game.changeState(GAME_STATE.READY_START, ThreeFaceReadyState, data);
  }

  onErrorMessage(data: any) {
    moon.dialog.showNotice(data[KEYS.MESSAGE]);
  }

  onActionInGame(data) {
    let subData = data[KEYS.DATA];
    if (GlobalInfo.room.roomId != subData[KEYS.ROOM_ID]) {
      return;
    }
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.READY_START_GAME:
          this.onReadyStartGame(subData);
          break;
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.BEGIN_CHOOSE_BANKER:
          this.onBeginChooseBanker(subData);
          break;
        case SERVER_EVENT.CHOOSE_BANKER:
          this.onChooseBanker(subData);
          break;
        case SERVER_EVENT.BANKER:
          this.onBanker(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        case SERVER_EVENT.ERROR_MESSAGE:
          this.onErrorMessage(subData);
          break;
      }
    };
    handleEvents();
  }


  private onStopGame() {
    this.game.showWaiting();
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let roomService = ThreeFaceRoomService.getInstance();
    if (GlobalInfo.me.userId == userId) {
      let backToMain = () => {
        this.game.clear();
        roomService.stopCountdown(roomId);
        this.clearWaitingActions();
        moon.dialog.showWaiting();
        moon.lobbySocket.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
        this.game.returnBackToGameLobby();
      };
      backToMain();
    } else {
      roomService.removePlayerInfo(userId);
      this.game.removePlayer(userId);
      let room = GlobalInfo.room as ThreeFaceRoom;
      if (room.playerInfoList.length > 1) {
        this.game.hideWaiting();
      } else {
        this.game.showWaiting();
      }
    }
  }

  onStartGame(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: ThreeFaceRoom = <ThreeFaceRoom> GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    room.betMoney = data.betMoney;
    room.matchId = data.matchId;
    ThreeFaceRoomService.getInstance().activeCountdown();
    ThreeFaceRoomService.getInstance().clearCardList();

    this.game.hideWaiting();
    this.game.changeState(GAME_STATE.START, ThreeFaceStartState, data);
  }

  onBeginBet(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    ThreeFaceRoomService.getInstance().activeCountdown();

    let myPlayerInfo = ThreeFaceRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    myPlayerInfo.betList = room.betList;

    this.game.changeState(GAME_STATE.BET, ThreeFaceBetState, data);
  }

  onBet(data: any) {
    let playerInfo: ThreeFacePlayerInfo = ThreeFaceRoomService.getInstance().getPlayerInfo(data[KEYS.USER_ID]);
    playerInfo.betFactor = data[KEYS.BET_FACTOR];

    this.game.setBetFactorOfPlayer(data[KEYS.USER_ID], data[KEYS.BET_FACTOR]);

    if (data[KEYS.USER_ID] == GlobalInfo.me.userId) {
      if (this.game.state instanceof ThreeFaceBetState) {
        this.game.state.onBetEnd();
      }
    }
  }

  onEndGame(data: any) {
    // Update Data/Model for client
    ThreeFaceRoomService.getInstance().updatePlayerInfoList(data[KEYS.PLAYER_INFO_LIST]);
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;

    // update animation and UI
    this.game.changeState(GAME_STATE.END, ThreeFaceEndGame, data);
  }

  updateRoomInfoList() {

  }

  onBeginChooseBanker(data: any) {
    // Update Data/Model for client
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_FIND_BANKER;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    ThreeFaceRoomService.getInstance().activeCountdown();

    // update animation and UI
    this.game.changeState(GAME_STATE.HOUSE, ThreeFaceHouseState, data);
  }

  onChooseBanker(data: any) {
    let playerInfo: ThreeFacePlayerInfo = ThreeFaceRoomService.getInstance().getPlayerInfo(data[KEYS.USER_ID]);
    playerInfo.isChoose = data[KEYS.IS_CHOOSE];

    let player = this.game.getPlayerByUserId(data[KEYS.USER_ID]);
    if (playerInfo.isChoose) {
      player.playerSnatch();
    } else {
      player.playerNonSnatch();
    }

    if (data[KEYS.USER_ID] == GlobalInfo.me.userId) {
      if (this.game.state instanceof ThreeFaceHouseState) {
        this.game.state.onBetEnd();
      }
    }
  }

  onBanker(data: any) {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.bankerId = data[KEYS.BANKER_ID];

    this.game.playSelectDealer(room.bankerId);
  }
}