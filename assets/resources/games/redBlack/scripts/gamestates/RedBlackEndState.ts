
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import { Card } from '../../../../../scripts/core/Card';
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { RedBlackGame } from "../RedBlackGame";
import { RedBlackRoom } from "../model/RedBlackRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackEndState extends GameState implements IGameState {

  @property(dragonBones.ArmatureDisplay)
  stopBetAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: RedBlackGame, data: any) {
    game.setRoom(<RedBlackRoom>GlobalInfo.room);
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
    this.stopBetAnim.playAnimation('action', 1);
    
  }

  onUpdate(game: RedBlackGame, updateData: UpdateData) {
  }

  onLeave(game: RedBlackGame, data: any) {
  }
}