import {Promise} from 'es6-promise';
import {RESOURCE_BLOCK} from './Constant';
import {LocalData} from './LocalStorage';
import {NodeUtils} from "./NodeUtils";
import {moon} from "./GlobalInfo";

export class ResourceBlock {
  skeletons = {};
  particles = {};
  spriteFrames = {};
}

export class ResourceManager {

  sounds = [];
  musics = [];
  loadedImage = {};

  audioClips = {};

  blocks = {
    [RESOURCE_BLOCK.SHARE]: new ResourceBlock(),
    [RESOURCE_BLOCK.GAME]: new ResourceBlock()
  };

  preloadSound() {
    cc.loader.loadResDir('sound', (err, resources) => {
      for (let res of resources) {
        this.audioClips[res.name] = res;
      }
    });
  }

  preloadFolders(listFolder, updateProgress, blockName = RESOURCE_BLOCK.SHARE) {
    return new Promise((resolve) => {
      let count = 0;
      let progesses = {};
      let totalFolder = listFolder.length;
      for (let folder of listFolder) {
        cc.loader.loadResDir(folder,
          (...args) => {
            progesses[folder] = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
            let totalProgress = 0;
            for (let key in progesses) {
              totalProgress += progesses[key];
            }
            updateProgress(totalProgress / totalFolder);
          },
          (...args) => {
            count++;
            progesses[folder] = 1;
            this.onSpriteFrameComplete(blockName, args[0], args[1], args[2]);
            if (count >= totalFolder) {
              resolve();
            }
          });
      }
    });
  }

  preloadFolder(resourceFolder, updateProgress, blockName = RESOURCE_BLOCK.SHARE) {
    return new Promise((resolve) => {
      cc.loader.loadResDir(
        resourceFolder,
        (...args) => {
          let percent = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
          updateProgress(percent);
        },
        (...args) => {
          this.onSpriteFrameComplete(blockName, args[0], args[1], args[2]);
          resolve();
        }
      );
    });
  }

  preloadPrefabAndFolders(prefab, localeFolders, updateProgress, blockName = RESOURCE_BLOCK.GAME) {
    return new Promise((resolve) => {
      let prefabProgress = 0;
      let folderProgress = 0;
      let count = 0;
      cc.loader.loadRes(prefab,
        (...args) => {
          prefabProgress = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
          updateProgress((prefabProgress + folderProgress) / 2);
        },
        (...args) => {
          prefabProgress = 1;
          count++;
          this.onSpriteFrameComplete(blockName, args[0], [args[1]], args[2]);
          if (count >= 2) {
            resolve();
          }
        });

      this.preloadFolders(localeFolders,
        (progress) => {
          folderProgress = progress;
          updateProgress((prefabProgress + folderProgress) / 2);
        },
        blockName)
        .then(() => {
          folderProgress = 1;
          count++;
          if (count >= 2) {
            resolve();
          }
        });
    });
  }

  private onSpriteFrameLoad(blockName, completedCount: number, totalCount: number, asset) {
    if (totalCount == 0) {
      return 1;
    }

    if (asset && asset.url && asset.type == 'plist') {
      if (!this.blocks[blockName]) {
        this.blocks[blockName] = new ResourceBlock();
      }
      let filename = asset.url.replace(/^.*[\\\/]/, '').replace(/\.[^/.]+$/, '');
      this.blocks[blockName].particles[filename] = asset.url;
    }

    return completedCount / totalCount;
  }

  private onSpriteFrameComplete(blockName, err, spriteFrames, paths) {
    let block = this.blocks[blockName];
    if (!block) {
      this.blocks[blockName] = new ResourceBlock();
      block = this.blocks[blockName];
    }
    let i = 0;
    for (let sprite of spriteFrames) {
      if (sprite instanceof sp.SkeletonData) {
        block.skeletons[sprite.name] = sprite;
      } else if (sprite instanceof dragonBones.DragonBonesAsset) {
        block.skeletons[sprite.name] = sprite;
      } else {
        block.spriteFrames[sprite.name] = sprite;
      }
      i++;
    }
  }

  cleanBlock(blockName = RESOURCE_BLOCK.SHARE) {
    if (this.blocks[blockName]) {
      this.blocks[blockName].spriteFrames = {};
      this.blocks[blockName].particles = {};
      this.blocks[blockName].skeletons = {};
      if (cc.sys.isNative) {
        cc.sys.garbageCollect();
      }
    }
  }

  getPrefab(frameName, blockName = RESOURCE_BLOCK.GAME) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].spriteFrames[frameName];
  }

  getGameSpriteFrame(frameName) {
    return this.getSpriteFrame(frameName, RESOURCE_BLOCK.GAME)
  }

  getSpriteFrame(frameName, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].spriteFrames[frameName];
  }

  getAtlas(frameName, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].spriteFrames[frameName + '.plist'];
  }

  getFont(fontName, blockName = RESOURCE_BLOCK.GAME) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].spriteFrames[fontName];
  }

  clearSpriteFrame(frameName, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    delete this.blocks[blockName].spriteFrames[frameName];
  }


  setRemoteImage(sprite: cc.Sprite, imageUrl) {
    return new Promise(resolve => {
      if (this.loadedImage[imageUrl]) {
        sprite.spriteFrame = this.loadedImage[imageUrl];
        resolve();
      } else if (cc.sys.isNative) {
         moon.string.imageURLToBase64(imageUrl)
          .then(NodeUtils.getSpriteFrameFromImageData)
          .then((sp: cc.SpriteFrame) => {
            sprite.node.stopAllActions();
            sprite.node.opacity = 0;
            sprite.spriteFrame = sp;
            sprite.node.runAction(cc.fadeIn(0.2));
            this.loadedImage[imageUrl] = sp;
            resolve();
          });
      } else  {
        cc.loader.load(imageUrl, (err, texture) => {
          if (typeof texture == "string" || texture == null) {
             moon.string.imageURLToBase64(imageUrl)
              .then(NodeUtils.getSpriteFrameFromImageData)
              .then((sp: cc.SpriteFrame) => {
                sprite.node.stopAllActions();
                sprite.node.opacity = 0;
                sprite.spriteFrame = sp;
                sprite.node.runAction(cc.fadeIn(0.2));
                this.loadedImage[imageUrl] = sp;
                resolve();
              });
          } else {
            let sp = new cc.SpriteFrame(texture);
            sprite.node.stopAllActions();
            sprite.node.opacity = 0;
            sprite.spriteFrame = sp;
            sprite.node.runAction(cc.fadeIn(0.2));
            this.loadedImage[imageUrl] = sp;
            resolve();
          }
        })
      }
    });
  }

  preloadRemoteImage(imageUrl) {
     moon.string.imageURLToBase64(imageUrl)
      .then(NodeUtils.getSpriteFrameFromImageData)
      .then((sp: cc.SpriteFrame) => {
        this.loadedImage[imageUrl] = sp;
      });
  }

  getLogo(gameId) {
    let basename = 'logo';
    return this.getSpriteFrame(`${basename}_${gameId}`);
  }

  getLogoText(gameId) {
    return this.getSpriteFrame(
      `logo_text_${gameId}`,
      RESOURCE_BLOCK.SHARE
    );
  }

  getSkelecton(name, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].skeletons[name];
  }

  getParticle(name, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].particles[name];
  }

  getCard(itemId, blockName = RESOURCE_BLOCK.GAME) {
    if (!this.blocks[blockName]) return;
    return this.getCardInfo(itemId);
  }

  readJsonFile(path) {
    return new Promise<any>((resolve) => {
      cc.loader.loadRes(path, function (err, res) {
        resolve(res.json);
      });
    });
  }

  setSound(on) {
    LocalData.sound = on ? 1 : 0;
    for (let sound of this.sounds) {
      if (on) {
        // cc.audioEngine.resume(sound);
      } else {
        cc.audioEngine.stop(sound);
      }
    }
    this.sounds = [];
    moon.storage.saveLocalData();
  }

  setMusic(on) {
    LocalData.music = on ? 1 : 0;
    for (let music of this.musics) {
      if (on) {
        // cc.audioEngine.resume(music);
      } else {
        cc.audioEngine.stop(music);
      }
    }
    moon.storage.saveLocalData();
  }

  playSound(soundFileName, isMusic = false, loop = false, volume = 1.0) {
    if (isMusic && LocalData.music != 1) return;
    if (!isMusic && LocalData.sound != 1) return;
    let audioId = cc.audioEngine.play(this.audioClips[soundFileName], loop, volume);
    if (isMusic) {
      this.musics.push(audioId);
    } else {
      this.sounds.push(audioId);
    }
  }

  stopAllSound(cleanup = false) {
    for (let music of this.musics) {
      cc.audioEngine.stop(music);
    }
    for (let sound of this.sounds) {
      cc.audioEngine.stop(sound);
    }
    if (cleanup) {
      this.musics = [];
      this.sounds = [];
    }
  }

  private getCardInfo(cardItem) {
    let cardType = cardItem % 4;
    let cardNum: any = Math.floor(cardItem / 4) + 1;
    if (cardNum == 1) {
      cardNum = 'A';
    } else if (cardNum == 11) {
      cardNum = 'J';
    } else if (cardNum == 12) {
      cardNum = 'Q';
    } else if (cardNum == 13) {
      cardNum = 'K';
    }

    return {
      cardType,
      cardNum
    };
  }

  static instance: ResourceManager;

  static getInstance(): ResourceManager {
    if (!ResourceManager.instance) {
      ResourceManager.instance = new ResourceManager();
    }

    return ResourceManager.instance;
  }
}