import {IShader} from '../scripts/shader/CMaterial';

const renderEngine = (<any>cc).renderer.renderEngine;
const renderer = renderEngine.renderer;

export const GrayShader: IShader = {
  name: 'Gray',
  params: [
  ],

  update(material, percent) {
  },

  defines: [],

  vert: `
    uniform mat4 viewProj;
    attribute vec3 a_position;
    attribute vec2 a_uv0;
    varying vec2 uv0;
    
    void main () {
        vec4 pos = viewProj * vec4(a_position, 1);
        gl_Position = pos;
        uv0 = a_uv0;
    }`,

  frag:
    `
      uniform sampler2D texture;
      varying vec2 uv0;
      
      void main()
      {
        vec4 fragColor = texture2D(texture, uv0);
        float grey = (fragColor.r + fragColor.g + fragColor.b)/3.0;
        fragColor = vec4(grey, grey, grey, fragColor.a);
        gl_FragColor = fragColor;
      }
    `
};