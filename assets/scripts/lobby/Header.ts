import {GlobalInfo, moon} from "../core/GlobalInfo";
import {SCENE_TYPE} from "../core/Constant";
import {UserInfo} from "../model/UserInfo";
import {Config} from "../Config";
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export class Header extends cc.Component {

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Sprite)
  avatar: cc.Sprite = null;

  @property(cc.Sprite)
  levelBg: cc.Sprite = null;

  @property(cc.Label)
  level: cc.Label = null;

  @property(cc.Label)
  totalPlayer: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Label)
  ping: cc.Label = null;

  @property(cc.Node)
  recordBtn: cc.Node = null;

  @property(cc.Node)
  settingBtn: cc.Node = null;

  @property(cc.Node)
  feedbackBtn: cc.Node = null;

  @property(cc.Node)
  fullscreenBtn: cc.Node = null;

  @property(cc.Node)
  outFullScreen: cc.Node = null;

  @property(cc.Node)
  fullScreen: cc.Node = null;

  @property(cc.Node)
  highPing: cc.Node = null;

  @property(cc.Node)
  mediumPing: cc.Node = null;

  @property(cc.Node)
  lowPing: cc.Node = null;

  @property(cc.Node)
  avatarLoading: cc.Node = null;

  isFull = false;
  private moneyChangeHandler;

  onLoad() {
    if (!cc.sys.isNative) {
      document.addEventListener("fullscreenchange", () => {
        this.isFull = !this.isFull;
        this.fullScreen.active = !this.isFull;
        this.outFullScreen.active = this.isFull;
      });
    }
  }

  onLanguageChange() {
    NodeUtils.setSprites(this.node,
      {
        'returnBtn': 'common_btn_back_1',
        'feedbackBtn': 'common_btn_fankui_1',
        'recordBtn': 'common_btn_record_1',
        'totalBtn': 'mainhall_btn_online_1',
        'helpBtn': 'mainhall_btn_help_1',
        'settingBtn': 'mainhall_btn_setting_1',
        'fullscreen': 'mainhall_btn_quanping_1',
        'outFullScreen': 'mainhall_btn_quanping_2',
      }
    )
  }

  setMainLobby() {
    this.hideAllButtons();
    this.settingBtn.active = true;
  }

  setGameLobby() {
    this.hideAllButtons();
    this.recordBtn.active = true;
    this.feedbackBtn.active = false;
  }

  hideAllButtons() {
    [this.recordBtn, this.settingBtn, this.feedbackBtn].map(btn => btn.active = false);
    if (cc.sys.isNative) {
      this.fullscreenBtn.active = false;
    }
  }

  setUserInfo(userInfo: UserInfo) {
    this.playerName.string = moon.string.limitText(userInfo.displayName, Config.maxNameLength);
    if (userInfo.avatar) {
      this.avatarLoading.active = true;
      moon.res.setRemoteImage(this.avatar, userInfo.avatar)
        .then(() => {
          this.avatarLoading.active = false;
        });
    }

    this.setLevel(userInfo.level || 1);
    this.setMoney(userInfo.money);
  }

  setLevel(level) {
    this.level.string = level;
    cc.loader.loadRes(`common/game_common_lvnum${level}`, (err, texture) => {
      if (!err) {
        this.levelBg.spriteFrame = new cc.SpriteFrame(texture);
      }
    });
  }

  setMoney(money) {
    this.money.string = moon.string.formatMoney(money);
  }

  updateTotalPlayer(count: number) {
    this.totalPlayer.string = '' + count;
  }

  updatePing(ms: number) {
    this.ping.string = `${Math.floor(ms)}ms`;
    this.lowPing.active = false;
    this.mediumPing.active = false;
    this.highPing.active = false;
    if (ms >= 500) {
      this.highPing.active = true;
    } else if (ms >= 200) {
      this.mediumPing.active = true;
    } else {
      this.lowPing.active = true;
    }
  }

  showHelp() {
    moon.audio.playAudio('game_sound_btn_click');
    if (moon.scene.isInScreen(SCENE_TYPE.PLAY)) {
      moon.dialog.showHelp(GlobalInfo.room.gameId);
    } else  {
      moon.dialog.showHelp();
    }
  }

  showRecord() {
    moon.audio.playAudio('game_sound_btn_click');
    moon.dialog.showWaiting();
    moon.gameSocket.getPlayHistory(GlobalInfo.room.gameId);
  }

  showSetting() {
    moon.audio.playAudio('game_sound_btn_click');
    moon.dialog.showSettings();
  }

  showFeedback() {
    moon.audio.playAudio('game_sound_btn_click');
  }

  showAvatars() {
    moon.audio.playAudio('game_sound_btn_click');
    moon.dialog.showAvatar();
  }

  toggleFullscreen() {
    moon.audio.playAudio('game_sound_btn_click');
    this.fullScreen.active = false;
    this.outFullScreen.active = false;
    if (this.isFull) {
      (<any>cc).screen.exitFullScreen();
    } else {
      (<any>cc).screen.requestFullScreen();
    }
  }

  onBack() {
    moon.audio.playAudio('game_sound_btn_click');
    if (moon.scene.isInScreen(SCENE_TYPE.MAIN_MENU)) {
      moon.service.logout();
    }

    if (moon.scene.isInScreen(SCENE_TYPE.PLAY)) {
      moon.scene.pushScene(SCENE_TYPE.MAIN_MENU);
    }
  }

  show(duration = 0.4) {
    if (!this.node.active) {
      this.node.opacity = 0;
      this.node.active = true;
      this.node.stopAllActions();
      this.node.runAction(cc.fadeIn(duration));
    }
    this.listenMoneyChange();
  }

  hide() {
    this.node.active = false;
  }

  private listenMoneyChange() {
    if (!this.moneyChangeHandler) {
      this.moneyChangeHandler = (val) => {
        this.setMoney(val);
      };
      moon.money.onMoneyChange(GlobalInfo.me, this.moneyChangeHandler);
    }
  }
}