import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export class Captcha extends cc.Component {

  @property(cc.Sprite)
  captchaImage: cc.Sprite = null;

  @property(cc.EditBox)
  captchaText: cc.EditBox = null;

  @property(cc.Node)
  reloadBtn: cc.Node = null;

  onRefresh;

  token = "";

  isShown() {
    return this.node.active == true;
  }

  onEnable() {
    NodeUtils.setLocaleLabel(this.node, 'c_editbox', 'enterCaptcha', {placeHolder: true});
  }

  show(base64Image?: any, token="", onRefresh?) {
    this.token = token;
    this.node.active = true;
    this.captchaText.node.active = true;
    if (base64Image) {
      base64Image = base64Image.replace('data:image/jpeg;base64,', 'data:image/png;base64,');
      NodeUtils.getSpriteFrameFromImageData(base64Image)
        .then((sp: cc.SpriteFrame) => {
          this.captchaImage.spriteFrame = sp;
        });
    }
    if (onRefresh) {
      this.onRefresh = onRefresh;
    }
  }

  refresh() {
    if (this.reloadBtn) {
      this.reloadBtn.runAction(
        cc.rotateBy(0.3, 180)
      );
    }
    if (this.onRefresh) {
      this.onRefresh();
    }
  }

  hide() {
    this.node.active = false;
  }

  getToken() {
    return this.token;
  }

  getInput() {
    return this.captchaText.string;
  }

  clear() {
    this.captchaText.string = '';
    this.token = '';
  }
}