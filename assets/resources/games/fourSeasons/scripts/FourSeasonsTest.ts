import { BetType } from './FourSeasonsConstants';
import { FourSeasonsService } from './service/FourSeasonsService';
import { FourSeasonsRoom } from './model/FourSeasonsRoom';
import { Random } from './../../../../scripts/core/Random';
import { SERVER_EVENT, GAME_ID, KEYS } from './../../../../scripts/core/Constant';
import { moon, GlobalInfo } from './../../../../scripts/core/GlobalInfo';
import { MockSocket } from './../../../../scripts/test/MockSocket';
import { FourSeasonsGame } from './FourSeasonsGame';
import { TestCase } from './../../../../scripts/test/TestCase';
import { UserInfo } from '../../../../scripts/model/UserInfo';

export class FourSeasonsTest extends TestCase {
  game: FourSeasonsGame;
  socket: MockSocket;

  setGame(gameNode: cc.Node) {
    cc.log('FourSeasonsTest::setGame');
    super.setGame(gameNode);
    this.game = gameNode.getComponent(FourSeasonsGame);
  }

  run() {
    super.run();
    cc.log('FourSeasonsTest::run');
    this.socket = new MockSocket();
    moon.lobbySocket.socket = (<any>this.socket);
    moon.gameSocket.socket = (<any>this.socket);

    this.setGameInfo({
      gameId: 5,
      packageName: 'fourSeasons',
      prefabPath: 'games/fourSeasons/fourSeasonsGame',
      localeFolder: 'games/fourSeasons/locale',
      service: 'FourSeasonsService',
      audioPath: 'resources/games/fourSeasons/sound'
    }).then(() => {
      this.runTestCases();
    })
  }

  setup() {
    // for testing command
    const FSService = moon.service.getCurrentService() as FourSeasonsService;
    FSService.game = this.game;
    moon.gameSocket.on(SERVER_EVENT.ACTION_IN_GAME, data => FSService.onActionInGame(data));

    this.game.onEnter();
  }

  setupPlayer() {
    GlobalInfo.me = new UserInfo();
    GlobalInfo.me.userId = 1;
  }

  setupRoom() {
    let room = new FourSeasonsRoom();
    room.roomId = 1;
    GlobalInfo.room = room;
  }

  runSequence(actions) {
    let ccActions = [];
    for (let action of actions) {
      let func = action[0];
      let delay = action[1];
      ccActions.push(cc.callFunc(() => {
        func.call(this);
      }));
      ccActions.push(cc.delayTime(delay));
    }

    this.game.node.runAction(cc.sequence(ccActions));
  }

  runTestCases() {
    this.setup();
    this.setupPlayer();
    this.setupRoom();
    this.game.onLanguageChange();
    this.runSequence([
      [this.testLobby, 1],
      [this.testPlayGame, 1],
      [this.testShuffleAndDealCards, 5],
      [this.testOtherPlayersBet, 3],
      [this.testOpenCard, 5],
      [this.testClear, 1],
      [this.testStartGameEffect, 3.5],
      [this.testStopGameEffect, 3.5],
    ]);
  }

  private testOtherPlayersBet() {
    const data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.UPDATE_PLAYERS_BET,
      [KEYS.DATA]: {
        [KEYS.PLACE_TYPE_LIST]: [
          {
            [KEYS.TYPE]: BetType.SPADE,
            [KEYS.BET_MONEY_TOTAL]: 200,
            [KEYS.BET_MONEY]: 200,
          },
          {
            [KEYS.TYPE]: BetType.HEART,
            [KEYS.BET_MONEY_TOTAL]: 200,
            [KEYS.BET_MONEY]: 200,
          },
          {
            [KEYS.TYPE]: BetType.CLUB,
            [KEYS.BET_MONEY_TOTAL]: 200,
            [KEYS.BET_MONEY]: 200
          },
          {
            [KEYS.TYPE]: BetType.DIAMOND,
            [KEYS.BET_MONEY_TOTAL]: 342,
            [KEYS.BET_MONEY]: 342
          },
        ],
        [KEYS.ROOM_ID]: 1
      }
    }

    moon.gameSocket.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  private testLobby() {
    let mockRoomList: Array<FourSeasonsRoom> = [];
    for (let index = 0; index < 8; index++) {
      mockRoomList[index] = {
        roomId: index + 1,
        gameId: 5,
        betMoney: 10,
        roomName: `room ${index}`,
        countdownInfo: {
          type: 1,
          timeout: 10
        },
        historyList: [],
        minBet: 5 * index,
        maxBet: 5 * index * 10,
        betList: []
      }
      let matchesCount = 0;
      while (matchesCount < 11) {
        matchesCount++;
        mockRoomList[index].historyList.push([
          new Random().integer(0, 1),
          new Random().integer(0, 1),
          new Random().integer(0, 1),
          new Random().integer(0, 1),
          new Random().integer(0, 1)
        ])
      }
    }
    this.game.lobby.updateRoomInfoList(mockRoomList);
  }

  private testPlayGame() {
    moon.gameSocket.on(SERVER_EVENT.PLAY_GAME, () => {
      let historyList = []
      let matchesCount = 0;
      while (matchesCount < 11) {
        historyList.push([
          new Random().integer(0, 1),
          new Random().integer(0, 1),
          new Random().integer(0, 1),
          new Random().integer(0, 1),
          new Random().integer(0, 1)
        ])
        matchesCount++;
      }

      const data = {
        "roomInfo": {
          "roomId": 1,
          "gameId": 5,
          "matchId": "155894649536612",
          "countdownInfo": {
            "type": 1,
            "timeout": 11
          },
          "playerInfo": {
            "userId": 1121,
            "displayName": "Mahijith",
            "avatar": "https://account.devuid.club/avatar/avatar-6.png",
            "money": 2238783.8,
            "placeBetTypeList": []
          },
          "playerNum": 1,
          "cardNum": 52,
          "placeBetInfoList": [
            {
              "placeBetType": -1,
              "cardList": []
            },
            {
              "placeBetType": 1,
              "cardList": []
            },
            {
              "placeBetType": 2,
              "cardList": []
            },
            {
              "placeBetType": 3,
              "cardList": []
            },
            {
              "placeBetType": 4,
              "cardList": []
            }
          ],
          "placeBetTypeList": [
            {
              "type": -1,
              "rate": 1,
              "specialCardList": [],
              "betMoneyTotal": 0,
              "betMoney": 0,
              "cardList": [],
              "resultType": -1
            },
            {
              "type": 1,
              "rate": 1,
              "specialCardList": [],
              "betMoneyTotal": 0,
              "betMoney": 0,
              "cardList": [],
              "resultType": -1
            },
            {
              "type": 2,
              "rate": 1,
              "specialCardList": [],
              "betMoneyTotal": 0,
              "betMoney": 0,
              "cardList": [],
              "resultType": -1
            },
            {
              "type": 3,
              "rate": 1,
              "specialCardList": [],
              "betMoneyTotal": 0,
              "betMoney": 0,
              "cardList": [],
              "resultType": -1
            },
            {
              "type": 4,
              "rate": 1,
              "specialCardList": [],
              "betMoneyTotal": 0,
              "betMoney": 0,
              "cardList": [],
              "resultType": -1
            }
          ],
          historyList,
          "betList": [
            2,
            5,
            10,
            20,
            50
          ],
          "timeout": 1
        }
      }

      GlobalInfo.room = data[KEYS.ROOM_INFO];
      GlobalInfo.rooms = [GlobalInfo.room];
      // generate mock up historyList
      this.game.onEnterRoom();
      const FSService = moon.service.getCurrentService() as FourSeasonsService;
      FSService.onJoinBoard(data);
    })

    moon.gameSocket.playFourSeasons(GAME_ID.FOUR_SEASONS, 1);
  }

  private testShuffleAndDealCards() {
    cc.log('FourSeasonsTest::testShuffleCards');
    this.game.table.shuffleThenDealCards();
  }

  private testOpenCard() {
    const data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.OPEN_CARD,
      [KEYS.DATA]: {
        "placeBetInfoList": [
          {
            // TODO: Ask anh Vu for using 0 for placeBetTYpe as Banker
            "placeBetType": 0,
            "cardList": [
              4,
              21,
              28,
              41,
              48
            ],
            "cardType": 9,
            "resultType": -1,
            "bonusTimes": 1
          },
          {
            "placeBetType": 1,
            "cardList": [
              6,
              11,
              26,
              44,
              49
            ],
            "cardType": 9,
            "resultType": 1,
            "bonusTimes": 1
          },
          {
            "placeBetType": 2,
            "cardList": [
              5,
              17,
              24,
              35,
              50
            ],
            "cardType": 9,
            "resultType": 0,
            "bonusTimes": 1
          },
          {
            "placeBetType": 3,
            "cardList": [
              10,
              23,
              45,
              46,
              51
            ],
            "cardType": 8,
            "resultType": 1,
            "bonusTimes": 1
          },
          {
            "placeBetType": 4,
            "cardList": [
              7,
              25,
              30,
              38,
              43
            ],
            "cardType": 9,
            "resultType": 0,
            "bonusTimes": 1
          }
        ],
        "placeBetTypeList": [
          1,
          3
        ],
        "timeout": 6,
        "roomId": 1
      }
    }
    moon.gameSocket.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  private testStartGameEffect() {
    this.game.table.effectLayer.playStartGame();
  }

  private testStopGameEffect() {
    this.game.table.effectLayer.playStopGame();
  }

  private testClear() {
    this.game.table.clear();
  }
}