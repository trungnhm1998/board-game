import { Card } from './../../../../scripts/core/Card';

import { TimerTask } from '../../../../scripts/core/TimerComponent';
import { moon, GlobalInfo } from '../../../../scripts/core/GlobalInfo';
import { RedBlackRoom } from './model/RedBlackRoom';
import { RedBlackPlayerInfo } from './model/RedBlackPlayerInfo';
import { RedBlackHistory } from './RedBlackHistory';
import { NodeUtils } from '../../../../scripts/core/NodeUtils';
import { COUNTDOWN_TYPE } from '../../../../scripts/core/Constant';
import { RedBlackService } from './service/RedBlackService';
import { Config } from '../../../../scripts/Config';

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackBetPot extends cc.Component {


  @property(cc.Node)
  betArea: cc.Node = null;

  @property(cc.Label)
  totalBet: cc.Label = null;

  @property(cc.Label)
  myBet: cc.Label = null;

  @property(cc.Node)
  chipSample: cc.Node = null;

  @property(cc.Node)
  betHighlight: cc.Node = null;


  
  chipAtlas: cc.SpriteAtlas = null;
  workingChips: cc.Node[] = [];
  chipPool: cc.NodePool;
  placeBetType = 1;
  betMoneyTotal = 0;
  


  start() {
    cc.log('bet bot start')
    this.chipPool = new cc.NodePool();
  }

  playHighlightAnim() {
    let delayTime = 0.3;
    let round = 3;
    this.betHighlight.active = true;
    this.node.runAction(cc.sequence(
      cc.callFunc(() => {
        this.betHighlight.opacity = 180;
      }),
      cc.delayTime(delayTime),
      cc.callFunc(() => {
        this.betHighlight.opacity = 0;
      }),
      cc.delayTime(delayTime),
    ).repeat(round))
  }

  onEnable() {
  }


  clear() {
    this.totalBet.string = '';
    //return chip
    this.clearChips();
  }

  getBetHighlight() {
    return this.betHighlight;
  }


  setPlaceBetType(type) {
    this.placeBetType = type;
  }

  setBetMoneyTotal(money) {
    this.betMoneyTotal = money;
    this.totalBet.string = this.betMoneyTotal.toString()
  }

  onPotClick() {
    
      let redBlackService: RedBlackService = moon.service.getCurrentService() as RedBlackService;
      // RedBlackService.setTempBet(this.bet, this.placeBetType);
       redBlackService.bet(this.placeBetType);
    
  }

  initChipMaterial(chipAtlas: cc.SpriteAtlas) {
    this.chipAtlas = chipAtlas;

  }
 

  letThrow(chipValues, startPos, isUseAnimation = true) {
    let i = 0;

    let desPositions = moon.random.nPointsFromRect(this.betArea.x, this.betArea.y, this.betArea.width, this.betArea.height,
      Object.keys(chipValues).length,
      5);


    for (let chip in chipValues) {
      let amount = chipValues[chip];
      if (amount > 0) {
        let chipNodes = this.getChips(+chip, amount);
        this.animateChipThrow(this.betArea, chipNodes, startPos, desPositions[i++], isUseAnimation);
      }
    }
  }

  clearChips() {
    cc.log('return all card');
    for (let node of this.workingChips) {
      node.stopAllActions();
      node.opacity = 255;
      node.active = false;
      this.chipPool.put(node);
    }
    this.workingChips = [];
  }

  animateChipThrow(betArea, nodes, startPos, destPos, isUseAnimation = true) {
    cc.log(startPos);
    let animTime = 0;
    if (isUseAnimation) {
      animTime = Config.chipFlyTime
    } 
    let radius = 200;
    for (let node of nodes) {
      betArea.addChild(node);
      let pos = moon.random.fromCenter(0, 0, radius);
      node.position = cc.v2(startPos.x, startPos.y);
      node.runAction(
        cc.moveTo(animTime, cc.v2(pos.x, pos.y)).easing(cc.easeCubicActionOut())
      )
    }
  }

  getChips(chip: number, amount: number) {
    let nodes = [];
    for (let i = 0; i < amount; i++) {
      nodes.push(this.getChip(chip));
    }

    return nodes;
  }

  getChip(chip: number) {
    let chipNode = this.chipPool.get();
    this.workingChips.push(chipNode);
    if (chipNode) {

    } else {
      chipNode = cc.instantiate(this.chipSample);
    }
    chipNode.active = true;
    let chipValue =  moon.string.toRoundString(chip,2);
    let chipSprite: cc.Sprite = chipNode.getComponent(cc.Sprite);
    chipSprite.spriteFrame = this.chipAtlas.getSpriteFrame('s' + chip);
    if (chipSprite.spriteFrame) {
      NodeUtils.hideByName(chipNode, 'chip_lbl');
    } else {
      chipSprite.spriteFrame = this.chipAtlas.getSpriteFrame('seb');
      NodeUtils.showByName(chipNode, 'chip_lbl');
      NodeUtils.setLabel(chipNode, 'chip_lbl', chipValue);
    }
    (<any>chipNode).value = chipValue;
    return chipNode;
  }

  throwChips(startPos, chipValues, isUseAnimation = true ) {
      this.letThrow(chipValues, startPos, isUseAnimation)
  }

  throwChipsFromAtoB(startPos,endPos, isUseAnimation = true ) {

  }
}
