import {ThreeFacePlayerInfo} from './model/ThreeFacePlayerInfo';
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {ThreeFacePlayer} from "./ThreeFacePlayer";
import {Config} from "../../../../scripts/Config";
import {moon} from '../../../../scripts/core/GlobalInfo';
import {THREE_FACE_AUDIO} from "./ThreeFaceConstants";


const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceCardLayer extends cc.Component {

  playerCards = {};

  setUpCards(playerInfoList: Array<ThreeFacePlayerInfo>) {
    this.node.removeAllChildren();
    this.playerCards = {};
    playerInfoList.forEach(playerInfo => {
      playerInfo.resultCard.cardList.forEach(cardId => {
        let cardNode = moon.cardPool.getCard(cardId);
        this.node.addChild(cardNode);
        cardNode.x = 0;
        cardNode.y = 0;
        cardNode.opacity = 0;
        if (this.playerCards[playerInfo.userId]) {
          this.playerCards[playerInfo.userId].push(cardNode);
        } else {
          this.playerCards[playerInfo.userId] = [];
          this.playerCards[playerInfo.userId].push(cardNode);
        }
      });
    });
  }

  dealCard(player: ThreeFacePlayer, delay = 0) {
    if (!player.playerInfo) return;
    let cards: Array<cc.Node> = this.playerCards[player.playerInfo.userId];
    let positions = player.getReceivedPositions();
    let delayFactor = 0.1;

    let i = 0;
    cards.forEach(card => {
      NodeUtils.swapParent(card, card.parent, player.chessContent);
      card.runAction(
        cc.sequence(
          cc.callFunc(() => {
            card.active = true;
          }),
          cc.delayTime(delayFactor * i),
          cc.callFunc(() => {
            moon.audio.playEffectInGame(THREE_FACE_AUDIO.CARD_FLY);
          }),
          cc.spawn(
            cc.fadeIn(0.2),
            cc.moveTo(Config.chessFlyTime, positions[i]).easing(cc.easeIn(1.0))
          )
        )
      );
      i++;
    });
    player.setCardNodes(cards);
  }
}