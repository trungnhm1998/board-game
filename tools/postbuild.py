# -*- coding: utf-8 -*-

import os
import shutil
import time
import sys
import glob
import fnmatch

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

myargs = getopts(sys.argv)


tempFile = "../assets/resources/project_temp.json"
destFile = "../assets/resources/project.json"
shutil.move(tempFile, destFile)
os.remove(tempFile + ".meta")