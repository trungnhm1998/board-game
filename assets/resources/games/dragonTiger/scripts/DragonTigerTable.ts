import { DragonTigerRoomOverview } from './DragonTigerRoomOverview';
import { DragonTigerMultiTableDialog } from './DragonTigerMultiTableDialog';
import { CountdownInfo } from './../../../../scripts/model/Room';
import { DragonTigerPlayer } from './DragonTigerPlayer';
import { DragonTigerEffect } from './DragonTigerEffect';
import { DragonTigerDealer } from './DragonTigerDealer';
import { DragonTigerHistory } from './DragonTigerHistory';
import { DragonTigerPot } from './DragonTigerPot';
import { TimerTask } from './../../../../scripts/core/TimerComponent';
import { DragonTigerService } from './service/DragonTigerService';
import { moon, GlobalInfo } from './../../../../scripts/core/GlobalInfo';
import { DragonTigerRoom } from './model/DragonTigerRoom';
import { DragonTigerBets } from './DragonTigerBets';
import { DragonTigerPlayerInfo } from './model/DragonTigerPlayerInfo';
import DragonTigerConstants from './DragonTigerConstants';
const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerTable extends cc.Component {
  /*
  * #### HEADER ####
  */
  @property(cc.Label)
  matchId: cc.Label = null;

  @property(cc.Label)
  betLimit: cc.Label = null;

  @property(cc.Label)
  cardRemains: cc.Label = null;

  @property(cc.Label)
  playerNum: cc.Label = null;

  @property(cc.Label)
  gameStatus: cc.Label = null;

  @property(cc.Label)
  clock: cc.Label = null;

  @property(cc.Label)
  chessRemain: cc.Label = null;

  @property(DragonTigerPot)
  potDragon: DragonTigerPot = null;

  @property(DragonTigerPot)
  potDraw: DragonTigerPot = null;

  @property(DragonTigerPot)
  potTiger: DragonTigerPot = null;

  @property(DragonTigerHistory)
  roomHistory: DragonTigerHistory = null;

  @property(DragonTigerDealer)
  dealer: DragonTigerDealer = null;

  @property(cc.Node)
  dropdown: cc.Node = null;

  /*
  * #### FOOTER ####
  */
  @property(DragonTigerBets)
  bets: DragonTigerBets = null;

  @property(DragonTigerEffect)
  effects: DragonTigerEffect = null;

  @property(DragonTigerPlayer)
  myPlayer: DragonTigerPlayer = null;

  @property([DragonTigerPlayer])
  topPlayers: DragonTigerPlayer[] = [];

  @property(DragonTigerMultiTableDialog)
  changeTableDialog: DragonTigerMultiTableDialog = null;

  /**
   * private
   */
  _countdownTask: TimerTask = null;
  //            dragon  tiger  draw
  _placePots = {};

  onLoad() {
    this.potDragon.setPlaceBetType(DragonTigerConstants.PLACE_TYPE.DRAGON);
    this.potTiger.setPlaceBetType(DragonTigerConstants.PLACE_TYPE.TIGER);
    this.potDraw.setPlaceBetType(DragonTigerConstants.PLACE_TYPE.DRAW);
    this.bets.setBetCallback((bet) => {
      this.onBetChange(bet);
    });
  
    this._placePots[DragonTigerConstants.PLACE_TYPE.DRAGON] = this.potDragon;
    this._placePots[DragonTigerConstants.PLACE_TYPE.TIGER] = this.potTiger;
    this._placePots[DragonTigerConstants.PLACE_TYPE.DRAW] = this.potDraw;
  }

  onEnable() {
  }

  onLanguageChange() {
    // super.onLanguageChange();
    // NodeUtils.setLocaleLabels(this.ui, {
    //   "match_lbl": ["matchID", {}],
    //   "room_lbl": ["room", {}],
    //   "limit_lbl": ["limit", {}],
    //   "switch_lbl": ["switch", {}],
    //   "bet_lbl": ["bet", {}],
    //   "quit_lbl": ["quit", {}],
    //   "help_lbl": ["help", {}],
    //   "playing_lbl": ["playing", {}]
    // });
    // this.switchTableDialog.onLanguageChange();
    this.effects.onLanguageChange();
  }

  /**
   * check return state cases here and update UI based on join the board on wich state
   */
  updateCurrentRoom() {
    let room = GlobalInfo.room as DragonTigerRoom;
    cc.log('room respnose', room, this._placePots);
    this.setupTable(room);
    this.setPlayer(room.playerInfo);
    const { SERVER_GAME_STATE } = DragonTigerConstants;
    switch (room.countdownInfo.type) {
      case SERVER_GAME_STATE.STATE_START_GAME:
        this.clearCards();
        this.effects.playStartMatch(room.matchOrder);
        break;
      case SERVER_GAME_STATE.STATE_DEAL_CARD:
        this.dealer.setCards(room.placeBetInfoList, this.potDragon, this.potTiger);
        break;
      case SERVER_GAME_STATE.STATE_BET:
        this.dealer.setCards(room.placeBetInfoList, this.potDragon, this.potTiger);
        for (let placeBetInfo of room.placeBetInfoList) {
          let pot: DragonTigerPot = this._placePots[placeBetInfo.type];
          this.bets.playPeopleBet(pot, placeBetInfo.betMoneyTotal, room.betList, 0);
        }
        break;
      case SERVER_GAME_STATE.STATE_OPEN_CARD:
        this.dealer.setCards(room.placeBetInfoList, this.potDragon, this.potTiger);
        for (let placeBetInfo of room.placeBetInfoList) {
          let pot: DragonTigerPot = this._placePots[placeBetInfo.type];
          this.bets.playPeopleBet(pot, placeBetInfo.betMoneyTotal, room.betList, 0);
        }
        break;
      case SERVER_GAME_STATE.STATE_RESULT:
        this.dealer.setCards(room.placeBetInfoList, this.potDragon, this.potTiger);
        for (let placeBetInfo of room.placeBetInfoList) {
          let pot: DragonTigerPot = this._placePots[placeBetInfo.type];
          this.bets.playPeopleBet(pot, placeBetInfo.betMoneyTotal, room.betList, 0);
        }
        break;
    }

    this.runCountdown(room.countdownInfo.timeout);
  }

  setRoom(room: DragonTigerRoom) {
  }

  updateStatus() {
  }

  /**
   * Update UI for table
   * @param room responded data from server and store in GlobalInfo.room
   */
  setupTable(room: DragonTigerRoom) {
    /**
     * setup header
     * setup footer
     *  setup bet list
     */
    const { PLACE_TYPE } = DragonTigerConstants;
    if (room.matchId != undefined) {
      this.matchId.string = '' + room.matchId;
    }
    
    this.betLimit.string = `${room.betList[0]}-${room.betList[room.betList.length - 1]}`;

    this.cardRemains.string = String(room.cardNum);
    this.playerNum.string = String(room.playerNum);

    const DTService = (moon.service.getCurrentService() as DragonTigerService);

    for (let placeBetInfo of room.placeBetInfoList) {
      let pot: DragonTigerPot;
      const { type, betMoneyTotal, resultType } = placeBetInfo;
      switch (type) {
        case PLACE_TYPE.DRAGON:
          pot = this.potDragon;
          break;
        case PLACE_TYPE.TIGER:
          pot = this.potTiger;
          break;
        case PLACE_TYPE.DRAW:
          pot = this.potDraw;
          break;
      }
      pot.setTotalMoney(betMoneyTotal);
      pot.setMyMoney(0);

      // for open card -> end game state
      // in other state resultType should be -1
      if (resultType != -1) {
        DTService.winPlaceType = type;
      }
    }

    this.bets.setBetList(room.betList);
    let status = DTService.getStatus(room.countdownInfo.type);
    cc.log('setupTable', room.countdownInfo, status);
    this.setStatus(status);
    this.roomHistory.setHistory(room.historyList);
  }

  /**
   * run count down and update UI for clock timer
   * @param timeout time to run count down from
   * @param onLastSecond callback on the last second
   */
  runCountdown(timeout, onLastSecond?) {
    this.clock.string = timeout;
    // clear task before run new one
    if (this._countdownTask)
      moon.timer.removeTask(this._countdownTask);
    this._countdownTask = moon.timer.runCountdown(timeout, (val) => {
      this.clock.string = val;
      // play audio count down on last 3 seconds
      if (val <= 3) {

      }

      if (val == 1 && onLastSecond) {
        onLastSecond();
      }
    })
  }

  setPlayer(playerInfo: DragonTigerPlayerInfo) {
    this.myPlayer.setPlayerInfo(playerInfo);
    for (let placeType of playerInfo.placeBetTypeList) {
      let pot: DragonTigerPot = this._placePots[placeType.type];
      pot.setMyMoney(placeType.betMoneyTotal);
    }
  }

  onBetChange(bet) {
    this.potDraw.setBetMoney(bet);
    this.potDragon.setBetMoney(bet);
    this.potTiger.setBetMoney(bet);
  }

  decreaseRemain() {
    let num = +this.chessRemain.string;
    num -= 1;
    this.chessRemain.string = '' + num;
  }

  setRemain(num) {
    this.chessRemain.string = num;
  }

  setStatus(text) {
    this.gameStatus.string = text;
  }

  setPlayerNum(count = "") {
    this.playerNum.string = count;
  }

  leaveGame() {
    moon.audio.playEffectInGame("btn_click");
    moon.gameSocket.leaveBoard();
    this.toggleDropdown();
  }

  toggleDropdown() {
    this.dropdown.active = !this.dropdown.active;
    moon.audio.playEffectInGame("btn_click");
  }

  showHelp() {
    moon.audio.playEffectInGame("btn_click");
    moon.dialog.showWaiting();
    moon.dialog.showHelp(GlobalInfo.room.gameId);
    this.toggleDropdown();
    moon.dialog.hideWaiting();
  }

  getPlacePots() {
    return this._placePots;
  }

  clear() {
    this.myPlayer.clear();
    for (let player of this.topPlayers) {
      player.clear();
    }
    this.potTiger.clear();
    this.potDragon.clear();
    this.potDraw.clear();
    this.bets.clear();
    this.roomHistory.clear();
    this.effects.hide();
    this.dealer.clear();
    // this.limit.string = ''; // remove?
    // this.roomId.string = ''; // remove?
    this.matchId.string = '';
    this.playerNum.string = '0';
    this.gameStatus.string = '';
    this.clearCards();
    this.clearChips();

    this.changeTableDialog.node.active = false;
  }

  clearChips() {
    this.bets.returnChips(this.potDragon);
    this.bets.returnChips(this.potTiger);
    this.bets.returnChips(this.potDraw);
  }

  clearPots() {
    this.potDragon.clear();
    this.potTiger.clear();
    this.potDraw.clear();
  }

  clearCards() {
    this.potDraw.clearCard();
    this.potDragon.clearCard();
    this.potTiger.clearCard();
  }

  showSetting() {
    moon.audio.playEffectInGame("btn_click");
    this.toggleDropdown();
    moon.dialog.showSettings();
  }

  showRecord() {
    moon.audio.playEffectInGame("btn_click");
    this.toggleDropdown();
    moon.gameSocket.getPlayHistory(GlobalInfo.room.gameId);
  }

  onButtonSwitchTablePress() {
    let fadeToOpacity = this.changeTableDialog.node.active ? 0 : 255;
    let fadeFromOpacity = fadeToOpacity == 255 ? 0 : 255;
    // if (this.showSwitchDialog) {
    //   // TODO: Wait for server
    //   // moon.gameSocket.getDragonTigerRoomList(true);
    // }
    this.changeTableDialog.node.active = true;
    this.changeTableDialog.node.opacity = fadeFromOpacity;
    moon.audio.playEffectInGame("btn_click");

    this.changeTableDialog.node.stopAllActions();
    this.changeTableDialog.node.runAction(
      cc.sequence(
        cc.fadeTo(0.3, fadeToOpacity),
        cc.callFunc(() => {
          moon.dialog.resizeMask(this.changeTableDialog.node);
          this.changeTableDialog.node.active = fadeToOpacity == 255 ? true : false;
        })
      )
    )
  }

  /**
   * will check for any children roomOverview node and update it label timestamp based on roomId
   * @param roomId for update the roomID only
   * @param val time value for 
   */
  updateRoomStatus(roomId, countdownInfo: CountdownInfo, val: number) {
    this.changeTableDialog.roomListNode.children.forEach(roomOverview => {
      let roomOverviewComponent = roomOverview.getComponent(DragonTigerRoomOverview);

      if (roomId == roomOverviewComponent._roomId) {
        roomOverviewComponent.updateRoomStatus(countdownInfo, val) ;
      }
    });
  }


}
