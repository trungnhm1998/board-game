import {Promise} from 'es6-promise';
import {HttpClient} from "./HttpClient";
import {moon} from "./GlobalInfo";

let emoticon = ':d :\') :-* /-heart /-strong :3 --b :b ;d :~ :> ;p :* ;o :(( :) :p :$ :-h :-(( x-) 8-) ;-d :q :( b-) ;? :| ;xx :--| ;g :o :z :l p-( :-bye :x |-) :wipe :! 8* :-dig :t &-( :-| :handclap >-| :-f :-l :-r /-showlove ;-x :-o ;-s ;8 ;! ;f :; :+ ;-a :-< :)) /-li /-beer';
let emoticonList = emoticon.split(' ');
let upperEmoticonList = emoticonList.map(item => item.toUpperCase());

export class CString {
  format(str: string, ...args: any[]) {
    var content = str;
    for (var i = 0; i < args.length; i++) {
      var replacement = '{' + i + '}';
      content = content.replace(replacement, args[i]);
    }
    return content;
  }

  formatCountdown(num: number) {
    num = Math.floor(num);
    if (num < 10) {
      return '0' + num;
    }
    return '' + num;
  }
  
  formatPlayerName(playerName, isMe = false) {
      let trimmedName = playerName.trim();
    if (isMe) {
      if (playerName.length > 7) {
        return trimmedName.slice(0, 6) + '...';
      } else {
        return trimmedName.slice();
      }
    } else {
      return '****' + trimmedName.slice(-3);
    }
  }

  formatMoney(num: number, isInt = false, decimal = 3) {
    let numstr = num + '';
    let x = numstr.split('.');
    let x1 = x[0];
    let x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    x2 = x2.slice(0, decimal);
    if (+x2 == 0) {
      x2 = '';
    }
    // if (x2.length == 2) {
    //   x2 += '0';
    // }
    // if (x2.length == 0) {
    //   x2 += '.00'
    // }
    return isInt ? x1 : x1 + x2;
  }

  filterNumber(string) {
    return string.split(/ /)[0].replace(/[^\d]/g, '');
  }

  parseMoney(numstr: string): number {
    if (numstr && numstr[0] == '.') {
      numstr = '0' + numstr;
    }
    return +numstr.replace(/,/g, '');
  }

  isNullOrEmpty(str): boolean {
    return (str == undefined || str == null || str == '');
  }

  zeroLeading(num: number, zeroCount: number) {
    let numLen = num.toString().length;
    let appendCount = zeroCount - numLen;
    if (appendCount > 0) {
      let zeros = new Array(appendCount + 1).join('0');
      return zeros + num.toString();
    }

    return num.toString();
  }

  loadedImage = {};

  imageURLToBase64(url): Promise<string> {
    return new Promise<string>((resolve) => {
      if (this.loadedImage[url]) {
        resolve(this.loadedImage[url]);
        return;
      }

      if (cc.sys.isNative) {
        moon.http.imageURLToBase64(url)
          .then((base64Str) => {
            let imageData = 'data:image/png;base64,' + base64Str;
            this.loadedImage[url] = imageData;
            resolve(imageData);
          })
      } else {
        let img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = () => {
          let canvas: any = document.createElement('CANVAS');
          let ctx = canvas.getContext('2d');
          let dataURL;
          canvas.height = (<any>this).naturalHeight;
          canvas.width = (<any>this).naturalWidth;
          ctx.drawImage(this, 0, 0);
          dataURL = canvas.toDataURL('image/png');
          this.loadedImage[url] = dataURL;
          resolve(dataURL);
        };
        img.src = url;
      }
    });
  }

  limitText(text, maxLength = 20) {
    if (!text) return '';

    if (text.length > maxLength) {
      return text.substr(0, maxLength) + '...';
    }

    return text;
  }

  round(number, decimals) {
    decimals = decimals || 0;
    return ( Math.floor(parseInt((number * Math.pow(10, decimals)).toFixed(decimals))) / Math.pow(10, decimals) );
  }

  toRoundString(number, decimals) {
    let numStr = number.toFixed(decimals);
    let [integer, decimal] = numStr.split('.');
    if (decimal) {
      let ret = [];
      decimal = decimal.slice(0, decimals);
      for (let i = decimal.length - 1; i >= 0; i--) {
        if (decimal[i] != '0') {
          ret.unshift(decimal[i]);
        }
      }
      decimal = ret.join('');
    }

    return (decimal != undefined && decimal.length > 0) ? `${integer}.${decimal}` : integer;
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  formatBigMoney(betMoney: number) {
    let bNum = this.round(betMoney / 1000000000, 2);
    let mNum = this.round(betMoney / 1000000, 2);
    if (bNum >= 1) {
      return '' + bNum + 'B';
    } else if (mNum >= 1) {
      return '' + mNum + 'M';
    } else {
      return this.formatMoney(betMoney, true);
    }
  }

  pasteTest(): Promise<string> {
    if (cc.sys.isNative) {
      return new Promise<string>(resolve => {
        resolve(moon.native.pasteText());
      });
    } else {
      return this.pasteTextOnWeb();
    }
  }

  pasteTextOnWeb() {
    if ((<any>window).navigator && (<any>window).navigator.clipboard) {
      return (<any>window).navigator.clipboard.readText().then(
        clipText => {
          return clipText;
        }
      );
    }
  }

  copyText(text) {
    if (cc.sys.isNative) {
      moon.native.copyText(text)
    } else {
      this.copyTextOnWeb(text);
    }
  }

  copyTextOnWeb(text) {
    let doc = parent.document || window.document;
    let win = parent.window || window;
    let newDiv = doc.createElement('input');
    // newDiv.style.userSelect = 'text';
    newDiv.setAttribute('readonly', 'true');
    newDiv.setAttribute('contenteditable', 'false');
    newDiv.setAttribute('value', text);
    doc.body.appendChild(newDiv);
    newDiv.select();

    let range = document.createRange();
    range.selectNodeContents(newDiv);
    let selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    newDiv.setSelectionRange(0, 999999);

    try {
      // Now that we've selected the anchor text, execute the copy command
      let successful = doc.execCommand('copy');
      let msg = successful ? 'successful' : 'unsuccessful';
      // console.log(msg);
    } catch (err) {
      // console.error(err);
    }

    // Remove the selections - NOTE: Should use
    // removeRange(range) when it is supported
    doc.body.removeChild(newDiv);
    win.getSelection().removeAllRanges();
  }

  parseEmoticon(text) {
    let emoList = [].concat(emoticonList);
    emoList.sort((a, b) => b.length - a.length);
    for (let i = 0; i < emoList.length; i++) {
      let emoticon = emoList[i];
      if (text.indexOf(emoticon) > -1) {
        let index = emoticonList.indexOf(emoticon);
        let emotext = `<img src='emoticon_${index + 1}' />`;
        text = text.split(emoticon).join(emotext);
      }

      let upperEmoticon = emoticon.toUpperCase();
      if (text.indexOf(upperEmoticon > -1)) {
        let upperIndex = upperEmoticonList.indexOf(upperEmoticon);
        let upperEmotext = `<img src='emoticon_${upperIndex + 1}' />`;
        text = text.split(upperEmoticon).join(upperEmotext);
      }
    }

    return text;
  }

  getEmoticonList() {
    return emoticonList;
  }

  hideEmail(email) {
    if (!email) {
      return email;
    }

    let [name, domain] = email.split('@');
    let first = name[0];
    let last = name[name.length - 1];
    if (name.length == 1) {
      last = '';
    }
    return `${first}****${last}@${domain}`;
  }

  hasSpecialChars(string: string) {
    let re = /(\!|\#|\$|\%|\^|\&|\*|\(|\)|\=|\+|\[|\{|\]|\}|\;|\:|\'|\"|\\|\||\,|\<|\>|\/|\?|\}|\))/g;
    return re.test(string);
  }

  formatDate(time) {
    let nowDate = new Date(time);
    let strMonth = nowDate.getMonth() < 10 ? ('0' + (nowDate.getMonth() + 1)) : (nowDate.getMonth() + 1);
    let strDate = nowDate.getDate() < 10 ? ('0' + nowDate.getDate()) : (nowDate.getDate());

    return (nowDate.getFullYear() + '-' + strMonth + '-' + strDate + "\n" + ((nowDate.getHours() < 10) ? ("0" + nowDate.getHours()) : (nowDate.getHours())) + ':' + ((nowDate.getMinutes() < 10) ? ("0" + nowDate.getMinutes()) : (nowDate.getMinutes())) + ':' + ((nowDate.getSeconds() < 10) ? ("0" + nowDate.getSeconds()) : (nowDate.getSeconds())));
  }

  getBaseName(path) {
    return path.match(/\w+(?:\.\w+)*$/)[0] || '';
  }
}