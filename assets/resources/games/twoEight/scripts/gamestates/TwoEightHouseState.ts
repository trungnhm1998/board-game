import {GameState, IGameState, UpdateData} from '../../../../../scripts/common/GameState';
import {TwoEightGame} from '../TwoEightGame';
import {GlobalInfo, moon} from '../../../../../scripts/core/GlobalInfo';
import {TwoEightRoom} from '../model/TwoEightRoom';
import {NodeUtils} from '../../../../../scripts/core/NodeUtils';
import {TWO_EIGHT_AUDIO} from '../../../../../scripts/core/Constant';
import {TwoEightRoomService} from '../service/TwoEightRoomService';

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightHouseState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  noDealer: cc.Node = null;

  @property(cc.Node)
  betDealerSample: cc.Node = null;

  @property(cc.Node)
  buttons: cc.Node = null;

  @property(cc.Node)
  childView: cc.Node = null;

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  countdownTask;

  onEnter(game: TwoEightGame, data: any) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;

    game.allPlayerThinking();
    let me = game.getPlayerByUserId(GlobalInfo.me.userId);
    cc.log('Housestate playerinfo', me)
    
    if (!me.playerInfo.snatch || me.playerInfo.snatch == 0) {
      this.showBetList()
    }

    for (let player of game.players) {
      if (player.playerInfo.snatch > 0) {
        player.setSnatchValue(player.playerInfo.snatch)
      } 
      if (player.playerInfo.snatch == -1) {
        player.setSnatchValue(-1)
      }
    }

    this.countdown.node.parent.active = false;
    this.startAnim.node.active = true;
    this.startAnim.timeScale = 1.5;
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.SNATCH_START, true);

    let lang = moon.locale.getCurrentLanguage();
    this.startAnim.armatureName = lang;
    this.startAnim.playAnimation(lang, 1);

    const playCountDownSound = () => this.playCountDownSound(game);
    setTimeout(() => {
      this.startAnim.node.active = false;
      this.countdown.node.parent.active = true;
      this.runCountdown(room.countdownInfo.timeout, playCountDownSound);
    }, 750);

  }

  showBetList() {
    this.childView.active = true;
    this.buttons.removeAllChildren();
    let betList = TwoEightRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId).betList;


    for (let betValue of betList) {
      if (betValue == -1) {
        this.noDealer.removeFromParent();
        this.buttons.addChild(this.noDealer);
        this.noDealer.active = true;
        this.noDealer.position = cc.v2();
      } else {
        let betBtnNode = cc.instantiate(this.betDealerSample);
        betBtnNode.active = true;
        let betUnit = moon.locale.get('bets');
        let valueNode = NodeUtils.findByName(betBtnNode, 'betValue');
        let valueLabel = valueNode.getComponent(cc.Label);
        valueLabel.string = '' + betValue + ' ' + betUnit;
        let betBtn: cc.Button = betBtnNode.getComponent(cc.Button);
        let clickEvent = betBtn.clickEvents[0];
        clickEvent.customEventData = betValue.toString();
        this.buttons.addChild(betBtnNode);
        betBtnNode.position = cc.v2();
      }
    }
  }

  playCountDownSound(game: TwoEightGame) {
    moon.audio.playEffectInGame(TWO_EIGHT_AUDIO.COUNTDOWN, false);
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {

  }

  onLeave(game: TwoEightGame, data: any) {
    this.noDealer.removeFromParent();
    this.node.addChild(this.noDealer);
    this.noDealer.active = false;

    this.countdown.node.parent.active = false;

    game.hidePlayerSnatchValue();

  }

  runCountdown(max, playSound) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = moon.timer.scheduleOnce(() => {
      playSound();
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num, playSound);
      } else {
        this.node.active = false;
        // moon.gameSocket.twoEightBetDealer(0);
      }
    }, 1);
  }

  stopCountdown() {
    moon.timer.removeTask(this.countdownTask);
  }

  onBetDealer(evt, value) {
    moon.gameSocket.twoEightBetDealer(+value);
  }

  onBetEnd() {
    this.childView.active = false;
  }
}