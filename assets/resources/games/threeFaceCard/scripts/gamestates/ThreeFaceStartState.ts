import {ThreeFaceRoom} from "../model/ThreeFaceRoom";
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {ThreeFaceGame} from "../ThreeFaceGame";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import {RESOURCE_BLOCK} from "../../../../../scripts/core/Constant";
import {THREE_FACE_AUDIO} from "../ThreeFaceConstants";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceStartState extends GameState implements IGameState {

  game: ThreeFaceGame;

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: ThreeFaceGame, data: any) {
    this.game = game;
    game.clearPlayersUI();
    game.betLayer.clear();
    game.hideBetInfo();
    game.clearPlayerBetFactor();
    moon.cardPool.returnAllCard();
    game.setRoom(<ThreeFaceRoom>GlobalInfo.room);
    moon.audio.playEffectInGame(THREE_FACE_AUDIO.GAMESTART, false);
    this.startAnim.node.active = true;
    this.startAnim.dragonAsset = moon.res.getSkelecton('gamestart_ske', RESOURCE_BLOCK.GAME);
    this.startAnim.dragonAtlasAsset = moon.res.getSpriteFrame('gamestart_tex', RESOURCE_BLOCK.GAME);
    this.startAnim.armatureName = 'win';
    this.startAnim.playAnimation('win', 1);
    this.startAnim.removeEventListener('complete', this.onEndAnim, this);
    this.startAnim.addEventListener('complete', this.onEndAnim, this);
  }

  onUpdate(game: ThreeFaceGame, updateData: UpdateData) {
  }

  onLeave(game: ThreeFaceGame, data: any) {
    this.startAnim.removeEventListener('complete', this.onEndAnim, this);
    this.startAnim.node.active = false;
  }

  onEndAnim() {
    cc.log('end start anim complete');
    this.startAnim.node.active = false;
    let room: ThreeFaceRoom = GlobalInfo.room as ThreeFaceRoom;
    this.game.cardLayer.setUpCards(room.playerInfoList);
    for (let player of this.game.players) {
      if (player.node.active) {
        this.game.cardLayer.dealCard(player);
      }
    }
  }
}