import { Config } from './../../../../scripts/Config';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
import { Random } from './../../../../scripts/core/Random';
import { moon } from './../../../../scripts/core/GlobalInfo';
import { FourSeasonsPot } from './FourSeasonsPot';
import { DECK_ID } from '../../../../scripts/core/Constant';
const {ccclass, property} = cc._decorator;

const CARD_SIZE = 52;

// for padding between card, the smaller it goes the more padding it have
const CARD_PADDING_FACTOR = 2.2;

/**
 * FourSeaonsDealer
 * @description uses for play shuffle cards, deal cards, clear cards on table
 */
@ccclass
export class FourSeasonsDealer extends cc.Component {

  _shufflingSpawnActions = [];

  onLoad() {
  }

  onEnable() {
    // init _cardList for "dealing" and "shuffling" later
    // presentaion purpose
    this.initDeck();
  }

  initDeck() {
    moon.cardPool.setCardDeck(DECK_ID.SIMPLIFIED_1);
    moon.cardPool.setCardScale(0.5);

    this.node.cleanup();
    while (this.node.children.length > 0) {
      let card = this.node.children.pop();
      card.stopAllActions();
      card.cleanup();
      moon.cardPool.putCards(card);
    }

    for (let i = 0; i < CARD_SIZE; i++) {
      let cardNode = moon.cardPool.getCard();
      cardNode.zIndex = i;
      cardNode.y -= (i * 0.2);
      this.node.addChild(cardNode);
    }
  }

  clear() {

  }
  
  /**
   * @description uses for play shuffle effect with random rectangle position
   * play some action to move card from here and there within random positions
   */
  shuffleCards() {
    this.node.children.forEach(card => {
      const { width, height } = this.node;
      const originalZIndex = card.zIndex;
      const originalPos = card.position;

      const shuffleAction = cc.sequence(
        cc.moveTo(0.25, new Random().fromRect(0, 0, width, height).x, new Random().fromRect(0, 0, width, height).y ).easing(cc.easeOut(3.0)),
        cc.spawn(
          cc.callFunc(() => {
            card.zIndex = Math.round(Math.random() * CARD_SIZE);
          }),
          cc.moveTo(0.25, originalPos)
        ),
        cc.moveTo(0.25, new Random().fromRect(0, 0, width, height).x, new Random().fromRect(0, 0, width, height).y ).easing(cc.easeOut(3.0)),
        cc.spawn(
          cc.callFunc(() => {
            card.zIndex = originalZIndex;
          }),
          cc.moveTo(0.25, originalPos)
        )
      ).repeat(2);

      this._shufflingSpawnActions.push(cc.callFunc(() => {
        card.runAction(shuffleAction);
      }));
    });

  }

  /**
   * @param {FourSeasonsPot} betArea get cardArea from betArea to deal card to
   */
  dealCards(betArea: FourSeasonsPot) {
    let cardCount = 0;
    while (cardCount < 5) {
      let card = this.node.children[this.node.children.length - 1];
      NodeUtils.swapParent(card, this.node, betArea.cardArea);
      card.zIndex = cardCount;
      const startedPos = (((card.width / CARD_PADDING_FACTOR) * 5) / 2) * -1;
      let moveToPos = cc.v2(startedPos + (cardCount * card.width / CARD_PADDING_FACTOR), 0);
      card.runAction(
        cc.sequence(
          cc.delayTime(0.09 * cardCount),
          cc.moveTo(Config.chessFlyTime, moveToPos),
        )
      );
      cardCount++
    }
  }

  /**
   * for SetCards without animation, returnGame purpose
   * @param betArea 
   */
  setCards(betArea: FourSeasonsPot) {
    let cardCount = 0;
    while (cardCount < 5) {
      let card = this.node.children[this.node.children.length - 1];
      NodeUtils.swapParent(card, this.node, betArea.cardArea);
      card.zIndex = cardCount;
      const startedPos = (((card.width / CARD_PADDING_FACTOR) * 5) / 2) * -1;
      let moveToPos = cc.v2(startedPos + (cardCount * card.width / CARD_PADDING_FACTOR), 0);
      card.setPosition(moveToPos);
      cardCount++
    }
  }
}