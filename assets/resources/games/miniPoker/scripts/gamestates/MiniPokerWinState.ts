import {MiniPokerRoom} from "../model/MiniPokerRoom";
import {GlobalInfo} from "../../../../../scripts/core/GlobalInfo";
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {MiniPokerGame} from "../MiniPokerGame";

const {ccclass, property} = cc._decorator;

@ccclass
export class MiniPokerWinState extends GameState implements IGameState {


  onEnter(game: MiniPokerGame, data: any) {
    // game.clear();
    game.setRoom(<MiniPokerRoom>GlobalInfo.room);
    let room: MiniPokerRoom = <MiniPokerRoom>GlobalInfo.room;

    // start at current frames


  }

  onUpdate(game: MiniPokerGame, updateData: UpdateData) {
  }

  onLeave(game: MiniPokerGame, data: any) {
  }
}