import os
import datetime
import shutil
import sys

now = datetime.datetime.now()

environment = sys.argv[1]
deployFolder = sys.argv[2]
filename = "z88-mahjong-%s-%s_%s_%s.apk" % (environment, now.day, now.month, now.year)

shutil.move('../build/jsb-link/frameworks/runtime-src/proj.android-studio/app/build/outputs/apk/release/moon-release.apk', '../build/jsb-link/frameworks/runtime-src/proj.android-studio/app/build/outputs/apk/release/%s' % filename)

if environment == "dev":
    shutil.copyfile('../build/jsb-link/frameworks/runtime-src/proj.android-studio/app/build/outputs/apk/release/%s' % filename, os.path.join(deployFolder, '%s' % filename))
    shutil.copyfile('../build/jsb-link/frameworks/runtime-src/proj.android-studio/app/build/outputs/apk/release/%s' % filename, os.path.join(deployFolder, '%s' % 'z88-mahjong.apk'))
else:
    shutil.copyfile('../build/jsb-link/frameworks/runtime-src/proj.android-studio/app/build/outputs/apk/release/%s' % filename, os.path.join(deployFolder, '%s' % 'mahjong.apk'))


