import plistlib
import sys

if len(sys.argv) > 1:
	plistFile = sys.argv[1]
else:
	plistFile = "twoEight.plist"

xmlFile = plistFile.replace('plist', 'xml')
xmlData ="""<TextureAtlas imagePath="{xmlFile}">{rows}
</TextureAtlas>
"""
xmlRow = '''<SubTexture name="{fileName}" x="{x}" y="{y}" width="{w}" height="{h}"/>'''

with open(plistFile, 'rb') as fp:
    pl = plistlib.readPlist(fp)

rows = ""

for fileName in pl["frames"]:
	frameData = pl["frames"][fileName]["frame"]
	data = eval(frameData.replace("{", "[").replace("}", "]"))
	[[x, y], [w, h]] = data
	row = xmlRow.format(fileName=fileName, x=x, y=y, w=w, h=h)
	rows += "\n" + "\t" + row 

outputData = xmlData.format(rows=rows, xmlFile=xmlFile)

with open(xmlFile, 'w') as fp:
    fp.write(outputData)


	
	