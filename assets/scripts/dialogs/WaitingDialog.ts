import {SceneComponent} from "../common/SceneComponent";

const {ccclass, property} = cc._decorator;


@ccclass
export class WaitingDialog extends SceneComponent {

  @property(cc.Label)
  progress: cc.Label = null;

  @property(cc.Label)
  text: cc.Label = null;

  @property(cc.Node)
  loading: cc.Node = null;

  private onTimeout: any;
  private timeout: any;

  onEnter() {
    super.onEnter();
    this.progress.node.active = false;
    this.text.node.active = false;
    this.onTimeout = null;
    this.loading.stopAllActions();
    this.loading.runAction(
      cc.rotateBy(3, 720).repeatForever()
    );
    if (this.timeout) {
      this.node.stopAllActions();
      this.node.runAction(
        cc.sequence(
          cc.delayTime(this.timeout),
          cc.callFunc(() => {
            this.closeDialog();
            if (this.onTimeout) {
              this.onTimeout();
            }
          })
        )
      )
    }
  }

  setTimeoutCallback(onTimeout: any, timeout: any) {
    this.onTimeout = onTimeout;
    this.timeout = timeout;
  }

  setProgress(progress) {
    this.progress.node.active = true;
    this.progress.string = '' + Math.floor(progress) + '%';
  }

  setText(text) {
    this.text.node.active = true;
    this.text.string = text;
  }

  closeDialog() {
    this.timeout = null;
    this.onTimeout = null;
    super.closeDialog();
  }
}