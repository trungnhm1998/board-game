import { RED_BLACK_TYPE_NAME } from './../../../../scripts/core/Constant';
import { RoomInfo } from "../../../../scripts/model/Room";
import { NodeUtils } from "../../../../scripts/core/NodeUtils";
import { moon } from "../../../../scripts/core/GlobalInfo";
import { GAME_ID } from "../../../../scripts/core/Constant";

const { ccclass, property } = cc._decorator;

@ccclass
export class RedBlackHistory extends cc.Component {
  @property(cc.SpriteAtlas)
  redBlackAtlas: cc.SpriteAtlas = null;

  @property(cc.Node)
  colorHistList: cc.Node = null;

  @property(cc.Node)
  typeHistList: cc.Node = null;

  @property(cc.Node)
  colorHistEx: cc.Node = null;

  @property(cc.Node)
  typeHistEx: cc.Node = null;

  renderColorHistory(histList) {
    for (let i = 0; i < histList.length; i++) {
      let histNode = cc.instantiate(this.colorHistEx);

      histNode.active = true;
      if (histList[i] === 0) {
        histNode.getComponent(
          cc.Sprite
        ).spriteFrame = this.redBlackAtlas.getSpriteFrame("hh_blackpoint");
      } else {
        histNode.getComponent(
          cc.Sprite
        ).spriteFrame = this.redBlackAtlas.getSpriteFrame("hh_redpoint");
      }
      this.colorHistList.addChild(histNode);
    }
  }

  renderTypeHistory(histList) {
    for (let i = 0; i < histList.length; i++) {
      let histNode = cc.instantiate(this.typeHistEx);
      let res = RED_BLACK_TYPE_NAME[histList[i]];
      histNode.active = true;
      
      histNode.getChildByName('type').getComponent(cc.Label).string = res
      this.typeHistList.addChild(histNode);
    }
  }
}
