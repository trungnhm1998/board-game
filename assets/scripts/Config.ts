// System uses "second" based time

import {ModelUtils} from './core/ModelUtils';
import {moon} from './core/GlobalInfo';

export let Config = {
  chipFlyTime: 0.4,
  chessFlyTime: 0.4,
  chessDownTime: 0.4,
  chessSortTime: 0.4,
  longPressTime: 0.5,
  winResultTime: 2.3,
  buyInEndGameTime: 1,
  defaultLanguage: 'en',
  languageOptions: [
    {name: 'en', value: 'English'},
    {name: 'zh', value: '中文'}
  ],
  gameInfo: [
    {
      gameId: 1,
      packageName: 'twoEight',
      prefabPath: 'games/twoEight/twoEightGame',
      localeFolder: 'games/twoEight/locale',
      hallAsset: 'games/twoEight/hall',
      hallEffect: 'games/twoEight/hall/effect',
      service: 'TwoEightService',
      audioPath: 'resources/games/twoEight/game/sound',
      deckPath: 'card/decks/1'
    },
    {
      gameId: 2,
      packageName: 'dragonTiger',
      prefabPath: 'games/dragonTiger/dragonTigerGame',
      localeFolder: 'games/dragonTiger/locale',
      service: 'DragonTigerService',
      audioPath: 'resources/games/dragonTiger/sound',
      deckPath: 'card/decks/1'
    },
    {
      gameId: 3,
      packageName: 'miniPoker',
      prefabPath: 'games/miniPoker/miniPokerGame',
      localeFolder: 'games/miniPoker/locale',
      service: 'MiniPokerService',
      audioPath: 'resources/games/miniPoker/game/sound',
      deckPath: 'card/decks/2',
      hallAsset: 'games/miniPoker/hall',
      hallEffect: 'games/miniPoker/hall/effect',
      cardTypeGuide: [{row: [39, 43, 47, 51, 3], text: 'straightFlush', bonus: 5},  
        {row: [0, 1, 2, 3, 51], text: 'fourOfAKind', bonus: 4},
        {row: [0, 1, 2, 50, 51], text: 'fullHouse', bonus: 3},
        {row: [35, 43, 47, 51, 3], text: 'flush', bonus: 2},
        {row: [36, 40, 45, 49, 2], text: 'straight', bonus: 2},
        {row: [0, 1, 2, 50, 44], text: 'threeOfAKind', bonus: 2},
        {row: [0, 1, 51, 50, 44], text: 'twoPairs', bonus: 1},
        {row: [0, 1, 51, 44, 41], text: 'onePair', bonus: 1},
        {row: [34, 43, 47, 51, 3], text: 'highCard', bonus: 1},
      ]
    },
    {
      gameId: 5,
      packageName: 'fourSeasons',
      prefabPath: 'games/fourSeasons/fourSeasonsGame',
      localeFolder: 'games/fourSeasons/locale',
      service: 'FourSeasonsService',
      audioPath: 'resources/games/fourSeasons/sound',
      deckPath: 'card/decks/2'
    },
    {
      gameId: 6,
      packageName: 'threeFaceCard',
      prefabPath: 'games/threeFaceCard/threeFaceCard',
      localeFolder: 'games/threeFaceCard/locale',
      service: 'ThreeFaceService',
      audioPath: 'resources/games/threeFaceCard/sound',
      deckPath: 'card/decks/2',
    },
    {
      gameId: 4,
      packageName: 'redBlack',
      prefabPath: 'games/redBlack/redBlackGame',
      localeFolder: 'games/redBlack/locale',
      service: 'RedBlackService',
      audioPath: 'resources/games/redBlack/sound',
      deckPath: 'card/decks/2',
    }
  ],

  curServer: 'wss://lobby.devmoo.club',
  gameSocket: 'wss://game.devmoo.club',
  host: 'localhost:7456',

  minPassLength: 6,
  minNameLength: 4,
  maxNameLength: 12,
  historyRowsPerFetch: 10,
  maxChatLength: 30,

  // API config
  fbId: '477996005883390',
  gaId:
    '416659385124-n5eih30gj26fqa3dbc6jj8sdm041cm8o.apps.googleusercontent.com',
  channel: 'z88',
  version: '1.0.1',
  env: 'dev',

  // Eknut config
  paymentApiUrl: 'http://payment-sandbox.z88.net',
  eknutConnectionId: '59faf36b4f1d66d20a8b4568',
  eknutSecretKey: 'IUYTSHCROECUOIUOWOUWAOCNAI',

  // Insight config
  insightApiUrl: 'http://data-sandbox.z88.net',
  insightConnectionId: '59ccca77e77348b4078b4569',
  insightSecretKey: '2GVWJ72TFVQQ830I7GJ6YHTRAB',

  supportUrl: 'http://casino.z88.net/contact',
  termUrl: 'https://devmoo.club/terms-of-service.htm',
  privacyUrl: 'https://devmoo.club/privacy-policy.htm',
  lite: false,
  IM: false
};

const firebaseConfigUrl = {
  web_dev: 'https://moon-1765d.firebaseio.com/Configs/Web/Dev.json',
  web_production: 'https://moon-1765d.firebaseio.com/Configs/Web/Production.json',
  android_dev: 'https://moon-1765d.firebaseio.com/Configs/Android/Dev.json',
  android_production: 'https:/moon-1765d.firebaseio.com/Configs/Android/Production.json',
  ios_dev: 'https://moon-1765d.firebaseio.com/Configs/Ios/Dev.json',
  ios_production: 'https://moon-1765d.firebaseio.com/Configs/Ios/Production.json',
  localhost: 'https://moon-1765d.firebaseio.com/Configs/Web/Localhost.json',
  tungMTP: 'https://moon-1765d.firebaseio.com/Configs/Web/TungMTP.json'
};

function getConfig(env_type) {
  const url = firebaseConfigUrl[env_type];
  return moon.http.get(url, {});
}

export function loadProjectConfig(): Promise<any> {
  cc.log('load config');
  return new Promise((resolve, reject) => {
    cc.loader.loadRes('project', (err, resource: any) => {
      if (resource && resource.json) {
        getConfig(resource.json.env).then(data => {
          if (data) {
            cc.log(resource.json.env, data);
            ModelUtils.merge(Config, data);
          }
          resolve();
        });
      }
    });
  });
}
