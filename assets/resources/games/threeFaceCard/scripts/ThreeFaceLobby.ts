import {RoomInfo} from "../../../../scripts/model/Room";
import {NodeUtils} from "../../../../scripts/core/NodeUtils";
import {GlobalInfo, moon} from "../../../../scripts/core/GlobalInfo";
import {GAME_ID} from "../../../../scripts/core/Constant";
import {GrayShader} from "../../../../shader/GrayShader";
import {NormalShader} from "../../../../shader/NormalShader";

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceLobby extends cc.Component {

  @property([cc.Node])
  rooms: cc.Node[] = [];

  @property(cc.Node)
  roomList: cc.Node = null;

  @property(cc.Node)
  bgSke: cc.Node = null;


  inDelayRoomClick = false;

  updateRoomInfoList(roomInfoList: Array<RoomInfo>) {
    let i = 0;
    for (let roomInfo of roomInfoList) {
      let room = this.rooms[i++];
      cc.log('set room info', i, roomInfo);
      NodeUtils.setLocaleLabels(room, {
        'bet': ['ante:', {params: [roomInfo.bet]}],
        'minBet': ['allowed:', {params: [roomInfo.minBet]}],
      });
      let bgNode = NodeUtils.findByName(room, 'bg');
      let betNode = NodeUtils.findByName(room, 'bet');
      let bgSprite: cc.Sprite = bgNode.getComponent(cc.Sprite);
      if (roomInfo.minBet > GlobalInfo.me.money) {
        NodeUtils.applyShader(bgSprite, GrayShader);
        betNode.color = cc.color().fromHEX('#d4d4d4');
      } else {
        NodeUtils.applyShader(bgSprite, NormalShader);
        betNode.color = cc.color().fromHEX('#ffffff');
      }

      let roomBtn = room.getComponent(cc.Button) as cc.Button;
      roomBtn.clickEvents[0].customEventData = roomInfo.bet;
      NodeUtils.setGameSprite(room, 'bg', 'room_' + i);
    }

    for (i; i <= this.rooms.length - 1; i++) {
      this.rooms[i].active = false;
    }
  }

  onResize() {
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);

    this.roomList.scale = uiRatio * 0.8;
  }

  onRoomClick(evt, value) {
    if (!this.inDelayRoomClick) {
      this.inDelayRoomClick = true;
      let numValue = parseInt(value);
      cc.log('choose room', numValue);
      moon.dialog.showWaiting();
      moon.gameSocket.playThreeFace(GAME_ID.THREE_CARD, numValue);
      moon.timer.scheduleOnce(() => {
        this.inDelayRoomClick = false;
      }, 1);
    }
  }
}