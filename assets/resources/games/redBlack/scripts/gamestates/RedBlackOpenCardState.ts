import { Card } from './../../../../../scripts/core/Card';
import { RED_BLACK_TYPE_NAME } from '../../../../../scripts/core/Constant';
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";
import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { RedBlackGame } from "../RedBlackGame";
import { RedBlackRoom } from "../model/RedBlackRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackOpenCardState extends GameState implements IGameState {


  @property(cc.Node)
  redSide: cc.Node = null;

  @property(cc.Node)
  blackSide: cc.Node = null;

  game:  RedBlackGame;

  

  onEnter(game: RedBlackGame, data: any) {
    this.game = game;
    let redCard = this.game.table.redCardNodeList;
    let blackCard = this.game.table.blackCardNodeList;
    let Queen = this.redSide.getChildByName('mascot').getComponent(dragonBones.ArmatureDisplay)
    Queen.playAnimation('action0',-1)
    let redType = RED_BLACK_TYPE_NAME[this.game.table.redCardType]
    let blackType = RED_BLACK_TYPE_NAME[this.game.table.blackCardType]
    this.redSide.getChildByName('cardType').getComponent(cc.Label).string = redType
    this.blackSide.getChildByName('cardType').getComponent(cc.Label).string = blackType

    

    console.log(redCard,blackCard)
    this.node.runAction(cc.sequence(
      cc.delayTime(0.2),
      cc.callFunc(() => {
        redCard[0].getComponent(Card).showCard();
        redCard[1].getComponent(Card).showCard();
      }),
      cc.delayTime(1),
      cc.callFunc(() => {
        blackCard[0].getComponent(Card).showCard();
        blackCard[1].getComponent(Card).showCard();
      }),
      cc.delayTime(0.5),
      cc.callFunc(() => {
        redCard[2].getComponent(Card).showCard();
        this.redSide.getChildByName('cardType').active = true;
      }),
      cc.delayTime(1),
      cc.callFunc(() => {
        blackCard[2].getComponent(Card).showCard();
        this.blackSide.getChildByName('cardType').active = true;
      })
    ))
  }

  onUpdate(game: RedBlackGame, updateData: UpdateData) {
  
  }

  onLeave(game: RedBlackGame, data: any) {
  }
}