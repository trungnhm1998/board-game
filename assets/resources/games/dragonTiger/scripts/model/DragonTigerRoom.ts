import { PLACE_TYPE } from './../DragonTigerConstants';
import {CountdownInfo, Room} from "../../../../../scripts/model/Room";
import {DragonTigerPlayerInfo} from "./DragonTigerPlayerInfo";

export class DragonTigerBetInfo {
  type: number;
  rate: number;
  betMoneyTotal: number;
  resultType: number;
  cardList: number[];
}

export class DragonTigerRoom extends Room {
  roomName: any;
  playerInfo: DragonTigerPlayerInfo;
  countdownInfo: CountdownInfo;
  matchId: number;
  matchOrder: number;
  timeout: number;
  cardNum: number;
  placeBetInfoList: DragonTigerBetInfo[];
  playerNum: number;
  chessList: number[];
  historyList: [];
  betList: number[];
  maxBet: number;
  minBet: number;
}