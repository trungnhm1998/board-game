export class NewsButton {
  data;
  type;
  caption;
}

export class News {
  title: string;
  content: string;
  icon: string;
  image: string;
  imageLarge: string;
  category: number;
  button1: NewsButton;
  button2: NewsButton;
  popup: boolean;
}