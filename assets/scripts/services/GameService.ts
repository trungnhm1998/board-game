import {
  CAPTCHA_TYPE,
  DIALOG_TYPE,
  KEYS,
  REASON_MONEY_TYPE,
  RESOURCE_BLOCK,
  SCENE_TYPE,
  SERVER_EVENT
} from '../core/Constant';
import {GlobalInfo, moon} from '../core/GlobalInfo';
import {UserInfo} from '../model/UserInfo';
import {LocalData} from '../core/LocalStorage';
import {Config} from '../Config';
import {ModelUtils} from '../core/ModelUtils';
import {FadeOutInTransition} from './SceneManager';
import {GameItem} from '../model/GameInfo';
import {GameSocket, LobbySocket} from './SocketService';
import {MainMenu} from '../scenes/MainMenu';
import {RecordDialog} from '../dialogs/RecordDialog';
import {GameScene} from '../common/GameScene';

declare var require: any;

export interface IGameService {
  name: string;

  isActive: boolean;

  handleGameCommand(socket: GameSocket);

  setGameNode(gameNode: cc.Node);

  onJoinBoard(data);

  onReturnGame(data);

  clearWaitingActions();

  processWaitingActions();

  updateRoomInfoList();
}

export class GameService {
  pauseQueue = [];
  playing: boolean = true;
  gameNodes = {};
  services = {};
  currentGameInfo;


  isPlaying() {
    return this.playing;
  }

  handleGameCommand(socket: GameSocket) {
    socket.on(SERVER_EVENT.JOIN_BOARD, data => this.onJoinBoard(data));
    socket.on(SERVER_EVENT.GET_ROOM_INFO_LIST, data => this.onGetRoomInfoList(data));
    socket.on(SERVER_EVENT.RETURN_GAME, data => this.onReturnGame(data));
    socket.on(SERVER_EVENT.GET_PLAY_HISTORY, data => this.onGetPlayHistory(data));
  }

  handleLobbyCommand(socket: LobbySocket) {
    socket.on(SERVER_EVENT.SET_CLIENT_INFO, data => this.onSetClientInfo(data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => this.onErrorMessage(data));
    socket.on(SERVER_EVENT.PING, data => this.onPing(data));
    socket.on(SERVER_EVENT.LOGIN_SUCCESS, data => this.onLoginSuccess(data));
    socket.on(SERVER_EVENT.GET_LIST_GAME, data => this.onGetListGame(data));
    socket.on(SERVER_EVENT.JOIN_LOBBY, data => this.onJoinLobby(data));
    socket.on(SERVER_EVENT.UPDATE_MONEY, data => this.onUpdateMoney(data));
    socket.on(SERVER_EVENT.GET_COUNT_USER_ONLINE, data => this.onGetCountUserOnline(data));
  }

  onLoginSuccess(data) {
    ModelUtils.merge(GlobalInfo.me, data[KEYS.USER_INFO]);
    GlobalInfo.configInfo = data[KEYS.CONFIG_INFO];

    GlobalInfo.me.avatar += '?t=' + Date.now();
    if (data[KEYS.USER_INFO].isVerify) {
      GlobalInfo.me.isVerify =
        data[KEYS.USER_INFO].isVerify == 'true' ||
        data[KEYS.USER_INFO].isVerify == true;
    }

    // moon.res.preloadRemoteImage(GlobalInfo.me.avatar);
    GlobalInfo.me.isLoggedIn = true;
    LocalData.isNextCaptcha = 0;
    moon.storage.saveLocalData();
    moon.header.setUserInfo(GlobalInfo.me);

    if (GlobalInfo.nextSceneInfo.sceneType != SCENE_TYPE.LOGIN) {
      GlobalInfo.nextSceneInfo.sceneType = SCENE_TYPE.MAIN_MENU;
    }

    moon.lobbySocket.getListGame();

    if (!moon.gameSocket.connected) {
      moon.gameSocket.connect(Config.gameSocket);
    }
  }

  onGetListGame(data: Array<GameItem>) {
    GlobalInfo.listGame = data;
    if (
      GlobalInfo.nextSceneInfo.sceneType == SCENE_TYPE.LOGIN &&
      moon.scene.isInScreen(SCENE_TYPE.LOGIN)
    ) {
      if (GlobalInfo.nextSceneInfo.action) {
        GlobalInfo.nextSceneInfo.action();
        GlobalInfo.nextSceneInfo.sceneType = SCENE_TYPE.MAIN_MENU;
        GlobalInfo.nextSceneInfo.action = null;
      }
    } else if (
      moon.scene.isInScreen(SCENE_TYPE.LOGIN) &&
      GlobalInfo.me.isLoggedIn &&
      !GlobalInfo.me.isReturn
    ) {
      GlobalInfo.nextSceneInfo.clear();
      let trasition = new FadeOutInTransition(0.4);
      moon.scene.pushScene(SCENE_TYPE.MAIN_MENU, trasition);
    } else if (moon.scene.isInScreen(SCENE_TYPE.MAIN_MENU)) {
      let mainmenu: MainMenu = moon.scene.curScene.getComponent(MainMenu);
      if (mainmenu) {
        mainmenu.setGameItems(GlobalInfo.listGame);
      }
    }
  }

  onUpdateMoney(data) {
    let money = data[KEYS.MONEY];
    let reason = data[KEYS.REASON];
    let msg = data[KEYS.MESSAGE];
    moon.money.set(GlobalInfo.me, money);
    if (msg) {
      moon.dialog.showNotice(msg);
    }

    switch (reason) {
      case REASON_MONEY_TYPE.DEFAULT:
        moon.lobbySocket.getVipCashBackInfo();
        break;
    }
  }

  onSetClientInfo(data) {
    LocalData.clientId = data[KEYS.CLIENT_ID];
    moon.storage.saveLocalData();
    if (LocalData.isNextCaptcha == 1) {
      moon.lobbySocket.getCaptcha(CAPTCHA_TYPE.LOGIN);
    }
  }

  onPing(data) {
    if (moon.scene.isInScreen(SCENE_TYPE.MAIN_MENU)) {
      moon.header.updatePing(moon.lobbySocket.getPing());
    } else {
      moon.header.updatePing(moon.gameSocket.getPing());
    }

    if (GlobalInfo.me && GlobalInfo.me.isLoggedIn) {
      moon.lobbySocket.getOnlineUsers();
    }
  }

  onErrorMessage(data) {
    moon.dialog.hideWaiting();
    let dlgMgr = moon.dialog;
    let sceneMgr = moon.scene;
    let command = data[KEYS.CAUSE_COMMAND];
    let msg = data[KEYS.MESSAGE];
    // Check cause command first
    moon.dialog.showNotice(msg);
  }

  onErrorByType(data) {
    let dlgMgr = moon.dialog;
    let sceneMgr = moon.scene;
    switch (data[KEYS.TYPE]) {

    }

    return false;
  }

  onJoinLobby(data) {
    moon.money.set(GlobalInfo.me, data[KEYS.MONEY]);
  }

  onReturnGame(data) {
    GlobalInfo.me.isReturn = false;
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    let currentRoomId = data[KEYS.ROOM_ID];
    GlobalInfo.room = GlobalInfo.rooms.filter(
      room => room.roomId == currentRoomId
    )[0];

    if (moon.scene.isInScreen(SCENE_TYPE.PLAY)) {
      let service = this.getCurrentService();
      service.onReturnGame(data);
    } else {
      this.goToPlayScene(GlobalInfo.room.gameId, () => {
        let service = this.getCurrentService();
        service.onReturnGame(data);
      });
    }
  }

  onGetRoomInfoList(data) {
    GlobalInfo.roomInfoList[data[KEYS.GAME_ID]] = data[KEYS.ROOM_LIST];
    let service = this.getCurrentService();
    if (service) {
      service.updateRoomInfoList();
    }
  }

  goToPlayScene(gameId, callback?) {
    for (let gameInfo of Config.gameInfo) {
      if (gameInfo.gameId == gameId) {
        this.currentGameInfo = gameInfo;
        let gameNode = this.getCurrentGameNode(gameId);
        let goToSceneFunc = (onEnd?) => {
          gameNode = this.getCurrentGameNode(gameId);
          this.activeCurrentService();
          let service: IGameService = this.getCurrentService();
          service.setGameNode(gameNode);
          service.processWaitingActions();
          if (!moon.scene.isInScreen(SCENE_TYPE.PLAY)) {
            moon.scene.pushScene(SCENE_TYPE.PLAY);
          }
          if (onEnd) {
            onEnd();
          }
        };

        if (!gameNode) {
          let loadingDlg = moon.dialog.showWaiting();
          (<any>cc.loader).onProgress = function (completedCount, totalCount, item) {
            let percent = 100 * completedCount / totalCount;
            if (percent <= 1) {
              loadingDlg.setProgress(100 * percent);
            }
          };
        }
        let merchantId = moon.http.getUrlParameter('mc');
        if (merchantId && cc.loader.downloader._subpackages) {
          let subpackage = cc.loader.downloader._subpackages[gameInfo.packageName];
          if (subpackage && subpackage.path && subpackage.path.indexOf('mc') < 0) {
            subpackage.path = `mc/${merchantId}/${subpackage.path}`;
          }
        }
        cc.loader.downloader.loadSubpackage(gameInfo.packageName, (err) => {
          console.log('load subpackage successfully.');
          this.preloadGameNode(gameInfo.gameId, () => {
            goToSceneFunc(callback);
          });
        });
        break;
      }
    }
  }


  getGameConfig(gameId): any {
    for (let gameInfo of Config.gameInfo) {
      if (gameInfo.gameId == gameId) {
        return gameInfo;
      }
    }
    return {};
  }

  preloadGameService(serviceName) {
    return new Promise<any>(resolve => {
      if (this.services[serviceName]) {
        resolve();
      } else {
        let serviceModule = require(serviceName);
        this.services[serviceName] = new serviceModule[serviceName]();
        this.services[serviceName].handleGameCommand(moon.gameSocket);
        resolve();
      }
    });
  }

  preloadGameNode(gameId, callback?) {
    for (let gameInfo of Config.gameInfo) {
      if (gameInfo.gameId == gameId) {
        let deckId = moon.string.getBaseName(gameInfo.deckPath);
        moon.cardPool.setCardDeck(deckId);
        this.preloadGameService(gameInfo.service)
          .then(() => {
            this.currentGameInfo = gameInfo;
            let gameNode = this.getCurrentGameNode(gameId);
            this.activeCurrentService();
            let service: IGameService = this.getCurrentService();

            if (gameNode) {
              moon.dialog.showWaiting();
              moon.res.preloadFolders(
                [
                  `${gameInfo.localeFolder}/${moon.locale.getCurrentLanguage()}`,
                  `${gameInfo.deckPath}`
                ],
                () => {
                },
                RESOURCE_BLOCK.GAME
              ).then(() => {
                moon.cardPool.updateAtlas();
                service.setGameNode(gameNode);
                if (callback) {
                  callback();
                }
              });
            } else {
              let loadingDlg = moon.dialog.showWaiting();
              moon.res
                .preloadPrefabAndFolders(
                  gameInfo.prefabPath,
                  [
                    `${gameInfo.localeFolder}/${moon.locale.getCurrentLanguage()}`,
                    `${gameInfo.deckPath}`
                  ],
                  percent => {
                    loadingDlg.setProgress(100 * percent);
                  },
                  RESOURCE_BLOCK.GAME
                )
                .then(() => {
                  moon.cardPool.updateAtlas();
                  let prefab = moon.string.getBaseName(gameInfo.prefabPath);
                  let gamePrefab = moon.res.getPrefab(prefab);
                  this.gameNodes[gameId] = cc.instantiate(gamePrefab);
                  service.setGameNode(this.gameNodes[gameId]);
                  if (callback) {
                    callback();
                  }
                });
            }
          });
      }
    }
  }

  onJoinBoard(data) {
    GlobalInfo.room = data[KEYS.ROOM_INFO];
    GlobalInfo.rooms = [GlobalInfo.room];

    let gameScene = this.getCurrentGameNode().getComponent(GameScene) as GameScene;
    gameScene.onEnterRoom();
    this.getCurrentService().onJoinBoard(data);
  }

  activeCurrentService() {
    for (let key in this.services) {
      this.services[key].isActive = false;
    }
    let service = this.getCurrentService();
    service.clearWaitingActions();
    service.isActive = true;
  }

  getCurrentService(): IGameService {
    return this.services[this.currentGameInfo.service];
  }

  getCurrentGameNode(gameId?) {
    if (gameId) {
      return this.gameNodes[gameId];
    } else if (GlobalInfo.room) {
      return this.gameNodes[GlobalInfo.room.gameId];
    }
    return;
  }

  handleReconnect() {
  }

  logout() {
    GlobalInfo.me = new UserInfo();
    moon.lobbySocket.logout();
    moon.lobbySocket.close();
    moon.gameSocket.close();
    moon.scene.pushScene(SCENE_TYPE.LOGIN, null, false, () => {
      if (moon.lobbySocket.sentClientInfo) {
        moon.dialog.hideWaiting();
      } else {
        let checkSentClientInfo = () => {
          if (moon.lobbySocket.sentClientInfo) {
            moon.dialog.hideWaiting();
          } else {
            let waitingDlg = moon.dialog.showWaiting();
            waitingDlg.setText(moon.locale.get('connecting'));
            moon.timer.scheduleOnce(() => {
              checkSentClientInfo();
            }, 0.5);
          }
        };

        checkSentClientInfo();
      }
    });

    moon.lobbySocket.connect(Config.curServer).then(() => {
    });
  }

  private static instance: GameService;

  static getInstance(): GameService {
    if (!GameService.instance) {
      GameService.instance = new GameService();
    }

    return GameService.instance;
  }

  pause() {
    cc.log('pause');
    if (moon.scene.isInScreen(SCENE_TYPE.PLAY)) {
      this.playing = false;
    }
  }

  resume() {
    cc.log('resume');
    if (this.playing) return;

    if (moon.scene.isInScreen(SCENE_TYPE.PLAY)) {
      this.playing = true;
      moon.gameSocket.returnGame(GlobalInfo.me.token, GlobalInfo.me.info);
    }
  }

  private onGetCountUserOnline(data) {
    let count = data[KEYS.COUNT];
    moon.header.updateTotalPlayer(count);
  }

  private onGetPlayHistory(data) {
    let gameId = data[KEYS.GAME_ID];
    let historyList = data[KEYS.HISTORY_LIST];
    let dlgMgr = moon.dialog;
    dlgMgr.getDialogByType(
      DIALOG_TYPE.RECORD,
      RecordDialog
    );
    moon.dialog.hideWaiting();
    dlgMgr.showPlayHistory(historyList);
  }

  onBackPressed() {
    let mainmenu: MainMenu = moon.scene.curScene.getComponent(
      MainMenu
    );
    if (mainmenu) {
      mainmenu.showBackConfirm();
    }
  }

  hideReconnect() {
    moon.root.hideReconnect();
  }

  getGameInfoById(gameId: number) {
    for (const gameInfo of Config.gameInfo) {
      if (gameInfo.gameId == gameId)
        return gameInfo;
    }

    console.error("gameId not found", gameId);
    return null;
  }
}
