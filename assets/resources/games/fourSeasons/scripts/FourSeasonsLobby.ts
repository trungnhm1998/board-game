import { GAME_ID, RESOURCE_BLOCK } from './../../../../scripts/core/Constant';
import { moon } from './../../../../scripts/core/GlobalInfo';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
import { FourSeasonsRoomOverview } from './FourSeasonsRoomOverview';
import { FourSeasonsRoom } from './model/FourSeasonsRoom';
const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsLobby extends cc.Component {
  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Node)
  roomListContainer: cc.Node = null;

  @property(cc.Prefab)
  FourSeasonsRoomOverviewPrefab: cc.Node = null;

  onEnable() {
    // moon.header.show();
  }

  onResize(scaleRatio) {
    cc.log("FourSeasonsLobby::onResize", scaleRatio);
    this.ui.scale = scaleRatio;
  }

  onLanguageChange() {
  }

  updateRoomInfoList(fourSeasonsRoomList: Array<FourSeasonsRoom>) {
    // loop through room list and instantitate new prefab
    let backgroundImage = 1;
    for (let i = 0; i < fourSeasonsRoomList.length; i++) {
      let roomOverviewNode = cc.instantiate(this.FourSeasonsRoomOverviewPrefab);

      roomOverviewNode.getComponent(cc.Sprite).spriteFrame = moon.res.getSpriteFrame(`gameroomBg${backgroundImage}`, RESOURCE_BLOCK.GAME);

      const fourSeasonsRoomOverview = roomOverviewNode.getComponent(FourSeasonsRoomOverview);
      fourSeasonsRoomOverview.init(fourSeasonsRoomList[i], backgroundImage);
      fourSeasonsRoomOverview.onlanguageChange();

      this.roomListContainer.addChild(roomOverviewNode);
      if (backgroundImage >= 4)
        backgroundImage = 1;
      else
        backgroundImage++;
    }
  }

  clear() {
    this.roomListContainer.children.forEach(roomOverviewNode => {
      const roomOverviewComponent = roomOverviewNode.getComponent(FourSeasonsRoomOverview);
      roomOverviewComponent.clear();
    });
  }
}