import {LocalData} from '../core/LocalStorage';
import {GlobalInfo, moon} from '../core/GlobalInfo';

const {ccclass, property} = cc._decorator;

@ccclass
export class AudioManager {
  private gender = 'lady';
  _musicsId = [];
  _soundsId = [];
  _maxAudioInstance = 0;
  _currentMusicPath;
  _fadeInTask;
  _fadeOutTask;
  // fade in/out effect only play for FADE_EFFECT_OFFSET seconds
  FADE_EFFECT_OFFSET = 2.5;

  private static instance: AudioManager;
  private onBackFromMute;

  constructor() {
    this._maxAudioInstance = cc.audioEngine.getMaxAudioInstance();
    LocalData.sound = +(LocalData.sound || 1);
    LocalData.music = +(LocalData.music || 1);

  }

  static getInstance(): AudioManager {
    if (!this.instance) {
      this.instance = new AudioManager();
    }
    return this.instance;
  }

  playAudio(fileName: String, isMusic = false, loop = false) {
    if (this._musicsId.length + this._soundsId.length == this._maxAudioInstance) return;
    let audioPath = '';
    if (fileName.substring(fileName.length - 5, fileName.length - 1) != '.mp3') {
      audioPath = `resources/sound/${fileName}.mp3`;
    } else {
      audioPath = `resources/sound/${fileName}`;
    }

    this.playAudioWithPath(audioPath, isMusic, loop);
  }

  playAudioWithPath(path: string, isMusic = false, loop = false) {
    if (isMusic) {
      if (this._currentMusicPath == path) return;
      this._currentMusicPath = path;
    }
    if (this._musicsId.length + this._soundsId.length == this._maxAudioInstance) return;
    let audioPath = path;
    let audioId = null;
    cc.loader.load(cc.url.raw(audioPath), (err, clip) => {
      if (err != null) return;

      if (isMusic) {
        this.stopAllMusic();
      }

      let volume = isMusic ? LocalData.music : LocalData.sound;

      if (isMusic) {
        if (volume == 0) {
          this.onBackFromMute = () => {
            this.playAudioWithPath(path, isMusic, true);
          };
          return;
        }
        cc.audioEngine.setMusicVolume(0.01);
        audioId = cc.audioEngine.playMusic(clip, false);
        let audioDuration = cc.audioEngine.getDuration(audioId);
        this._musicsId.push(audioId);
        this._fadeInTask = moon.timer.tweenNumber(0, volume,
          (val) => {
            cc.audioEngine.setMusicVolume(val);
          }, (val) => {
            cc.audioEngine.setMusicVolume(volume);
          }, this.FADE_EFFECT_OFFSET);
        this._fadeOutTask = moon.timer.scheduleOnce(() => {
          moon.timer.tweenNumber(volume, -volume,
            (val) => {
              cc.audioEngine.setMusicVolume(val);
            }, (val) => {
              cc.audioEngine.setVolume(audioId, 0);
              if (loop) {
                this.playAudioWithPath(path, isMusic, true);
              }
            }, this.FADE_EFFECT_OFFSET);
        }, audioDuration - this.FADE_EFFECT_OFFSET);
      } else {
        audioId = cc.audioEngine.playEffect(clip, false);
        this._soundsId.push(audioId);
      }

      cc.audioEngine.setFinishCallback(audioId, this.stopAudio.bind(this, audioId, isMusic));
    });

  }

  clearMusicTasks() {
    moon.timer.removeTask(this._fadeOutTask);
    moon.timer.removeTask(this._fadeInTask);
    this._fadeOutTask = null;
    this._fadeInTask = null;
  }

  stopAllMusic() {
    for (let musicId of this._musicsId) {
      cc.audioEngine.stop(musicId);
    }
    this._musicsId = [];
    this._currentMusicPath = null;
    this.clearMusicTasks();
  }

  stopAudio(id: number, isMusic = false) {
    let idx = -1;
    if (isMusic) {
      idx = this._musicsId.indexOf(id);
    } else {
      idx = this._soundsId.indexOf(id);
    }

    if (idx > -1) {
      if (isMusic)
        this._musicsId.splice(idx, 1);
      else
        this._soundsId.splice(idx, 1);
    }
  }

  stopAllAudio() {
    this.stopAllMusic();
    cc.audioEngine.stopAll();
    this._soundsId = [];
  }

  getMusicState(): cc.audioEngine.AudioState {
    if (this._musicsId.length <= 0)
      return cc.audioEngine.AudioState.STOPPED;
    return cc.audioEngine.getState(this._musicsId[0]);
  }

  setSoundVolume(value) {
    LocalData.sound = value;
    cc.audioEngine.setEffectsVolume(value);
    moon.storage.saveLocalData();
  }

  setMusicVolume(value) {
    let backFromMute = LocalData.music == 0 && value > 0;
    LocalData.music = value;
    if (this._fadeInTask) {
      moon.timer.removeTask(this._fadeInTask);
    }

    if (backFromMute && this.onBackFromMute) {
      this.onBackFromMute();
    }
    cc.audioEngine.setMusicVolume(value);
    moon.storage.saveLocalData();
  }

  playVoiceInGame(fileName: string) {
    let resPath = this.getGameResPath();
    let language = moon.locale.getCurrentLanguage();
    let path = `${resPath}/${language}/${this.gender}/${fileName}.mp3`;
    this.playAudioWithPath(path);
  }

  playEffectInGame(fileName: string, isLocale = false) {
    let resPath = this.getGameResPath();
    if (!isLocale) {
      let path = `${resPath}/${fileName}.mp3`;
      this.playAudioWithPath(path);
    } else {
      let language = moon.locale.getCurrentLanguage();
      let path = `${resPath}/${language}/${fileName}.mp3`;
      this.playAudioWithPath(path);
    }
  }

  playMusicInGame(fileName: string, isLoop = true) {
    let resPath = this.getGameResPath();
    let path = `${resPath}/${fileName}.mp3`;
    this.playAudioWithPath(path, true, isLoop);
  }

  private getGameResPath() {
    if (!GlobalInfo.room.gameId) return;
    let gameConfig = moon.service.getGameConfig(GlobalInfo.room.gameId);
    return gameConfig.audioPath;
  }
}
