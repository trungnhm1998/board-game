export class RedBlackPlayerInfo {
    userId: number;
    money: number;
    betMoney: number;
    winMoney: number;
    cardList: number[];
    betList: number[];
    seat: number;
    displayName: string;
    avatar: string;
    bet: number;
    betDealer: number;
    snatch: number;
    formatedDisplayName:string;
    //data from server
    betFactor: number;
  }