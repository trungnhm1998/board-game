export interface IUser {
  money: number;
  trialmoney: number;
  userId: number;
}

export class UserInfo implements IUser {
  accountId: number;
  displayName: string;
  type: string;
  seat: number;
  money: number;
  trialmoney: number;
  userId: number;
  isVerify: boolean;
  isReturn: boolean;
  gameId: number;
  info: string;
  token: string;
  email: string;
  avatar: string;
  phone: string;
  countryCode: string;
  accessToken = null;
  isLoggedIn = false;
  loginType = -1;
  date: string;
  address: string;
  birthday: string;
  gender: string;
  verifyStep2: boolean;
  verifyStep2Type: string;
  level: number;
}

