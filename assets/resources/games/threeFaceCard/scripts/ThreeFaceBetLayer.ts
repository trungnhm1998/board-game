import {ThreeFacePlayer} from './ThreeFacePlayer';

const {ccclass, property} = cc._decorator;

@ccclass
export class ThreeFaceBetLayer extends cc.Component {

  @property(cc.Node)
  chipSample: cc.Node = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  @property(cc.Node)
  betArea: cc.Node = null;

  chipPool: cc.NodePool;

  onLoad() {
    this.chipPool = new cc.NodePool();
  }

  moveChipsToHouse(housePlayer: ThreeFacePlayer, losers: ThreeFacePlayer[], callback?) {
    let i = 0;
    for (let loser of losers) {
      if (loser.playerInfo.userId == housePlayer.playerInfo.userId) {
        continue;
      }
      if (i >= losers.length - 1) {
        this.moveChipsToWinner(loser, housePlayer, loser.playerInfo.winMoney, callback);
      } else {
        this.moveChipsToWinner(loser, housePlayer, loser.playerInfo.winMoney);
      }
      i++;
    }

    if (losers.length == 0 && callback) {
      callback();
    }
  }

  moveChipsToWinner(loser: ThreeFacePlayer, winner: ThreeFacePlayer, winMoney: number, callback?) {
    let loserWorldPos = loser.getAvatarPosition();
    let loserNodePos = this.betArea.convertToNodeSpaceAR(loserWorldPos);
    let winnerWorldPos = winner.getAvatarPosition();
    let winnerNodePos = this.betArea.convertToNodeSpaceAR(winnerWorldPos);
    let actions = [];
    loserNodePos.y -= 50;
    winnerNodePos.y -= 50;
    const chipsThrows = Math.ceil(Math.abs(winMoney) > 10 ? 10 : Math.abs(winMoney));
    for (let i = 0; i < chipsThrows; i++) {
      let chipNode = this.getChip(1);
      this.betArea.addChild(chipNode);
      chipNode.position = loserNodePos;
      chipNode.active = true;
      chipNode.opacity = 0;
      actions.push(
        cc.callFunc(() => {
          chipNode.runAction(
            cc.sequence(
              cc.spawn(
                cc.moveTo(0.5, cc.v2(winnerNodePos.x, winnerNodePos.y)),
                cc.fadeIn(0.07),
              ),
              cc.callFunc(() => {
                chipNode.active = false;
                chipNode.removeFromParent();
                this.chipPool.put(chipNode);
              }),
            )
          );
        })
      );
      actions.push(cc.delayTime(0.05));
    }
    this.node.runAction(
      cc.sequence(
        cc.sequence(actions),
        cc.delayTime(0.6),
        cc.callFunc(() => {
          if (callback) {
            callback();
          }
        })
      )
    );
  }

  getChip(chip: number) {
    let chipNode = this.chipPool.get();
    if (!chipNode) {
      chipNode = cc.instantiate(this.chipSample);
    }
    chipNode.active = true;
    // let chipSprite: cc.Sprite = chipNode.getComponent(cc.Sprite);
    // chipSprite.spriteFrame = this.atlas.getSpriteFrame('chip' + chip);
    // (<any>chipNode).value = chip;
    return chipNode;
  }

  returnChips(nodes?) {
    let chipsNodes = [].concat(this.betArea.children);
    if (nodes) {
      chipsNodes = chipsNodes.concat(nodes);
    }

    chipsNodes.forEach(chipNode => {
      chipNode.removeFromParent();
      chipNode.active = false;
      this.chipPool.put(chipNode);
    });
  }

  clear() {
    this.returnChips();
  }
}