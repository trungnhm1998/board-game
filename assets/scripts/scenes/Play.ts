import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {CArray} from "../core/Array";
import {moon, GlobalInfo} from "../core/GlobalInfo";
import {SCENE_TYPE} from '../core/Constant';

const {ccclass, property} = cc._decorator;

@ccclass
export class Play extends SceneComponent {
  name = SCENE_TYPE.PLAY;

  @property(cc.Node)
  gameContainer: cc.Node = null;

  gameScene: SceneComponent;

  gameNodes = [];

  onEnter() {
    super.onEnter();
    moon.header.setGameLobby();
    if (GlobalInfo.roomInfoList[GlobalInfo.room.gameId]){
      moon.header.show();
    }else{
      moon.header.hide()
    }
   
    let gameNode = moon.service.getCurrentGameNode();
    if (gameNode) {
      CArray.pushElement(this.gameNodes, gameNode);
      if (!gameNode.parent) {
        this.gameContainer.addChild(gameNode);
      }
      gameNode.active = true;
      gameNode.position = cc.v2();
      NodeUtils.updateWidgetAlignment(gameNode);
      this.gameScene = gameNode.getComponent(SceneComponent);
      this.gameScene.onEnter();
    }
  }

  onLeave() {
    super.onLeave();
    if (this.gameScene) {
      this.gameScene.onLeave();
      this.gameScene = null;
    }
    for (let gameNode of this.gameNodes) {
      gameNode.active = false;
    }
  }

  onLanguageChange() {
    super.onLanguageChange();
    moon.header.onLanguageChange();
    if (this.gameScene) {
      this.gameScene.onLanguageChange();
    }
  }

  onResize() {
    super.onResize();
    if (this.gameScene) {
      this.gameScene.onResize();
    }
  }
}