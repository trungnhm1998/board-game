#include "autogentestbindings.hpp"
#include "cocos2d.h"
#include "cocos2d_specifics.hpp"
#include "js_manual_conversions.h"

USING_NS_CC;

JSClass  *jsb_sdkbox_SimpleNativeClass_class;

void onGetImage(RenderTexture*, const std::string&) {
}

// ***********SimpleNativeClass*************
SimpleNativeClass::SimpleNativeClass() {
}

SimpleNativeClass::~SimpleNativeClass() {
}

void SimpleNativeClass::addTexture2DFromBase64(std::string &base64string, std::string &key) {
    int len = 0;
    unsigned char *buffer;
    len = base64Decode((unsigned char*)base64string.c_str(), (unsigned int)base64string.length(), &buffer);
    Image *img = new Image();
    bool ok = img->initWithImageData(buffer, len);
    Texture2D* texture = new Texture2D();
    texture->initWithImage(img);
    Director::getInstance()->getTextureCache()->addImage(img, key);
}


char* SimpleNativeClass::getBase64FromTexture2D(std::string &key) {
    Texture2D* texture = Director::getInstance()->getTextureCache()->getTextureForKey(key);
    int width = texture->getPixelsWide();
    int height = texture->getPixelsHigh();
    Sprite* sourceSprite = Sprite::createWithTexture(texture);
    RenderTexture* renderTexture = RenderTexture::create(width, height);
    renderTexture->begin();
    sourceSprite->setPosition(width/2, height/2);
    sourceSprite->visit();
    renderTexture->end();
    Director::getInstance()->getRenderer()->render();
    std::string fullpath = FileUtils::getInstance()->getWritablePath() + "tempsaved.png";
    Image *img = renderTexture->newImage(true);
    img->saveToFile(fullpath, true);
    Data data = FileUtils::getInstance()->getDataFromFile(fullpath);
    unsigned char* tdata = data.getBytes();
    auto tsize = data.getSize();
    auto sssize = static_cast<unsigned int>(tsize);
    char *tbuffer;
    int tlen = base64Encode(tdata, sssize, &tbuffer);
    return tbuffer;
}

// ***********BINDING*************
bool js_SimpleNativeClassJS_SimpleNativeClass_addTexture2DFromBase64(JSContext *cx, uint32_t argc, JS::Value *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        jsval_to_std_string(cx, args.get(0), &arg0);
        jsval_to_std_string(cx, args.get(1), &arg1);

        SimpleNativeClass* cls = new SimpleNativeClass();
        cls->addTexture2DFromBase64(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportErrorUTF8(cx, "js_SimpleNativeClassJS_SimpleNativeClass_addTexture2DFromBase64 : wrong number of arguments");
    return false;
}

bool js_SimpleNativeClassJS_SimpleNativeClass_getBase64FromTexture2D(JSContext *cx, uint32_t argc, JS::Value *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 1) {
        std::string arg0;
        jsval_to_std_string(cx, args.get(0), &arg0);

        SimpleNativeClass* cls = new SimpleNativeClass();
        char* ret = cls->getBase64FromTexture2D(arg0);

        JS::RootedValue jsret(cx);
        c_string_to_jsval(cx, ret, &jsret, strlen(ret));
        args.rval().set(jsret);
        return true;
    }
    JS_ReportErrorUTF8(cx, "js_SimpleNativeClassJS_SimpleNativeClass_getBase64FromTexture2D : wrong number of arguments");
    return false;
}

void js_register_SimpleNativeClassJS_SimpleNativeClass(JSContext *cx, JS::HandleObject global) {
    static JSClass PluginAgeCheq_class = {
            "SimpleNativeClass",
            JSCLASS_HAS_PRIVATE,
            nullptr
    };

    jsb_sdkbox_SimpleNativeClass_class = &PluginAgeCheq_class;

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("addTexture2DFromBase64", js_SimpleNativeClassJS_SimpleNativeClass_addTexture2DFromBase64, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getBase64FromTexture2D", js_SimpleNativeClassJS_SimpleNativeClass_getBase64FromTexture2D, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("isLoggedIn", js_SimpleNativeClassJS_SimpleNativeClass_isLoggedIn, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("getUserID", js_SimpleNativeClassJS_SimpleNativeClass_getUserID, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("requestGift", js_SimpleNativeClassJS_SimpleNativeClass_requestGift, 3, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("init", js_SimpleNativeClassJS_SimpleNativeClass_init, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("setAppURLSchemeSuffix", js_SimpleNativeClassJS_SimpleNativeClass_setAppURLSchemeSuffix, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("logEvent", js_SimpleNativeClassJS_SimpleNativeClass_logEvent, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("logout", js_SimpleNativeClassJS_SimpleNativeClass_logout, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("sendGift", js_SimpleNativeClassJS_SimpleNativeClass_sendGift, 4, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("setAppId", js_SimpleNativeClassJS_SimpleNativeClass_setAppId, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("fetchFriends", js_SimpleNativeClassJS_SimpleNativeClass_fetchFriends, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("logPurchase", js_SimpleNativeClassJS_SimpleNativeClass_logPurchase, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("login", js_SimpleNativeClassJS_SimpleNativeClass_login, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
//        JS_FN("getAccessToken", js_SimpleNativeClassJS_SimpleNativeClass_getAccessToken, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JS::RootedObject parent_proto(cx, nullptr);
    JSObject* objProto = JS_InitClass(
        cx, global,
        parent_proto,
        jsb_sdkbox_SimpleNativeClass_class,
        dummy_constructor<SimpleNativeClass>, 0, // no constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, objProto);

    jsb_register_class<SimpleNativeClass>(cx, jsb_sdkbox_SimpleNativeClass_class, proto);
}

void register_all_autogentestbindings(JSContext *cx, JS::HandleObject obj) {
//    jsval nsval;
//    JSObject *ns;
//    JS_GetProperty(cx, obj, "cc",&nsval;);
//    if (nsval == JSVAL_VOID) {
//        ns = JS_NewObject(cx, NULL, NULL, NULL);
//        nsval = OBJECT_TO_JSVAL(ns);
//        JS_SetProperty(cx, obj, "cc",&nsval;);
//    } else {
//        JS_ValueToObject(cx,nsval, &ns;);
//    }
//    obj = ns;
//    js_register_autogentestbindings_SimpleNativeClass(cx, obj);

//    JS::RootedValue  nsval(cx);
//    JS::RootedObject ns(cx);
//    JS_GetProperty(cx, obj, "cc", &nsval);
//    if (nsval == JSVAL_VOID) {
//        ns = JS_NewObject(cx, NULL, NULL, NULL);
//        nsval = OBJECT_TO_JSVAL(ns);
//        JS_SetProperty(cx, obj, "cc", nsval);
//    } else {
//        JS_ValueToObject(cx, nsval, &ns);
//    }
//    // Pass the RootedObject to JSObject
//    obj = ns;
//    JS_DefineFunction(cx, obj, "_doesObjectExist", JSB_ekDoesObjectExist, 1, JSPROP_READONLY | JSPROP_PERMANENT );

    JS::RootedObject ns(cx);
    get_or_create_js_obj(cx, obj, "cc", &ns);
    js_register_SimpleNativeClassJS_SimpleNativeClass(cx, ns);
}