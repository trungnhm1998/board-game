import {SceneComponent} from "../common/SceneComponent";
import {Config} from "../Config";
import {NodeUtils} from "../core/NodeUtils";
import {moon} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;


@ccclass
export class LanguageDialog extends SceneComponent {

  @property(cc.Node)
  itemSample: cc.Node = null;

  @property(cc.Node)
  container: cc.Node = null;

  onEnter() {
    super.onEnter();
    this.initLanguages();
  }

  initLanguages() {
    this.container.removeAllChildren();
    let langCode = moon.locale.getCurrentLanguage();
    for (let lang of Config.languageOptions) {
      let item = cc.instantiate(this.itemSample);
      item.active = true;
      NodeUtils.setLabel(item, 'language', lang.value);
      NodeUtils.setGameSprite(item, 'flag', 'flag_' + lang.name, {shareBlock: true});

      let toggle: cc.Toggle = item.getComponent(cc.Toggle);
      let checkEvent = new cc.Component.EventHandler();
      checkEvent.target = this.node;
      checkEvent.component = 'LanguageDialog';
      checkEvent.handler = 'onItemCheck';
      checkEvent.customEventData = lang.name;
      toggle.checkEvents = [checkEvent];
      this.container.addChild(item);
      toggle.isChecked = langCode == lang.name;
    }
  }

  onItemCheck(evt, lang) {
    moon.dialog.showWaiting();
    moon.locale.setLang(lang)
      .then(() => {
        moon.dialog.hideWaiting();
      });
    moon.lobbySocket.setLanguage(lang);
  }

  onSave() {
    this.closeDialog();
  }
}