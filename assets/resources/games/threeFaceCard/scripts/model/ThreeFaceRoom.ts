import {ThreeFacePlayerInfo} from "./ThreeFacePlayerInfo";
import {CountdownInfo, Room} from "../../../../../scripts/model/Room";

export class ThreeFaceRoom extends Room {
  matchId;
  betList;
  timeout;
  bankerId;
  playerInfoList: ThreeFacePlayerInfo[];
  countdownInfo: CountdownInfo;
}