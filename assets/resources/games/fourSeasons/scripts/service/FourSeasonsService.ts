import { FourSeasonsRoom } from './../model/FourSeasonsRoom';
import { BetType } from './../FourSeasonsConstants';
import { FourSeasonsPot } from './../FourSeasonsPot';
import { COUNTDOWN_TYPE, SERVER_EVENT, KEYS, GAME_ID, ERROR_TYPE } from './../../../../../scripts/core/Constant';
import { GlobalInfo, moon } from './../../../../../scripts/core/GlobalInfo';
import { GameSocket } from './../../../../../scripts/services/SocketService';
import { FourSeasonsGame } from './../FourSeasonsGame';
import { IGameService } from './../../../../../scripts/services/GameService';
import { ModelUtils } from '../../../../../scripts/core/ModelUtils';

export class FourSeasonsService implements IGameService {
  isActive = false;

  name = 'FourSeasonsService';

  game: FourSeasonsGame;

  waitingActions: any[] = [];
  _temp = {
    [BetType.SPADE]: 0,
    [BetType.HEART]: 0,
    [BetType.CLUB]: 0,
    [BetType.DIAMOND]: 0,
  };

  handleGameCommand(socket: GameSocket) {
    cc.log('FourSeasonsService::handleGameCommand');
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({ event: event, data: data });
      }
    };
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => processAction(this.onErrorMessage, data));
  }

  setGameNode(gameNode: cc.Node) {
    this.game = gameNode.getComponent(FourSeasonsGame);
  }

  onActionInGame(data) {
    cc.log('FourSeasonsService::onActionInGame');
    let subData = data[KEYS.DATA];
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        case SERVER_EVENT.UPDATE_PLAYERS_BET:
          this.onOtherPlayersBet(subData);
          break;
        case SERVER_EVENT.OPEN_CARD:
          this.onOpenCard(subData);
          break;
        case SERVER_EVENT.DEAL_CARD:
          this.onDealCard(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
      }
    };

    handleEvents();
  }

  onErrorMessage(data) {
    // if (data[KEYS.TYPE] == ERROR_TYPE.BUBBLE) {
    this.game.showNotify(data[KEYS.MESSAGE]);
    // }
  }

  onStartGame(subData: any) {
    let roomId = subData[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    ModelUtils.merge(GlobalInfo.room, subData);
    let room = GlobalInfo.room as FourSeasonsRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;

    const { timeout } = subData;
    const { table } = this.game;
    table.clear();
    table.updateStateStatus();
    table.runCountdown(timeout);
  }

  onEndGame(subData: any) {
    const { winMoney, money, historyList, timeout, roomId } = subData;
    const { table } = this.game;

    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as FourSeasonsRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;

    table.updateStateStatus();
    table.runCountdown(timeout);
  }

  onBeginBet(subData: any) {
    const { timeout, roomId } = subData;
    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as FourSeasonsRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    const { table } = this.game;
    table.updateStateStatus();
    table.runCountdown(timeout);
    table.effectLayer.playStartGame();
  }

  onOtherPlayersBet(subData: any) {
    cc.log('FourSeasonsService::onOtherPlayersBet', subData);
    let roomId = subData[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    const { table } = this.game;

    for (let placeBetType of subData[KEYS.PLACE_TYPE_LIST]) {
      const { type, betMoneyTotal, betMoney } = placeBetType;
      let finalBetMoney = betMoney;

      if (this._temp[type] > 0) {
        finalBetMoney -= this._temp[type];
        this._temp[type] = 0;
      }

      let pot: FourSeasonsPot = table._potMap[type];
      // check pot exists before doing anything in case of server change type
      if (pot && finalBetMoney > 0) {
        pot.updateTotalBetMoney(moon.string.round(betMoneyTotal, 2));
        table.betLayer.onOtherPlayersBet(pot, finalBetMoney);
      }
    }
  }

  onDealCard(subData: any) {
    let roomId = subData[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as FourSeasonsRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_DEAL_CARD
    const { timeout } = subData;
    const { table } = this.game;
    table.updateStateStatus();
    table.runCountdown(timeout);
    table.shuffleThenDealCards();
  }

  onBet(subData: any) {
    cc.log('FourSeasonsService::onBet', subData);
    const { betMoney, placeBetType, userId, betMoneyTotalPlayer } = subData;
    const { table } = this.game;

    let room = GlobalInfo.room as FourSeasonsRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    this._temp[placeBetType] += betMoney;
    cc.log('this._temp', this._temp);

    table.updateStateStatus();

    // find which pot player bet
    if (userId == GlobalInfo.me.userId) {
      let pot: FourSeasonsPot;
      switch (placeBetType) {
        case BetType.SPADE:
          pot = table.spadePot;
          break;
        case BetType.HEART:
          pot = table.heartPot;
          break;
        case BetType.CLUB:
          pot = table.clubPot;
          break;
        case BetType.DIAMOND:
          pot = table.diamondPot;
          break;
      }
      pot.playerBetMoney.string = String(moon.string.round(betMoneyTotalPlayer, 2));
      table.betLayer.onBet(pot, moon.string.round(betMoney, 2));
    }
  }

  onOpenCard(subData) {
    const { placeBetInfoList, timeout, roomId, placeBetTypeList} = subData;
    const { table } = this.game;

    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as FourSeasonsRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;

    table.updateStateStatus();
    table.runCountdown(timeout);
    table.effectLayer.playStopGame();

    let potsLoseToDealer: Array<FourSeasonsPot> = [];
    let potsWonToDealer: Array<FourSeasonsPot> = [];

    placeBetInfoList.forEach(placeBetInfo => {
      const { placeBetType, cardList, cardType, resultType, bonusTimes } = placeBetInfo;
      if (resultType == 0) {
        cc.log(`placeBetType: ${placeBetType} - ${resultType} `)
        potsLoseToDealer.push(table._potMap[placeBetType]);
      } else {
        if (placeBetType > 0)
          potsWonToDealer.push(table._potMap[placeBetType]);
      }

      (table._potMap[placeBetType] as FourSeasonsPot)._cardListToOpen = cardList;
      (table._potMap[placeBetType] as FourSeasonsPot)._cardTypeToOpen = cardType;
    });

    table.betLayer.node.runAction(
      cc.sequence(
        // reveal cards
        cc.callFunc(() => {
          (table._potMap[BetType.SPADE] as FourSeasonsPot).openCard();
        }),
        cc.delayTime(0.5),
        cc.callFunc(() => {
          (table._potMap[BetType.HEART] as FourSeasonsPot).openCard();
        }),
        cc.delayTime(0.5),
        cc.callFunc(() => {
          (table._potMap[BetType.CLUB] as FourSeasonsPot).openCard();
        }),
        cc.delayTime(0.5),
        cc.callFunc(() => {
          (table._potMap[BetType.DIAMOND] as FourSeasonsPot).openCard();
        }),
        cc.delayTime(0.5),
        cc.callFunc(() => {
          (table._potMap[BetType.DEALER] as FourSeasonsPot).openCard();
        }),
        cc.delayTime(1),
        // move chips to house and player
        cc.callFunc(() => {
          while (potsLoseToDealer.length > 0) {
            const potLost = potsLoseToDealer.pop();
            table.betLayer.moveChipsFromPotToNode(potLost, table.banker);
          }
        }),
        cc.delayTime(0.5),
        cc.callFunc(() => {
          let i = 0;
          while (i < potsWonToDealer.length) {
            const potWon = potsWonToDealer[i];
            table.betLayer.moveChipsFromHouseToPot(table.banker, potWon);
            i++;
          }
        }),
        cc.delayTime(0.5),
        cc.callFunc(() => {
          let i = 0;
          while (i < potsWonToDealer.length) {
            const potWon = potsWonToDealer[i];
            table.betLayer.moveChipsFromPotToNode(potWon, table.playerNum.node.parent);
            i++;
          }
        })
      )
    )

  }

  onLeaveBoard(subData: any) {
    cc.log('FourSeasonsService::onLeaveBoard');
    const { message } = subData;
    if (message && message != "") {
      moon.dialog.showNotice(message);
      // this.game.showNotify(message);
    }
    this.game.onLeaveRoom();
  }

  onJoinBoard(data: any) {
    // clear lobby countdownTask for roomOverviews
    const { table, lobby } = this.game;
    lobby.clear();
    table.clear();
    table.updateCurrentRoom();
  }

  onReturnGame(data: any) {
    // moon.gameSocket.leaveBoard();
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    const { table, lobby } = this.game;
    table.node.active = true;
    table.node.opacity = 255;
    lobby.node.active = false;
    table.clear();
    table.updateCurrentRoom()

    moon.dialog.hideWaiting();

    // if (!GlobalInfo.roomInfoList[GlobalInfo.room.gameId])
    //   moon.gameSocket.getRoomInfoList(GlobalInfo.room.gameId);
  }

  clearWaitingActions() {
  }

  processWaitingActions() {
  }

  updateRoomInfoList() {
    if (this.game) {
      this.game.updateRoomInfoList(GlobalInfo.roomInfoList[GlobalInfo.room.gameId]);
    }
  }

  getStatus(type) {
    let locale = moon.locale;
    let msg = "";
    switch (type) {
      case COUNTDOWN_TYPE.STATE_BET:
        msg = locale.get('betting');
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CARD:
        msg = locale.get('dealing');
        break;
      case COUNTDOWN_TYPE.STATE_OPEN_CARD:
        msg = locale.get('opening');
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        msg = locale.get('result');
        break;
      case COUNTDOWN_TYPE.STATE_START_GAME:
        msg = locale.get('preparing');
        break;
    }
    return msg;
  }
}