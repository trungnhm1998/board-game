import {TestCase} from '../../../../scripts/test/TestCase';
import {GlobalInfo, moon} from '../../../../scripts/core/GlobalInfo';
import {UserInfo} from '../../../../scripts/model/UserInfo';
import {COUNTDOWN_TYPE, GAME_ID, GAME_STATE, KEYS, SERVER_EVENT} from '../../../../scripts/core/Constant';
import {CountdownInfo} from '../../../../scripts/model/Room';
import {ThreeFaceGame} from './ThreeFaceGame';
import {ThreeFaceRoom} from './model/ThreeFaceRoom';
import {ThreeFacePlayerInfo} from './model/ThreeFacePlayerInfo';
import {ThreeFaceStartState} from './gamestates/ThreeFaceStartState';
import {ThreeFaceHouseState} from './gamestates/ThreeFaceHouseState';
import {ThreeFaceBetState} from './gamestates/ThreeFaceBetState';
import {MockSocket} from "../../../../scripts/test/MockSocket";
import {ThreeFaceService} from "./service/ThreeFaceService";

export class ThreeFaceTest extends TestCase {

  game: ThreeFaceGame;
  socket: MockSocket;

  setGame(gameNode) {
    cc.log('setGame');
    super.setGame(gameNode);
    this.game = gameNode.getComponent(ThreeFaceGame);
  }

  run() {
    super.run();
    this.socket = new MockSocket();
    moon.lobbySocket.socket = (<any>this.socket);
    moon.gameSocket.socket = (<any>this.socket);
    cc.log('run test case');
    this.setGameInfo({
      gameId: 6,
      packageName: 'threeFaceCard',
      prefabPath: 'games/threeFaceCard/threeFaceCard',
      localeFolder: 'games/threeFaceCard/locale',
      service: 'ThreeFaceService',
      audioPath: 'resources/games/threeFaceCard/sound',
      deckPath: 'resources/card/decks/2',
    }).then(() => {
      this.runTestCases();
    });
  }

  setup() {
    this.setupRoom();
    this.setupPlayers();
    this.game.onEnter();
    // this.game.onEnter();
  }

  setupRoom() {
    let room = new ThreeFaceRoom();
    room.gameId = 1;
    room.roomId = 1;
    room.betMoney = 1;
    room.matchId = 1;
    room.timeout = 5;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_NOT_START_GAME;
    room.countdownInfo.timeout = 5;
    room.betList = [
      0,
      3,
      68,
      134,
      200
    ];
    GlobalInfo.room = room;
    cc.log('set up room', GlobalInfo);

    this.game.setRoom(room);
  }

  setupPlayers() {
    GlobalInfo.me = new UserInfo();
    GlobalInfo.me.userId = 1;

    let playerInfo1 = new ThreeFacePlayerInfo();
    playerInfo1.userId = 1;
    playerInfo1.displayName = 'player 1';
    playerInfo1.money = 2000;
    playerInfo1.seat = 1;

    let playerInfo2 = new ThreeFacePlayerInfo();
    playerInfo2.userId = 2;
    playerInfo2.displayName = 'player 2';
    playerInfo2.money = 2000;
    playerInfo2.seat = 2;

    let playerInfo3 = new ThreeFacePlayerInfo();
    playerInfo3.userId = 3;
    playerInfo3.displayName = 'player 3';
    playerInfo3.money = 2000;
    playerInfo3.seat = 3;

    let playerInfo4 = new ThreeFacePlayerInfo();
    playerInfo4.userId = 4;
    playerInfo4.displayName = 'player 4';
    playerInfo4.money = 2000;
    playerInfo4.seat = 4;

    let playerInfo5 = new ThreeFacePlayerInfo();
    playerInfo5.userId = 5;
    playerInfo5.displayName = 'player 5';
    playerInfo5.money = 2000;
    playerInfo5.seat = 5;

    this.game.setPlayers([
      playerInfo1, playerInfo2, playerInfo3, playerInfo4, playerInfo5
    ], -1);
  }

  runTestCases() {
    this.setup();
    this.runSequence([
      // Test command
      [this.testLobby, 1],
      [this.testPlayGame, 1],

      // [this.testReturnNotStart, 2],
      // [this.testReturnEndGame, 2]

      [this.testPlayerJoin, 1],
      [this.testReadyStartGame, 5],
      [this.testStartGame, 4],
      [this.testBeginChooseBanker, 4],
      [this.testChooseBanker, 2],
      [this.testBanker, 5],
      [this.testBeginBet, 5],
      [this.testBet, 2],
      [this.testEndGame, 5],
      [this.testReadyStartGame, 5],

      [this.testStartGame, 4],
      [this.testBeginChooseBanker, 4],
      [this.testChooseBanker, 2],
      [this.testBanker, 5],
      [this.testBeginBet, 5],
      [this.testBet, 2],
      [this.testEndGame, 5],

      // Test UI
      // [this.testStartState, 2],
      // [this.testDealChess, 2],
      // [this.testChooseHouse, 4],
      // [this.testShowSnatch, 2],
      // [this.testChooseDealer, 4],
      // [this.testPlayerBetState, 8]
    ]);

    this.finalize();
  }

  runSequence(actions) {
    let ccActions = [];
    for (let action of actions) {
      let func = action[0];
      let delay = action[1];
      ccActions.push(cc.callFunc(() => {
        func.call(this);
      }));
      ccActions.push(cc.delayTime(delay));
    }

    this.game.node.runAction(cc.sequence(ccActions));
  }

  finalize() {
    moon.lobbySocket.offAll();
    moon.gameSocket.offAll();
    this.game.onLeaveRoom();
    this.game.onLeave();
  }

  private testReturnNotStart() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    this.game.onEnterRoom(true);
    threeFaceService.onReturnGame(
      {
        "roomList": [{
          "roomId": 1,
          "gameId": 6,
          "betMoney": 1,
          "countdownInfo": {"type": 0, "timeout": 0},
          "bankerId": -1,
          "playerInfoList": [{
            "userId": 1,
            "displayName": "thinht3",
            "avatar": "https://account.devuid.club/avatar/45f993b4d7cdd6af82c7ade13a8ba9f5.png",
            "money": 1000,
            "seat": 1,
            "isChoose": false,
            "betList": [],
            "betFactor": 0,
            "resultCard": null
          }]
        }], "roomId": 1
      }
    );
  }

  private testReturnEndGame() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    this.game.onEnterRoom(true);
    threeFaceService.onReturnGame(
      {
        "roomList": [{
          "roomId": 3,
          "gameId": 6,
          "betMoney": 1,
          "countdownInfo": {"type": 9, "timeout": 3},
          "matchId": "156091121249938",
          "bankerId": 1,
          "playerInfoList": [{
            "userId": 1,
            "displayName": "thinht1",
            "avatar": "https://account.devuid.club/avatar/57ed8b2db2146a5df6f1f55f991e3b83.png",
            "money": 967.2,
            "seat": 1,
            "isChoose": false,
            "betList": [7, 14, 17, 23, 35],
            "betFactor": 7,
            "resultCard": {"cardList": [16, 28, 33], "cardType": 5, "bonusFactor": 1, "point": 2}
          }, {
            "userId": 2,
            "displayName": "b2_PhucXO",
            "avatar": "https://account.devuid.club/avatar/f3d64a9a1fff87f693b9126288dcd607.png",
            "money": 1021.15,
            "seat": 2,
            "isChoose": false,
            "betList": [],
            "betFactor": 0,
            "resultCard": {"cardList": [15, 12, 46], "cardType": 4, "bonusFactor": 1, "point": 8}
          }]
        }], "roomId": 3
      }
    );
  }

  private testLobby() {
    this.game.lobby.updateRoomInfoList([
      {
        roomName: 1,
        maxBet: 100,
        minBet: 10,
        bet: 1,
        betList: [1, 2, 3, 4, 5],
        roomIdList: []
      },
      {
        roomName: 2,
        maxBet: 200,
        minBet: 20,
        bet: 1,
        betList: [1, 2, 3, 4, 5],
        roomIdList: []
      },
      {
        roomName: 3,
        maxBet: 300,
        minBet: 30,
        bet: 1,
        betList: [1, 2, 3, 4, 5],
        roomIdList: []
      },
      {
        roomName: 4,
        maxBet: 400,
        minBet: 40,
        bet: 1,
        betList: [1, 2, 3, 4, 5],
        roomIdList: []
      },
      {
        roomName: 5,
        maxBet: 500,
        minBet: 50,
        bet: 1,
        betList: [1, 2, 3, 4, 5],
        roomIdList: []
      },
    ]);
  }

  private testPlayGame() {
    moon.gameSocket.on(SERVER_EVENT.PLAY_GAME, () => {
      GlobalInfo.room = <any>{
        "roomId": 1,
        "gameId": 6,
        "betMoney": 1,
        "playerInfoList": [{
          "userId": 1,
          "displayName": "player 1",
          "avatar": "https://account.devuid.club/avatar/avatar-6.png",
          "money": 662014.98,
          "seat": 1
        }],
        "maxUser": 5,
        "betList": [1, 2, 3, 4, 5]
      };
      let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
      this.game.onEnterRoom(true);
      threeFaceService.onJoinBoard(GlobalInfo.room);
    });
    moon.gameSocket.playThreeFace(GAME_ID.THREE_CARD, 1);
  }

  private testEndGame() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onEndGame({
      "playerInfoList": [{
        "userId": 1,
        "winMoney": -2,
        "money": 3443.1,
        "resultCard": {"cardList": [11, 18, 29], "cardType": 5, "bonusFactor": 1, "point": 6}
      }, {
        "userId": 2,
        "winMoney": 3.8,
        "money": 4688.8,
        "resultCard": {"cardList": [10, 16, 44], "cardType": 4, "bonusFactor": 2, "point": 8}
      }, {
        "userId": 3,
        "winMoney": 3.8,
        "money": 4500.45,
        "resultCard": {"cardList": [26, 49, 1], "cardType": 4, "bonusFactor": 2, "point": 8}
      }, {
        "userId": 4,
        "winMoney": 9.5,
        "money": 1080.15,
        "resultCard": {"cardList": [20, 22, 21], "cardType": 1, "bonusFactor": 5, "point": 8}
      }, {
        "userId": 5,
        "winMoney": -16,
        "money": 1119.75,
        "resultCard": {"cardList": [27, 34, 37], "cardType": 5, "bonusFactor": 1, "point": 6}
      }], "gameId": 6, "roomId": 5
    });
  }

  private testBet() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onBet({
      "userId": 1,
      "betFactor": 14,
      "betMoney": 1,
      "betMoneyTotal": 3,
      "money": 5653.45,
      "roomId": 1
    });
    threeFaceService.onBet({
      "userId": 2,
      "betFactor": 35,
      "betMoney": 1,
      "betMoneyTotal": 3,
      "money": 5653.45,
      "roomId": 1
    });
    threeFaceService.onBet({
      "userId": 3,
      "betFactor": 7,
      "betMoney": 1,
      "betMoneyTotal": 3,
      "money": 5653.45,
      "roomId": 1
    });
    threeFaceService.onBet({
      "userId": 4,
      "betFactor": 14,
      "betMoney": 1,
      "betMoneyTotal": 3,
      "money": 5653.45,
      "roomId": 1
    });
    threeFaceService.onBet({
      "userId": 5,
      "betFactor": 7,
      "betMoney": 1,
      "betMoneyTotal": 3,
      "money": 5653.45,
      "roomId": 1
    });
  }

  private testBeginBet() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onBeginBet({"betList": [7, 14, 17, 23, 35], "timeout": 5, "roomId": 1});
  }

  private testBanker() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onBanker({"bankerId": 1, "roomId": 1});
  }

  private testChooseBanker() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onChooseBanker({"userId": 1, "isChoose": 1, "roomId": 1});
    threeFaceService.onChooseBanker({"userId": 2, "isChoose": 1, "roomId": 1});
    threeFaceService.onChooseBanker({"userId": 3, "isChoose": 0, "roomId": 1});
    threeFaceService.onChooseBanker({"userId": 4, "isChoose": 1, "roomId": 1});
    threeFaceService.onChooseBanker({"userId": 5, "isChoose": 0, "roomId": 1});
  }

  private testBeginChooseBanker() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onBeginChooseBanker({"timeout": 5, "roomId": 1});
  }

  private testStartGame() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onStartGame({"matchId": "156040836163962", "betMoney": 1, "cardList": [-1, -1, -1], "roomId": 1});
  }

  private testReadyStartGame() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onReadyStartGame({"timeout": 5, "roomId": 1});
  }

  private testPlayerJoin() {
    let threeFaceService: ThreeFaceService = moon.service.getCurrentService() as ThreeFaceService;
    threeFaceService.onPlayerJoinBoard({
      "roomId": 1,
      "playerInfo": {
        "userId": 2,
        "displayName": "player 2",
        "avatar": "https://account.devuid.club/avatar/avatar-7.png",
        "money": 23845,
        "seat": 2
      }
    });
    threeFaceService.onPlayerJoinBoard({
      "roomId": 1,
      "playerInfo": {
        "userId": 3,
        "displayName": "player 3",
        "avatar": "https://account.devuid.club/avatar/avatar-7.png",
        "money": 3241,
        "seat": 3
      }
    });
    threeFaceService.onPlayerJoinBoard({
      "roomId": 1,
      "playerInfo": {
        "userId": 4,
        "displayName": "player 4",
        "avatar": "https://account.devuid.club/avatar/avatar-7.png",
        "money": 7431.96,
        "seat": 4
      }
    });
    threeFaceService.onPlayerJoinBoard({
      "roomId": 1,
      "playerInfo": {
        "userId": 5,
        "displayName": "player 5",
        "avatar": "https://account.devuid.club/avatar/avatar-7.png",
        "money": 154231.02,
        "seat": 5
      }
    });
  }

  private testChooseHouse() {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.timeout = 5;

    this.game.changeState(GAME_STATE.HOUSE, ThreeFaceHouseState);

  }

  private testShowSnatch() {
    this.game.setPlayerSnatch(1, true);
    this.game.setPlayerSnatch(2, true);
    this.game.setPlayerSnatch(3, false);
    this.game.setPlayerSnatch(4, false);
  }

  private testPlayerHouseStatus() {
    this.game.players[0].placeBet(16);
    this.game.players[1].placeBet(0);
    this.game.players[2].placeBet(-1);
    this.game.players[3].placeBet(1);
  }

  private testChooseDealer() {
    this.game.playSelectDealer(2);
  }

  private testPlayerBetState() {
    let room: ThreeFaceRoom = <ThreeFaceRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.timeout = 5;
    this.game.changeState(GAME_STATE.BET, ThreeFaceBetState, {
      [KEYS.BET_LIST]: [1, 33, 50, 66],
      [KEYS.TIMEOUT]: 5
    });
  }

  private testStartState() {
    this.game.changeState(GAME_STATE.START, ThreeFaceStartState, {});
  }

  private testDice() {
    // this.game.diceLayer.playDiceAnim(2, 1);
  }

  private testDealChess() {
    let playerInfo1 = new ThreeFacePlayerInfo();
    let playerInfo2 = new ThreeFacePlayerInfo();
    let playerInfo3 = new ThreeFacePlayerInfo();
    let playerInfo4 = new ThreeFacePlayerInfo();
    let playerInfo5 = new ThreeFacePlayerInfo();
    playerInfo1.userId = 1;
    playerInfo2.userId = 2;
    playerInfo3.userId = 3;
    playerInfo4.userId = 4;
    playerInfo5.userId = 5;

    playerInfo1.resultCard = {
      cardList: [15, 15, 21],
      point: 5,
      cardType: 2,
      bonusFactor: 1
    };
    playerInfo2.resultCard = {
      cardList: [15, 15, 21],
      point: 5,
      cardType: 2,
      bonusFactor: 1
    };
    playerInfo3.resultCard = {
      cardList: [15, 15, 21],
      point: 5,
      cardType: 2,
      bonusFactor: 1
    };
    playerInfo4.resultCard = {
      cardList: [15, 15, 21],
      point: 5,
      cardType: 2,
      bonusFactor: 1
    };

    playerInfo5.resultCard = {
      cardList: [15, 15, 21],
      point: 5,
      cardType: 2,
      bonusFactor: 1
    };

    this.game.cardLayer.setUpCards([
      playerInfo1, playerInfo2, playerInfo3, playerInfo4, playerInfo5
    ]);

    cc.log('deal card', this.game.players);

    setTimeout(() => {
      this.game.cardLayer.dealCard(this.game.players[0]);
      this.game.cardLayer.dealCard(this.game.players[1]);
      this.game.cardLayer.dealCard(this.game.players[2]);
      this.game.cardLayer.dealCard(this.game.players[3]);
      this.game.cardLayer.dealCard(this.game.players[4]);
    }, 500);

    setTimeout(() => {
      this.game.players[0].showCards();
      this.game.players[1].showCards();
      this.game.players[2].showCards();
      this.game.players[3].showCards();
      this.game.players[4].showCards();
    }, 2500);
  }

  private testMoveChips() {
    // this.game.betLayer.playBet(1, 59);
    // setTimeout(() => {
    //   this.game.betLayer.moveChipsToHouse(2);
    // }, 1000);
    // setTimeout(() => {
    //   this.game.betLayer.moveChipsToWinner(2, 3, 33);
    // }, 2000);

  }

  private testPlayerWin() {
    this.game.players[0].playWinEffect();
  }
}