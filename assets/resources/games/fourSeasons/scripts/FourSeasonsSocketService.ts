import { KEYS as FS_KEYS } from './FourSeasonsConstants';
import { GlobalInfo, moon } from './../../../../scripts/core/GlobalInfo';
import { KEYS, SERVER_EVENT } from './../../../../scripts/core/Constant';
export class FourSeasonsSocket {
  static bet(betValue, placeBetType) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BET,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [FS_KEYS.PLACE_BET_TYPE]: placeBetType,
        [KEYS.BET_MONEY]: betValue
      }
    };

    moon.gameSocket.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }
}