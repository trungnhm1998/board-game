import {SceneComponent} from "../common/SceneComponent";
import {Selection} from "../common/Selection";
import {ToggleButton} from "../common/ToggleButton";
import {GlobalInfo, moon} from "../core/GlobalInfo";
import {KEYS} from "../core/Constant";
import {NodeUtils} from "../core/NodeUtils";


const {ccclass, property} = cc._decorator;

@ccclass
export class RankDialog extends SceneComponent {


  @property([cc.Toggle])
  tabs: cc.Toggle[] = [];


  @property(Selection)
  language: Selection = null;

  @property(ToggleButton)
  sound: ToggleButton = null;

  @property(cc.Node)
  backBtn: cc.Node = null;

  @property(cc.Node)
  view: cc.Node = null;

  @property(cc.Node)
  body: cc.Node = null;
  @property(cc.Node)
  loadingIndicator: cc.Node = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Node)
  rankList: cc.Node = null;

  @property(cc.Node)
  rankItem: cc.Node = null;
  @property(cc.Node)
  userRank: cc.Node = null;


  currentTabs = 0;
  gameIdOfTab = {
    0: 1,
    1: 2,
    2: 3
  }

  gameList = [
    'mahjong',
    'twoEight',
    'dragonTiger'
  ]

  onEnter() {
    super.onEnter();

    for (let i = 0; i < this.tabs.length; i++) {
      let tab = this.tabs[i];
      tab.checkEvents[0].customEventData = "" + i;
      NodeUtils.setLocaleLabel(tab.node, "gameName", this.gameList[i]);
      if (i == 0) {
        this.onTabChange(null, i);
      }
    }

  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, "title", "tutorial", {upper: true});
    NodeUtils.setLocaleLabel(this.node, "active_lbl", "active");
    NodeUtils.setLocaleLabel(this.node, "activeOTP_lbl", "activeOTP");
    NodeUtils.setLocaleLabel(this.node, "sound_lbl", "sound");
    NodeUtils.setLocaleLabel(this.node, "logout_lbl", "logout");
    NodeUtils.setLocaleLabel(this.node, "language_lbl", "language");
    NodeUtils.setLocaleLabel(this.node, "security_lbl", "security");
    NodeUtils.setLocaleLabel(this.node, "id_lbl", "id:");
    NodeUtils.setLocaleLabel(this.node, "id_lbl", "id:");
    switch (GlobalInfo.me.type) {
      case KEYS.EMAIL:
        NodeUtils.setLocaleLabel(this.node, "securityShow", "useOTPViaEmail");
        break;
      case KEYS.APPLICATION:
        NodeUtils.setLocaleLabel(this.node, "securityShow", "useOTPViaApp");
        break;
      default:
        NodeUtils.setLocaleLabel(this.node, "securityShow", "notSetup");
    }
  }

  showLoading() {
    cc.log('loading')
    this.loadingIndicator.active = true;
    this.body.getChildByName('body').active = false
  }

  hideLoading() {
    cc.log('not loading')
    this.loadingIndicator.active = false;
    this.body.getChildByName('body').active = true

  }

  onTabChange(evt, index) {
    cc.log(index)
    this.currentTabs = index;
    let gameId = this.gameIdOfTab[index]
    moon.lobbySocket.getRankList(gameId)
    this.unSelectTabs();

    let tab = this.tabs[index];
    tab.isChecked = true;
    let nameNode = NodeUtils.findByName(tab.node, "gameName");
    nameNode.color = cc.color().fromHEX("#ffffff");

    // let content = this.leftTabContents[index];
    // content.active = true;
    // this.showRightTab();
  }


  unSelectTabs() {
    for (let tab of this.tabs) {
      tab.isChecked = false;
      let nameNode = NodeUtils.findByName(tab.node, "gameName");
      nameNode.color = cc.color().fromHEX("#25ed78");
    }
  }

  addRankItem(item) {
    let rankItem = cc.instantiate(this.rankItem);

    //set score
    let scoreNumber = rankItem.getChildByName('userInfo').getChildByName('score').getChildByName('scoreNumber').getComponent(cc.Label);
    let scoreLabel = rankItem.getChildByName('userInfo').getChildByName('score').getChildByName('scoreLabel').getComponent(cc.Label);

    scoreNumber.string = item.score
    rankItem.active = true;

    scoreLabel.string = moon.locale.get('score:')


    //set rank number

    let rankNumber = rankItem.getChildByName('rankNumber')
    if (item.rank < 4)
      rankNumber.getChildByName('rank_' + item.rank).active = true
    else {
      let rank = rankNumber.getChildByName('rank_4');
      rank.active = true;
      rank.getChildByName('number').getComponent(cc.Label).string = item.rank
    }


    //set avatar

    let avatar = rankItem.getChildByName('avatarHolder').getChildByName('mask').getChildByName('avatar')
    if (!item.avatar) avatar.active = false
    else moon.res.setRemoteImage(avatar.getComponent(cc.Sprite), item.avatar)
    avatar.setContentSize(85, 85)

    //set displayName
    rankItem.getChildByName('userInfo').getChildByName('displayName').getComponent(cc.Label).string = item.displayName

    this.rankList.addChild(rankItem)
  }


  updateUserRank(data) {
    this.userRank.getChildByName('textLabel')
      .getChildByName('rankText')
      .getComponent(cc.Label).string = moon.locale.get('yourRank').toUpperCase()
    this.userRank.getChildByName('scoreNumber')
      .getComponent(cc.Label).string = moon.locale.get('score:') + ' ' + (data.score || '0');
  }

  updateUI(data) {
    this.rankList.removeAllChildren()
    for (let item of data.rankList) {
      this.addRankItem(item)
    }

    this.updateUserRank(data)

  }

}