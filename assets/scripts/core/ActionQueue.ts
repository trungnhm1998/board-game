import {GameAction} from './ActionQueue';
import {Promise} from 'es6-promise';


export interface ActionRun {
  (): Promise<any>;
}

export interface GameAction {
  run: ActionRun;
}

export class ActionQueue {
  queue = [];
  isPaused = false;
  isRunning = false;

  pushAction(action) {
    this.queue.push(action);
  }

  clean() {
    this.queue = [];
    this.isRunning = true;
    this.isPaused = true;
  }

  unshiftAction(action) {
    this.queue.unshift(action);
  }

  isEmpty() {
    return this.queue.length == 0;
  }

  trigger(force = false) {
    if ((this.isPaused || this.isRunning) && !force) return;
    this.isRunning = true;
    if (this.queue.length > 0) {
      let action = this.queue.shift();
      action
        .run()
        .then(() => {
          this.isRunning = false;
          this.trigger(true);
        });
    } else {
      this.isRunning = false;
    }
  }

  pause() {
    this.isPaused = true;
  }

  resume() {
    this.isPaused = false;
    this.trigger();
  }
}