
import {GameState, IGameState, UpdateData} from "../../../../../scripts/common/GameState";

import {GlobalInfo, moon} from "../../../../../scripts/core/GlobalInfo";
import { RedBlackGame } from "../RedBlackGame";
import { RedBlackRoom } from "../model/RedBlackRoom";
import { Card } from "../../../../../scripts/core/Card";
import { NodeUtils } from "../../../../../scripts/core/NodeUtils";
import { Config } from "../../../../../scripts/Config";

const {ccclass, property} = cc._decorator;

@ccclass
export class RedBlackDealCardState extends GameState implements IGameState {


  @property(cc.Node)
  start_pos: cc.Node = null;

  game: RedBlackGame;
  redCards = [];
  blackCards = [];
  onEnter(game: RedBlackGame, data: any) {
    game.setRoom(<RedBlackRoom>GlobalInfo.room);
    let room: RedBlackRoom = <RedBlackRoom>GlobalInfo.room;
    this.game = game;
    this.setupCards();
  	this.dealCards();
  }
    

  setupCards() {
    this.clear()
    
    // add card to red card list
    for (let i = 0; i < 3; i++) {
        let cardNode = moon.cardPool.getCard()
        let cardHeigh = moon.cardPool.cardHeight;
        let cardWidth = moon.cardPool.cardWidth;
        cardNode.getComponent(Card).setCardSize(cardWidth, cardHeigh)
        this.start_pos.addChild(cardNode)
        cardNode.x = 0;
        cardNode.opacity = 0;
        this.redCards.push(cardNode)
    }

        // add card to black card list
    for (let i = 0; i < 3; i++) {
        let cardNode = moon.cardPool.getCard()
        let cardHeigh = moon.cardPool.cardHeight;
        let cardWidth = moon.cardPool.cardWidth;
        cardNode.getComponent(Card).setCardSize(cardWidth, cardHeigh)
        this.start_pos.addChild(cardNode)
        cardNode.x = 0;
        cardNode.opacity = 255;
        this.blackCards.push(cardNode)
    }
 
	}
	

  dealCards(isUseAnimation = true) {
		
			let redPositions = this.game.table.getDealCardPostion();
			let blackPositions = this.game.table.getDealCardPostion();
			let delayFactor = 0;
			let flyTime = 0;
			if (isUseAnimation) {
				delayFactor = 0.1;
				flyTime = Config.chessFlyTime;
			}
		

			//deal red card
			let i = 0;
			this.redCards.forEach(card => {
				NodeUtils.swapParent(card, card.parent, this.game.table.redCardContent);
				card.runAction(
					cc.sequence(
						cc.callFunc(() => {
							card.active = true;
						}),
						cc.delayTime(delayFactor * i),
						cc.spawn(
							cc.fadeIn(0.2),
							cc.moveTo(flyTime, redPositions[i]).easing(cc.easeIn(1.0))
						)
					)
				);
				i++;
			});
			this.game.table.setCardList(1, this.redCards);


				//deal black card
				i = 0;
				this.blackCards.forEach(card => {
					NodeUtils.swapParent(card, card.parent, this.game.table.blackCardContent);
					card.runAction(
						cc.sequence(
							cc.callFunc(() => {
								card.active = true;
							}),
							cc.delayTime(delayFactor * i),
							cc.spawn(
								cc.fadeIn(0.2),
								cc.moveTo(flyTime, blackPositions[i]).easing(cc.easeIn(1.0))
							)
						)
					);
					i++;
				});
				this.game.table.setCardList(2, this.blackCards);
		
	}
	

  onUpdate(game: RedBlackGame, updateData: UpdateData) {
  }

  onLeave(game: RedBlackGame, data: any) {
  }

  clear() {
      this.redCards = [];
      this.blackCards = [];
      this.start_pos.removeAllChildren();
  }
}