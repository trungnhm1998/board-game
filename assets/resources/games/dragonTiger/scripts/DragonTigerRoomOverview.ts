import { DragonTigerSocket } from './service/DragonTigerSocketService';
import { DragonTigerHistory } from './DragonTigerHistory';
import { DragonTigerService } from './service/DragonTigerService';
import { TimerTask } from './../../../../scripts/core/TimerComponent';
import { DragonTigerRoom } from './model/DragonTigerRoom';
import { CountdownInfo } from './../../../../scripts/model/Room';
import { GAME_ID } from './../../../../scripts/core/Constant';
import { moon } from './../../../../scripts/core/GlobalInfo';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerRoomOverview extends cc.Component {
  @property(cc.Label)
  timerLbl: cc.Label = null;

  @property(cc.Label)
  roomStateLbl: cc.Label = null

  @property(DragonTigerHistory)
  roomHistory: DragonTigerHistory = null;

  _roomId = null;
  _roomInfo: DragonTigerRoom = null;

  updateUI(dragonTigerRoominfo: DragonTigerRoom) {
    this._roomInfo = dragonTigerRoominfo;
    const { roomId, minBet, maxBet, playerNum, countdownInfo } = this._roomInfo;
    this._roomId = roomId;
    const DTService = (moon.service.getCurrentService() as DragonTigerService);
    if (DTService.game._lobbyCountdownTask)
      this.updateRoomStatus(countdownInfo, DTService.game._lobbyCountdownTask[this._roomId].val);
    NodeUtils.setLabel(this.node, "room_id_lbl", roomId);
    NodeUtils.setLabel(this.node, "bet_value_lbl", `${minBet}-${maxBet}`);
    NodeUtils.setLabel(this.node, "player_count_lbl", playerNum);
    NodeUtils.setLocaleLabel(this.node, "Online_lbl", "online");
    NodeUtils.setLocaleLabel(this.node, "Quota_lbl", "quota");
    NodeUtils.setLocaleLabel(this.node, "lblEnter", "enter");

  }

  onLanguageChange() {
    NodeUtils.setSprites(this.node, {
      "Background": "lhmall_btn_1"
    }, false);
  }

  updateRoomStatus(countdownInfo: CountdownInfo, roomTimerVal) {
    const { timeout, type } = countdownInfo;
    const DTService = (moon.service.getCurrentService() as DragonTigerService);
    let roomState: string = DTService.getStatus(type);
    if (this.roomStateLbl.string != roomState)
      this.roomStateLbl.string = roomState;

    if (DTService.game._lobbyCountdownTask) {
      if (!DTService.game._lobbyCountdownTask[this._roomId].ended)
        this.timerLbl.string = roomTimerVal;
      else
        this.timerLbl.string = "0";
    }
  }

  playGame() {
    DragonTigerSocket.play(GAME_ID.DRAGON_TIGER, this._roomId);
  }

  changeRoom() {
    const DTService = (moon.service.getCurrentService() as DragonTigerService);
    const { minBet, maxBet } = this._roomInfo;
    DTService.changeRoom(`${minBet}-${maxBet}`, this._roomId);
    moon.gameSocket.leaveBoard();
  }

  onDisable() {
  }
}
