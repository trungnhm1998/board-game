#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159

varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
uniform float percent;


void main()
{
    vec4 color = texture2D(CC_Texture0, v_texCoord);
    vec2 v = vec2(v_texCoord.x-0.5, v_texCoord.y-0.5);
    float angleRad =  atan(v.y, v.x);



    if (v.x < 0.0 && v.y < 0.0) {
        angleRad += 2.0 * PI;
    }

    angleRad += PI / 2.0;

    float startAngle = percent * PI * 2.0;
    if ((angleRad < 2.0 * PI  && angleRad > startAngle ) ) {
        color = color * v_fragmentColor;
    } else {
        color = vec4(0.0);
    }

    gl_FragColor = color;
}