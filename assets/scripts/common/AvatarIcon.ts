import {NodeUtils} from "../core/NodeUtils";
import {moon} from "../core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class AvatarIcon extends cc.Component {
  @property(cc.Sprite)
  avatar: cc.Sprite = null;

  callback;
  base64Image;
  url;

  onLoad() {
  }

  addClickEvent(callback) {
    this.callback = callback;
    let btn: cc.Button = this.node.getComponent(cc.Button);
    if (!btn) {
      btn = this.node.addComponent(cc.Button);
    }
    let clickEvent = new cc.Component.EventHandler();
    clickEvent.handler = 'onClick';
    clickEvent.component = 'AvatarIcon';
    clickEvent.target = this.node;
    btn.clickEvents = [clickEvent];
  }

  setImageData(base64Image) {
    base64Image = base64Image.replace('data:image/jpeg;base64,', 'data:image/png;base64,');
    this.base64Image = base64Image;
    NodeUtils.getSpriteFrameFromImageData(base64Image)
      .then((sp: cc.SpriteFrame) => {
        this.avatar.node.stopAllActions();
        this.avatar.node.opacity = 0;
        this.avatar.spriteFrame = sp;
        this.avatar.node.runAction(cc.fadeIn(0.2));
      });
  }

  setImageUrl(url) {
    if (url) {
      this.url = url;
      return moon.res.setRemoteImage(this.avatar, url);
    } else {
      return new Promise<any>(resolve => {
      });
    }
  }

  getSpriteFrame() : cc.SpriteFrame {
    return this.avatar.spriteFrame;
  }

  getBase64Image() {
    return new Promise((resolve) => {
      if (this.base64Image) {
        resolve(this.base64Image)
      } else {
         moon.string.imageURLToBase64(this.url).then((base64Image) => {
          this.base64Image = base64Image;
          resolve(this.base64Image)
        });
      }
    });
  }

  onClick(evt) {
    if (this.callback) {
      if (this.url) {
        this.callback(evt.target, this.url);
        //  moon.string.imageURLToBase64(this.url).then((base64Image) => {
        //   this.base64Image = base64Image;
        //   this.callback(evt.target, this.base64Image);
        // });
      } else {
        this.callback(evt.target, this.base64Image);
      }
    }
  }

  setSpriteFrame(spriteFrame) {
    this.avatar.node.stopAllActions();
    this.avatar.spriteFrame = spriteFrame;
    this.avatar.node.runAction(cc.fadeIn(0.2));
  }

  clear() {
    this.url = '';
    this.base64Image = '';
    this.avatar.spriteFrame = null;
  }
}