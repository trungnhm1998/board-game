import { RESOURCE_BLOCK } from './../../../../scripts/core/Constant';
import { moon } from './../../../../scripts/core/GlobalInfo';
import { NodeUtils } from './../../../../scripts/core/NodeUtils';
import { Card } from './../../../../scripts/core/Card';
import { BetType, BetTypeStr, CardType } from './FourSeasonsConstants';
const {ccclass, property} = cc._decorator;

@ccclass
export class FourSeasonsPot extends cc.Component {
  @property({ type: cc.Enum(BetType) })
  betType: BetType = BetType.SPADE;

  _potClickHandler = () => {};

  // hold bet chips on table for current bet type
  @property(cc.Node)
  chipArea: cc.Node = null;

  // locale sprite purpose
  @property(cc.Sprite)
  typeSprite: cc.Sprite = null;

  @property(cc.Node)
  cardArea: cc.Node = null;

  @property(cc.Label)
  totalBetMoney: cc.Label = null;

  @property(cc.Label)
  playerBetMoney: cc.Label = null;

  @property(cc.Node)
  cardTypeNode: cc.Node = null;

  _cardListToOpen: number[] = [];
  _cardTypeToOpen: number = -1;

  setPlayerMoney(money) {
    this.playerBetMoney.string = moon.string.formatMoney(moon.string.round(money, 2));
  }

  setTotalMoeny(money) {
    this.totalBetMoney.string = moon.string.formatMoney(moon.string.round(money, 2));
  }

  clear() {
    if (this.totalBetMoney && this.playerBetMoney) {
      this.totalBetMoney.string = '';
      this.playerBetMoney.string = '';
    }
    this._cardListToOpen = [];
    this._cardTypeToOpen = -1;

    this.cardTypeNode.stopAllActions();
    this.cardTypeNode.cleanup();
    this.cardTypeNode.active = false;

    while (this.cardArea.children.length > 0) {
      const card = this.cardArea.children.pop();
      card.stopAllActions();
      card.cleanup();
      moon.cardPool.putCards(card);
    }
  }

  onLanguageChange() {
    // TODO: Update cardType image based on lange

  }
  
  onBetClick() {
    // cc.log('betType', BetTypeStr[this.betType]);
    if (this._potClickHandler) {
      this._potClickHandler();
    }

  }

  updateTotalBetMoney(value) {
    this.totalBetMoney.string = String(value);
  }

  openCardWithoutAnim() {
    const cardCount = this.cardArea.children.length;
    if (this._cardListToOpen) {
      for (let i = 0; i < cardCount; i++) {
        const card = this.cardArea.children[i];
        const cardComponent = card.getComponent(Card);
        cardComponent.setCardId(this._cardListToOpen[i]);
        cardComponent.showCard();
      }
    }

    if (this._cardTypeToOpen != -1) {
      this.cardTypeNode.active = true;
      this.cardTypeNode.scaleX = 1;
      const cardTypeLocaleNode = this.cardTypeNode.getChildByName('card_type');
      cardTypeLocaleNode.active = true;
      NodeUtils.setGameSprite(this.cardTypeNode, 'card_type', CardType[this._cardTypeToOpen].name);
      this.cardTypeNode.getComponent(cc.Sprite).spriteFrame = moon.res.getSpriteFrame(CardType[this._cardTypeToOpen].banner, RESOURCE_BLOCK.GAME);
    }
  }

  openCard() {
    const cardCount = this.cardArea.children.length;
    let originalPos: cc.Vec2[] = [];


    this.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          for (let i = 0; i < cardCount; i++) {
            const card = this.cardArea.children[i];
            originalPos.push(card.getPosition());
            card.runAction(cc.moveTo(0.2, cc.v2()));
          }
        }),
        cc.delayTime(0.2),
        cc.callFunc(() => {
          for (let i = 0; i < cardCount; i++) {
            const card = this.cardArea.children[i];
            const cardComponent = card.getComponent(Card);
            cardComponent.setCardId(this._cardListToOpen[i]);
            cardComponent.showCard();

            card.runAction(cc.moveTo(0.2, originalPos[i]));
          }
        }),
        cc.delayTime(0.2),
        cc.callFunc(() => {
          if (this._cardTypeToOpen != -1) {
            this.cardTypeNode.active = true;
            this.cardTypeNode.scaleX = 0;
            const cardTypeLocaleNode = this.cardTypeNode.getChildByName('card_type');
            cardTypeLocaleNode.active = false;
            NodeUtils.setGameSprite(this.cardTypeNode, 'card_type', CardType[this._cardTypeToOpen].name);
            this.cardTypeNode.getComponent(cc.Sprite).spriteFrame = moon.res.getSpriteFrame(CardType[this._cardTypeToOpen].banner, RESOURCE_BLOCK.GAME);
            this.cardTypeNode.runAction(cc.scaleTo(0.3, 1, 1).easing(cc.easeBounceOut()));
          }
        }),
        cc.delayTime(0.1),
        cc.callFunc(() => {
          const cardTypeLocaleNode = this.cardTypeNode.getChildByName('card_type');
          cardTypeLocaleNode.active = true;
        })
      )
    )
  }
}